'''
Python solution to the Watch Out For Those Hailstones!
problem. 
'''
def h(v, n):
    if n == 1:
        return
    elif n % 2 == 0:
        v.append(n // 2)
        h(v, n//2)
    else:
        v.append(3 * n + 1)
        h(v, 3*n + 1)

n = int(input())

if n == 1:
    print(1)
else:
    v = [n]
    h(v, n)
    print(sum(v))