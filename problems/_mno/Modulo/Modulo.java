import java.util.*;

/**
 * Java solution to the Modulo problem from Kattis.
 * 
 * @author Mark M. Meysenburg
 * @version 10/30/2019
 */
public class Modulo {
    public static void main(String[] args) {
        // a set will keep track of distinct values
        Set<Integer> set = new HashSet<>();

        Scanner stdin = new Scanner(System.in);

        // read numbers and add their mod 42 values
        // to the set
        for (int i = 0; i < 10; i++) {
            set.add(stdin.nextInt() % 42);
        }

        // output number of distinct values mod 42
        System.out.println(set.size());
    }
}