'''
 * Python 3 solution to the Modulo Kattis problem.
'''
import sys

# Place all inputs (mod 42) into a list
inp = [(int(x) % 42) for x in sys.stdin.readlines()]

# Use a list to keep track of distinct values seen.
# Each number we've seen will have a 1 at its index
# in the list.
dis = [0]*42
for i in inp:
    dis[i] = 1

# Add up the 1's in the list for the count of the 
# numbers we've seen
print(sum(dis))