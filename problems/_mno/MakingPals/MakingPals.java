import java.util.*;

/**
 * Solution to the "Making Pals" programming contest problem.
 * Find the cheapest way to make a palindrome out of a given
 * string of digits. 
 */
public class MakingPals {
    
    /**
     * Class to hold cost / length pairs in a priority queue.
     */
    private static class MyPair implements Comparable<MyPair> {
        public int cost;
        public int length;
        
        public MyPair(int cost, int length) {
            this.cost = cost;
            this.length = length;
        }        

        /**
         * Comparator to sort min-cost, max length pairs
         * to the front of the queue. 
         * @param o
         * @return 
         */
        @Override
        public int compareTo(MyPair o) {
            if(cost == o.cost && length == o.length) {
                return 0;
            } 
            if(cost > o.cost) return 1;
            if(cost == o.cost && length < o.length) return 1;
            return -1;
        }
        
    } // MyPair
    
    /** maximum cost that would be required to make a palindrome */
    private static int maxCost;
    
    /** 
     * Priority queue holding cost / length pairs; eventually, 
     * the least expensive cost and its length will pop out of
     * the queue.
     */
    private static PriorityQueue<MyPair> q = new PriorityQueue<>();
    
    /**
     * Iterative palindrome checker. 
     * @param s String to check.
     * @return true if s is a palindrome; false otherwise.
     */
    private static boolean isPal(String s) {
        int l = 0, r = s.length() - 1;
        while(l <= r) {
            if(s.charAt(l++) != s.charAt(r--)){
                return false;
            }
        }
        return true;
    }
    
    /**
     * Four-way recursive method to try all the possible palindromes
     * that can be made from a given string, using the two operations
     * we're allowed to use. 
     * 
     * @param s String to try to make into a palindrome
     * @param cost Number of operations performed to make it a 
     * palindrome. 
     */
    private static void myTry(String s, int cost) {
        // don't go deeper if our maximum cost is met, or if our
        // string length is zero
        if(cost <= maxCost && s.length() > 0) {
            // if s is a palindrome at this point, add the 
            // cost it took to make it that way, and the length
            // of the string, as a cost / length pair to the 
            // priority queue
            if(isPal(s)) {
                q.add(new MyPair(cost, s.length()));
            } else {
                // if s is not a palindrome, start making things
                // that might be palindromes, via the operations
                // we have
                
                // use a set to hold the characters in the string.
                // we won't worry about adding other digits, because
                // making a palindrome that way would take too many
                // operations
                TreeSet<Character> charset = new TreeSet<>();
                
                // put the characters in the set
                for(int i = 0; i < s.length(); i++) {
                    charset.add(s.charAt(i));
                }
                
                // now, try to make palindromes by adding a character
                // to the left or to the right side of the string
                for(char c : charset) {                    
                    // add to the left
                    myTry(s + c, cost + 1);
                    
                    // add to the right
                    myTry(c + s, cost + 1);
                }
                
                // Now try removing left or right character, right 
                // first
                String s1 = s.substring(0, s.length() - 1);
                myTry(s1, cost + 1);
                
                // left side
                s1 = s.substring(1);
                myTry(s1, cost + 1);
            }
        }
    }
    
    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this app. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String word;
        int cases = 1;
        
        while(stdin.hasNextLine()) {
            word = stdin.nextLine().trim();
            
            // paranoia -- make sure we skip the empty last line
            if(word.length() == 0) break;
            
            // in the worst case, we can make a palindrome in 
            // n - 1 operations
            maxCost = word.length() - 1;
            
            // find 'em!
            myTry(word, 0);
            
            // first element of priority queue will be the 
            // shortest cost of making the word into a
            // palindrome
            MyPair p = q.remove();
            System.out.printf("Case %d, sequence = %s, "
                    + "cost = %d, length = %d\n", 
                    cases++, word, p.cost, p.length);
            
            // empty queue for the next string
            q.clear();
        }
    }
} // MakingPals