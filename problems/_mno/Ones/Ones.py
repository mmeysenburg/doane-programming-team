# Solution to the Ones programming competition problem.
# 
# Uses this fact about modular arithmetic:
# 
# if a = b (mod n), then
# 
# p(a) = p(b) (mod n), for polynomial with integer coefficients.
# 
# a is the current potential factor (e.g, 111), and b is a % n.
# p(a) = 10a + 1 (e.g., next potential factor, 1111).
# 
# Since n cannot be larger than 100000, we can find the appropriate
# factor without overflowing an int, and we can do it very quickly.

import sys
ns = [int(x) for x in sys.stdin.readlines()]

for n in ns:
    pa = 1 % n
    nDigits = 1

    while pa != 0:
        pa = (pa * 10 + 1) % n
        nDigits += 1
    print(nDigits)