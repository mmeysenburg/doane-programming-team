
# naive approach to Ones -- this will work, but exceeds the
# time limit of 1s. 
import sys
ns = [int(x) for x in sys.stdin.readlines()]

for n in ns:
    if n == 1:
        print(1)
    else:
        # starting at 11, find the first value that's all 1's
        # that is divisible by n
        numDigits = 2
        mult = 11
        while mult % n != 0:
            numDigits += 1
            mult = mult * 10 + 1
        print(numDigits)