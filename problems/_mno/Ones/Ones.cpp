#include <iostream>
#include <cstdlib>

using namespace std;

int main() {
	        int p, d, ans;

        while (cin >> p) {
            d = 1 % p;
            ans = 1;
            while (d != 0) {
                d = (d * 10 + 1) % p;
                ans++;
            }
            cout << ans  << endl;
        }
		
	return EXIT_SUCCESS;
}