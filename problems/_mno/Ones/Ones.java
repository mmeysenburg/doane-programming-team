import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Solution to the Ones programming competition problem.
 * 
 * Uses this fact about modular arithmetic:
 * 
 * if a = b (mod n), then
 * 
 * p(a) = p(b) (mod n), for polynomial with integer coefficients.
 * 
 * a is the current potential factor (e.g, 111), and b is a % n.
 * p(a) = 10a + 1 (e.g., next potential factor, 1111).
 * 
 * Since n cannot be larger than 100000, we can find the appropriate
 * factor without overflowing an int, and we can do it very quickly.
 *
 * @author Mark M. Meysenburg
 * @version 02/20/2019
 */
public class Ones {

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored.
     */
    public static void main(String[] args) throws IOException {
        // faster than scanner
        BufferedReader stdin = new BufferedReader(
                new InputStreamReader(System.in));

        int n;          // input number
        int pa;         // potential factor (mod n)
        int nDigits;    // number of digits in p.f.
        String line;    // String to obtain n

        // loop while we have input to read
        while ((line = stdin.readLine()) != null) {
            // convert line to int
            n = Integer.parseInt(line);

            // calculate initial potential factor
            // (0 or 1, 0 iff n == 1)
            pa = 1 % n;
            // one digit so far
            nDigits = 1;

            // loop until factor is found
            while (pa != 0) {
                // move to next p.f. (mod n)
                pa = (pa * 10 + 1) % n;
                // one more digit 
                nDigits++;
            }

            // report results
            System.out.println(nDigits);
        }
    }
}