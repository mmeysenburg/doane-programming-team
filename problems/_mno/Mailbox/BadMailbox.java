import java.util.*;

/**
 * Recursive "solution" to the
 * Mailbox Manufacturers Problem from Kattis.
 *
 * @author Mark M. Meysenburg
 * @version 04/09/2021
 */
class BadMailbox {

    /**
     * Calculate minimum number of firecrackers to test k mailboxes, for a + 1
     * to b firecrackers.
     */
    private static int boom(int k, int a, int b) {
            // base case: no range of firecrackers, return 0
            if (a == b) {
                return 0;
            }
            // base case: one mailbox, return sum of [a + 1, b]
            else if (k == 1) {
                return (b + a + 1) * (b - a) / 2;
            }
            // general case: look at min worst-case value for
            // a + 1 .. b
            else {
                int val = Integer.MAX_VALUE;
                for (int i = a + 1; i <= b; i++) {
                    int v = i + Math.max(
                            boom(k, i, b), // mailbox didn't blow up
                            boom(k - 1, a, i - 1) // mailbox blew up
                    );

                    if (v < val) {
                        val = v;
                    }
                }
                return val;
            }
        
    }

    /**
     * App entry point.
     */
    public static void main(String[] args) {
        // read number of cases
        Scanner stdin = new Scanner(System.in);
        int N = Integer.parseInt(stdin.nextLine());

        // process all the cases
        for (int caseNo = 0; caseNo < N; caseNo++) {
            int k = stdin.nextInt();
            int m = stdin.nextInt();

            System.out.println(boom(k, 0, m));
        }
    }
}