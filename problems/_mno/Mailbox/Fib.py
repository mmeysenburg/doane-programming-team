def fib(n):
    if n == 1 or n == 2:
        return 1
    else:
        return fib(n - 1) + fib(n - 2)

def fib1(n, dict):
    if n == 1 or n == 2:
        return 1
    elif n in dict:
        return dict[n]
    else:
        val = fib1(n - 1, dict) + fib1(n - 2, dict)
        dict[n] = val
        return val


dict = { }
for i in range(1, 101):
    print('{0:d}: {1:d}'.format(i, fib1(i, dict)))