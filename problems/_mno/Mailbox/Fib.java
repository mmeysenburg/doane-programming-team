import java.util.*;

public class Fib {

    public static long fib(long n, Map<Long, Long> map) {
        if(n == 1L || n == 2L) {
            return 1L;
        } else if(map.containsKey(n)) {
            return map.get(n);
        } else {
            long value = fib(n - 1, map) + fib(n - 2, map);
            map.put(n, value);
            return value;
        }
    }

    public static void main(String[] args) {
        Map<Long, Long> map = new HashMap<>();
        for(long i = 1; i <= 100L; i++) {
            System.out.printf("%d: %d\n", i, fib(i, map));
        }
    }
}