#include <algorithm>
#include <iostream>
#include <climits>

using namespace std;

int mem[11][101][101];

int boom(int k, int a, int b) {
    if(mem[k][a][b] == -1) {
        if(a == b) {
            mem[k][a][b] = 0;
        } else if(k == 1) {
            mem[k][a][b] = (b + a + 1) * (b - a) / 2;
        } else {
            int val = INT_MAX;
            for(int i = a + 1; i <= b; i++) {
                int v = i + std::max(boom(k, i, b), boom(k - 1, a, i - 1));
                if(v < val) {
                    val = v;
                }
            }
            mem[k][a][b] = val;
        }
    }

    return mem[k][a][b];
}

int main() {
    int N, k, m;
    cin >> N;

    for(int caseNo = 0; caseNo < N; caseNo++) {
        cin >> k;
        cin >> m;
        
        for(int i = 0; i < 11; i++) {
            for(int j = 0; j < 101; j++) {
                for(int kk = 0; kk < 101; kk++) {
                    mem[i][j][kk] = -1;
                }
            }
        }

        cout << boom(k, 0, m) << endl;
    }
}