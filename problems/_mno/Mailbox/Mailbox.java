import java.util.*;

/**
 * Memoized dynamic programming solution to the
 * Mailbox Manufacturers Problem from Kattis.
 *
 * @author Mark M. Meysenburg
 * @version 04/09/2021
 */
class Mailbox {

    /** Memory for dynamic programming. */
    private static int[][][] mem;

    /**
     * Calculate minimum number of firecrackers to test k mailboxes, for a + 1
     * to b firecrackers.
     */
    private static int boom(int k, int a, int b) {
        // only proceed if we haven't calculated already
        if (mem[k][a][b] == -1) {
            // base case: no range of firecrackers, return 0
            if (a == b) {
                mem[k][a][b] = 0;
            }
            // base case: one mailbox, return sum of [a + 1, b]
            else if (k == 1) {
                mem[k][a][b] = (b + a + 1) * (b - a) / 2;
            }
            // general case: look at min worst-case value for
            // a + 1 .. b
            else {
                int val = Integer.MAX_VALUE;
                for (int i = a + 1; i <= b; i++) {
                    int v = i + Math.max(
                            boom(k, i, b), // mailbox didn't blow up
                            boom(k - 1, a, i - 1) // mailbox blew up
                    );

                    if (v < val) {
                        val = v;
                    }
                }
                mem[k][a][b] = val;
            }
        }

        return mem[k][a][b];
    }

    /**
     * App entry point.
     */
    public static void main(String[] args) {
        // construct memory for dynamic programming
        mem = new int[11][101][101];

        // read number of cases
        Scanner stdin = new Scanner(System.in);
        int N = Integer.parseInt(stdin.nextLine());

        // process all the cases
        for (int caseNo = 0; caseNo < N; caseNo++) {
            int k = stdin.nextInt();
            int m = stdin.nextInt();

            // reset memory for each case
            for (int i = 0; i < mem.length; i++) {
                for (int j = 0; j < mem[i].length; j++) {
                    for (int kk = 0; kk < mem[i][j].length; kk++) {
                        mem[i][j][kk] = -1;
                    }
                }
            }

            System.out.println(boom(k, 0, m));
        }
    }
}