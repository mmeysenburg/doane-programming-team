'''
Smarter solution to Oktalni Kattis problem. 

Convert input line to an integer, then to octal;
strip off the prefix characters and print. 
'''
print(oct(int('0b' + input(), 2))[2:])