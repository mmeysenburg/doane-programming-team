'''
Python solution to the Oktalni problem on Kattis.
'''
# lookup table for creating octal 
conv = {'000':'0', '001':'1', '010':'2', '011':'3',
        '100':'4', '101':'5', '110':'6', '111':'7'}

binary = input()

# pad input number
if len(binary) % 3 != 0:
    binary = ('0' * (3 - len(binary) % 3)) + binary

# use the lookup to form the octal string as a list
octal = [conv[binary[i:i+3]] for i in range(0,len(binary), 3)]

# print out the octal number as a string
print(''.join(octal))
