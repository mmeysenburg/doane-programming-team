﻿using System;

class Oktalni
{
    public static void Main(string[] args)
    {
        string line = Console.ReadLine();
        while(line.Length % 3 != 0)
        {
            line = "0" + line;
        }
        string output = "";
        for(int i = 0; i < line.Length; i+=3)
        {
            output += Convert.ToInt32(line[i..(i+3)], 2);
        }
        Console.WriteLine(output);
    }
}