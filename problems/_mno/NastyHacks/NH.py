import sys
lines = [[int(y) for y in x.split()] for x in sys.stdin.readlines()[1:]]
for line in lines:
    r = line[0]
    e = line[1]
    c = line[2]
    net = e - c
    if net == r:
        print('does not matter')
    elif net > r:
        print('advertise')
    else:
        print('do not advertise')