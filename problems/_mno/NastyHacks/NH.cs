using System;

namespace NH {
    class NH {
        static void Main() {
            int n = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i < n; i++) {
                string[] sVals = Console.ReadLine().Split(' ');
                int r = Convert.ToInt32(sVals[0]);
                int e = Convert.ToInt32(sVals[1]);
                int c = Convert.ToInt32(sVals[2]);
                int net = e - c;
                if(net == r) {
                    Console.WriteLine("does not matter");
                } else if(net > r) {
                    Console.WriteLine("advertise");
                } else {
                    Console.WriteLine("do not advertise");
                }
            }
        }
    }
}