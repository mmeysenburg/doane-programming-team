
import java.util.*;

/**
 * Solution to "Nasty Hacks" programming competition problem. Should 
 * we advertise our malware or not?
 */
public class NastyHacks {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();

        for (int i = 0; i < n; i++) {
            // revenue without advertising
            int r = stdin.nextInt();
            // revenue with advertising
            int e = stdin.nextInt();
            // cost of advertising
            int c = stdin.nextInt();

            // will we make $ if we advertise?
            int net = e - c;
            if (net == r) {
                System.out.println("does not matter");
            } else if (net > r) {
                System.out.println("advertise");
            } else {
                System.out.println("do not advertise");
            }
        } // for 
        
        stdin.close();
    } // main
} // class
