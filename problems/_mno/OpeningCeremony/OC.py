'''
Python solution to "Opening Ceremony" Kattis problem.
'''
n = int(input())

# count how many time each height occurs
heights = {}
for t in [int(x) for x in input().split()]:
    heights[t] = heights.get(t, 0) + 1

best = n
x = 0
for h in sorted(heights.keys()):
    x += heights[h]
    best = min(best, h + n - x)

print(best)