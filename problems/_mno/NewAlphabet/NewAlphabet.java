
import java.util.*;

/**
 * Solution to "A New Alphabet" programming competition problem. 
 * Translate English text into a new, elite alphabet!
 */
public class NewAlphabet {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // construct a map to translate from English letters to
        // the specified output
        Map<Character, String> map = new HashMap<>();

        map.put('a', "@");      map.put('n', "[]\\[]");
        map.put('b', "8");      map.put('o', "0");
        map.put('c', "(");      map.put('p', "|D");
        map.put('d', "|)");     map.put('q', "(,)");
        map.put('e', "3");      map.put('r', "|Z");
        map.put('f', "#");      map.put('s', "$");
        map.put('g', "6");      map.put('t', "']['");
        map.put('h', "[-]");    map.put('u', "|_|");
        map.put('i', "|");      map.put('v', "\\/");
        map.put('j', "_|");     map.put('w', "\\/\\/");
        map.put('k', "|<");     map.put('x', "}{");
        map.put('l', "1");      map.put('y', "`/");
        map.put('m', "[]\\/[]");    map.put('z', "2");

        // now, read the input and make it all lowercase
        String line = stdin.nextLine().toLowerCase();
        stdin.close();

        // translate the letters one at a time
        for (int i = 0; i < line.length(); i++) {
            
            char ch = line.charAt(i);
            // if the map has the character...
            if (map.containsKey(ch)) {
                // ...output the translation;
                System.out.print(map.get(ch));
            } else {
                // ...if not, output original 
                System.out.print(ch);
            }
        }
        // end of line character
        System.out.println("");
    }
}
