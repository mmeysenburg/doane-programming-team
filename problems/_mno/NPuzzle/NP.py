import sys

locs = {'A':(0, 0), 'B':(0, 1), 'C':(0, 2), 'D':(0, 3),
        'E':(1, 0), 'F':(1, 1), 'G':(1, 2), 'H':(1, 3),
        'I':(2, 0), 'J':(2, 1), 'K':(2, 2), 'L':(2, 3), 
        'M':(3, 0), 'N':(3, 1), 'O':(3, 2), '.':(3, 3)}

lines = [x.replace('\n', '') for x in sys.stdin.readlines()]

dist = 0
for r, line in enumerate(lines):
    for c, ch in enumerate(line):
        if ch != '.':
            dist += abs(r - locs[ch][0]) + abs(c - locs[ch][1])
print(dist)