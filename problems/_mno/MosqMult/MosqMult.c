#include <stdio.h>
#include <stdlib.h>

int main() {
    int M, P, L, E, R, S, N, eggs, i;

    while(scanf("%d %d %d %d %d %d %d", &M, &P, &L, &E, &R, &S, &N) != EOF) {
        for(i = 0; i < N; i++) {
            eggs = M * E;
            M = P / S;
            P = L / R;
            L = eggs;
        }
        printf("%d\n", M);
    }
    return EXIT_SUCCESS;
}