
import java.util.*;

/**
 * Solution to "Mosquito Multiplication" programming competition problem.
 */
public class MosqMult {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        while (stdin.hasNext()) {
            int M = stdin.nextInt(); // mosquitos
            int P = stdin.nextInt(); // pupae
            int L = stdin.nextInt(); // larvae
            int E = stdin.nextInt(); // eggs per mosquito
            int R = stdin.nextInt(); // larvae survival rate
            int S = stdin.nextInt(); // pupae survival rate
            int N = stdin.nextInt(); // number of weeks

            for (int i = 0; i < N; i++) {
                // adults each lay E eggs
                int eggs = M * E;
                // new adults emerge from pupae, only 1/S survive
                M = P / S;
                // only 1/R of larvae turn into pupae
                P = L / R;
                // eggs hatch into larvae
                L = eggs;
            }

            // Number of adult mosquitos is what we're interested in
            System.out.println(M);

        } // while

    } // main

} // class
