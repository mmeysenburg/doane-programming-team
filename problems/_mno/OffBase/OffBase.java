
import java.math.*;
import java.util.*;

/**
 * Solution for the "Off Base" programming challenge question.
 *
 */
public class OffBase {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int Case = 1;

        while (stdin.hasNextLine()) {
            String line = stdin.nextLine();

            // set up a StringTokenizer to use whitespace, plus + and -,
            // as tokens
            StringTokenizer tzr = new StringTokenizer(line, "+-=", true);

            if (tzr.hasMoreTokens()) {
                // get the tokens from the line
                String n1 = tzr.nextToken();
                String op = tzr.nextToken();
                String n2 = tzr.nextToken();
                String eq = tzr.nextToken();
                String n3 = tzr.nextToken();

                int smallestBase = -1;

                // now, try to force each string representation into a 
                // BigInteger, using each base 2..36; if the numbers
                // work for the base, add 'em and see if they equal the
                // result. If they do, we've found the smallest base
                // that these values make sense for
                for (int base = 2; base <= 36; base++) {

                    // use try / catch to skip over instances where the
                    // numbers don't make sense in the specified base
                    try {
                        BigInteger bi1 = new BigInteger(n1, base);
                        BigInteger bi2 = new BigInteger(n2, base);
                        BigInteger bi3 = new BigInteger(n3, base);

                        // see if the numbers add up to the result
                        BigInteger result = null;
                        if (op.equals("+")) {
                            result = bi1.add(bi2);
                        } else {
                            result = bi1.subtract(bi2);
                        }

                        if (result.equals(bi3)) {
                            smallestBase = base;
                            break;
                        }
                    } 
                    
                    catch (NumberFormatException nfe) {
                        // empty catch; don't do anything at all if 
                        // the bases don't work
                    }

                } // for each base

                // output results
                if (smallestBase == -1) {
                    System.out.println("Case " + Case++
                            + ": expression is invalid.");
                } else {
                    System.out.println("Case " + Case++
                            + ": minimum base is "
                            + smallestBase);
                }

            } // if there are tokens

        } // while there are lines

    } // main

} // public class OffBase
