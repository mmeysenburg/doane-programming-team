import sys

vals = [int(x) for x in sys.stdin.readlines()]
for x in vals[1:]:
    if x % 2 == 0:
        print('{0:d} is even'.format(x))
    else:
        print('{0:d} is odd'.format(x))

