'''
Python solution to the "Once Chicken Per Person!" Kattis problem.
'''
# read input as a list of integers
data = [int(x) for x in input().split()]

# did we buy too much chicken? 
if data[1] > data[0]:
    d = data[1] - data[0]
    # use piece / pieces as appropriate
    if d > 1:
        print('Dr. Chaz will have {0:d} pieces' + \
            ' of chicken left over!'.format(d))
    else:
        print('Dr. Chaz will have {0:d} piece of' + \
            ' chicken left over!'.format(d))
else:
    d = data[0] - data[1]
    if d > 1:
        print('Dr. Chaz needs {0:d} more pieces' + \
            ' of chicken!'.format(d))
    else:
        print('Dr. Chaz needs {0:d} more piece' + \
            ' of chicken!'.format(d))