using System;

class OE {
    static void Main() {
        int n = Convert.ToInt32(Console.ReadLine());
        string[] words = new string[n];
        for (int i = 0; i < n; i++)
        {
            words[i] = Console.ReadLine();
        }
        for(int i = 0; i < n; i += 2) {
            Console.WriteLine(words[i]);
        }
    }
}