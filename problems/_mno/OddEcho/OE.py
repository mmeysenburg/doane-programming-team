import sys

allLines = [x.replace('\n', '') for x in sys.stdin.readlines()]

s = '\n'.join(allLines[1::2])
print(s)