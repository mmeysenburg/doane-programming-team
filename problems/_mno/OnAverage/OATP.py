import sys

def BFS(adj, src, dest, v, pred, dist):
    queue = []
    visited = [False] * (v + 1)

    for i in range(0, v+1):
        visited[i] = False
        dist[i] = 2000000
        pred[i] = -1

    visited[src] = True
    dist[src] = 0
    queue.append(src)

    while len(queue) > 0:
        u = queue.pop(0)
        for i in range(0, len(adj[u])):
            if not visited[adj[u][i]]:
                visited[adj[u][i]] = True
                dist[adj[u][i]] = dict[u] + 1
                pred[adj[u][i]] = u
                queue.append(adj[u][i])

                if adj[u][i] == dest:
                    return True

    return False

def printShortestDistance(adj, s, dest, v):
    pred = [] * (v + 1)
    dist = [] * (v + 1)

    if not BFS(adj, s, dest, v, pred, dist):
        return

    path = []
    crawl = dest
    path.append(crawl)
    while pred[crawl] != -1:
        path.append(pred[crawl])
        crawl = pred[crawl]

    print(dist[dest] - 1)

# main program
data = sys.stdin.readlines()
line = data[0].split()
N = int(line[0])
M = int(line[1])

adj = [[]]
for i in range(N):
    subadj = []
    adj.append(subadj)
    subadj = 0

for line in data[1:]:
    line1 = line.split()
    a = int(line1[0])
    b = int(line1[1])
    adj[a].append(b)
    adj[b].append(a)

printShortestDistance(adj, 1, N, N)