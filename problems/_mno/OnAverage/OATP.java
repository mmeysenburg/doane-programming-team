/**
 * @see https://www.geeksforgeeks.org/shortest-path-unweighted-graph/
 */
import java.io.*;
import java.util.*;

public class OATP {
    /**
     * @see https://www.geeksforgeeks.org/fast-io-in-java-in-competitive-programming/
     */
    static class FastReader 
	{ 
		BufferedReader br; 
		StringTokenizer st; 

		public FastReader() 
		{ 
			br = new BufferedReader(new
					InputStreamReader(System.in)); 
		} 

		String next() 
		{ 
			while (st == null || !st.hasMoreElements()) 
			{ 
				try
				{ 
					st = new StringTokenizer(br.readLine()); 
				} 
				catch (IOException e) 
				{ 
					e.printStackTrace(); 
				} 
			} 
			return st.nextToken(); 
		} 

		int nextInt() 
		{ 
			return Integer.parseInt(next()); 
		} 

		long nextLong() 
		{ 
			return Long.parseLong(next()); 
		} 

		double nextDouble() 
		{ 
			return Double.parseDouble(next()); 
		} 

		String nextLine() 
		{ 
			String str = ""; 
			try
			{ 
				str = br.readLine(); 
			} 
			catch (IOException e) 
			{ 
				e.printStackTrace(); 
			} 
			return str; 
		} 
	} 

    private static void addEdge(ArrayList<ArrayList<Integer>> adj, int src, int dest) {
        adj.get(src).add(dest);
        adj.get(dest).add(src);
    }

    private static boolean BFS(ArrayList<ArrayList<Integer>> adj, int src, int dest, 
        int v, int[] pred, int[] dist) {
        LinkedList<Integer> queue = new LinkedList<>();
        boolean[] visited = new boolean[v + 1];
        for(int i = 0; i <= v; i++) {
            visited[i] = false;
            dist[i] = Integer.MAX_VALUE;
            pred[i] = -1;
        }

        visited[src] = true;
        dist[src] = 0;
        queue.add(src);

        while(!queue.isEmpty()) {
            int u = queue.removeFirst();
            for(int i = 0; i < adj.get(u).size(); i++) {
                if(!visited[adj.get(u).get(i)]) {
                    visited[adj.get(u).get(i)] = true;
                    dist[adj.get(u).get(i)] = dist[u] + 1;
                    pred[adj.get(u).get(i)] = u;
                    queue.add(adj.get(u).get(i));

                    if(adj.get(u).get(i) == dest) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    private static void printShortestDistance(ArrayList<ArrayList<Integer>> adj, int s, int dest, int v) {
        int[] pred = new int[v + 1];
        int[] dist = new int[v + 1];

        if(!BFS(adj, s, dest, v, pred, dist)) {
            return;
        }

        ArrayList<Integer> path = new ArrayList<>();
        int crawl = dest;
        path.add(crawl);
        while(pred[crawl] != -1) {
            path.add(pred[crawl]);
            crawl = pred[crawl];
        }

        System.out.println(dist[dest] - 1);
    }
    public static void main(String[] args) {
        FastReader stdin = new FastReader();

        int N = stdin.nextInt();
        int M = stdin.nextInt();

        ArrayList<ArrayList<Integer>> adj = new ArrayList<>();
        for(int i = 0; i <= N; i++) {
            adj.add(new ArrayList<>());
        }

        for(int i = 0; i < M; i++) {
            int a = stdin.nextInt();
            int b = stdin.nextInt();
            addEdge(adj, a, b);
        }

        int src = 1, dest = N;
        printShortestDistance(adj, src, dest, N);
    }
}