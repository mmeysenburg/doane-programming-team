'''
Python program to prepare for the 4 Thought problem.
This code goes through all possible combinations of
the expression 

4 op 4 op 4 op 4

where op is one of [+, -, *, //], and places the 
value and expression in a dictionary. It then dumps
the dictionary to the console, so it can be copied / 
pasted into the actual solution program. 
'''
ops = ['+', '-', '*', '//']
exprs = {}
for o1 in ops:
    for o2 in ops:
        for o3 in ops:
            expr = '4 {0:s} 4 {1:s} 4 {2:s} 4'.format(o1, o2, o3)
            n = eval(expr)
            exprs[n] = expr.replace('//', '/')

print(exprs)