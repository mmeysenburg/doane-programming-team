
import java.util.*;

/**
 * Solution to "4 Thought" programming competition problem. 
 * Determine if an equation of the form 4op4op4op4 = n is valid
 * for operators in [+, -, *, /]. The code uses a custom infix
 * expression evaluator.
 */
public class FourThought {

    // this map will hold values we've already found, so we
    // don't bother to calculate them twice
    private static Map<Integer, String> map = new HashMap<>();
    
    // this map holds precedence levels for the operators
    private static Map<Character, Integer> precedence = new HashMap<>();

    /**
     * Perform an operation and save the result. Pops the operator
     * off the operator stack, the two operands off the value stack,
     * computes the result, and pushes it on to the value stack.
     * @param valueStack
     * @param operatorStack 
     */
    private static void applyOperator(Stack<Integer> valueStack,
            Stack<Character> operatorStack) {
        char op = operatorStack.pop();
        int b = valueStack.pop();
        int a = valueStack.pop();
        switch (op) {
            case '+':
                valueStack.push(a + b);
                break;
            case '-':
                valueStack.push(a - b);
                break;
            case '*':
                valueStack.push(a * b);
                break;
            case '/':
                valueStack.push(a / b);
                break;
        } // switch
    } // applyOperator

    /**
     * Evaluate an infix notation expression and return its value.
     * Equation is of the form 4op4op4op4, where op is one of 
     * [+, -, *, /]. 
     * 
     * Uses an abbreviated version of the algorithm for infix evaluation
     * found at https://www.geeksforgeeks.org/expression-evaluation/
     * 
     * while there are still tokens to be read in
     *      Get the next token.
     *      If the token is
     *          A number: push it onto the value stack.
     * 
     *          variable: get its value, and push onto the value stack.
     * 
     *          A left parenthesis: push it onto the operator stack.
     * 
     *          A right parenthesis:
     *              While the thing on top of the operator stack is not a 
     *                left parenthesis,
     * 
     *                  Pop the operator from the operator stack.
     * 
     *                  Pop the value stack twice, getting two operands.
     * 
     *                  Apply the operator to the operands, in the 
     *                      correct order.
     * 
     *                  Push the result onto the value stack.
     * 
     *              Pop the left parenthesis from the operator stack, and 
     *                discard it.
     * 
     *          An operator (call it thisOp):
     *              While the operator stack is not empty, and the top 
     *                thing on the operator stack has the same or 
     *                greater precedence as thisOp,
     * 
     *                  Pop the operator from the operator stack.
     * 
     *                  Pop the value stack twice, getting two operands.
     * 
     *                  Apply the operator to the operands, in the correct 
     *                    order.
     * 
     *                  Push the result onto the value stack.
     * 
     *              Push thisOp onto the operator stack.
     * 
     * While the operator stack is not empty,
     * 
     *  Pop the operator from the operator stack.
     * 
     *  Pop the value stack twice, getting two operands.
     * 
     *  Apply the operator to the operands, in the correct order.
     * 
     *  Push the result onto the value stack.
     * 
     * Return top value on value stack
     * 
     * @param equation
     * @return 
     */
    private static int evaluate(char[] equation) {
        Stack<Integer> valueStack = new Stack<>();
        Stack<Character> operatorStack = new Stack<>();

        for (int i = 0; i < equation.length; i++) {
            // if current token is a number, push onto value stack
            if (equation[i] == '4') {
                valueStack.push(4);
            } else {
                // only other option is that token is an operator
                while (!operatorStack.isEmpty()
                        && precedence.get(operatorStack.peek())
                        >= precedence.get(equation[i])) {
                    applyOperator(valueStack, operatorStack);
                }
                operatorStack.push(equation[i]);
            }
        }

        while (!operatorStack.isEmpty()) {
            applyOperator(valueStack, operatorStack);
        }

        return valueStack.pop();
    }

    /**
     * Convert an equation in a character array to the format
     * expected in the output.
     * @param equation character array holding the equation
     * @return formatted String version of the equation, including
     * a trailing space
     */
    private static String makeEqString(char[] equation) {
        StringBuilder sb = new StringBuilder();

        for (int i = 0; i < equation.length; i++) {
            sb.append(equation[i]);
            sb.append(' ');
        }

        return sb.toString();
    }

    /**
     * Iterate through all 64 possible equations, and store
     * their values. Keep only the first equation that evaluates
     * to a particular value.
     */
    private static void makeMap() {
        char[] ops = {'+', '-', '*', '/'};
        char[] eq = new char[7];
        eq[0] = eq[2] = eq[4] = eq[6] = '4';

        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                for (int k = 0; k < 4; k++) {
                    eq[1] = ops[i];
                    eq[3] = ops[j];
                    eq[5] = ops[k];
                    int n = evaluate(eq);

                    if (!map.containsKey(n)) {
                        map.put(n, makeEqString(eq));
                    } // if
                } // for k
            } // for j
        } // for i
    } // makeMap

    /**
     * Application entry point.
     * 
     * @param args Command-line arguments; ignored
     */
    public static void main(String[] args) {
        // populate precedence map
        precedence.put('+', 0);
        precedence.put('-', 0);
        precedence.put('*', 1);
        precedence.put('/', 1);

        // figure out what values are possible, and
        // save their respective equations in a map
        makeMap();

        // now just read input and see if each value of n
        // is in the map. 
        Scanner stdin = new Scanner(System.in);
        int m = stdin.nextInt();
        for (int i = 0; i < m; i++) {
            int n = stdin.nextInt();
            if (map.containsKey(n)) {
                System.out.println(map.get(n) + "= " + n);
            } else {
                System.out.println("no solution");
            }
        }
        stdin.close();
    }
} // FourThought
