'''
Python solution to the 4 Thought problem. Another program was used to 
calculate all of the possible expressions / values. Then, we placed
those into a dictionary, so all we have to do is look up the values 
in the dictionary and print the expression (or 'no solution').
'''
import sys

exprs = {16: '4 / 4 * 4 * 4 = 16', 8: '4 / 4 * 4 + 4 = 8', 
         24: '4 * 4 + 4 + 4 = 24', 9: '4 / 4 + 4 + 4 = 9', 
         0: '4 / 4 / 4 / 4 = 0', -8: '4 - 4 * 4 + 4 = -8', 
         7: '4 - 4 / 4 + 4 = 7', 68: '4 * 4 * 4 + 4 = 68', 
         1: '4 / 4 * 4 / 4 = 1', 4: '4 / 4 / 4 + 4 = 4', 
         -16: '4 - 4 * 4 - 4 = -16', -1: '4 - 4 / 4 - 4 = -1', 
         -60: '4 - 4 * 4 * 4 = -60', 32: '4 * 4 + 4 * 4 = 32', 
         17: '4 / 4 + 4 * 4 = 17', 15: '4 * 4 - 4 / 4 = 15', 
         60: '4 * 4 * 4 - 4 = 60', 256: '4 * 4 * 4 * 4 = 256', 
         2: '4 / 4 + 4 / 4 = 2', -7: '4 / 4 - 4 - 4 = -7', 
         -15: '4 / 4 - 4 * 4 = -15', -4: '4 / 4 / 4 - 4 = -4'}

for n in [int(x) for x in sys.stdin.readlines()[1:]]:
    print(exprs.get(n, 'no solution'))