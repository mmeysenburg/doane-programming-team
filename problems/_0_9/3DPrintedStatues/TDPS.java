
import java.util.*;

/**
 * Solution to "3D Printed Statues" programming competition problem. Find
 * minimum number of days required to print n statues.
 */
public class TDPS {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();
        stdin.close();

        // Most efficient strategy is to print printers until
        // there are enough to print all n statues in one day.
        // This is like a full binary tree, where each 
        // level of the tree represents the number of printers 
        // available on day 0, 1, 2, etc. So, if we find the
        // depth of the full binary tree with at least n nodes
        // in the last level, and add one, we have the number of 
        // days required to print the statues. We can do that by 
        // finding the
        // ceiling of lg(n) plus one. 
        int days
                = (int) (Math.ceil(Math.log(n) / Math.log(2)) + 1);

        System.out.println(days);
    }
}
