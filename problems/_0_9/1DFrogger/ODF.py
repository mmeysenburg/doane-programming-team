'''
Python solution to the Kattis 1-D Frogger (Easy) problem.
'''
n, s, m = [int(x) for x in input().split()]      # problem parameters

# we'll add an extra, unused slot to the board so the problem's
# 1-based indexing will work 
board = [0] + [int(x) for x in input().split()]  # board, with spare slot 0

# List of booleans representing which squares we have visitied
visited = [False] * len(board)

# keep moving as long as none of the end conditions have been reached
h = 0
while s >= 1 and s <= n and not visited[s] and board[s] != m:
    h += 1
    visited[s] = True 
    s += board[s] # this takes care of moving left or right

# output the results
if s < 1:
    print('left')
elif s > n:
    print('right')
elif visited[s]:
    print('cycle')
else:
    print('magic')

print(h)