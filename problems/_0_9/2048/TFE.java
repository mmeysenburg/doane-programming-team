
import java.util.*;

/**
 * Solution to "2048" programming competition problem. Perform a single 
 * move of a given game.
 */
public class TFE {

    /**
     * Perform a left move on the grid.
     *
     * @param grid Array holding the game numbers.
     */
    private static void left(int[][] grid) {
        // for each row...
        for (int row = 0; row < 4; row++) {
            // ... left justify the row
            for (int col = 1; col < 4; col++) {
                // if current cell is not empty,
                if (grid[row][col] != 0) {
                    
                    // try to slide its value to the left
                    int j = col - 1;
                    while (j >= 0) {
                        // but only if the next cell leftwards
                        // is empty
                        if (grid[row][j] == 0) {
                            grid[row][j] = grid[row][j + 1];
                            grid[row][j + 1] = 0;
                        }
                        j--;
                    } // while j
                } // if 
            } // for col

            // now merge any cells that can be combined in the row.
            // Start with leftmost value; if it is not zero and it
            // is same as the next to the right, add values and then
            // move everything else over
            if (grid[row][0] != 0 && grid[row][0] == grid[row][1]) {
                grid[row][0] += grid[row][1];
                grid[row][1] = grid[row][2];
                grid[row][2] = grid[row][3];
                grid[row][3] = 0;
            }
            // repeat for 2nd value in the row
            if (grid[row][1] != 0 && grid[row][1] == grid[row][2]) {
                grid[row][1] += grid[row][2];
                grid[row][2] = grid[row][3];
                grid[row][3] = 0;
            }
            // and again for the 3rd value
            if (grid[row][2] != 0 && grid[row][2] == grid[row][3]) {
                grid[row][2] += grid[row][3];
                grid[row][3] = 0;
            }
        } // for each row
    } // left

    /**
     * Perform an up move on the grid.
     *
     * @param grid Array holding the game numbers.
     */
    private static void up(int[][] grid) {
        // see comments in left method; this is very similar
        for (int col = 0; col < 4; col++) {
            for (int row = 1; row < 4; row++) {
                if (grid[row][col] != 0) {
                    int i = row - 1;
                    while (i >= 0) {
                        if (grid[i][col] == 0) {
                            grid[i][col] = grid[i + 1][col];
                            grid[i + 1][col] = 0;
                        }
                        i--;
                    }
                }
            }

            if (grid[0][col] != 0 && grid[0][col] == grid[1][col]) {
                grid[0][col] += grid[1][col];
                grid[1][col] = grid[2][col];
                grid[2][col] = grid[3][col];
                grid[3][col] = 0;
            }
            if (grid[1][col] != 0 && grid[1][col] == grid[2][col]) {
                grid[1][col] += grid[2][col];
                grid[2][col] = grid[3][col];
                grid[3][col] = 0;
            }
            if (grid[2][col] != 0 && grid[2][col] == grid[3][col]) {
                grid[2][col] += grid[3][col];
                grid[3][col] = 0;
            }
        }

    }

    /**
     * Perform a right move on the grid.
     *
     * @param grid Array holding the game numbers.
     */
    private static void right(int[][] grid) {
        // see comments in left method; this is very similar
        for (int row = 0; row < 4; row++) {
            for (int col = 2; col >= 0; col--) {
                int j = col + 1;
                while (j < 4) {
                    if (grid[row][j] == 0) {
                        grid[row][j] = grid[row][j - 1];
                        grid[row][j - 1] = 0;
                    }
                    j++;
                }
            }

            if (grid[row][3] != 0 && grid[row][3] == grid[row][2]) {
                grid[row][3] += grid[row][2];
                grid[row][2] = grid[row][1];
                grid[row][1] = grid[row][0];
                grid[row][0] = 0;
            }
            if (grid[row][2] != 0 && grid[row][2] == grid[row][1]) {
                grid[row][2] += grid[row][1];
                grid[row][1] = grid[row][0];
                grid[row][0] = 0;
            }
            if (grid[row][1] != 0 && grid[row][1] == grid[row][0]) {
                grid[row][1] += grid[row][0];
                grid[row][0] = 0;
            }
        }
    }

    /**
     * Perform a down move on the grid.
     *
     * @param grid Array holding the game numbers.
     */
    private static void down(int[][] grid) {
        // see comments in left method; this is very similar
        for (int col = 0; col < 4; col++) {
            for (int row = 2; row >= 0; row--) {
                if (grid[row][col] != 0) {
                    int i = row + 1;
                    while (i < 4) {
                        if (grid[i][col] == 0) {
                            grid[i][col] = grid[i - 1][col];
                            grid[i - 1][col] = 0;
                        }
                        i++;
                    }
                }
            }

            if (grid[3][col] != 0 && grid[3][col] == grid[2][col]) {
                grid[3][col] += grid[2][col];
                grid[2][col] = grid[1][col];
                grid[1][col] = grid[0][col];
                grid[0][col] = 0;
            }
            if (grid[2][col] != 0 && grid[2][col] == grid[1][col]) {
                grid[2][col] += grid[1][col];
                grid[1][col] = grid[0][col];
                grid[0][col] = 0;
            }
            if (grid[1][col] != 0 && grid[1][col] == grid[0][col]) {
                grid[1][col] += grid[0][col];
                grid[0][col] = 0;
            }
        }
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int[][] grid = new int[4][4];

        // read and store the grid
        for (int i = 0; i < 4; i++) {
            for (int j = 0; j < 4; j++) {
                grid[i][j] = stdin.nextInt();
            }
        }

        // read the move to perform
        int move = stdin.nextInt();

        // do the appropriate move
        switch (move) {
            case 0: // left
                left(grid);
                break;
            case 1: // up
                up(grid);
                break;
            case 2: // right
                right(grid);
                break;
            case 3: // down
                down(grid);
                break;
        }

        // print out resulting grid
        for (int row = 0; row < 4; row++) {
            for (int col = 0; col < 4; col++) {
                System.out.printf("%d ", grid[row][col]);
            }
            System.out.println();
        }
        
        stdin.close();
    } // main
}
