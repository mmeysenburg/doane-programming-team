
def get_ints():
    line = str(input())
    return(line)


def calculate(line):
    lengths = []
    sequence = []
    args = line.split()
    i = int(args[0])
    j = int(args[1])
    if i > j:
        x = range(j,i)
    else:
        x = range(i, j)
    for n in x:
        sequence.append(n)
        while n != 1.0:
            if n == 0:
                break
            elif n % 2 == 0:
                n = n/2
                sequence.append(n)
            else:
                n = n*3+1
                sequence.append(n)
        lengths.append(len(sequence))
        sequence = []
    maximum = max(lengths)
    print(i, j, maximum)


def main():
    values = get_ints()
    while values != '0 0':
        calculate(values)
        values = get_ints()

main()
