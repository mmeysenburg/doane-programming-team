import sys

# read all of the input, converting into list of sorted lists of integers
inp = [sorted([int(x) for x in line.strip().split()]) for line in sys.stdin.readlines()]

# process all of the input cases
for x, y in inp:
    max_len = -1
    for i in range(x, y + 1):
        length = 1
        n = i
        while n != 1:
            if n % 2 == 0:
                n /= 2
            else:
                n = 3 * n + 1
                n /= 2
                length += 1
            length += 1
        if length > max_len:
            max_len = length
    print(x, y, max_len)
