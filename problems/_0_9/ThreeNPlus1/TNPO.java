
import java.util.*;

/**
 * Solution to the 3n+1 problem.
 *
 */
public class TNPO {

    /**
     * Determine 3n + 1 cycle length for a given input. If n is even, 
     * divide it by two; otherwise multiply by three and add one. Repeat 
     * until n becomes one. Count how many cycles this takes.
     *
     * @param n Initial value of n
     *
     * @return Cycle length; number of iterations it takes for n to 
     * become 1.
     */
    private static int cycleLength(int n) {
        int len = 1;                // counter for cycle length
        long nl = n;                // do calculations with longs
        while (nl != 1l) {
            if (nl % 2l == 0l) {
                nl >>= 1;           // bit shifting to speed things up
                len++;
            } else {
                nl = 3l * nl + 1l;
                nl >>= 1;           // do the next cycle right here, 
                len += 2;           // because 3n+1 will be even
            }
        }

        return len;
    }

    /**
     * Application entry point.
     *
     * @param args Ignored.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // continue until we reach sentinel values
        while (stdin.hasNextInt()) {
            int i = stdin.nextInt();
            int j = stdin.nextInt();
            int i0 = i;    // preserve original input
            int j0 = j;    // order

            // if output is out of order, switch 'em
            if (i > j) {
                int t = i;
                i = j;
                j = t;
            }

            // find largest cycle length between i and j, inclusive
            int maxLen = Integer.MIN_VALUE;
            for (int n = i; n <= j; n++) {
                int len = cycleLength(n);
                if (len > maxLen) {
                    maxLen = len;
                }
            }

            // report results
            System.out.println(i0 + " " + j0 + " " + maxLen);
        } // while
    } // main
}
