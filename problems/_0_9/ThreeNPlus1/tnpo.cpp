#include <iostream>
#include <cstdlib>

int cycleLength(int n) {
    int len = 1;
    long nl = n;
    while(nl != 1L) {
        if(nl % 2L == 0L) {
            nl >>= 1;
            len++;
        } else {
            nl = 3 * nl + 1L;
            nl >>= 1;
            len += 2;
        }
    }

    return len;
}

int main() {
    using namespace std;

    int i, j, i0, j0, t, maxLen, n, len;
    while (cin >> i) {
        cin >> j;
        i0 = i;
        j0 = j;

        if(i > j) {
            t = i;
            i = j;
            j = t;
        }

        maxLen = -1;
        for(n = i; n <= j; n++) {
            len = cycleLength(n);
            if(len > maxLen) {
                maxLen = len;
            }
        }

        cout << i0 << " " << j0 << " " << maxLen << endl;
    }
    return EXIT_SUCCESS;
}