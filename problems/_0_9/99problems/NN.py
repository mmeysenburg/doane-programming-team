'''
Solution to the 99 Problems Kattis problem.
'''
# get input and input modulo 100
v = int(input())
m = v % 100

# calculate distance to each "99"
up = 99 - m
dn = m + 1

# equal distances? go up
if up == dn:
    print(v + up)

# down closer *and* still positiver? go down
elif dn < up and (v - dn) > 0:
    print(v - dn)

# otherwise, go up
else:
    print(v + up)