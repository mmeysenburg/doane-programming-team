'''
Python solution to 10 Kinds of People Kattis problem. 
'''

# input h, w
h_w = [int(x) for x in input().split()]
h = h_w[0]
w = h_w[1]

# build map
map = []
for i in range(h):
    line = input()
    map.append([int(c) for c in line])

# prepare data structures
group = [0] * (h * w)
nextGroup = 1
open = []

# process cases
n = int(input())
for i in range(n):
    coords = [(int(x) - 1) for x in input().split()]
    r1 = coords[0]
    c1 = coords[1]
    r2 = coords[2]
    c2 = coords[3]

    dim1_1 = r1 * w + c1
    dim1_2 = r2 * w + c2

    if dim1_1 == dim1_2:
        # start and end in the same place? no search necessary
        print('binary' if map[r1][c1] == 0 else 'decimal')
    elif map[r1][c1] != map[r2][c2]:
        # start and end not same kind? no search necessary
        print('neither')
    elif group[dim1_1] == 0 and group[dim1_2] == 0:
        # search from r1, c1 to r2, c2; keep track of results in the
        # group list
        open.append((r1, c1))
        while len(open) > 0:
            curr = open.pop()
            cR = curr[0]
            cC = curr[1]
            currVal = map[cR][cC]
            dim1 = cR * w + cC

            if group[dim1] != 0:
                continue
    
            group[dim1] = nextGroup

            if cR > 0 and map[cR - 1][cC] == currVal and \
                group[(cR - 1) * w + cC] == 0:

                open.append((cR - 1, cC))
            if cR < h - 1 and map[cR + 1][cC] == currVal and \
                group[(cR + 1) * w + cC] == 0:

                open.append((cR + 1, cC))
            if cC > 0 and map[cR][cC - 1] == currVal and \
                group[cR * w + cC - 1] == 0:

                open.append((cR, cC - 1))
            if cC < w - 1 and map[cR][cC + 1] == currVal and \
                group[cR * w + cC + 1] == 0:
                
                open.append((cR, cC + 1))

        nextGroup += 1

        if group[dim1_2] != 0:
            print('decimal' if map[r1][c1] == 1 else 'binary')
        else:
            print('neither')
    elif group[dim1_1] == group[dim1_2]:
        # we already found reachability for these coords
        print('decimal' if map[r1][c1] == 1 else 'binary')
    else:
        # we already found unreachability  for these coords
        print('neither')
