tokens = input().split()
L = int(tokens[0])
K = int(tokens[1])
S = int(tokens[2])

record = {} 
for i in range(L):
    tokens = input().split()
    racer = int(tokens[0])
    lapTime = float(tokens[1])
    if racer in record:
        record[racer][0] += 1 # add a lap
        record[racer][1] += lapTime # increment total time
    else:
        record[racer] = [1, lapTime] # time for 1st lap

finalRecord = [[x, round(record[x][1],2)] for x in record if record[x][0] == K]

def racerKey(a):
    return a[1]

finalRecord.sort()
finalRecord.sort(key=racerKey)

for x in finalRecord:
    print(x[0])