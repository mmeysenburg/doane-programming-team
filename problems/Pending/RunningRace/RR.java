import java.util.*;

public class RR {
    static class Racer implements Comparable<Racer> {
        int racerNum;
        float totalTime;

        public Racer(int r, float t) {
            racerNum = r;
            totalTime = t;
        }

        public int compareTo(Racer r) {
            if (totalTime == r.totalTime) {
                return racerNum - r.racerNum;
            } else {
                if (totalTime < r.totalTime) {
                    return -1;
                } else {
                    return 1;
                }
            }
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        int l = Integer.parseInt(tokens[0]);
        int k = Integer.parseInt(tokens[1]);
        int s = Integer.parseInt(tokens[2]);

        float[] times = new float[s + 1];
        short[] laps = new short[s + 1];
        for (int i = 0; i < l; i++) {
            tokens = stdin.nextLine().split(" ");
            int racer = Integer.parseInt(tokens[0]);
            float lapTime = Float.parseFloat(tokens[1]);
            laps[racer]++;
            times[racer] += lapTime;
        }

        List<Racer> list = new LinkedList<>();
        for (int i = 1; i < laps.length; i++) {
            if (laps[i] == k) {
                list.add(new Racer(i,
                        Math.round(times[i] * 100) / 100));
            }
        }

        Collections.sort(list);

        for (Racer r : list) {
            System.out.println(r.racerNum);
        }
    }
}