stuff = ['$', '|', '*']
n = int(input())
for i in range(n):
    adv = input()
    pack = []
    ok = True
    for j in range(len(adv)):
        if adv[j] in stuff:
            pack.append(adv[j])
        elif adv[j] == 'b':
            if len(pack) > 0 and pack[-1] == '$':
                pack.pop()
            else:
                ok = False
                break
        elif adv[j] == 'j':
            if len(pack) > 0 and pack[-1] == '*':
                pack.pop()
            else:
                ok = False
                break
        elif adv[j] == 't':
            if len(pack) > 0 and pack[-1] == '|':
                pack.pop()
            else:
                ok = False
                break
    if ok and len(pack) == 0:
        print('YES')
    else:
        print('NO')
        