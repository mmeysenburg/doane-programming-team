#include <cstdlib>
#include <iostream>
#include <list>

using namespace std;

#define ull unsigned long long

void clearGraph(unsigned **G, size_t R, size_t C) {
    for(size_t r = 0; r < R; r++) {
        for(size_t c = 0; c < C; c++) {
            G[r][c] = 0u;
        }
    }
}

void setEdge(unsigned **G, size_t a, size_t b, size_t C) {
    unsigned mask = 1u;
    size_t whichCol = C - b / (sizeof(unsigned) * 8u) - 1u;
    size_t whichBit = b % (sizeof(unsigned) * 8u);
    mask <<= whichBit;

    G[a][whichCol] |= mask;
}

void unsetEdge(unsigned **G, size_t a, size_t b, size_t C) {
    unsigned mask = 1u;
    size_t whichCol = C - b / (sizeof(unsigned) * 8u) - 1u;
    size_t whichBit = b % (sizeof(unsigned) * 8u);
    mask <<= whichBit;
    mask = ~mask;

    G[a][whichCol] &= mask;
}

void unsetAllForA(unsigned **G, size_t a, size_t R, size_t C) {
    // zap all outgoing edges
    for(size_t c = 0; c < C; c++) {
        G[a][c] = 0u;
    }
    // remove all incoming edges
    unsigned mask = 1u;
    size_t whichCol = C - a / (sizeof(unsigned) * 8u) - 1u;
    size_t whichBit = a % (sizeof(unsigned) * 8u);
    mask <<= whichBit;
    mask = ~mask;
    for(size_t r = 0; r < R; r++) {
        if(r != a) {
            G[r][whichCol] &= mask;
        }
    }
}

bool isEdge(unsigned** G, size_t a, size_t b, size_t C) {
    unsigned mask = 1u;
    size_t whichCol = C - b / (sizeof(unsigned) * 8u) - 1u;
    size_t whichBit = b % (sizeof(unsigned) * 8u);
    mask <<= whichBit;
    return G[a][whichCol] & mask;
}

list<size_t> getOutgoing(unsigned** G, size_t a, size_t C) {
    list<size_t> outgoing;

    size_t idx = 0u;
    for(int c = C - 1u; c >= 0; c--) {
        unsigned mask = 1u;
        for(size_t b = 0; b < sizeof(unsigned) * 8; b++) {
            if(G[a][c] & mask) {
                outgoing.push_back(idx);
            }
            mask <<= 1;
            idx++;
        }
    }

    return outgoing;
}

int main() {
    size_t V, E, Q, a, b, q;
    cin >> V >> E >> Q;

    // build powers of 7 % 1000000007
    ull pows[V];
    pows[0] = 1;
    for(size_t i = 1; i < V; i++) {
        pows[i] = pows[i - 1] * 7 % 1000000007;
    }

    // initialize bitwise adjacency matrix for storing edges
    size_t GRAPH_ROWS = 4000;
    size_t GRAPH_COLUMNS = GRAPH_ROWS / (sizeof(unsigned) * 8) + 1;
    unsigned **G, **Gp, **Gt;
    G = new unsigned*[GRAPH_ROWS];
    Gp = new unsigned*[GRAPH_ROWS];
    for(size_t i = 0; i < GRAPH_ROWS; i++) {
        G[i] = new unsigned[GRAPH_COLUMNS];
        Gp[i] = new unsigned[GRAPH_COLUMNS];
    }

    clearGraph(G, GRAPH_ROWS, GRAPH_COLUMNS);
    clearGraph(Gp, GRAPH_ROWS, GRAPH_COLUMNS);

    // build initial graph
    for(int i = 0; i < E; i++) {
        cin >> a >> b;
        setEdge(G, a, b, GRAPH_COLUMNS);
    }

    // process queries
    for(int i = 0; i < Q; i++) {
        cin >> q;
        switch (q) {
            case 1:
                // add new vertex
                V++;
                break;
        
            case 2:
                // add new edge
                cin >> a >> b;
                setEdge(G, a, b, GRAPH_COLUMNS);
                break;
        
            case 3:
                // remove all edges for vertex a
                cin >> a;
                unsetAllForA(G, a, GRAPH_ROWS, GRAPH_COLUMNS);
                break;
        
            case 4:
                // remove single edge
                cin >> a >> b;
                unsetEdge(G, a, b, GRAPH_COLUMNS);
                break;
        
            case 5:
                // transpose
                clearGraph(Gp, GRAPH_ROWS, GRAPH_COLUMNS);
                for(a = 0; a < V; a++) {
                    for(b = 0; b < V; b++) {
                        if(a != b && isEdge(G, b, a, GRAPH_COLUMNS)) {
                            setEdge(Gp, a, b, GRAPH_COLUMNS);
                        }
                    }
                }
                Gt = G;
                G = Gp;
                Gp = Gt;
                break;
        
            case 6:
                // complement
                clearGraph(Gp, GRAPH_ROWS, GRAPH_COLUMNS);
                for(a = 0; a < V; a++) {
                    for(b = 0; b < V; b++) {
                        if(a != b && !isEdge(G, a, b, GRAPH_COLUMNS)) {
                            setEdge(Gp, a, b, GRAPH_COLUMNS);
                        }
                    }
                }
                Gt = G;
                G = Gp;
                Gp = Gt;
                break;
        
        } // switch
    } // for Q


    cout << V << endl;    
    for(a = 0; a < V; a++) {
        list<size_t> outgoing = getOutgoing(G, a, GRAPH_COLUMNS);
        cout << outgoing.size() << " ";
        ull sum = 0;
        int i = 0;
        for(size_t b : outgoing)  {
            sum += ((pows[i++] * b) % 1000000007);
            sum %= 1000000007;
        }
        cout << sum << endl;
    }

    for(size_t i = 0; i < GRAPH_ROWS; i++) {
        delete [] G[i];
        delete [] Gp[i];
    }

    delete [] G;
    delete [] Gp;

    return EXIT_SUCCESS;
}