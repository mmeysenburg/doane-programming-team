tokens = input().split()
V = int(tokens[0])
E = int(tokens[1])
Q = int(tokens[2])

adjlist = [[] for i in range(V)]

# add edges to the graph
for i in range(E):
    tokens = input().split()
    adjlist[int(tokens[0])].append(int(tokens[1]))

# now do the queries
for i in range(Q):
    tokens = input().split()
    q = int(tokens[0])
    if q == 1:
        # add vertex
        adjlist.append([])
    elif q == 2:
        # add edge
        adjlist[int(tokens[1])].append(int(tokens[2]))
    elif q == 3:
        # delete incoming / outgoing edges
        b = int(tokens[1])
        adjlist[b].clear()
        for a in adjlist:
            while b in a:
                a.remove(b)
    elif q == 4:
        # delete one edge
        adjlist[int(tokens[1])].remove(int(tokens[2]))
    elif q == 5:
        # transpose G
        newAdjlist = [ [] for i in range(len(adjlist)) ]
        for a in range(len(adjlist)):
            for b in range(len(adjlist)):
                if (a != b) and (a in adjlist[b]):
                    newAdjlist[a].append(b)
        adjlist = newAdjlist
    elif q == 6:
        # complement G
        newAdjlist = [ [] for i in range(len(adjlist)) ]
        for a in range(len(adjlist)):
            for b in range(len(adjlist)):
                if (a != b) and (b not in adjlist[a]):
                    newAdjlist[a].append(b)
        adjlist = newAdjlist

print(len(adjlist))
divisor = 1000000007
for a in adjlist:
    d_i = len(a)
    h_i = 0
    prod = 1
    for n_i in a:
        h_i = (h_i + (prod * n_i) % divisor) % divisor
        prod *= 7
    print('{0:d} {1:d}'.format(d_i, h_i))