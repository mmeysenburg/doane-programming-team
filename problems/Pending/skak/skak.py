x_h, y_h = [int(x) for x in input().split()]
x_p, y_p = [int(x) for x in input().split()]
if x_h == x_p or y_h == y_p:
    print(1)
else:
    min_d = min(x_h - x_p, y_h - y_p)
    print(min_d + 1)