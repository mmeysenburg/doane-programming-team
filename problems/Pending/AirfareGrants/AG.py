import sys

values = [int(x) for x in sys.stdin.readlines()[1:]]
min_cost = min(values)
reimbursement = max(values) // 2
if reimbursement >= min_cost:
    print(0)
else:
    print(min_cost - reimbursement)