import numpy as np

n = int(input())
adj = np.zeros((n + 1, n + 1), dtype=np.int_)

for i in range(1, n):
    vals = [0]
    for x in input().split():
        vals.append(int(x))
        
    for j in range(1, len(vals)):
        adj[i][i + j] = vals[j]
        adj[i + j][i] = vals[j]

print(adj)