import sys
import math

readlines = sys.stdin.readlines
sqrt = math.sqrt
values = [int(x) for x in readlines()[1:]]

def which_is_it(x):
    is_odd = not (x % 2 == 0)
    is_square = (int(sqrt(x)) == sqrt(x))
    if is_odd and is_square:
        print("OS")
    elif is_odd:
        print("O")
    elif is_square:
        print("S")
    else:
        print("EMPTY")

for x in values:
    which_is_it(x)