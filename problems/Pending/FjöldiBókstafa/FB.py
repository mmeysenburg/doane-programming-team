import string
inp=input().upper()
print(sum([1 for c in inp if c in string.ascii_uppercase]))