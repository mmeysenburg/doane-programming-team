N = int(input())

sum = 0
dim = -1
height = 0
while sum <= N:
    dim += 2
    sum += dim * dim
    height += 1

print(height - 1)