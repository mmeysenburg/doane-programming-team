import sys
bd = [[c for c in line] for line in sys.stdin.readlines()[1:]]
coords = []
sum = 0
for r in range(1, len(bd) + 1):
    for c in range(1, len(bd[r-1]) + 1):
        if bd[r-1][c-1] == '*':
            coords.append((r, c))
            sum += 1

print(sum)
for v in coords:
    print(v[0], v[1])