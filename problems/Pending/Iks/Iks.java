import java.util.*;

public class Iks {
    /** List to hold factors of a number. */
    private static LinkedList<Integer> factors = new LinkedList<>();

    /** Number of operations required to produce the result. */
    private static int min = Integer.MIN_VALUE;

    /**
     * Do a prime factorization of n. The factors are stored in the 
     * factors list, which continues to build each time the method
     * is called. 
     *
     * @param n Number to factor
     */
    private static void factor(int n) {

        // count number of factors
        int num = 0;

        // determine max value to check
        int cap = (int)Math.sqrt(n);

        // naive algorithm: starting at 2, up to sqrt(n), see which 
        // values divide n, and add them to the list
        for (int i = 2; i <= cap; i++) {
            // use current factor value as many times as we can
            while (n % i == 0) {
                n /= i;
                factors.add(i);
                num++;
            }
        }
        // if n wasn't prime, add n itself to the end of the list
        if (n > 1) {
            factors.add(n);
            num++;
        }

        // update min # of ops
        num--;
        if(num > min) {
            min = num;
        }
    }

    public static void main(String[] args) {
        Scanner std = new Scanner(System.in);

        int N = std.nextInt();
        int[] nums = new int[N];
        for(int i = 0; i < N; i++) {
            nums[i] = 1;
            factor(std.nextInt());
        }

        // sort the factors list
        Collections.sort(factors);

        // did we have all primes to begin with? 
        HashSet<Integer> facSet = new HashSet<>();
        for(int j : factors) {
            facSet.add(j);
        }
        if(factors.size() == 0 || factors.size() == N - 1) {
            if(facSet.size() == 1) {
                // all primes, all the same
                System.out.printf("%d 0\n", factors.removeFirst());
            } else {
                // all primes, not all the same
                System.out.printf("1 0\n");
            }
        } else {
            // not all primes to begin with
            int i = 0;
            while(!factors.isEmpty()) {
                nums[i] *= factors.removeFirst();
                i = (i + 1) % nums.length;
            }

            Arrays.sort(nums);
            i = 0;
            while(nums[i] == 1) {
                i++;
            }

            System.out.printf("%d %d\n", nums[i], min);
        }
    }
}