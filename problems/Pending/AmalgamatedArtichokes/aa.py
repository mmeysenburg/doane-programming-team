from math import sin
from math import cos 

inp = [int(x) for x in input().split()]
prices = [inp[0] * (sin(inp[1] * k + inp[2]) + cos(inp[3] * k + inp[4])) for k in range(inp[5])]
p_o = ['{0:0.4f}'.format(x) for x in prices]
print(p_o)
# max_decl = -4294967296
# for i in range(len(prices) - 1):
#     decl = prices[i] - min(prices[i+1:])
#     if decl > max_decl:
#         max_decl = decl
# if max_decl < 0:
#     max_decl = 0.0
# print(max_decl)