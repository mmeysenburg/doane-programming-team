'''
Python solution to the Highly Recursive Function MICS problem,
using dynamic programming to remember solved cases.
'''
import sys

# read all of the input lines, and convert each line to a list of integers
d = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]

# dictionary to remember which F(m, n) values we've already calculated
mem = {}

def f(m, n):
    '''
    Recursive function, as defined in the problem. Uses dynamic programming
    to remember which values we've already calculated.
    '''
    # mark the dictionary as a global rather than local variable; we 
    # could alternatively pass it as a parameter
    global mem

    # new base case: have we solved this (m, n) already?
    if (m, n) in mem:
        # if so, send back the result we calculated
        return mem[(m, n)]
    elif m < n and n > 10:
        # recursive case; save result...
        v = f(m + 1, n - 2) + f(m + 3, n) - f(m + 4, n - 1)
        # ... and store it in the dictionary before returning
        mem[(m, n)] = v
        return v
    elif n <= 10:
        # base case: calculate, save, return
        v = m + n
        mem[(m, n)] = v
        return v
    elif m >= n and n > 10:
        # final base case: calculate, save, return 
        mem[(m, n)] = 10
        return 10
    
# now go through the cases and print the results
for i in range(1, len(d)):
    m = d[i][0]
    n = d[i][1]
    print('Case {0:d}: F({1:d},{2:d}) = {3:d}'.format(i, m, n, f(m, n)))