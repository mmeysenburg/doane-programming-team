'''
Pythos solution to the Monthly Weather Summaries MICS 2024 problem.
'''
import sys

# read all input lines execpt the first, trimming off the '\n'
d = [line[:-1] for line in sys.stdin.readlines()]

# integer to string month conversions
months = ['January', 'February', 'March', 'April', 'May', 'June',
          'July', 'August', 'Septemper', 'October', 'November', 'December']

# store low and high temperatures in dictionaries of lists. The keys
# will be (year, month) tuples, and the values will be lists of low / high
# temps for that tuple
low_temps = {}
high_temps = {}

# process all the temperature data
for line in d[1:]:
    # break line into tokens by comma
    tks = line.split(',')
    # break date up by /
    mdy = tks[0].split('/')

    # only process data for March and April 
    if mdy[0] == '3' or mdy[0] == '4':
        # form the dictionary key
        month = (int(mdy[2]), int(mdy[0]))

        # if there is a valid low temperature for this date...
        if len(tks) >= 3 and tks[2].isnumeric():
            # accumulate the temp in its list
            if month in low_temps:
                low_temps[month].append(int(tks[2]))
            else:
                low_temps[month] = [int(tks[2])]

        # if there is a valid high temperature for this date...
        if len(tks) >= 2 and tks[1].isnumeric():
            # accumulate the temp in its list
            if month in high_temps:
                high_temps[month].append(int(tks[1]))
            else:
                high_temps[month] = [int(tks[1])]

# make a list of all the month tuples, sorted by year first and
# then by month number
all_months = sorted(list(low_temps.keys()))

# for each sorted month tuple
for month in all_months:
    # determine the mean low and high temps
    low_mean = sum(low_temps[month]) / len(low_temps[month])
    high_mean = sum(high_temps[month]) / len(high_temps[month])

    # format the month / year for printing
    p_month = months[month[1] - 1] + ' ' + str(month[0])

    # print the results 
    print('{0:s}: Average daily high temperature {1:.1f} '
          'and Average daily low temperature {2:.1f}'.format(
              p_month, high_mean, low_mean
    ))