x = int(input())
caseNum = 1
while x != 0:
    y = x * 2
    print('Case {0:d}: Double of {1:d} is {2:d}'.format(caseNum, x, y))

    x = int(input())
    caseNum += 1