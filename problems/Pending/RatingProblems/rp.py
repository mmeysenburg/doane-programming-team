n, k = [int(x) for x in input().split()]
sum_r = 0
for i in range(k):
    sum_r += int(input())
    
max_sum_r = sum_r
min_sum_r = sum_r
for i in range(n - k):
    max_sum_r += 3
    min_sum_r -= 3
    
print(min_sum_r / n, max_sum_r / n)