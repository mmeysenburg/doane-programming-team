import sys
all_inp = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]
x_s = {}
y_s = {}
for coord in all_inp:
    x_s[coord[0]] = x_s.get(coord[0], 0) + 1
    y_s[coord[1]] = y_s.get(coord[1], 0) + 1
x = -1
y = -1
for t in x_s:
    if x_s[t] == 1:
        x = t
        break
for t in y_s:
    if y_s[t] == 1:
        y = t
        break
print(x, y)