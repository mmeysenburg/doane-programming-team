import sys
all=''.join([s.strip() for s in sys.stdin.readlines()[1:]])
print(''.join([c for c in all if c != '.']))