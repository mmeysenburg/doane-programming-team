'''
Python solution to the Kattis Anti-Palindrome problem, from the
2023 ACM NCNA Programming Contest
'''
import string

def isPal(val):
    '''
    Return True if val is a palindrome, False otherwise.

    The function uses slicing to produce a reversed copy
    of val, the compares val with the reversed version.
    '''
    return val == val[-1::-1]

# Read in the input, converting to lowercase, and omitting non-
# alphabetic characters. The input is stored in a list. 
raw = [x for x in input().lower() if x in string.ascii_lowercase]

# Take a brute force approach: check all substrings. If one is
# a palindrome, print the result and exit. 
for i in range(len(raw) - 1):
    for j in range(i + 1, len(raw)):
        if isPal(raw[i:j + 1]):
            print('Palindrome')
            exit()

# If we get here, no palindromes were found, so print the 
# appropriate output.
print('Anti-palindrome')
