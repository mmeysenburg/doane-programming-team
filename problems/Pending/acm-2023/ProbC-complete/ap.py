import string

def isPal(val):
    return val == val[-1::-1]

raw = ''.join([x for x in input().lower() if x in string.ascii_lowercase])

checked = {}
for i in range(len(raw) - 1):
    for j in range(i + 1, len(raw)):
        val = raw[i:j + 1]
        if val not in checked:
            checked[val] = isPal(val)
        if checked[val]:
            print('Palindrome')
            exit()
print('Anti-palindrome')
