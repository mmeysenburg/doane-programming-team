import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

public class SG {
    private static boolean relative(BigDecimal z, BigDecimal zp) {
        if(z.compareTo(BigDecimal.ZERO) == 0) {
            return zp.compareTo(BigDecimal.ZERO) == 0;
        }

        if(zp.compareTo(BigDecimal.ZERO) == 0) return false;

        BigDecimal error = z.subtract(zp).abs();
        error = error.divide(zp.abs(), 50, RoundingMode.HALF_UP);
        return error.doubleValue() < 1e-9;
    }

    private static boolean absolute(BigDecimal z, BigDecimal zp) {
        if(z.compareTo(BigDecimal.ZERO) == 0) {
            return zp.compareTo(BigDecimal.ZERO) == 0;
        }

        BigDecimal error = z.subtract(zp).abs();
        return error.doubleValue() < 1e-9;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        BigDecimal x = new BigDecimal(stdin.nextLine());
        BigDecimal y = new BigDecimal(stdin.nextLine());
        BigDecimal add = new BigDecimal(stdin.nextLine());
        BigDecimal sub = new BigDecimal(stdin.nextLine());
        BigDecimal mul = new BigDecimal(stdin.nextLine());
        BigDecimal div = new BigDecimal(stdin.nextLine());

        BigDecimal actual = x.add(y);
        if(relative(actual, add) && absolute(actual, add)) {
            System.out.println("Correct");
        } else {
            System.out.println("Incorrect");
        }

        actual = x.subtract(y);
        if(relative(actual, sub) && absolute(actual, sub)) {
            System.out.println("Correct");
        } else {
            System.out.println("Incorrect");
        }

        actual = x.multiply(y);
        if(relative(actual, mul) && absolute(actual, mul)) {
            System.out.println("Correct");
        } else {
            System.out.println("Incorrect");
        }

            actual = x.divide(y, 50, RoundingMode.HALF_UP);
            if(relative(actual, div) && absolute(actual, div)) {
                System.out.println("Correct");
            } else {
                System.out.println("Incorrect");
            }
    }
}