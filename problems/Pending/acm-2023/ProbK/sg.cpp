#include <cmath>
#include <cstdlib>
#include <iostream>

using namespace std;


int main() {
    long double x, y, add, sub, mul, div;
    cin >> x >> y >> add >> sub >> mul >> div;

    if(abs( (x + y) - add) < 1e-9) {
        cout << "Correct" << endl;
    } else {
        cout << "Incorrect" << endl;
    }

    if(abs( (x - y) - sub) < 1e-9) {
        cout << "Correct" << endl;
    } else {
        cout << "Incorrect" << endl;
    }

    if(abs( (x * y) - mul) < 1e-9) {
        cout << "Correct" << endl;
    } else {
        cout << "Incorrect" << endl;
    }

    if(abs( (x / y) - div) < 1e-9) {
        cout << "Correct" << endl;
    } else {
        cout << "Incorrect" << endl;
    }
}