x = float(input())
y = float(input())

add = float(input())
if abs((x + y) - add) < 1e-9:
    print('Correct')
else:
    print('Incorrect')

sub = float(input())
if abs((x - y) - sub) < 1e-9:
    print('Correct')
else:
    print('Incorrect')

mul = float(input())
if abs((x * y) - mul) < 1e-9:
    print('Correct')
else:
    print('Incorrect')

div = float(input())
if abs((x / y) - div) < 1e-9:
    print('Correct')
else:
    print('Incorrect')