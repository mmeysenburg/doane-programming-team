vals = [int(x) for x in input().split()]
expr = [c for c in input()]
moves = [input().split() for i in range(vals[1])]
for i in range(0, len(expr), 2):
    expr[i] = int(expr[i])

def makeMove(expr, move):
    if move[0] == 's':
        i = 2 * (int(move[1]) - 1)
        j = 2 * (int(move[2]) - 1)
        if i != j:
            t = expr[i]
            expr[i] = expr[j]
            expr[j] = t
    elif move[0] == 'f':
        i = 2 * int(move[1]) - 1
        if expr[i] == '+':
            expr[i] = '*'
        elif expr[i] == '*': 
            expr[i] = '+'
    elif move[0] == 'a':
        for i in range(1, len(expr), 2):
            if expr[i] == '+':
                expr[i] = '*'
            elif expr[i] == '*':
                expr[i] = '+'

def evalExpr(expr):
    tExpr = expr[:]
    for i in range(1, len(tExpr), 2):
        if tExpr[i] == '*':
            tExpr[i + 1] = (tExpr[i + 1] * tExpr[i - 1]) % 1000000007
            tExpr[i - 1] = 0
            tExpr[i] = ' '
    return sum([x for x in tExpr if not isinstance(x, str)])
    
val = evalExpr(expr) % 1000000007
print(val)
for move in moves:
    makeMove(expr, move)
    val = evalExpr(expr) % 1000000007
    print(val)