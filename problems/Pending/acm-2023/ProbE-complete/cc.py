times = [int(x) for x in input().split(':')]
h = times[0]
m = times[1]
N = int(input())

tts = 0
if m == 0:
    tts += h
elif m == 15 or m == 30 or m == 45:
    tts += 1

if tts >= N:
    print('{0:02d}:{1:02d}'.format(h, m))
    exit()

if m % 15 != 0:
    if m < 15:
        m = 15
        tts += 1
    elif m < 30:
        m = 30
        tts += 1
    elif m < 45:
        m = 45
        tts += 1
    else:
        m = 0
        h += 1
        if h > 12:
            h = 1
        tts += h
    

while tts < N:
    # print('{0:02d}:{1:02d}'.format(h, m))
    m += 15
    if m == 60:
        h += 1
        if h > 12:
            h = 1
        tts += h
        m = 0
    else:
        tts += 1

print('{0:02d}:{1:02d}'.format(h, m))