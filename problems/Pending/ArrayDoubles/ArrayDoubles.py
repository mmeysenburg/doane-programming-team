import random

def makeArray():
    arr = [ ]
    for i in range(100):
        x = random.randrange(1, 1000)
        while x in arr:
            x = random.randrange(1, 1000)
            
        arr.append(x)
        arr.append(x)
    arr.append(0)
    random.shuffle(arr)
    return arr

def findNonTwo(arr):
    # put values into a dictionary
    dict = {}
    for i in arr:
        dict[i] = dict.get(i, 0) + 1
    
    # find the one that's not two
    for i in dict.keys():
        if dict[i] != 2:
            print(i)
            break


arr = makeArray()
findNonTwo(arr)