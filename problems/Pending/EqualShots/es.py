a, b = [int(x) for x in input().split()]
alc1 = 0
for i in range(a):
    v, c = [int(x) for x in input().split()]
    alc1 += v * c
alc2 = 0
for i in range(b):
    v, c = [int(x) for x in input().split()]
    alc2 += v * c
if alc1 == alc2:
    print('same')
else:
    print('different')