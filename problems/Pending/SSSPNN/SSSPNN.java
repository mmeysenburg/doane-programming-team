import java.util.*;

public class SSSPNN {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        int n = Integer.parseInt(tokens[0]);
        int m = Integer.parseInt(tokens[1]);
        int q = Integer.parseInt(tokens[2]);
        int s = Integer.parseInt(tokens[3]);

        int[][] graph = new int[n][n];

        for(int i = 0; i < n; i++)  {
            for(int j = 0; j < n; j++) {
                graph[i][j] = Integer.MAX_VALUE;
            }
        }

        for(int i = 0; i < m; i++) {
            tokens = stdin.nextLine().split(" ");
            int u = Integer.parseInt(tokens[0]);
            int v = Integer.parseInt(tokens[1]);
            int w = Integer.parseInt(tokens[2]);

            graph[u][v] = w;
        }

        for(int i = 0; i < q; i++) {
            int dest = Integer.parseInt(stdin.nextLine());

            System.out.println("0");
        }

        System.out.println();
    }
}