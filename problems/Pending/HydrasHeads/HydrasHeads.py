h, t = list(map(int, input().split()))
while h != 0 or t != 0:
    moves = 0
    if t > 4:
        if t % 4 > 0:
            new_t = t % 4
            t_diff = (t - new_t)/2
            h += t_diff
            moves += t_diff
            t = new_t
        else:
            temp = 2 * (t // 4 - 1)
            moves += temp
            h += temp
            t = 4
    if h > 1:
        new_h = h % 2
        moves += (h - new_h) / 2
        h = new_h
    if h == 0:
        if t > 0:
            moves += 7 - t
        t = 0
    else:
        if 0 < t < 5:
            if t == 1:
                moves += 3
            elif t == 2:
                moves += 2
            elif t == 3:
                moves += 8
            else:
                moves += 7
            t = 0
            h = 0
    if h == 0 and t == 0:
        print(int(moves))
    else:
        print("-1")
    h, t = list(map(int, input().split()))