import java.util.*;

public class HydrasHeads {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] input = stdin.nextLine().split(" ");
        int h = Integer.parseInt(input[0]);
        int t = Integer.parseInt(input[1]);
        while(!(h == 0 && t == 0)) {
            int moves = 0;

            if (t > 4) {
                if (t % 4 > 0) {
                    int newT = t % 4;
                    int diffT = (t - newT) / 2;
                    h += diffT;
                    moves += diffT;
                    t = newT;
                } else {
                    int temp = 2 * (t / 4 - 1);
                    moves += temp;
                    h += temp;
                    t = 4;
                }
            }

            if (h > 1) {
                int newH = h % 2;
                moves += (h - newH) / 2;
                h = newH;
            }

            if (h == 0) {
                if (t > 0) {
                    moves += 7 - t;
                }
                t = 0;
            } else {
                if (t > 0 && t < 5) {
                    switch (t) {
                        case 1:
                            moves += 3;
                            break;
                        case 2:
                            moves += 2;
                            break;
                        case 3:
                            moves += 8;
                            break;
                        default:
                            moves += 7;
                            break;
                    }
                    t = h = 0;
                }
            }

            if (h == 0 && t == 0) {
                System.out.println(moves);
            } else {
                System.out.println("-1");
            }
            
            input = stdin.nextLine().split(" ");
            h = Integer.parseInt(input[0]);
            t = Integer.parseInt(input[1]);
        }
    }
}