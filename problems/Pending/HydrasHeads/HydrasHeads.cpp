#include<cstdlib>
#include<iostream>

    int main() {
        using namespace std;
        int h, t;
        cin >> h >> t;
        while(!(h == 0 && t == 0)) {
            int moves = 0;

            if (t > 4) {
                if (t % 4 > 0) {
                    int newT = t % 4;
                    int diffT = (t - newT) / 2;
                    h += diffT;
                    moves += diffT;
                    t = newT;
                } else {
                    int temp = 2 * (t / 4 - 1);
                    moves += temp;
                    h += temp;
                    t = 4;
                }
            }

            if (h > 1) {
                int newH = h % 2;
                moves += (h - newH) / 2;
                h = newH;
            }

            if (h == 0) {
                if (t > 0) {
                    moves += 7 - t;
                }
                t = 0;
            } else {
                if (t > 0 && t < 5) {
                    switch (t) {
                        case 1:
                            moves += 3;
                            break;
                        case 2:
                            moves += 2;
                            break;
                        case 3:
                            moves += 8;
                            break;
                        default:
                            moves += 7;
                            break;
                    }
                    t = h = 0;
                }
            }

            if (h == 0 && t == 0) {
               cout << moves << endl;
            } else {
                cout << "-1" << endl;;
            }
            
            cin >> h >> t;
        }

        return EXIT_SUCCESS;
    }