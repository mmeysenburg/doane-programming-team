#include <cstdlib>
#include <iostream>
#include <map>

int main() {
    using namespace std;

    int N;
    cin >> N;

    int* data = new int[N];
    for(int i = 0; i < N; i++) {
        cin >> data[i];
    }

    map<int, int> myMap;

    for(int i = 0; i < N; i++) {
        for(int j = 0; j < N; j++) {
            if(i != j) {
                int sum = data[i] + data[j];
                if(myMap.find(sum) == myMap.end()) {
                    myMap[sum] = 1;
                } else {
                    int count = myMap[sum] + 1;
                    myMap[sum] = count;
                }
            }
        }
    }

    // for(auto it = myMap.begin(); it != myMap.end(); ++it) {
    //     cout << it->first << " " << it->second << endl;
    // }

    int ways = 0;
    for(int i = 0; i < N; i++) {
        auto it = myMap.find(data[i]);
        if(it != myMap.end()) {
            ways += it->second;
        }
    }

    cout << ways << endl;

    delete [] data;

    return EXIT_SUCCESS;
}