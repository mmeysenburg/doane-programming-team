import java.io.*;
import java.util.*;

public class MultiplicationGame {

    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(
            new InputStreamReader(System.in));

        long n;
        String line;
        
        while ((line = stdin.readLine()) != null) {
            n = Long.parseLong(line);
			boolean flag = true;
			long num = 3;
			while(num < n) {
				if(flag) {
					num *= 2;
				} else {
					num *= 9;
				}
				flag = !flag;
			}
			if(flag) {
				System.out.println("Ollie wins");
			} else {
				System.out.println("Stan wins");
			}
        }
    }
}