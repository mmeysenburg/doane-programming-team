import java.util.*;

/**
 * Solution to Problem D Some Sum from the 2019 ACM Programming Contest.
 * This uses a switch statement to account for all 10 possible inputs.
 * 
 * @author Mark M. Meysenburg
 * @version 11/12/2019
 */
public class SomeSum {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int N = stdin.nextInt();
        switch (N) {
            case 1:
            case 3:
            case 5:
            case 7:
            case 9: 
                System.out.println("Either");
                break;
            case 2:
            case 6:
            case 10: 
                System.out.println("Odd");
                break;
            case 4:
            case 8:
                System.out.println("Even");
                break;
            default:
                break;
        }
    }
}