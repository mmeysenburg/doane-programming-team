import sys

all_inp = [[int(x) for x in line.split()] for line in sys.stdin.readlines()[1:]]
l_max = max([v[0] for v in all_inp])
r_min = min([v[1] for v in all_inp])
if l_max <= r_min:
    print(r_min - l_max + 1, l_max)
else:
    print('bad news')