import sys

all_inp = [ [int(x) for x in line.split()] for line in sys.stdin.readlines()[1:]]
max_left = max([c[0] for c in all_inp])
min_right = min([c[1] for c in all_inp])
if max_left <= min_right:
    print(min_right - max_left + 1, max_left)
else:
    print('bad news')