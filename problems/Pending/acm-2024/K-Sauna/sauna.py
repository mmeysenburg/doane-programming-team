import sys

inp = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]
mins = []
maxes = []
for pair in inp[1:]:
    mins.append(pair[0])
    maxes.append(pair[1])

all_min = max(mins)
all_max = min(maxes)

if all_min <= all_max:
    delta = all_max - all_min + 1
    print(delta, all_min)
else:
    print('bad news')