﻿using System;

int n = Convert.ToInt32(Console.ReadLine());
int highestLow = Int32.MinValue;
int lowestHigh = Int32.MaxValue;

for (int i = 0; i < n; i++)
{
    string[] tkns = Console.ReadLine().Split(" ");
    int low = Convert.ToInt32(tkns[0]);
    int high = Convert.ToInt32(tkns[1]);

    highestLow = low > highestLow ? low : highestLow;
    lowestHigh = high < lowestHigh ? high : lowestHigh;
}

if (lowestHigh < highestLow)
{
    Console.WriteLine("bad news");
} else
{
    int delta = lowestHigh - highestLow + 1;
    Console.WriteLine(delta.ToString() + " " + highestLow.ToString());
}
