import math

N, D, L = [int(x) for x in input().split()]
n_i = [int(x) for x in input().split()]

depth = int(math.log(N) / math.log(2))

T = depth * D + L + max(n_i)

print(T)