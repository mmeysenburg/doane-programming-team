def countDivisibles(A, B, M):
    # Add 1 explicitly as A is divisible by M
    if (A % M == 0):
        return ((B // M) - (A // M)) + 1
 
    # A is not divisible by M
    return ((B // M) - (A // M))

nums = [int(x) for x in input().split()]
if nums[2] == nums[3]:
    nums[2] = 1
elif nums[2] < nums[3]:
    if nums[2] != 1 and nums[3] % nums[2] == 0:
        nums[2] = 1
else:
    if nums[3] != 1 and nums[2] % nums[3] == 0:
        nums[3] = 1
print(countDivisibles(nums[0], nums[1], nums[2] * nums[3]))