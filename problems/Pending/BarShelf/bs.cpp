#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <list>

int main() {
	using namespace std;

	int n;
	int* bottles;
	int* a;
	list<int> b;
	scanf("%d", &n);
	bottles = new int[n];
	a = new int[n];
	for (int i = 0; i < n; i++) {
		scanf("%d", (bottles + i));
		a[i] = 0;
	}

	for (int i = 0; i < n - 1; i++) {
		for (int j = i + 1; j < n; j++) {
			if (bottles[j] * 2 <= bottles[i]) {
				a[i]++;
				b.push_back(j + i + 1);
			}
		}
	}

	long count = 0L;
	for (int i : b) {
		printf("%d ", i);
		count += a[i];
	}

	printf("\n%ld\n", count);

	delete[] bottles;
	delete[] a;

	return EXIT_SUCCESS;
}