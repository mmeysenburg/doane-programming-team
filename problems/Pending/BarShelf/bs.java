import java.util.*;

public class bs {
    public static void main(String[] args) {
       Scanner stdin = new Scanner(System.in);
       int n;
       
       n = Integer.parseInt(stdin.nextLine());
       String[] tokens = stdin.nextLine().split(" ");
       int[] listt = new int[n + 1];
       long[] a = new long[n + 1];
       for(int i = 0; i < n; i++) {
           listt[i] = Integer.parseInt(tokens[i]);
           a[i] = 0;
       }

       List<List<Long>> b = new LinkedList<>();
       for(int i = 0; i < n - 1; i++) {
           List<Long> b_temp = new LinkedList<>();
           for(long j = i; j < n; j++) {
               if(listt[(int)j] * 2 <= listt[i]) {
                   a[i] ++;
                   b_temp.add(j);
               }
           }
           b.add(b_temp);
        }


        // System.out.println(b);

    long count = 0;
    Iterator<List<Long>> i = b.iterator();
    while(i.hasNext()) {
        List<Long> ii = i.next();
        for(long j : ii) {
            count += a[(int)j];
        }
    }

    System.out.println(count);
    }
}



/*
n = int(stdin.readline())

listt = [int(x) for x in stdin.readline().split()]
a = [0] * n
b = []

for i, left_elem in enumerate(listt[:-1]):
    #print(i)
    b_temp = []
    for j, right_elem in enumerate(listt[i+1:]):
        #print(i, j)
        if right_elem*2 <= left_elem:
            a[i]+=1
            #print(b)
            b_temp.append(j+i+1)
    b.append(b_temp)

count = 0

for i in b:
    for j in i:
        count+=a[j]

print(count)*/