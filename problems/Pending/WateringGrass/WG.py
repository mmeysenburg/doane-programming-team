import math
import sys

allLines = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]

caseIdx = 0
while caseIdx < len(allLines):
    # first line of each case
    n = allLines[caseIdx][0]
    l = allLines[caseIdx][1]
    w = allLines[caseIdx][2]

    caseIdx += 1

    # get sprinklers as list of (r, x) tuples
    sprinklers = []
    for i in range(n):
        sprinklers.append((allLines[caseIdx][1], allLines[caseIdx][0]))
        caseIdx += 1

    # go greedy - sort descending by radius
    sprinklers.sort(reverse=True)

    for sprinkler in sprinklers:
        x = sprinkler[1]
        r = sprinkler[0]
        # only look at sprinkers that touch the horizontal edges
        if r >= 0.5 * w:
            delta = math.sqrt(r * r - 0.25 * w * w)
            xMin = x - delta
            xMax = x + delta
            
