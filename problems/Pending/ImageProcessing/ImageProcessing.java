import java.io.*;
import java.util.*;

public class ImageProcessing {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer stk = new StringTokenizer(br.readLine());
        int H = Integer.parseInt(stk.nextToken());
        int W = Integer.parseInt(stk.nextToken());
        int N = Integer.parseInt(stk.nextToken());
        int M = Integer.parseInt(stk.nextToken());
        int[][] image = new int[H][W];
        int[][] kernel = new int[N][M];
        int OH = H - N + 1;
        int OW = W - M + 1;
        int[][] output = new int[OH][OW];
        int i, j, k, l;
        for(i = 0; i < H; i++) {
            stk = new StringTokenizer(br.readLine());
            for(j = 0; j < W; j++) {
                image[i][j] = Integer.parseInt(stk.nextToken());
            }
        }
        for(i = N - 1; i >= 0; i--) {
            stk = new StringTokenizer(br.readLine());
            for(j = M - 1; j >= 0; j--) {
                kernel[i][j] = Integer.parseInt(stk.nextToken());
            }
        }

        for(i = 0; i < OH; i++) {
            for(j = 0; j < OW; j++) {
                for(k = 0; k < N; k++) {
                    for(l = 0; l < M; l++) {
                        output[i][j] += image[i + k][j + l] * kernel[k][l];
                    }
                }
            }
        }

        for(i = 0; i < OH; i++) {
            for(j = 0; j < OW; j++) {
                System.out.printf("%d ", output[i][j]);
            }
            System.out.println();
        }

    }
}