doors = [0] * 101

# monkeys toggle doors
for m in range (1, 101):
    for d in range(m, 101, m):
        if doors[d] == 0:
            doors[d] = 1
        else:
            doors[d] = 0

# how many are open?
print(sum(doors))