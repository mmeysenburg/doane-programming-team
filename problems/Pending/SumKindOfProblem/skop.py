import sys
all_inp = [[int(x) for x in line.split()] for line in sys.stdin.readlines()[1:]]
for line in all_inp:
    all_sum = 0
    even_sum = 0
    odd_sum = 0
    even = 2
    odd = 1
    for i in range(1, line[1] + 1):
        all_sum += i
        even_sum += even
        odd_sum += odd
        even += 2
        odd += 2
    print(line[0], all_sum, odd_sum, even_sum)