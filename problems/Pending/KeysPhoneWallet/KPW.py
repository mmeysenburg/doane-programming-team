import sys
inp = sys.stdin.readlines()
items = {x.strip():1 for x in inp[1:]}
needed = ['keys', 'phone', 'wallet']
all_good = True
for i in needed:
    if i not in items:
        print(i)
        all_good = False
if all_good:
    print('ready')
