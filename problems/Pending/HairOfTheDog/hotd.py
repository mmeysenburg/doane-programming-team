import sys
days = [x.strip() for x in sys.stdin.readlines()[1:]]
last_day=days[0]
hangovers=0
for i in range(1, len(days)):
    if days[i] == 'sober' and last_day == 'drunk':
        hangovers+=1
    last_day = days[i]
print(hangovers)