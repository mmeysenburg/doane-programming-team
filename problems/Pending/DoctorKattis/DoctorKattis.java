import java.io.*;
import java.util.*;

public class DoctorKattis {
    public static void main(String[] args) throws Exception {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int N = Integer.parseInt(br.readLine());
        MaxHeap heap = new MaxHeap();
        for(int i = 0; i < N; i++) {
            String[] tokens = br.readLine().split(" ");
            switch(tokens[0]) {
                case "0":
                    heap.insert(tokens[1], Integer.parseInt(tokens[2]));
                    break;
                case "1":
                    heap.update(tokens[1], Integer.parseInt(tokens[2]));
                    break;
                case "2":
                    heap.remove(tokens[1]);
                    break;
                case "3":
                    System.out.println(heap.query());
                    break;
            }
            // System.out.println(heap);
        }
    }

    ///////////////////////////////////////////////////////////////////////////
    // Node class
    ///////////////////////////////////////////////////////////////////////////
    static class Node {
        public Node(String name, int key) {
            this.name = name;
            this.key = key;
        }

        String name;
        int key;

        // @Override
        // public String toString() {
        //     return "(" + name + ", " + key + ")";
        // }
    }

    ///////////////////////////////////////////////////////////////////////////
    // MaxHeap class
    ///////////////////////////////////////////////////////////////////////////
    static class MaxHeap {
        Node[] vec = new Node[200001];
        int end = 1;

        public MaxHeap() {

        }

        public void heapIncreaseKey(int i, int key) {
            vec[i].key = key;
            int p = i >> 1;
            while(i > 1 && vec[p].key < vec[i].key) {
                swap(i, p);
                i = p;
                p = i >> 1;
            }
        }

        public void insert(String name, int key) {
            vec[end++] = new Node(name, Integer.MIN_VALUE);
            heapIncreaseKey(end - 1, key);
        }

        // public int left(int i) {
        //     return i << 1;
        // }

        public void maxHeapify(int i) {
            int le = i << 1;
            int ri = le + 1;
            int largest = i;

            if(le < end && vec[le].key > vec[i].key) {
                largest = le;
            }
            if(ri < end && vec[ri].key > vec[largest].key) {
                largest = ri;
            }

            if(largest != i) {
                swap(i, largest);
                maxHeapify(largest);
            }
        }

        // public int parent(int i ) {
        //     return i >> 1;
        // }

        public String query() {
            if(end == 1) {
                return "The clinic is empty";
            } else {
                return vec[1].name;
            }
        }

        public void remove(String name) {
            int i = 1;
            while(!vec[i].name.equals(name)) {
                i++;
            }
            vec[i] = vec[end - 1];
            maxHeapify(i);
            end--;
        }

        // public int right(int i) {
        //     return (i << 1) + 1;
        // }

        public void swap(int i, int j) {
            Node t = vec[i];
            vec[i] = vec[j];
            vec[j] = t;
        }

        // @Override
        // public String toString() {
        //     String s = "[";
        //     for(int i = 1; i < end; i++) {
        //         s += vec[i].toString() + " ";
        //     }
        //     return s + "]";
        // }

        public void update(String name, int key) {
            int i = 1;
            while(!vec[i].name.equals(name)) {
                i++;
            }
            heapIncreaseKey(i, vec[i].key + key);
        }
    }
}