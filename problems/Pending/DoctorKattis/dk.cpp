#include <climits>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

class Node {
public:
    Node(string n = "", int a = 0, int k = 0) : name(n), arr(a), key(k) { }

    bool operator<(const Node &o) const {
        if(key != o.key) {
            return key < o.key;
        } else {
            return arr < o.arr;
        }
    }

    string name;
    int arr;
    int key;
};

class MaxHeap {
public:
    MaxHeap() { 
        end = 1; 
        pVec = new Node*[200001];
    }

    ~MaxHeap() {
        for(int i = 0; i < end; i++) {
            delete pVec[i];
        }
        delete [] pVec;
    }

    void heapIncreaseKey(int i, int key) {
        pVec[i]->key = key;
        int p = i >> 1;
        while(i > 1 && *(pVec[p]) < *(pVec[i])) {
           swap(i, p);
           i = p;
           p = i >> 1;
        }
    }

    void insert(string name, int arr, int key) {
        pVec[end++] = new Node(name, arr, INT_MIN);
        heapIncreaseKey(end - 1, key);
    }

    void maxHeapify(int i) {
        int le = i << 1;
        int ri = le + 1;
        int largest = i;

        if(le < end && *(pVec[i]) < *(pVec[le])) {
            largest = le;
        }
        if(ri < end && *(pVec[largest]) < *(pVec[ri])) {
            largest = ri;
        }

        if(largest != i) {
            swap(i, largest);
            maxHeapify(largest);
        }
    }

    string query() {
        if(end == 1) {
            return "The clinic is empty";
        } else {
            return pVec[1]->name;
        }
    }

    void remove(string name) {
        int i = 1;
        while(pVec[i]->name != name) {
            i++;
        }
        pVec[i] = pVec[end - 1];
        maxHeapify(i);
        end--;
    }

    void swap(int i, int j) {
        Node *t = pVec[i];
        pVec[i] = pVec[j];
        pVec[j] = t;
    }

    void update(string name, int key) {
        int i = 1;
        while(pVec[i]->name != name) {
                i++;
        }
        heapIncreaseKey(i, pVec[i]->key + key);
    }

    Node **pVec;
    int end;
};

int main() {
    int N;
    cin >> N;

    MaxHeap heap;

    for(int i = 0; i < N; i++) {
        int cmd, iLvl, arr = 0;
        string name;
        cin >> cmd;
        switch(cmd) {
            case 0:
                cin >> name >> iLvl;
                heap.insert(name, arr++, iLvl);
                break;
            case 1:
                cin >> name >> iLvl;
                heap.update(name, iLvl);
                break;
            case 2:
                cin >> name;
                heap.remove(name);
                break;
            case 3:
                cout << heap.query() << endl;
                break;
        }
    }

    return EXIT_SUCCESS;
}