import java.util.*;

public class ErdosNumbers {
    public static void main(String[] args) {
        // get input as a list of string lines
        List<String> lines = new LinkedList<>();
        Scanner stdin = new Scanner(System.in);
        while(stdin.hasNextLine()) {
            lines.add(stdin.nextLine());
        }

        // build a map holding all all authors, w/ indices for each,
        // and list of output authors
        Map<String, Integer> authorMap = new TreeMap<>();
        List<String> outputAuthors = new LinkedList<>();
        int idx = 0;
        for(String line : lines) {
            String[] tokens = line.split(" ");
            outputAuthors.add(tokens[0]);
            for(String name : tokens) {
                if(!authorMap.containsKey(name)) {
                    authorMap.put(name, idx++);
                }
            }
        }
        // turn the map into an array of index / names 
        String[] allAuthors = new String[authorMap.size()];
        int erdosIdx = -1;
        Set<String> authorKeys = authorMap.keySet();
        for(String name : authorKeys) {
            allAuthors[authorMap.get(name)] = name;
            if(name.equals("PAUL_ERDOS")) {
                erdosIdx = authorMap.get(name);
            }
        }

        // build adjacency matrix of authorship
        int[][] graph = new int[allAuthors.length][allAuthors.length];
        for(String line : lines) {
            String[] tokens = line.split(" ");
            int row = authorMap.get(tokens[0]);
            for(int i = 1; i < tokens.length; i++) {
                int col = authorMap.get(tokens[i]);
                graph[row][col]++;
                graph[col][row]++;
            }
        }

        System.out.println(outputAuthors);
        for(int i = 0; i < allAuthors.length; i++) {
            System.out.println(Arrays.toString(graph[i]));
        }
    }
}