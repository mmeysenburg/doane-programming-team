import java.util.*;

public class EN {
    public static void main(String[] args) {
        // read all the lines as arrays of strings
        Scanner stdin = new Scanner(System.in);
        List<String[]> lines = new LinkedList<>();
        while(stdin.hasNextLine()) {
            lines.add(stdin.nextLine().split(" "));
        }
        
        // get all distinct authors and output authors
        LinkedList<String> outputNames = new LinkedList<>();
        Set<String> authorNames = new TreeSet<>();
        for(String[] line : lines) {
            outputNames.add(line[0]);
            for(String name : line) {
                authorNames.add(name);
            }
        }

        // map authors to indices
        Map<String, Integer> authorIndices = new TreeMap<>();
        int idx = 0;
        for(String name : authorNames) {
            authorIndices.put(name, idx++);
        }
        int erdosIdx = authorIndices.get("PAUL_ERDOS");

        // build adjacency matrix
        int numAuthors = authorIndices.size();
        int[][] matrix = new int[numAuthors][numAuthors];
        for(String[] line : lines) {
            int rowIdx = authorIndices.get(line[0]);
            for(int i = 1; i < line.length; i++) {
                int colIdx = authorIndices.get(line[i]);
                matrix[rowIdx][colIdx] = 1;
                matrix[colIdx][rowIdx] = 1; 
            }
        }

        // find and output Erdos numbers
        for(String name : outputNames) {
            if(name.equals("PAUL_ERDOS")) {
                System.out.println("PAUL_ERDOS 0");
            } else {
                System.out.printf("%s %d\n", name, 1);
            }
        }

    }
}