import sys

allNames = [line.split() for line in sys.stdin.readlines()]

collaborators = { name : { } for line in allNames for name in line}
erdosNumbers = { name : [sys.maxsize] for line in allNames for name in line}

erdosNumbers['PAUL_ERDOS'].append(0)

for names in allNames:
    for name in names[1:]:
        collaborators[names[0]][name] = 0
        collaborators[name][names[0]] = 0

doneEm = ['PAUL_ERDOS']

def makeNumbers(colabs, number):
    for name in colabs:
        erdosNumbers[name].append(number)
        if name not in doneEm:
            doneEm.append(name)
            makeNumbers(collaborators[name], number + 1)
            doneEm.pop(-1)

makeNumbers(collaborators['PAUL_ERDOS'], 1)

for names in allNames:
    nums = erdosNumbers[names[0]]
    if len(nums) == 1:
        print(names[0], 'no-connection')
    else:
        print(names[0], min(nums))