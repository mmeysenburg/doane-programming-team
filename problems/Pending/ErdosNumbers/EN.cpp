#include <cstdlib>
#include <iostream>
#include <map>
#include <set>
#include <string>
#include <vector>

using namespace std;

int main() {
    string line;
    vector<vector<string>> allNames;
    map<string, set<string>> colaborators;

    int idx = 0;
    while(getline(cin, line)) {
        allNames.push_back(vector<string>());
        int lIdx = 0;
        int rIdx = line.find(' ', 0);
        while(rIdx != string::npos) {
            allNames[idx].push_back(line.substr(lIdx, rIdx));
            lIdx = rIdx + 1;
            rIdx = line.find(' ', lIdx);
        }
        idx++;
    }

    for(vector<string> v : allNames) {
        for(string s : v) {
            cout << s << " ";
        }
        cout << endl;
    }

    return EXIT_SUCCESS;
}