import sys

lines = sys.stdin.readlines()
sum = 0.0
for line in lines[1:]:
    tokens = line.split()
    sum += float(tokens[0]) * float(tokens[1])
print('{0:.3f}'.format(sum))