import java.util.*;

public class StringRearrangment {

    static Map<Character, Integer> frequencies = new HashMap<>();

    static class Comp implements Comparator<Character> {
        public int compare(Character o1, Character o2) {
            int f1 = frequencies.get(o1);
            int f2 = frequencies.get(o2);

            if (f1 == f2) {
                return (int)(o2.charValue() - o1.charValue());
            } else {
                return f2 - f1;
            }
        }
    }
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        Comp comp = new Comp();
        int n = stdin.nextInt();
        stdin.nextLine();
        for (int i = 0; i < n; i++) {
            String word = stdin.nextLine();
            frequencies.clear();

            // count frequencies
            Character[] cword = new Character[word.length()];
            for (int j = 0; j < word.length(); j++) {
                char c = word.charAt(j);
                cword[j] = new Character(c);
                if (frequencies.containsKey(c)) {
                    int count = frequencies.get(c);
                    count++;
                    frequencies.put(c, count);
                } else {
                    frequencies.put(c, 1);
                }
            }

            
            Arrays.sort(cword, comp);
            String nword = "";
            for (int j = 0; j < cword.length; j++) {
                nword += cword[j].charValue();
            }

            System.out.println("The string \"" + word +
                "\" rearranges to \"" + nword +
                "\"");
        }
    }
}