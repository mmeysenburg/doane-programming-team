import java.util.*;

public class SkylineSort {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();
        Integer[] arr = new Integer[n];
        for (int i = 0; i < n; i++) {
            arr[i] = stdin.nextInt();
        }

        int midx = -1;
        int max = Integer.MIN_VALUE;
        for(int i = 0; i < n; i++) {
            if(arr[i] > max) {
                max = arr[i];
                midx = i;
            }
        }

        Arrays.sort(arr, 0, midx, (x, y) -> Integer.compare(x, y));
        Arrays.sort(arr, midx + 1, arr.length, (x, y) -> -Integer.compare(x, y));

        System.out.println(n);
        for (int i = 0; i < n; i++) {
            System.out.println(arr[i]);
        }
    }
}