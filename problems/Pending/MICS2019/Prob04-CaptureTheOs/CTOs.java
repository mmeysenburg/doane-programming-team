import java.util.*;

/**
 * Incomplete - only reads in the input
 */
public class CTOs {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int R = stdin.nextInt(); int C = stdin.nextInt(); int D = stdin.nextInt();
        stdin.nextLine();

        char[][][] board = new char[D][R][C];

        for (int k = 0; k < D; k++) {
            for (int j = 0; j < R; j++) {
                String line = stdin.nextLine();
                for (int i = 0; i < C; i++) {
                    board[k][j][i] = line.charAt(i);
                }
            }
            stdin.nextLine();
        }
    }
}