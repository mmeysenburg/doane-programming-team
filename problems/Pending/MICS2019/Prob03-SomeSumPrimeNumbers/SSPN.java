import java.util.*;

public class SSPN {
    private static long sumOfDigits(long x) {
        long sum = 0L;

        while (x > 0L) {
            sum += x % 10L;
            x /= 10L;
        }

        return sum;
    }
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int N = Integer.parseInt(stdin.nextLine());
        for (int i = 0; i < N; i++) {
            long x = Long.parseLong(stdin.nextLine());
            long saveX = x;
            while (x > 9L) {
                x = sumOfDigits(x);
            }
            if (x == 2L || x == 3L || x == 5L || x == 7L) {
                System.out.println(saveX + " IS SUM-PRIME");
            } else {
                System.out.println(saveX + " IS NOT SUM-PRIME");
            }
        }
    }
}