import java.util.*;

/**
 * Incomplete -- just reads in the input
 */
public class MSL {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int K = stdin.nextInt(); int Q = stdin.nextInt();
        stdin.nextLine();

        String[] keys = new String[K];
        for (int k = 0; k < K; k++) {
            keys[k] = stdin.nextLine();
        }   

        for (int q = 0; q < Q; q++) {
            String[] vals = stdin.nextLine().split(" ");
            System.out.println(vals[0] + " " + vals[1]);
        }
    }
}