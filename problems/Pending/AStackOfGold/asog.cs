using System;

string[] tkns = Console.ReadLine().Split(" ");
int w = Convert.ToInt32(tkns[0]);
int s = Convert.ToInt32(tkns[1]);

int c = (s * (s + 1)) / 2;

for(int i = 1; i <= s; i++)
{
    if((i * 29370 + (c - i) * 29260) == w)
    {
        Console.WriteLine(i);
        break;
    }
}