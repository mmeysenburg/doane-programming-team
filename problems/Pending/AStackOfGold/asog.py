w, s = [int(x) for x in input().split()]
w_W = 29260
w_Au = 29370
c = (s * (s + 1)) // 2
for i in range(1, s+1):
    if (i * w_Au + (c - i) * w_W) == w:
        print(i)
        break