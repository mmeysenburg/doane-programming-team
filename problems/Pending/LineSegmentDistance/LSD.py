import math

def pToPDistance(x1, y1, x2, y2):
    return math.sqrt((x1 - x2) ** 2 + (y1 - y2) ** 2)

def pToLineSegDistance(x1, y1, x2, y2, x3, y3):
    u = (x3 - x1) * (x2 - x1) + (y3 - y1) * (y2 - y1)
    denom = pToPDistance(x1, y1, x2, y2) ** 2

    if denom == 0:
        return 0

    u /= denom 

    x = x1 + u * (x2 - x1)
    y = y1 + u * (y2 - y1)

    return pToPDistance(x, y, x3, y3)

def onSegment(px, py, qx, qy, rx, ry):
    return qx <= max(px, rx) and qx >= min(px, rx) and qy <= max(py, ry) and qy >= min(py, ry)

def orientation(px, py, qx, qy, rx, ry):
    val = float(qy - py) * (rx - qx) - float(qx - px) * (ry - qy)
    if val > 0:
        return 1
    elif val < 0:
        return 2
    else:
        return 0

def doIntersect(p1x, p1y, q1x, q1y, p2x, p2y, q2x, q2y):
    o1 = orientation(p1x, p1y, q1x, q1y, p2x, p2y)
    o2 = orientation(p1x, p1y, q1x, q1y, q2x, q2y)
    o3 = orientation(p2x, p2y, q2x, q2y, p1x, p1y)
    o4 = orientation(p2x, p2y, q2x, q2y, q1x, q1y)

    if o1 != o2 and o3 != o4:
        return True
    
    if o1 == 0 and onSegment(p1x, p1y, p2x, p2y, q1x, q1y):
        return True
    
    if o2 == 0 and onSegment(p1x, p1y, q2x, q2y, q1x, q1y):
        return True
    
    if o3 == 0 and onSegment(p2x, p2y, p1x, p1y, q2x, q2y):
        return True

    if o4 == 0 and onSegment(p2x, p2y, q1x, q1y, q2x, q2y):
        return True

    return False


n = int(input())

for case in range(n):
    tokens = input().split()
    x1 = float(tokens[0])
    y1 = float(tokens[1])
    x2 = float(tokens[2])
    y2 = float(tokens[3])
    x3 = float(tokens[4])
    y3 = float(tokens[5])
    x4 = float(tokens[6])
    y4 = float(tokens[7])

    # are the 'line segments' really points?
    if x1 == x2 and y1 == y2 and x3 == x4 and y3 == y4:
        distance = pToPDistance(x1, y1, x3, y3)
    # one point, one segment?
    elif x1 == x2 and y1 == y2:
        distance = pToLineSegDistance(x3, y3, x4, y4, x1, y1)
    elif x3 == x4 and y3 == y4:
        distance = pToLineSegDistance(x1, y1, x2, y2, x3, y3)
    # lines intersect?
    elif doIntersect(x1, y1, x2, y2, x3, y3, x4, y4):
        distance = 0
    else:
        d1 = pToLineSegDistance(x3, y3, x4, y4, x1, y1)
        d2 = pToLineSegDistance(x3, y3, x4, y4, x2, y2)
        d3 = pToLineSegDistance(x1, y1, x2, y2, x3, y3)
        d4 = pToLineSegDistance(x1, y1, x2, y2, x4, y4)

        distance = min(d1, d2, d3, d4)

    print('{0:0.2f}'.format(distance))