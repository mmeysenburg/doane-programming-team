'''
Python solution to EbAltoSaxaphonePlayer Kattis problem. 
'''
# dictionary holding fingers for each note
fingers = {'c': [2, 3, 4, 7, 8, 9, 10],
        'd':[2, 3, 4, 7, 8, 9],
        'e':[2, 3, 4, 7, 8],
        'f':[2, 3, 4, 7],
        'g':[2, 3, 4],
        'a':[2, 3],
        'b':[2],
        'C':[3],
        'D':[1, 2, 3, 4, 7, 8, 9],
        'E':[1, 2, 3, 4, 7, 8],
        'F':[1, 2, 3, 4, 7],
        'G':[1, 2, 3, 4],
        'A':[1, 2, 3],
        'B':[1, 2]}

t = int(input())

for c in range(t):
    # enclose 'normal' process in a try / except, to handle the case
    # when we have an empty song
    try:
        line = input()
        nums = [0] * 11
    
        # count first note
        for finger in fingers[line[0]]:
            nums[finger] += 1
        # count the remaining notes
        for i in range(1, len(line)):
            # look at each finger for the current note
            for finger in fingers[line[i]]:
                # if the finger was not in the *previous* note, 
                # accumulate it
                if finger not in fingers[line[i - 1]]:
                    nums[finger] += 1
    
        # output finger counts
        print('{0:d} {1:d} {2:d} {3:d} {4:d} {5:d} {6:d} {7:d} {8:d} {9:d}'.format(
            nums[1], nums[2], nums[3], nums[4], nums[5], nums[6],
            nums[7], nums[8], nums[9], nums[10]
        ))   
    except:
        # output all 0s in the empty song case. 
        print('0 0 0 0 0 0 0 0 0 0')