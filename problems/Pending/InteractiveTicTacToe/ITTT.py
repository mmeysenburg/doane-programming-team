import math

def read_board():
    board = []
    line = input()
    board.append([c for c in line])
    line = input()
    board.append([c for c in line])
    line = input()
    board.append([c for c in line])

    return board

def write_board(board):
    for line in board:
        print(''.join(line))

def get_moves(board):
    moves = []
    for r in range(3):
        for c in range(3):
            if board[r][c] == '.':
                moves.append((r, c))
    return moves

def make_move(board, mark, pos):
    board[pos[0]][pos[1]] = mark

def undo_move(board, pos):
    board[pos[0]][pos[1]] = '.'

def column_winner(b, c, m):
    return b[0][c] == m and b[1][c] == m and b[2][c] == m

def row_winner(b, r, m):
    return b[r][0] == m and b[r][1] == m and b[r][2] == m

def diag1_winner(b, m):
    return b[0][0] == m and b[1][1] == m and b[2][2] == m

def diag2_winner(b, m):
    return b[0][2] == m and b[1][1] == m and b[2][0] == m

def winner(board, mark):
    won = True
    for r in range(3):
        won = won and row_winner(board, r, mark)
    if won:
        return mark
    
    won = True
    for c in range(3):
        won = won and column_winner(board, c, mark)
    if won:
        return mark
    
    if diag1_winner(board, mark) or diag2_winner(board, mark):
        return mark

def get_state(board):
    # do we have a winner?
    if winner(board, 'x'):
        return 'x'
    if winner(board, 'o'):
        return 'o'
    
    # do we have a draw?
    mark_sum = 0
    for line in board:
        for c in line:
            if c == 'x' or c == 'o':
                mark_sum += 1
    if mark_sum == 9:
        return 'DRAW'
    else:
        return '.'
    
def minimax(is_max_turn, max_mark, board):
    state = get_state(board)
    if state == 'DRAW':
        return 0
    elif state == 'x' or state == 'o':
        return 1 if state is max_mark else -1
    
    scores = []
    for move in get_moves(board):
        make_move(board, move)
        scores.append(minimax(not is_max_turn, max_mark, board))
        undo_move(board, move)

    return max(scores) if is_max_turn else min(scores)

def make_best_move(board):
    best_score = -math.inf
    best_move = None
    for move in get_moves(board):
        make_move(board, move)

turn = input()
board = read_board()
print(get_moves(board))