import math

numCases = int(input())

def distance(p1, p2):
    return math.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)

for i in range(numCases):
    input() # eat blank line
    n = int(input())
    graph = []
    for j in range(n):
        graph.append([0] * n)
    
    points = []
    for j in range(n):
        points.append([float(x) for x in input().split()])

    for r in range(n):
        for c in range(n):
            if r != c and graph[r][c] == 0:
                graph[r][c] = distance(points[r], points[c])

