#include <cstdlib>
#include <iostream>

using namespace std;

class RC {
public:
    int r;
    int c;

    RC(int ir, int ic) : r(ir), c(ic) { }

    
};

int main() {

    int N, M, Q, i, r, c;
    char space;

    cin >> N;
    cin >> M;

    for(r = 1; r <= N; r++) {
        for(c = 1; c <= M; c++) {
            cin >> space;
            cout << space;
        }
        cout << endl;
    }

    cin >> Q;
    for(i = 0; i < Q; i++) {
        cin >> r >> c;
        cout << r << " " << c << endl;
    }

    return EXIT_SUCCESS;
}