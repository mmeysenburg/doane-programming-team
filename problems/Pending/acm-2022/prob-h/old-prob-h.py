dim = [int(x) for x in input().split()]
n = dim[0]
B = [0, 0]
C = [dim[1], 0]

mem = {}

def area(x1, y1, x2, y2, x3, y3):
    if (x1, y1, x2, y2, x3, y3) in mem:
        return mem[(x1, y1, x2, y2, x3, y3)]
    
    mem[(x1, y1, x2, y2, x3, y3)] = abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0)
    return mem[(x1, y1, x2, y2, x3, y3)]

# A function to check whether point P(x, y)
# lies inside the triangle formed by
# A(x1, y1), B(x2, y2) and C(x3, y3)
def inside(x1, y1, x2, y2, x3, y3, x, y, ta):

	# Calculate area of triangle ABC
	A = area (x1, y1, x2, y2, x3, y3)

	# Calculate area of triangle PBC
	A1 = area (x, y, x2, y2, x3, y3)
	
	# Calculate area of triangle PAC
	A2 = area (x1, y1, x, y, x3, y3)
	
	# Calculate area of triangle PAB
	A3 = area (x1, y1, x2, y2, x, y)
	
	# Check if sum of A1, A2 and A3
	# is same as A
	if(A == A1 + A2 + A3):
		return True
	else:
		return False

t = []
for i in range(n):
    t.append([int(x) for x in input().split()])

for i in range(n):
    bucks = 0
    A = [t[i][0], t[i][1]]
    ta = area(A[0], A[1], B[0], B[1], C[0], C[1])
    for j in range(n):
        if i != j:
            P = [t[j][0], t[j][1]]
            if inside(A[0], A[1], B[0], B[1], C[0], C[1], P[0], P[1], ta):
                # print(ta)
                bucks += t[j][2]

    print(bucks)