#include <iostream>
#include <cmath>

using namespace std;

double area(int x1, int y1, int x2, int y2, int x3, int y3) {
    return abs((x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)) / 2.0);
}

bool inside(int x1, int y1, int x2, int y2, int x3, int y3, int x, int y) {
	double A = area (x1, y1, x2, y2, x3, y3);
	double A1 = area (x, y, x2, y2, x3, y3);
	double A2 = area (x1, y1, x, y, x3, y3);
	double A3 = area (x1, y1, x2, y2, x, y);
    //cout << A << " " << A1 << " " << A2 << " " << A3 << endl;
	return A == (A1 + A2 + A3);
}


int main() {
    int n, x, Bx, By, Cx, Cy;
    cin >> n >> x;
    Bx = 0; By = 0;
    Cx = x; Cy = 0;

    int **t = new int*[n];
    for(int i = 0; i < n; i++) {
        t[i] = new int[2];
    }
    int *v = new int[n];

    for(int i = 0; i < n; i++) {
        cin >> t[i][0] >> t[i][1] >> v[i];
    }

    for(int i = 0; i < n; i++) {
        int bucks = 0;
        for(int j = 0; j < n; j++) {
            if(i != j) {
                if(inside(Bx, By, Cx, Cy, t[i][0], t[i][1], t[j][0], t[j][1])) {
                    bucks += v[j];
                }
            }
        }
        cout << bucks << endl;
    }

    for(int i = 0; i < n; i++) {
        delete [] t[i];
    }
    delete [] t;
    delete [] v;

    return 0;
}