import math
n, w, h = [int(x) for x in input().split()]
d = math.sqrt(w*w + h*h)
for i in range(n):
    m = int(input())
    if m <= w or m <= h or m <=d:
        print('DA')
    else:
        print('NE')