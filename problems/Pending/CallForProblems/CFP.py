import sys
inp = [int(x) for x in sys.stdin.readlines()[1:]]
print(sum([1 for x in inp if x % 2 == 1]))