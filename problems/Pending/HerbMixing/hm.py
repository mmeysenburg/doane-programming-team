g, r = [int(x) for x in input().split()]
boost = 0
while r >= 1 and g >= 1:
    r -= 1
    g -= 1
    boost += 10

while g >= 3:
    g -= 3
    boost += 10

while g >= 2:
    g -= 2
    boost += 3

while g >= 1:
    g -= 1
    boost += 1

print(boost)