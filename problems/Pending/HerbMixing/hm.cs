﻿using System;

string[] tkns = Console.ReadLine().Split(" ");
int g = Convert.ToInt32(tkns[0]);
int r = Convert.ToInt32(tkns[1]);
int boost = 0;
while (r >= 1 && g >= 1)
{
    r--; g--;
    boost += 10;
}
while (g >= 3)
{
    g -= 3;
    boost += 10;
}
while (g >= 2)
{
    g -= 2;
    boost += 3;
}
while (g >= 1)
{
    g--;
    boost++;
}
Console.WriteLine(boost);