import java.math.BigDecimal;
import java.util.*;

/**
 * ProbC
 */
public class ProbC {

    private static long gcdb(long u, long v) {
        int shift;

        if (u == 0L) return v;
        if (v == 0L) return u;

        for(shift = 0; ((u | v) & 1) == 0; shift++) {
            u >>= 1;
            v >>= 1;
        }

        while ((u & 1) == 0) {
            u >>= 1;
        }

        do {
            while ((v & 1) == 0) v >>= 1;

            if(u > v) {
                long t = v;
                v = u;
                u = t;
            }

            v = v - u;
        } while(v != 0L);

        return u << shift;
    }

    // Iterative function to return gcd of a and b, using Euler's
    // algorithm
    private static long gcd(long a, long b) {
        while (!(a == 0L || b == 0L)) {
            if (a == 1L || b == 1L) {
                return 1L;
            }
            if (a == b) {
                return a;
            } else {
                if (a > b) {
                    a = a - b;
                } else {
                    b = b - a;
                }
            }
        }

        if (a == 0L) {
            return b;
        } else {
            return a;
        }
    } 

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        String line = stdin.nextLine();
        StringTokenizer stk = new StringTokenizer(line);
        String orig = stk.nextToken();
        int numRep = Integer.parseInt(stk.nextToken());
        int rLoc = orig.length() - numRep;
        String rep = orig.substring(rLoc);
        int dLoc = orig.indexOf('.');
        int shift = orig.length() - dLoc - 1;

        // calculate terms for x = (B - D) / (A - C)
        BigDecimal ten = new BigDecimal("10");
        BigDecimal A = ten.pow(shift);
        BigDecimal B = new BigDecimal(orig).multiply(A).add(new BigDecimal("0." + rep));
        BigDecimal C = ten.pow(rLoc - dLoc - 1);
        BigDecimal D = new BigDecimal(orig).multiply(C);

        long numerator = B.subtract(D).longValue();
        long denominator = A.subtract(C).longValue();
        long g = gcdb(numerator, denominator);
        if(g != 0) {
            numerator /= g;
            denominator /= g;
        }

        System.out.println(numerator + "/" + denominator);

        stdin.close();
    }
}