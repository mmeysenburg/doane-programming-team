#include <cstdlib>
#include <cmath>
#include <iostream>

using namespace std;

int main() {
	int N;
	
	cin >> N;
	double *x = new double[N];
	double *z = new double[N];
	
	for(int i = 0; i < N; i++) {
		cin >> x[i];
		cin >> z[i];
	}
	
	double L = -10000000000.0, n, d, y;
	
	for(int i = 0; i < N; i++) {
		for(int j = i + 1; j < N; j++) {
			n = abs(z[i] - z[j]);
			d = abs(x[i] - x[j]);
			if(d != 0) {
				y = n / d;
				if(y > L) {
					L = y;
				}
			}
		}
	}
	
	cout << L << endl;
	
	delete [] x;
	delete [] z;
	
	return EXIT_SUCCESS;
}