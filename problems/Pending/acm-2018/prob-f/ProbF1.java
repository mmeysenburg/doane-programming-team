import java.util.*;

public class ProbF1 {

	private static double minValue(double[] a) {
		double min = Double.MAX_VALUE;
		for(int i = 0; i < a.length; i++) {
			if(a[i] < min) {
				min = a[i];
			}
		}
		return min;
	}
	
	private static double maxValue(double[] a) {
		double max = Double.MIN_VALUE;
		for(int i = 0; i < a.length; i++) {
			if(a[i] > max) {
				max = a[i];
			}
		}
		return max;
	}

	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		
		int N = stdin.nextInt();
		double[] x = new double[N];
		double[] z = new double[N];

		for(int i = 0; i < N; i++) {
			x[i] = stdin.nextDouble();
			z[i] = stdin.nextDouble();
		}
		
		double maxX = maxValue(x);
		double minX = minValue(x);
		double maxF = maxValue(z);
		double minF = minValue(z);
		
		double L = Math.abs(maxF - minF) / Math.abs(maxX - minX);
		
		System.out.println(L);
	}
}