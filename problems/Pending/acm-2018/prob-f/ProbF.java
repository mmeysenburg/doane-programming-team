import java.util.*;

public class ProbF {
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		
		int N = stdin.nextInt();
		int[] x = new int[N];
		double[] z = new double[N];

		double L = Double.MIN_VALUE, n, d, y;		
		
		for(int i = 0; i < N; i++) {
			x[i] = stdin.nextInt();
			z[i] = stdin.nextDouble();
		}
		
		for(int i = 0; i < N; i++) {
			for(int j = i + 1; j < N; j++) {
				n = Math.abs(z[i] - z[j]);
				d = Math.abs(x[i] - x[j]);
				if(d != 0.0) {
					y = n / d;
					if(y > L) {
						L = y;
					}
				}
			}
		}
		
		System.out.println(L);
	}
}