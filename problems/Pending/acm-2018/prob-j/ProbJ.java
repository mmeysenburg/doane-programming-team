import java.util.*;

/**
 * ProbJ
 */
public class ProbJ {

    private static boolean isPal(String s) {
        int i = 0, j = s.length() - 1;
        while(i <= j) {
            if(s.charAt(i) != s.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    private static boolean isPal(int[] s) {
        int i = 0, j = s.length - 1;
        while(i <= j) {
            if(s[i] != s[j]) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int a = stdin.nextInt();
        int b = stdin.nextInt();
        if(a > b) {
            int t = a;
            a = b;
            b = t;
        }
        int k = stdin.nextInt();

        int num = 0;

        for(int i = a; i <= b; i++) {
            // get i in base 2, as a string and...
            String bin = Integer.toString(i, 2);

            if(isPal(bin)) {
                boolean all = true;
                for(int j = 3; j <= k; j++) {
                    // get i in base j
                    if(j <= Character.MAX_RADIX) {
                        // if the radix is small enough, use Integer.toString()
                        bin = Integer.toString(i, j);
                        if(!isPal(bin)) {
                            all = false;
                            j = k + 1;
                        }
                    } else {
                        // larger radices require manual conversions
                        int n = i;
                        LinkedList<Integer> temp = new LinkedList<>();
                        while(n != 0) {
                            int r = n % j;
                            temp.addFirst(r);
                            n /= j;
                        }
                        int[] dummy = new int[temp.size()];
                        int idx = 0;
                        for(int v : temp) {
                            dummy[idx++] = v;
                        }
                        if(!isPal(dummy)) {
                            all = false;
                            j = k + 1;
                        }
                    }
                }
                if(all) num++;
            }
        }

        System.out.println(num);
    }
}