#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    int n;
    cin >> n;

    long double fact = 1.0;
    long double e = 1.0;

    for(int i = 1; i <= n; i++) {
        fact *= i;
        e += (1.0 / fact);
    }

    printf("%0.16Lf\n", e);

    return EXIT_SUCCESS;
}