import sys

rolls = [sorted(line.split()) for line in sys.stdin.readlines()[1:]]

for roll in rolls:
    # count occurrences of each die
    counts = {}
    for die in roll:
        counts[die] = counts.get(die, 0) + 1

    counts = list(counts.items())
    print(counts)