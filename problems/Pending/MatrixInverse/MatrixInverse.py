import sys

def inverse(a, b, c, d):
    det = 1 / (a * d - b * c)
    row1 = [d * det, -b * det]
    row2 = [-c * det, a * det]
    return [row1, row2]

lines = sys.stdin.readlines()

lineNum = 0
caseNum = 1
while lineNum < len(lines):
    line = lines[lineNum]
    lineNum += 1
    if len(line) > 1:
        ab = [int(x) for x in line.split()]
        line = lines[lineNum]
        lineNum += 1
        cd = [int(x) for x in line.split()]
        inv = inverse(ab[0], ab[1], cd[0], cd[1])
        print('Case {0:d}:'.format(caseNum))
        for row in inv:
            print(int(round(row[0])), int(round(row[1])))
        caseNum += 1
            