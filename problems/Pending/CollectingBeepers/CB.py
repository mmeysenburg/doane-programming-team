N = int(input())

def manhattanDistance(a, b):
    return abs(a[0] - b[0]) + abs(a[1] - b[1])

for case in range(N):
    vals = [int(x) for x in input().split()]
    height = vals[0]
    width = vals[1]
    vals = [int(x) for x in input().split()]
    sx = vals[0]
    sy = vals[1]
    nodes = [(sx, sy)]
    numBeepers = int(input())
    for i in range(numBeepers):
        vals = [int(x) for x in input().split()]
        pos = (vals[0], vals[1])
        if pos not in nodes:
            nodes.append(pos)

    # make a graph w/ manhattan distances between
    # all nodes
    dim = len(nodes) + 1
    graph =[ [float('-inf')] * dim for i in range(dim)]

    for i, n1 in enumerate(nodes):
        for j, n2 in enumerate(nodes):
            graph[i + 1][j + 1] = manhattanDistance(n1, n2)

    # now find distance of shortest tour that hits 
    # all nodes exactly once