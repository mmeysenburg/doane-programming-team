t = int(input())
for i in range(t):
    line = input().split()
    w = int(line[0])
    h = int(line[1])
    n = int(line[2])

    p1map = [[c for c in input()] for j in range(h)]
    p2map = [[c for c in input()] for j in range(h)]

    shots = []
    for j in range(n):
        shot = [int(x) for x in input().split()]
        shots.append(shot)

    # output result for the case
    print('player two wins')