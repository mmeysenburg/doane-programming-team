#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    int t;
    cin >> t;

    // process all test cases
    for(int testCase = 0; testCase < t; testCase++) {
        int w, h, n;
        cin >> w >> h >> n;

        char p1Map[h][w], p2Map[h][w];

        // input player 1's map
        for(int r = 0; r < h; r++) {
            for(int c = 0; c < w; c++) {
                cin >> p1Map[r][c];
            }
        }

        // input player 2's map
        for(int r = 0; r < h; r++) {
            for(int c = 0; c < w; c++) {
                cin >> p2Map[r][c];
            }
        }

        int shots[n * 2];

        // input shots
        for(int i = 0; i < n * 2; i++) {
            cin >> shots[i];
        }

        int player = 1;
        char **cMap = player == 1 ? p2Map : p1Map;
        int idx = 0;
        while(idx < n) {
            int r = shots[idx++];
            int c = shots[idx++];
            if(cMap[r][c] == '#') {
                cMap[r][c] == '_';
            } else {
                player = player == 1 ? 2 : 1;
                cMap = player == 1 ? p2Map : p1Map;
            }
        } // while 

        for(int r = 0; r < h; r++) {
            for(int c = 0; c < w; c++) {
                cout << p1Map[r][c];
            }
            cout << endl;
        }

        for(int r = 0; r < h; r++) {
            for(int c = 0; c < w; c++) {
                cout << p2Map[r][c];
            }
            cout << endl;
        }

    } // for test cases

    return EXIT_SUCCESS;
}