import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class Bridge {

    private static LinkedList<String> output = new LinkedList<>();

    private static int travel(int[] group, int n) {
        if(n < 3) {
            output.addLast(group[0] + " " + group[1] + "\n");
            return group[n - 1];
        } else if(n == 3) {
            return group[0] + group[1] + group[2];
        } else {
            int temp1 = group[n - 1] + group[0] + group[n - 2] + group[0];
            int temp2 = group[1] + group[0] + group[n - 1] + group[1];

            if(temp1 < temp2) {
                return temp1 + travel(group, n - 2);
            } else if(temp2 < temp1) {
                return temp2 + travel(group, n - 2);
            } else {
                return temp2 + travel(group, n - 2);
            }
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = Integer.parseInt(stdin.nextLine());
        int[] group = new int[n];

        for(int i = 0; i < n; i++) {
            group[i] = Integer.parseInt(stdin.nextLine());
        }
        Arrays.sort(group);

        System.out.println(travel(group, group.length));
        for(String s : output) {
            System.out.print(s);
        }
    }
}