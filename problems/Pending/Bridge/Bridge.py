import sys

allInput = [int(x) for x in sys.stdin.readlines()]

n = allInput[0]

left = allInput[1:]
right = []

elapsed = 0
side = 'L'

trips = []
while len(left) > 0:
    if side == 'L':
        left.sort()
        if len(right) == 0 or min(left[-2:]) <= min(right, default=sys.maxsize):
            a = left.pop(0)
            b = left.pop(0)
            send = [a, b]
        else:
            a = left.pop(-1)
            b = left.pop(-1)
            send = [a, b]
        elapsed += max(send)
        right += send
        trips.append(str(send[0]) + ' ' + str(send[1]))
        side = 'R'
    else:
        right.sort()
        a = right.pop(0)
        left.append(a)
        elapsed += a
        trips.append(str(a))
        side = 'L'

print(elapsed)
for s in trips:
    print(s)