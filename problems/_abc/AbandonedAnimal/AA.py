N = int(input())
K = int(input())

itemsFoundIn = {}
for i in range(K):
    storeNum, item = input().split()
    if item in itemsFoundIn:
        itemsFoundIn[item][int(storeNum)] = 0
    else:
        itemsFoundIn[item] = {int(storeNum) : 0}

M = int(input())

items = [input() for i in range(M)]

print(itemsFoundIn)
maxHere = 0
for curr in items:
    rem = []
    for i in itemsFoundIn[curr]:
        if i < maxHere:
            rem.append(i)
    for i in rem:
        itemsFoundIn[curr].pop(i)
    if len(itemsFoundIn) > 0:
        maxHere = max(maxHere, min(itemsFoundIn[curr].keys(), default=0))

print(maxHere)
print(rem)
print(itemsFoundIn)

minHere = float('inf')
for curr in reversed(items):
    rem = []
    for i in itemsFoundIn[curr]:
        if i > minHere:
            rem.append(i)
    for i in rem:
        itemsFoundIn[curr].pop(i)
    if len(itemsFoundIn[curr]) > 0:
        minHere = min(minHere, max(itemsFoundIn[curr].keys()))

print(minHere)
print(rem)
print(itemsFoundIn)

works = True
ambiguous = False
for i in items:
    if len(itemsFoundIn[i]) < 1:
        works = False
    if len(itemsFoundIn[i]) > 1:
        ambiguous = True

if not works:
    print('impossible')
elif ambiguous:
    print('ambiguous')
else:
    print('unique')