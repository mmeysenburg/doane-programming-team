N = int(input())
K = int(input())

maxStoreNo = -1
inventories = {}
for i in range(K):
    tokens = input().split()
    storeNo = int(tokens[0])
    if storeNo > maxStoreNo:
        maxStoreNo = storeNo
    inv = inventories.get(storeNo, {})
    inv[tokens[1]] = 0
    inventories[storeNo] = inv

M = int(input())

groceries = [input() for i in range(M)]

seenIt = {}


def countWays(tour, storeNo, groc):
    if storeNo > maxStoreNo:
        return 0
    elif len(groc) == 0:
        thisInst = str(tour)
        if thisInst in seenIt:
            return 0
        else:
            seenIt[thisInst] = 0
            return 1
    else:
        itm = groc[0]
        if itm in inventories[storeNo]:
            numA = countWays(tour + [storeNo], storeNo, groc[1:])
            numB = countWays(tour + [storeNo], storeNo + 1, groc[1:])
            val = numA + numB
        else:
            val = countWays(tour, storeNo + 1, groc)
        return val

val = countWays([], 0, groceries)

if val == 0:
    print('impossible')
elif val == 1:
    print('unique')
else:
    print('ambiguous')