import java.util.*;

public class AA {
    private static Map<Integer, Set<String>> inventories;
    private static List<String> itemsBought;
    // private static int numGoodRoutes = 0;

    public static int tryStore(int store, int N, LinkedList<String> shoppingList) {
        if(shoppingList.isEmpty()) {
            return 1;
        } else {
            int sum = 0;
            if(inventories.get(store).contains(shoppingList.getFirst())) {
                String item = shoppingList.removeFirst();
                for (int i = store; i < N; i++) {
                    sum += tryStore(i, N, shoppingList);
                }
                shoppingList.addFirst(item);
            }
            for(int i = store + 1; i < N; i++) {
                sum += tryStore(i, N, shoppingList);
            }

            return sum;
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int N = Integer.parseInt(stdin.nextLine());
        int K = Integer.parseInt(stdin.nextLine());
        inventories = new HashMap<>();
        for (int i = 0; i < K; i++) {
            String[] tokens = stdin.nextLine().split(" ");
            int store = Integer.parseInt(tokens[0]);
            if(inventories.containsKey(store)) {
                inventories.get(store).add(tokens[1]);
            } else {
                Set<String> items = new HashSet<>();
                items.add(tokens[1]);
                inventories.put(store, items);
            }
        } // reading inventories

        int M = Integer.parseInt(stdin.nextLine());
        itemsBought = new LinkedList<>();
        for (int i = 0; i < M; i++) {
            itemsBought.add(stdin.nextLine());
        } // reading items bought

        int sum = 0;
        // for(int startStore = 0; startStore < N; startStore++) {
            LinkedList<String> shoppingList = new LinkedList<>(itemsBought);
            sum += tryStore(0, N, shoppingList);
        // }

        System.out.println(sum);
        // String result = "beef";
        // switch(numGoodRoutes) {
        //     case 0:
        //         result = "impossible";
        //         break;
        //     case 1:
        //         result = "unique";
        //         break;
        // }
        // System.out.println(result);
    }
}