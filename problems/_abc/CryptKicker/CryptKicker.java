
import java.util.*;

/**
 * Solution to the "CryptKicker" programming contest problem.
 */
public class CryptKicker {

    /**
     * holds dictionary words, grouped by their lengths.
     */
    private static Map<Integer, Set<String>> dictionary;

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored by this application.
     */
    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        // create and fill the dictionary
        dictionary = new TreeMap<>();
        int n = Integer.parseInt(stdIn.nextLine());
        for (int i = 0; i < n; i++) {
            String word = stdIn.nextLine().trim();
            int wLen = word.length();

            // if we don't have a word of this length yet, add a set for 
            // the new length
            if (!dictionary.containsKey(wLen)) {
                dictionary.put(wLen, new TreeSet<>());
            }

            // add word to the appropriate set
            dictionary.get(wLen).add(word);
        }

        // now decrypt each of the lines of ciphertext in the input
        while (stdIn.hasNextLine()) {
            String line = stdIn.nextLine();

            // convert line to list of words, removing any duplicates 
            // in the line.
            
            // break line into array of words
            String[] arr = line.split(" "); 
            
            // make into a list
            List<String> list = Arrays.asList(arr);  
            
            // make into a set
            Set<String> set = new TreeSet<>(list);   
            
            // make back into a set
            List<String> cipherWords = new ArrayList<>(set);

            // sort the cipher words by the number of times words of that
            // length occur in the dictionary
            Collections.sort(cipherWords, new NumLengthsComparator());

            // create a decoder to decipher the ciphertext
            Decoder decoder = findDecoder(cipherWords, 0, new Decoder());

            // did it work? If not, make an empty decoder
            if (decoder == null) {
                decoder = new Decoder();
            }

            // output plaintext
            System.out.println(decoder.decode(line));

        } // while there are more lines

    } // main

    /**
     * Build a decoder by trying all of the possible ciphers.
     *
     * @param cipherWords List of ciphered words in the ciphertext line, 
     * sorted in ascending order by the number of times words of the 
     * same length appear in the dictionary
     * @param i Which word we're working on
     * @param decoder The current decoder at this level of the recursion
     * @return A new decoder that deciphers the ciphertext, or null if 
     * no such decoder could be constructed
     */
    private static Decoder findDecoder(List<String> cipherWords, int i,
            Decoder decoder) {

        // base case: if we've successfully processed all words, our 
        // parameter decoder works
        if (i >= cipherWords.size()) {
            return decoder;
        }

        // otherwise, let's look at all possible ciphers, by examining 
        // all the words with the same length in the dictionary
        String cipherWord = cipherWords.get(i);

        // if we don't have any words of the same length as the cipher 
        // word in the dictionary, we cannot have a cipher, so return 
        // null.
        Set<String> dictionaryWords = dictionary.get(cipherWord.length());
        if (dictionaryWords == null) {
            return null;
        }

        // otherwise, keep going
        for (String dictionaryWord : dictionaryWords) {
            // try to add this dictionary word as the plaintext for the
            // cipher word
            Decoder newDecoder = 
                    decoder.addWords(cipherWord, dictionaryWord);

            // did it work?
            if (newDecoder == null) {
                continue; // if not, skip to next cipher word
            }

            // if we're still OK, recurse to the next word
            Decoder nextDecoder = 
                    findDecoder(cipherWords, i + 1, newDecoder);

            // did the recursion work?
            if (nextDecoder != null) {
                // if so, send back a plausible decoder
                return nextDecoder; 
            }
        }

        // nothing worked? return null
        return null;
    }

    /**
     * Comparator class to compare two cipher stings, based on the number 
     * of times words of the same length occur in the dictionary.
     */
    private static class NumLengthsComparator implements 
            Comparator<String> {

        /**
         * Get number of times words of the same length occur in the 
         * dictionary.
         *
         * @param word Ciphertext word to look up.
         * @return Number of times words of the same length as word occur 
         * in the dictionary.
         */
        private int numLengths(String word) {
            Set<String> set = dictionary.get(word.length());
            return set == null ? 0 : set.size();
        }

        @Override
        /**
         * Compare t and t1 based on number of times words of same 
         * length appear in the dictionary.
         *
         */
        public int compare(String t, String t1) {
            return numLengths(t) - numLengths(t1);
        }

    } // NumLengthsComparator

    /**
     * Class to decode ciphertext into plaintext.
     */
    private static class Decoder {

        /**
         * Map from cipher character to plaintext character.
         */
        private Map<Character, Character> map;

        /**
         * Default constructor. Create an empty decoding map.
         */
        public Decoder() {
            map = new TreeMap<>();
        }

        /**
         * Copy constructor.
         *
         * @param d Decoder to copy from
         */
        public Decoder(Decoder d) {
            map = new TreeMap<>();
            for (char c : d.map.keySet()) {
                map.put(c, d.map.get(c));
            }
        }

        /**
         * Decode the specified line.
         *
         * @param line Line of ciphertext to decode.
         */
        private String decode(String line) {
            // an empty map means no solution found
            if (map.isEmpty()) {
                line = line.replaceAll("[a-z]", "*");
            } else {
                // otheriwse, decode each non-space character using the 
                // decode map
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < line.length(); i++) {
                    char c = line.charAt(i);
                    if (c == ' ') {
                        sb.append(" ");
                    } else {
                        sb.append(map.get(c));
                    }
                }
                line = sb.toString();
            }

            return line;
        }

        /**
         * Create a new Decoder, based on this one, with new cipher to 
         * plain text mappings based on the provided words.
         *
         * @param cipherWord Cipher word to try to add characters from
         * @param dictionaryWord Plaintext word to try to add characters
         * from
         * @return A new Decoder including information gleaned from 
         * cipherWord and dictionaryWord, or null if no such Decoder is 
         * possible
         */
        private Decoder addWords(String cipherWord, 
                String dictionaryWord) {
            
            // make a new Decoder just like this one
            Decoder d = new Decoder(this);

            // look at each cipher / plain character pair
            for (int i = 0; i < cipherWord.length(); i++) {
                char cipherC = cipherWord.charAt(i);
                char plainC = dictionaryWord.charAt(i);

                // don't have this cipher / plain pair yet? awesome!
                // add it
                if (!d.map.containsKey(cipherC)) {
                    d.map.put(cipherC, plainC);
                } else {
                    // does the map have this exact pair? awesome! 
                    // do nothing
                    if (d.map.get(cipherC) == plainC) {
                        // nothing
                    } else {
                        // to get here, we have an existing cipher / 
                        // plain pair of characters that's different. 
                        // Bummer! That means this attempt at a decoder 
                        // has failed
                        return null;
                    }
                }
            }

            return d;
        }
    }

}
