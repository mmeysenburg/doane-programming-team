
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class Maker {
    public static void main(String[] args) throws FileNotFoundException {
        List<String> words = new LinkedList<>();
        Scanner in = new Scanner(new FileReader(new File("1000words.txt")));
        while(in.hasNextLine()) {
            String word = in.nextLine().trim().toLowerCase();
            if(word.length() > 16) {
                word = word.substring(0, 16);
            }
            words.add(word);
        }
        in.close();
        
        // output dictionary information
        System.out.println(words.size());
        for(String word : words) {
            System.out.println(word);
        }
        
        // create a cypher
        Random prng = new Random();
        char[] cypher = new char[26];
        for(int i = 0; i < 26; i++) {
            cypher[i] = (char)('a' + i);
        }
        for(int i = 0; i < 26; i++) {
            char t = cypher[i];
            int idx = prng.nextInt(26);
            cypher[i] = cypher[idx];
            cypher[idx] = t;
        }
        
        // create a sentence
        StringBuilder sb = new StringBuilder();
        StringBuilder sbPlain = new StringBuilder();
        String randWord = words.get(prng.nextInt(words.size()));
        while(sb.length() + randWord.length() < 80) {
            StringBuilder sb1 = new StringBuilder();
            for(int i = 0; i < randWord.length(); i++) {
                sb1.append(cypher[randWord.charAt(i) - 'a']);
            }
            sb.append(sb1.toString());
            sb.append(" ");
            sbPlain.append(randWord);
            sbPlain.append(" ");
            randWord = words.get(prng.nextInt(words.size()));
        }
        
        // output sentence
        System.out.println(sb.toString());
        // output plaintext sentence
        System.out.println(sbPlain.toString());
        
        // create another sentence
        sb = new StringBuilder();
        sbPlain = new StringBuilder();
        randWord = words.get(prng.nextInt(words.size()));
        while(sb.length() + randWord.length() < 80) {
            StringBuilder sb1 = new StringBuilder();
            for(int i = 0; i < randWord.length(); i++) {
                sb1.append(cypher[randWord.charAt(i) - 'a']);
            }
            sb.append(sb1.toString());
            sb.append(" ");
            sbPlain.append(randWord);
            sbPlain.append(" ");
            randWord = words.get(prng.nextInt(words.size()));
        }
        
        // output sentence
        System.out.println(sb.toString());
        // output plaintext sentence
        System.out.println(sbPlain.toString());
        
        // now make an invalid cipher
        for(int i = 0; i < 13; i++) {
            cypher[i] = 'a';
            cypher[i + 13] = 'b';
        }
        // create another sentence
        sb = new StringBuilder();
        sbPlain = new StringBuilder();
        randWord = words.get(prng.nextInt(words.size()));
        while(sb.length() + randWord.length() < 80) {
            StringBuilder sb1 = new StringBuilder();
            for(int i = 0; i < randWord.length(); i++) {
                sb1.append(cypher[randWord.charAt(i) - 'a']);
            }
            sb.append(sb1.toString());
            sb.append(" ");
            sbPlain.append(randWord);
            sbPlain.append(" ");
            randWord = words.get(prng.nextInt(words.size()));
        }
        
        // output sentence
        System.out.println(sb.toString());
        // output plaintext sentence
        System.out.println(sbPlain.toString());
    }
}
