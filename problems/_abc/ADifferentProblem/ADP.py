'''
Python solution to A Different Problem Kattis problem.

This is SOOOO easy in Ptyhon, because integer values
have infinite precision by default.
'''
import sys

# read all the lines as list of integers
d = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]

for line in d:
    # calculate the absolute value of the difference
    print(abs(line[0] - line[1]))