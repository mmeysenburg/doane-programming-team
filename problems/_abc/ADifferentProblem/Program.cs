﻿using System;

class ADP
{
    public static void Main(string[] args)
    {
        long[] a = {0, 0};
        string line;

        while((line = Console.ReadLine()) != null && line != "")
        {
            string[] split = line.Split(' ');
            a[0] = long.Parse(split[0]);
            a[1] = long.Parse(split[1]);
            Console.WriteLine(Math.Abs(a[0] - a[1]));
        }
    }
}