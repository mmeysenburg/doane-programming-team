
import java.util.*;

/**
 * Solution to "Above Average" programming competition problem. 
 * Determine what percentage of students are above average. 
 */
public class AboveAverage {

    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);

        // number of cases
        int C = stdIn.nextInt();

        for (int i = 0; i < C; i++) {
            // number of students
            double N = stdIn.nextDouble();
            
            // read grades and accumulate sum
            double sum = 0.0;
            List<Double> nums = new LinkedList<>();
            for (int j = 1; j <= N; j++) {
                double num = stdIn.nextDouble();
                sum += num;
                nums.add(num);
            }
            
            // mean
            sum /= N;
            
            // count number above average
            double above = 0;
            for (double j : nums) {
                if (j > sum) {
                    above += 1.0;
                }
            }
            
            // convert to percentage and round to three places
            double aa = Math.round((above / N) * 100.0 * 1000.0) / 1000.0;
            
            // output value for this case
            System.out.print(String.format("%.3f", aa));
            System.out.println("%");
        }
    }
}
