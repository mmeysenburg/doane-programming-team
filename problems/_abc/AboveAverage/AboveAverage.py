'''
Python solution to the Above Average 
Kattis programming competition problem.
'''
# read number of cases first
C = int(input())

# process each case
for caseNo in range(C):
    # grab input line as a list of integers
    numbers = [int(x) for x in input().split()]

    # N is the first int in the list
    N = numbers[0]

    # compute class average
    s = sum(numbers[1:])
    mean = s / N

    # count number of students above average
    s = 0
    for grade in numbers[1:]:
        if grade > mean:
            s += 1

    # output the percentage of students above
    # average, to three decimal points accuracy
    mean = s / N * 100
    print('{0:0.3f}%'.format(mean))