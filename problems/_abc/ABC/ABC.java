
import java.util.*;

/**
 * Solution to "ABC" programming competition problem. 
 * Sort three numbers, then output in a specified order.
 */
public class ABC {

    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);
        
        // read input and place into an array of ints
        String[] sNums = stdIn.nextLine().split(" ");
        int[] nums = new int[3];
        nums[0] = Integer.parseInt(sNums[0]);
        nums[1] = Integer.parseInt(sNums[1]);
        nums[2] = Integer.parseInt(sNums[2]);

        // sort the numbers
        Arrays.sort(nums);

        // assign as a, b, c
        int a = nums[0];
        int b = nums[1];
        int c = nums[2];

        // now output in the given order
        String order = stdIn.nextLine().trim();
        for (int i = 0; i < 3; i++) {
            switch (order.charAt(i)) {
                case 'A':
                    System.out.print(a + " ");
                    break;
                case 'B':
                    System.out.print(b + " ");
                    break;
                case 'C':
                    System.out.print(c + " ");
                    break;
            }
        }
        System.out.println();
    }
}
