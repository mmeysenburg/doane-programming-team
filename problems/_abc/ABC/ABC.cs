using System;
using System.Collections;

namespace ABC 
{
    class ABC
    {
        static void Main() {
            int[] nums = {0, 0, 0};

            string[] line = Console.ReadLine().Split(' ');
            nums[0] = Convert.ToInt32(line[0]);
            nums[1] = Convert.ToInt32(line[1]);
            nums[2] = Convert.ToInt32(line[2]);
            Array.Sort(nums);

            string order = Console.ReadLine();

            foreach (char c in order)
            {
                switch (c)
                {
                    case 'A':
                        Console.Write(nums[0] + " ");
                        break;
                    case 'B':
                        Console.Write(nums[1] + " ");
                        break;
                    case 'C':
                        Console.Write(nums[2] + " ");
                        break;
                } // switch
            } // foreach
        } // Main
    } // class ABC
} // namespace ABC