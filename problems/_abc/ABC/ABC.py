'''
 ' Python solution to the ABC programming competition problem.
 '
 ' Mark M. Meysenburg
 ' 05/03/2021
'''

# get first line of input as list of integers, sort them
values = sorted([int(x) for x in input().split()])

# set up indices for each letter
indices = {'A':0, 'B':1, 'C':2}

# form output line as list of strings, with values 
# ordered according to the 2nd line of input
output = [str(values[indices[ch]]) for ch in input()]

# print output with each separated by spaces
print(' '.join(output))