
import java.util.*;

/**
 * Solution to "Aaah!" programming competition problem. 
 * Help Jon Marius decide if he can go to the doctor. 
 */
public class Aaah {

    public static void main(String[] args) {
        
        Scanner stdIn = new Scanner(System.in);
        // what he can say
        String can = stdIn.nextLine().trim();
        
        // what doc wants
        String doc = stdIn.nextLine().trim();

        // output if Jon should go to the doc 
        if (doc.length() > can.length()) {
            System.out.println("no");
        } else {
            System.out.println("go");
        }
    }
}
