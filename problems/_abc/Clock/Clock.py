'''
 * Clunky but effective solution to the Clock problem on Kattis.
'''

# strategy: compute the degree difference between hour and minute
# hand for each possible time, and store corresponding string 
# in a dictionary. Then, look up input value and spit out the time.
degPerM = 360 / 60
degPerH = 360 / 12 / 60

degToTime = {}

degh = 0 # degree of hour hand (0 at 12, clockwise)
for h in range(12):
    degm = 0 # degree of minute hand (0 and 0, clockwise)

    # compute degrees between hour and minute hand
    for m in range(60):
        if degh <= degm:
            degd = degm - degh
        else:
            degd = 360 - (degh - degm)

        # prepare and store the degree / time in dictionary
        time = '{0:02d}:{1:02d}'.format(h, m) 
        deg = int(degd * 10)
        degToTime[deg] = time

        # increment hand positions
        degm += degPerM
        degh += degPerH

# output the result!
deg = int(input())
print(degToTime[deg])