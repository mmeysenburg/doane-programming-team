import java.util.*;

/**
 * Solution to "All About That Base" programming competition problem. 
 * Examines a  * series of equations, to see which numeric bases, if 
 * any, each is valid for.
 */
public class AATB {

    /**
     * Try to interpret a string as a base one (unary tally marks) value.
     *
     * @param s String to interpret.
     * @return Decimal integer equivalent of s, if it is a unary value.
     * @throws NumberFormatException If s is not a unary value
     */
    private static int baseOne(String s) throws NumberFormatException {
        int val = 0;
        // count up the number of tally marks in s
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                val++;
            } else {
                // if string has anything but '1's in it, it is not a
                // valid unary number
                throw new NumberFormatException();
            }
        }
        return val;
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored.
     */
    public static void main(String[] args) {
        // each character represents a base, 1 - 36
        String bases = "123456789abcdefghijklmnopqrstuvwxyz0";

        Scanner stdin = new Scanner(System.in);

        // get number of cases 
        int n = Integer.parseInt(stdin.nextLine());

        // examine each case
        for (int i = 0; i < n; i++) {
            // read the equation into an array of strings: 
            // [X, op, Y, =, Z]
            String[] eqn = stdin.nextLine().split(" ");

            // create StringBuilder for output
            StringBuilder sb = new StringBuilder();

            // now try all the possible bases
            for (int base = 1; base <= 36; base++) {
                try {
                    // first, attempt base conversion; a number format
                    // exception means at least one of X, Y, Z, is not
                    // a legal representation in the current base, so
                    // we will then skip trying the math, and try again 
                    // for the next base
                    double X, Y, Z;
                    if (base == 1) {
                        // use out own method to try to interpret base one
                        X = baseOne(eqn[0]);
                        Y = baseOne(eqn[2]);
                        Z = baseOne(eqn[4]);
                    } else {
                        // Java already has the ability to parse the 
                        // other bases! 
                        X = Integer.parseInt(eqn[0], base);
                        Y = Integer.parseInt(eqn[2], base);
                        Z = Integer.parseInt(eqn[4], base);
                    }

                    // if we haven't thrown an exception, X, Y, and Z were
                    // valid values in the current base. Now, try to do 
                    // the math to see if the equation works
                    double actual = 0;
                    switch (eqn[1]) {
                        case "+":
                            actual = X + Y;
                            break;
                        case "-":
                            actual = X - Y;
                            break;
                        case "*":
                            actual = X * Y;
                            break;
                        case "/":
                            actual = X / Y;
                            break;
                    }

                    // if the equation works, add the character for the
                    // current base to the output string
                    if (Z == actual) {
                        sb.append(bases.charAt(base - 1));
                    }

                } catch (NumberFormatException nfe) {
                    // empty catch to skip over invalid representations
                    // for the current base
                }

            } // for bases

            // output
            if (sb.length() == 0) {
                System.out.println("invalid");
            } else {
                System.out.println(sb.toString());
            }

        } // for cases

        stdin.close();

    } // main
}
