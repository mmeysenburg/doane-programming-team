'''
Solution to the Another Candies Kattis problem.

Mark M. Meysenburg
06/24/2021
'''
# for each test case, sum up all the candies, and see if the sum
# is divisible by n
t = int(input())
input()     # skip the pesky blank lines between cases
for i in range(t):
    n = int(input())
    sum = 0
    for j in range(n):
        sum += int(input())
    if sum % n == 0:
        print("YES")
    else:
        print("NO")
    if i < t - 1: # don't try to skip a line for the last case
        input()