import java.util.Scanner;

public class Main {

    private static int length(int n) {
        int len = 0;
        if(n < 10) return 1;
        while (n > 10) {
            n /= 10;
            len++;
        }
        return len;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int k, e, n;
        double d1, d2, d3, d4;
        double left, right;

        d4 = Math.log(2.0);
        d3 = Math.log(10.0) / d4;

        while (stdin.hasNextInt()) {
            n = stdin.nextInt();

            d1 = Math.log(n) / d4;
            d2 = Math.log(n + 1) / d4;

            k = length(n) + 1;
            left = d1 + k * d3;
            right = d2 + k * d3;

            e = (int)Math.ceil(left);
            while (e >= right) {
                k++;
                left = d1 + k * d3;
                right = d2 + k * d3;
                e = (int)Math.ceil(left);
            }

            System.out.println(e);
        }
    }
}