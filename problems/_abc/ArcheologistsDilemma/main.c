#include <stdio.h>
#include <stdlib.h>
#include <math.h>

/**
 * C solution to the Archaeologist's Dilemma problem.
 */

// function prototypes
void process(int);
int length(int);

// main function
int main() {
    int in;

    // keep reading as long as there are
    // more integers to read
    while(scanf("%u", &in) != EOF)
        process(in);

    // signal correct termination
    return 0;
}

/**
 * Find e for given n. If there are k digits missing,
 * then e must satisfy this relation:
 * 
 * lg(n) + k * lg(10) <= e < lg(n + 1) + k * lg(10)
 * 
 * The function iterates k until an integer e is found
 * satisfying the relation.
 * 
 * @param n input int
 */
void process(int n) {
    // k: number of missing digits
    // e: 1st exponent of 2 such that 2^e starts with n
    int k, e;

    // d1: lg(n)
    // d2: lg(n + 1)
    // d3: lg(10)
    // d4: log(2), used for calculating d1, d2, d3
    double d1, d2, d3, d4;

    // left: left side of relation
    // right: right side of relation
    double left, right;

    d4 = log(2);
    d3 = log(10) / d4;
    d1 = log(n) / d4;
    d2 = log(n + 1) / d4;

    // k must start at |n| + 1, since more than half
    // of the digits are mission
    k = length(n) + 1;

    // calculate elements of relation
    left = d1 + k * d3;
    right = d2 + k * d3;
    e = (int)ceil(left);

    // keep going until k is big enough
    while(e >= right) {
        k++;
        left = d1 + k * d3;
        right = d2 + k * d3;
        e = (int)ceil(left);
    }

    // output e
    printf("%d\n", e);
}

/**
 * Determine number of digits in n.
 * 
 * @param n
 * @return number of digits in n.
 */
int length(int n) {
    int len = 0;
    if (n < 10) return 1;
    while(n > 0) {
        n /= 10;
        len++;
    }
    return len;
}