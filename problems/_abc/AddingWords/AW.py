'''
Python solution to the Adding Words Kattis problem.
'''
import sys

# map will contain word / value pairs
map = {}

def calc(tokens):
    '''
    Perform simple infix evaluation of the expression contained
    in the list of strings named line. 
    '''
    val = 0        # accumulator for the calculation's result
    isVar = True   # should this token be a variable?
    isPlus = True  # should we add to the accumulator? 

    for token in tokens:
        # is the token a variable?
        if isVar:
            # do we have a value for the word?
            # if so, accumulate the value
            if token in map:
                if isPlus:
                    val += map[token]
                else:
                    val -= map[token]
            else:
                # if not, we're done
                return 'unknown'
            
            # the next token should be an operator
            isVar = False
        else:
            # toggle additioin / subtraction flag
            if token == '+':
                isPlus = True
            elif token == '-':
                isPlus = False
            elif token == '=':
                # ignore =
                pass
            else:
                # failsafe for unknown operator
                return 'unknown'
            
            # next token should be a variable
            isVar = True

    # do we have a matching word for the result?
    for k in map:
        if map[k] == val:
            # found one, so send back the word
            return k
    
    # no matching word? return unknown
    return 'unknown'

# get all input as a list of lists of strings
allLines = [x.split() for x in sys.stdin.readlines()]

# process the instructions
for line in allLines:
    if line[0] == 'def':
        # store a word / value pair
        map[line[1]] = int(line[2])
    elif line[0] == 'clear':
        # empty the map
        map.clear()
    elif line[0] == 'calc':
        # perform a calculation
        print(' '.join(line[1:]) + ' ' + calc(line[1:]))

