
import java.util.*;

/**
 * Solution to "Adding Words" programming competition problem. Create a
 * calculator that can assign integer values to words, then evaluate 
 * simple infix expressions using those words and addition or 
 * subtraction.
 */
public class AddingWords {

    /**
     * Perform simple infix evaluation of an expression containing 
     * variables, addition or subtraction.
     * @param stk String tokenizer for the line being evaluated; 
     * assumes it has already eaten the word "calc"
     * @param map Map of variables to values
     * @return String containing result (variable name or unknown)
     */
    private static String calc(StringTokenizer stk, 
            Map<String, Integer> map) {
        
        int val = 0;            // computed value
        boolean isVar = true;   // is token a variable?
        boolean isPlus = true;  // is operator '+'?

        // consume the rest of the expressions in the line
        while (stk.hasMoreTokens()) {
            // get token
            String token = stk.nextToken();
            
            // is it a variable? (first time it must be)
            if (isVar) {
                // do we have a value? 
                if (map.containsKey(token)) {
                    // if so, add or subtract from value
                    if (isPlus) {
                        val += map.get(token);
                    } else {
                        val -= map.get(token);
                    }
                } else {
                    // if not, abandon the process with unknown
                    return "unknown";
                }
                
                // the next token should not be a variable
                isVar = false;
                
            } else {
                // if it's not a variable, it's an operator
                switch (token) {
                    case "+":
                        isPlus = true;
                        break;
                    case "-":
                        isPlus = false;
                        break;
                    case "=":
                        // ignore =
                        break;
                    default:
                        // invalid operator: return unknown
                        return "unknown";
                }
                
                // next token should be a variable
                isVar = true;
                
            } // if variable  
            
        } // while more tokens

        // once we're done with tokens, see if we have a matching
        // value in our variable to value map
        for(String s : map.keySet()) {
            if(map.get(s) == val) {
                return s;
            }
        }

        // if there's no match, return unknown
        return "unknown";
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored
     */
    public static void main(String[] args) {
        // Map variable names to values
        Map<String, Integer> map = new HashMap<>();

        Scanner stdin = new Scanner(System.in);

        // read all the lines of input
        while (stdin.hasNextLine()) {

            String line = stdin.nextLine();
            StringTokenizer stk = new StringTokenizer(line);

            // we need the first token to see what we're supposed to do
            String token = stk.nextToken();

            // check out our options
            switch (token) {

                case "def":
                    // definition: get the variable, value, and store
                    String var = stk.nextToken();
                    int val = Integer.parseInt(stk.nextToken());
                    if (map.containsKey(var)) {
                        map.replace(var, val);
                    } else {
                        map.put(var, val);
                    }
                    break;

                case "calc":
                    // calc: first, form the equation part of the 
                    // output
                    StringTokenizer stk2 = new StringTokenizer(line);
                    stk2.nextToken();
                    StringBuilder outLine = new StringBuilder();
                    while (stk2.hasMoreTokens()) {
                        outLine.append(stk2.nextToken());
                        outLine.append(" ");
                    }

                    // then, call the calc method to determine the result
                    System.out.println(outLine.toString() + 
                            calc(stk, map));
                    break;
                    
                case "clear":
                    // clear: just empty the contents of the map
                    map.clear();
                    break;
            } // switch token
        } // while we have lines
        
        stdin.close();
    } // main
} // class
