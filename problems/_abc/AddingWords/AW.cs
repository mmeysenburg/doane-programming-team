using System;
using System.Collections.Generic;

/// <summary>
/// C# program to solve the Adding Words Kattis problem.
/// </summary>
class AW
{
    /// <summary>
    /// Perform simple infix evaluation of the expression contained in the
    /// array of strings named tokens.
    /// </summary>
    private static string calc(Dictionary<string, int> map, string[] tokens)
    {
        int val = 0;        // accumulator for the calculation's result
        bool isVar = true;  // should this token be a variable?
        bool isPlus = true; // should we add to the accumulator?

        for(int i = 1; i < tokens.Length; i++)
        {
            // is the token a variable?
            if(isVar)
            {
                // do we have a value for the word? If so, accumulate 
                if(map.ContainsKey(tokens[i]))
                {
                    if(isPlus)
                    {
                        val += map[tokens[i]];
                    } else
                    {
                        val -= map[tokens[i]];
                    }
                } // in map
                else
                {
                    // if we didn't have a value, we're done
                    return "unknown";
                }

                // next token should be an operator
                isVar = false;
            } else
            {
                switch(tokens[i])
                {
                    // toggle addition / subtraction flag
                    case "+":
                        isPlus = true;
                        break;
                    case "-":
                        isPlus = false;
                        break;
                    case "=":
                        ; // ignore =
                        break;
                    default: 
                        // failsafe for unknown operator
                        return "unknown";

                }
                
                // next token should be a variable
                isVar = true;

            } // non-variable
        } // for tokens

        // do we have a matching word for the result? 
        foreach(string s in map.Keys)
        {
            if(map[s] == val)
            {
                // yep! send it back
                return s;
            }
        }

        // no matching word? unknown
        return "unknown";
    }

    /// <summary>
    /// Application entry point
    /// </summary>
    public static void Main(string[] args)
    {
        // map holding word / value pairs
        Dictionary<string, int> map = new Dictionary<string, int>();

        string line;
        while((line = Console.ReadLine()) != null && line != "")
        {
            string[] tokens = line.Split(' ');
            switch(tokens[0])
            {
                case "def":
                    // add a definition to the map
                    map[tokens[1]] = int.Parse(tokens[2]);
                    break;
                case "clear":
                    // erase all definitions
                    map.Clear();
                    break;
                case "calc":
                    // make a calculation and output the results
                    Console.WriteLine(line.Substring(5) + " " + calc(map, tokens));
                    break;
            } // switch
        } // while we have input
    } // main
} // class