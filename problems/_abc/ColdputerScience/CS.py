import sys

lines = sys.stdin.readlines()
vals = [1 for x in lines[1].split() if int(x) < 0]
print(sum(vals))