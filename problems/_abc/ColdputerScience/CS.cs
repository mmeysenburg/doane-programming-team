using System;

namespace CS
{
    class CS
    {
        static void Main() {
            Console.ReadLine();
            string[] sNums = Console.ReadLine().Split(' ');
            int sum = 0;
            for(int i = 0; i < sNums.Length; i++) {
                if(Convert.ToInt32(sNums[i]) < 0) {
                    sum++;
                }
            }
            Console.WriteLine(sum);
        }
    }
}