
import java.util.*;

/**
 * Solution to "Cold-puter Science" programming competition problem. 
 * Read N integers and count the number of negatives.
 */
public class ColdputerScience {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int i, t, sum = 0;
        int N = stdin.nextInt();
        for (i = 0; i < N; i++) {
            t = stdin.nextInt();
            if (t < 0) {
                sum++;
            }
        }

        System.out.println(sum);
    }
}
