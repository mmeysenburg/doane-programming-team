
import java.util.*;

/**
 * Solution to the "Acceptable Strings" programming contest
 * problem. 
 */
public class AcceptableStrings {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int[][] table = new int[2][101];
        Set<Integer> finalStates = new TreeSet<>();
        int Case = 1;

        int N = stdin.nextInt();
        int S = stdin.nextInt();
        int F = stdin.nextInt();

        while (N != 0 && S != 0 && F != 0) {
            zeroTable(table);
            readEdges(stdin, S, table);
            finalStates.clear();
            readFinals(stdin, F, finalStates);

            System.out.println("Case " + Case++ + ":");
            System.out.print("    Length = 0, ");

            // the zero-length string is only accepted if state 1 is in
            // the final state set
            if (finalStates.contains(1)) {
                System.out.println("1 string accepted.");
            } else {
                System.out.println("0 strings accepted.");
            }

            // now, look at strings of length 1 to N
            for (int length = 1; length <= N; length++) {

                int acceptedCount = 0;

                // determine maximum integer for this string length
                int maxNum = (int) (Math.pow(2, length)) - 1;

                // generate the strings of this length
                for (int num = 0; num <= maxNum; num++) {

                    // use the Integer class to get a binary string,
                    // and pad it out with zeros on the left
                    String s = padString(Integer.toString(num, 2), 
                            length);

                    // see if the string is accepted
                    if (finalStates.contains(runDFA(s, table))) {
                        acceptedCount++;
                    }

                } // for each string of this length

                // dump stats for this length
                System.out.print("    Length = " + length + ", "
                        + acceptedCount + " ");
                if (acceptedCount == 1) {
                    System.out.println("string accepted.");
                } else {
                    System.out.println("strings accepted.");
                }

            } // for the string lengths 1 .. N

            N = stdin.nextInt();
            S = stdin.nextInt();
            F = stdin.nextInt();
        } // while there are more cases 

    } // main

    /**
     * Zero out the transition table.
     */
    private static void zeroTable(int[][] table) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 101; j++) {
                table[i][j] = 0;
            }
        }

    } // zeroTable

    /**
     * Read edge data into the transition table.
     */
    private static void readEdges(Scanner in, int S, int[][] table) {
        for (int i = 1; i <= S; i++) {
            table[0][i] = in.nextInt();
            table[1][i] = in.nextInt();
        }
    } // readEdges

    /**
     * Read final states into the final state set.
     */
    private static void readFinals(Scanner in, int F,
            Set<Integer> finalStates) {
        for (int i = 0; i < F; i++) {
            finalStates.add(in.nextInt());
        }

    } // readFinals

    /**
     * Execute the DFA on the input string, returning the final state the 
     * DFA ends in.
     */
    private static int runDFA(String s, int[][] table) {
        int state = 1;

        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '0') {
                state = table[0][state];
            } else {
                state = table[1][state];
            }
        }

        return state;
    } // runDFA

    /**
     * Pad a binary string on the left with zeros, up to the specified 
     * length.
     */
    private static String padString(String s, int length) {
        if (s.length() != length) {
            int n = length - s.length();
            for (int i = 0; i < n; i++) {
                s = "0" + s;
            }
        }

        return s;
    }

} // public class AcceptableStrings
