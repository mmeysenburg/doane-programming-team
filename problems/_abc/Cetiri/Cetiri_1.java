import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.StringTokenizer;

/**
 * Faster solution to Cetiri. Uses BufferedReader/InputStreamReader
 * combination vs. Scanner. 
 */
public class Cetiri_1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        StringTokenizer stk = new StringTokenizer(br.readLine());
        int[] arr = {0, 0, 0};
        for (int i = 0; i < arr.length; i++) {
            arr[i] = Integer.parseInt(stk.nextToken());
        }
        Arrays.sort(arr);
        int d1 = arr[1]-arr[0];
        int d2 = arr[2]-arr[1];
        if (d1 == d2) {
            System.out.println(arr[2] + d1);
        } else if (d1 < d2) {
            System.out.println(arr[1] + d1);
        } else {
            System.out.println(arr[0] + d2);
        }
    }
}