'''
Python solution to the Cetiri Kattis problem.
'''
# read input as sorted list of integers
arr = sorted([int(x) for x in input().split()])

# compute differences between the numbers
d1 = arr[1] - arr[0]
d2 = arr[2] - arr[1]

# if the differences are the same, missing number
# is before or after the sequence; we'll print the
# "after" number
if d1 == d2:
    print(arr[2] + d1)
elif d2 > d1:
    # if 2nd difference is larger, missing number
    # goes between 2nd and 3rd value
    print(arr[1] + abs(d1))
else:
    # otherwise, missisng number goes between 
    # 1st and 2nd value 
    print(arr[0] + abs(d2))