import java.util.*;

/**
 * Java solution to the Cetiri problem from Kattis.
 * 
 * @author Mark M. Meysenburg
 * @version 10/30/2019
 */
public class Cetiri {
    public static void main(String[] args) {
        // hold the three input numbers in an array, and then
        // sort ascending
        int[] array = {0, 0, 0};
        Scanner stdin = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            array[i] = stdin.nextInt();
        }
        Arrays.sort(array);

        // compute differences between the numbers
        int diff1 = array[1] - array[0];
        int diff2 = array[2] - array[1];

        // if differences are the same, the missing number
        // belongs before or after the input sequence
        if(diff1 == diff2) {
            System.out.println(array[2] + diff1);
        } 
        // if the second difference is larger, missing number 
        // goes between 2nd and 3rd value in the input
        else if(diff2 > diff1) {
            System.out.println(array[1] + Math.abs(diff1));
        } 
        // if the first difference is larger, missing number 
        // goes between 1st and 2nd value in the input
        else {
            System.out.println(array[0] + Math.abs(diff2));
        }
    }
}