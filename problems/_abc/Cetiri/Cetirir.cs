using System;

/**
 * C# solution to Cetiri Kattis problem.
 */
class Program {
  public static void Main (string[] args) {
    // read input, convert to int array, and sort
    String[] tokens = Console.ReadLine().Split(' ');
    int[] vals = new int[] {0, 0, 0};
    for(int i = 0; i < 3; i++) {
        vals[i] = Convert.ToInt32(tokens[i]);
    }
    Array.Sort(vals);

    // calculate the differences between values
    int d1 = vals[1] - vals[0];
    int d2 = vals[2] - vals[1];

    /* if the differences are the same, missing number
      is before or after the sequence; we'll print the
      "after" number
     */
    if(d1 == d2) {
        Console.WriteLine(vals[2] + d1);
    } else if(d2 > d1) {
      /* if 2nd difference is larger, missing number
        goes between 2nd and 3rd value
       */
        Console.WriteLine(vals[1] + Math.Abs(d1));
    } else {
        /* otherwise, missisng number goes between 
          1st and 2nd value
        */
        Console.WriteLine(vals[0] + Math.Abs(d2));
    }
  } // Main
} // Program