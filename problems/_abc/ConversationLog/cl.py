'''
Python solution to the Conversation Log problem.
'''
import sys

# read all lines of input
allinp = sys.stdin.readlines()

# dictionary of user,word-dictionary pairs, containing
# all the words used by each user
usersAndWords = {}

# dictionary with frequency of each word used
wordCounts = {}

# build the usersAndWords and wordCounts data structures
for i in range(1, len(allinp)):
    # split current line into tokens
    tokens = allinp[i].split()

    # is the user already recorded?
    if tokens[0] in usersAndWords:
        # yes! get their word dictionary and add all the 
        # words in this line
        wordDict = usersAndWords[tokens[0]]
        for j in range(1, len(tokens)):
            # in user's word dictionary, the values are just 
            # placeholder 0's
            wordDict[tokens[j]] = 0
            # count the word frequencies for this line
            wordCounts[tokens[j]] = wordCounts.get(tokens[j], 0) + 1
    else:
        # no! make a new word dictionary and start to populate it
        wordDict = {}
        for j in range(1, len(tokens)):
            # in user's word dictionary, the values are just 
            # placeholder 0's
            wordDict[tokens[j]] = 0
            # count word frequencies for this line
            wordCounts[tokens[j]] = wordCounts.get(tokens[j], 0) + 1
        # associate the newly created word dictionary with the
        # user
        usersAndWords[tokens[0]] = wordDict

# find the words used by everyone
allWords = []
for word in wordCounts:
    # for each word used...
    flag = True
    for user in usersAndWords:
        # ... if the word wasn't used by any user
        if word not in usersAndWords[user]:
            # ... set the flag so we won't add it
            flag = False
            break
    if flag:
        allWords.append(word)

# now output results
if len(allWords) == 0:
    print('ALL CLEAR')
else:
    # if there are words, sort them decreasing by frequency
    # then alphabetically 
    allWords.sort(key = lambda word: (-wordCounts[word], word))
    print('\n'.join(allWords))