# Python solution to Conversation Log Kattis problem
import sys

# read all of the input lines in one swoop
lines = sys.stdin.readlines()

# track words used by each author
authorWords = { }
for line in lines[1:]:
    line = line.split()
    author = line[0]
    authorWords[author] = authorWords.get(author, []) + line[1:]

# count number of times each word used
wordCounts = { }
for author in authorWords:
    for word in authorWords[author]:
        wordCounts[word] = wordCounts.get(word, 0) + 1

# create output words - tuples of count, word
outWords = []
for word in wordCounts:
    if wordCounts[word] >= len(authorWords):
        found = True
        for author in authorWords:
            if not word in authorWords[author]:
                found = False
                break
        if found:
            # negate word count in tuple so sort order
            # is correct 
            outWords.append((-wordCounts[word], word))

if len(outWords) == 0:
    print('ALL CLEAR')
else:
    # sort by count then alphabetical - tuples are sorted
    # by first field, then second by default
    outWords.sort()
    for ow in outWords:
        print(ow[1])