import java.io.*;
import java.util.*;

/**
 * Solution to the Conversation Log programming challenge problem.
 * 
 * @author Mark M. Meysenburg
 * @version 03/07/2019
 */
public class ConversationLog {
    /** 
     * Class for sorting words used by frequency, or alphabetically
     * if the frequencies are equal. 
     */
    private static class WNP implements Comparable<WNP> {
        String w;
        int n;

        public WNP(String w, int n) {
            this.w = w;
            this.n = n;
        } // constructor

        public int compareTo(WNP o) {
            if (n != o.n) {
                return o.n - n;
            } else {
                return w.compareTo(o.w);
            }
        } // compareTo
    }// WNP

    /**
     * Application entry point. 
     */
    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(
            new InputStreamReader(System.in));

        int M = Integer.parseInt(stdin.readLine());

        // make a list of words used by each author
        Map<String, List<String>> authorWords = new HashMap<>();
        for (int i = 0; i < M; i++) {
            String[] words = stdin.readLine().split(" ");
            if (!authorWords.containsKey(words[0])) {
                authorWords.put(words[0], new LinkedList<String>());
            }
            for (int j = 1; j < words.length; j++) {
                authorWords.get(words[0]).add(words[j]);
            }
        }

        // count how many time each word occurs
        Map<String, Integer> wordCounts = new HashMap<>();
        Set<String> names = authorWords.keySet();
        for (String name : names) {
            List<String> words = authorWords.get(name);
            for (String word : words) {
                if (!wordCounts.containsKey(word)) {
                    wordCounts.put(word, 1);
                } else {
                    int n = wordCounts.get(word);
                    n++;
                    wordCounts.put(word, n);
                }
            }
        }

        // collect words used by everyone
        Set<WNP> outWords = new TreeSet<>();
        Set<String> words = wordCounts.keySet();
        for (String word : words) {
            int n = wordCounts.get(word);
            if (n >= names.size()) {
                boolean found = true;
                for (String name : names) {
                    found = authorWords.get(name).contains(word);
                    if(!found) break;
                }
                if(found)
                    outWords.add(new WNP(word, n));
            }
        }

        // print words
        if (outWords.size() == 0) {
            System.out.println("ALL CLEAR");
        } else {
            for (WNP wnp : outWords) {
                System.out.println(wnp.w);
            }                
        }
    } // main
} // ConversationLog