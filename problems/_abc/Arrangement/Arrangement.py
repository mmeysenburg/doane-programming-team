'''
Python solution to the Kattis Arrangements problem
'''
N = int(input()) # number of rooms
M = int(input()) # number of teams

rr = M // N      # number of teams in a "regular" room
re = M % N       # number of rooms with 1 extra team

# print the rooms with extra teams
# Order doesn't matter, so we'll group them in
# successive lines
for i in range(re):
    # Python string repetition used to get the 
    # correct number of asterisks
    print('*' * (rr + 1)) 

# print the rooms with regular number of teams
for i in range(N - re):
    print('*' * rr)