
import java.util.*;

/**
 * Solution to "Cudoviste" programming competition problem. Where can
 * Mirko park his car? 
 */
public class Cudoviste {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int R = stdin.nextInt();
        int C = stdin.nextInt();
        // ****** GOTCHA! *******
        // if you're reading both ints and strings with a Scanner, 
        // you need to "eat" the endline after the ints before
        // reading lines
        String line = stdin.nextLine();

        char[][] map = new char[R][C];

        // read in the map, store in 2D array of chars
        for (int i = 0; i < R; i++) {
            line = stdin.nextLine();
            for (int j = 0; j < C; j++) {
                map[i][j] = line.charAt(j);
            }
        }
        
        stdin.close();

        // array holds number of spaces Mirko can park in while
        // squishing 0, 1, 2, 3, or 4 cars.
        int[] num = new int[5];

        // look at every possible upper-left position for Mirko's
        // car
        for (int i = 0; i < R - 1; i++) {
            for (int j = 0; j < C - 1; j++) {
                // if it is possible to park there (no buildings)
                if (isValid(map, i, j)) {
                    // increment the correct counter
                    num[numCars(map, i, j)]++;
                }
            }
        }

        // output the results
        for (int i = 0; i < 5; i++) {
            System.out.printf("%d\n", num[i]);
        }
    }

    /**
     * Is it possible to park the car in the location with 
     * a given upper-left coordinate? 
     * 
     * @param map 2D array holding the map
     * @param i Row of upper-left corner
     * @param j Column of upper-left corner
     * @return True if Mirko can park here, false otherwise
     */
    private static boolean isValid(char[][] map, int i, int j) {
        // if any of the four locations are a building, 
        // Mirko can't park there!
        return map[i][j] != '#'
                && map[i + 1][j] != '#'
                && map[i][j + 1] != '#'
                && map[i + 1][j + 1] != '#';
    }

    /**
     * Determine the number of cars Mirko will squish by parking in
     * a given location. 
     * 
     * @param map 2D array holding the map
     * @param row Row of upper-left corner
     * @param col Column of upper-left corner
     * @return Number of cars squished
     */
    public static int numCars(char[][] map, int row, int col) {
        int num = 0;
        // loop through the four spaces covered by the car,
        // counting all the other cars squished
        for (int i = row; i < row + 2; i++) {
            for (int j = col; j < col + 2; j++) {
                if (map[i][j] == 'X') {
                    num++;
                }
            }
        }
        return num;
    }
}
