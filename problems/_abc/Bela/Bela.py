# read parameters from first line of input
params = input().split()
N = int(params[0])
B = params[1]

# make a dictionary to hold the scores for each card. First value in 
# each tuple is the "Dominant" score, second value is the "Not dominant"
# score
scores = {'A':(11, 11), 'K':(4, 4), 'Q':(3, 3), 'J':(20, 2), 'T':(10, 10),
    '9':(14, 0), '8':(0, 0), '7':(0, 0)}

# read all of the hands, and accumulate the scores
totalScore = 0
for i in range(N):
    # each hand is 4 cards
    handScore = 0
    for j in range(4):
        line = input()
        # accumulate dominant or not dominant point value
        if line[1] == B:
            handScore += scores[line[0]][0]
        else:
            handScore += scores[line[0]][1]
    totalScore += handScore

# display results
print(totalScore)