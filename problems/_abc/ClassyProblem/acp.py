import sys

# dictionary used to convert class names to digits
classNums = {'lower':'3', 'middle':'2', 'upper':'1'}

'''
Function to convert a list of class names to a sortable
numeric representation. 
'''
def classNamesToNumber(classes):
    # class names to digits
    newClasses = [classNums[x] for x in classes]
    # reverse and pad to ten digits
    newClasses.reverse()
    lim = 10 - len(newClasses)
    for i in range(lim):
        newClasses.append('2')
    # return number version of the list o digits
    return int(''.join(newClasses))

# read all input
all_lines = sys.stdin.readlines()

T = int(all_lines[0])
line_idx = 1
for case_num in range(T):
    n = int(all_lines[line_idx])
    line_idx += 1
    pairs = []
    for person in range(n):
        tokens = all_lines[line_idx].split()
        classes = tokens[1].split('-')
        pairs.append((classNamesToNumber(classes), tokens[0][:-1]))
        line_idx += 1
    pairs.sort()
    for pair in pairs:
        print(pair[1])
    print('==============================')