
import java.util.*;

/**
 * Solution to "A Classy Problem" programming competition problem. 
 * Rank a series of people according to their classes. Idea is to
 * replace words "upper", "middle", and "lower" with numbers, then
 * sort based on the numbers. 
 */
public class ClassyProblem {

    /** 
     * inner class associating names with classes 
     */
    private static class Combo implements Comparable<Combo> {

        public String cl;
        public String name;

        public Combo(String cl, String name) {
            this.cl = cl;
            this.name = name;
        }

        @Override
        public int compareTo(Combo o) {
            // if classes are equal, 
            if (o.cl.equals(cl)) {
                // ... compare by name
                return name.compareTo(o.name);
            } else {
                // otheriwse compare by class; note reversed
                // order
                return o.cl.compareTo(cl);
            }
        }
    }

    /**
     * Application entry point.
     * 
     * @param args Command line arguments; ignored
     */
    public static void main(String[] args) {

        Scanner stdin = new Scanner(System.in);
        List<Combo> people = new ArrayList<>();

        // number of cases
        int T = stdin.nextInt();
        for (int t = 0; t < T; t++) {
            people.clear();

            // number of people in the case
            int n = stdin.nextInt();
            // eat newline character so we can read strings
            stdin.nextLine();

            // magnitude of largest class number, so we can 
            // fill shorter ones
            int mag = Integer.MIN_VALUE;

            for (int i = 0; i < n; i++) {
                // split input line based on spaces, colon, and '-'
                // (\\W actually splits on any "non-word" character)
                // name is toks[0]
                // classes start in toks[2]
                String[] toks = stdin.nextLine().split("\\W");
                
                // convert class names to numbers, backwards, since
                // highest precedence class name is on the right
                char[] classes = new char[toks.length - 3];
                int k = 0;
                for(int j = toks.length - 2; j >= 2; j--) {
                    switch(toks[j]) {
                        case "upper":
                            classes[k++] = '3';
                            break;
                        case "middle":
                            classes[k++] = '2';
                            break;
                        case "lower":
                            classes[k++] = '1';
                            break;
                    } // switch
                } // for j
                
                // track largest class number
                if(classes.length > mag) {
                    mag = classes.length;
                }
                
                // add person to the list
                people.add(new Combo(new String(classes), toks[0]));
            } // for i

            // pad each class with '2' so all are same length
            for(Combo c : people) {
                if(c.cl.length() < mag) {
                    StringBuilder sb  = new StringBuilder(c.cl);
                    for(int i = c.cl.length(); i < mag; i++) {
                        sb.append('2');
                    }
                    c.cl = sb.toString();
                }
            }
            
            // sort the list
            Collections.sort(people);
            
            // output the names
            for(Combo c : people) {
                System.out.println(c.name);
            }
            
            System.out.println("==============================");
        } // for t
    }
}
