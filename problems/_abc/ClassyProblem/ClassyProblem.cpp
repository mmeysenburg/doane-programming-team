#include <cstdlib>
#include <iostream>
#include <list> 
#include <string>

unsigned charToDigit(char c) {
    switch(c) {
        case 'u':
            return 1u;
        case 'm':
            return 2u;
        case 'l':
            return 3u;
        default:
            std::cerr << "ILLEGAL CHARACTER " << c << std::endl;
            exit(-1);
    }
}

unsigned classesToNum(std::string classes) {
    unsigned num = 0, digits = 0;

    for(int i = classes.length() - 1; i > 0; i--) {
        if(classes[i] == '-') {
            num *= 10u;
            num += charToDigit(classes[i + 1]);
            digits++;
        }
    }

    num *= 10u;
    num += charToDigit(classes[0]);
    digits++;

    while(digits < 10u) {
        num *= 10u;
        num += 2u;
        digits++;
    }

    return num;
}

class Pair {
public:
    Pair(std::string s, unsigned n) : name(s), number(n) { }

    bool operator<(const Pair& other) {
        if(number == other.number) {
            return name < other.name;
        } else {
            return number < other.number;
        }
    }

    std::string name;
    unsigned number;
};

int main() {
    int T, n, r;
    std::string name, classes, junk;
    std::list<Pair> pList;

    std::cin >> T;
    for(int caseNum = 0; caseNum < T; caseNum++) {
        pList.clear();
        std::cin >> n;
        for(int person = 0; person < n; person++) {
            std::cin >> name >> classes >> junk;
            name = name.substr(0, name.length() - 1);
            pList.push_back(Pair(name, classesToNum(classes)));
        }
        pList.sort();
        for(Pair p : pList) {
            std::cout << p.name << std::endl;
        }
        std::cout << "==============================" << std::endl;
    }

    return EXIT_SUCCESS;
}