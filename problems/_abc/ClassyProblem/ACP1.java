import java.io.*;
import java.util.*;

public class ACP1 {
    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(
            new InputStreamReader(System.in));
        Iterator<String> idx = br.lines().iterator();

        int T = Integer.parseInt(idx.next());

        HashMap<String, Integer> wordToNum = new HashMap<>();
        wordToNum.put("upper", 3);
        wordToNum.put("middle", 2);
        wordToNum.put("lower", 1);

        int i, j, n, person;
        String[] tokens;
        long classNum = 1;
        long[] classNumbers = new long[100];
        String[] names = new String[100];

        for (int caseNum = 0; caseNum < T; caseNum++) {
            n = Integer.parseInt(idx.next());
            for (person = 0; person < n; person++) {
                tokens = idx.next().split("\\W");
                // [mom, , 3, 3, 1, 2, class]

                // translate class names to numbers, with the rightmost
                // class the most significant digit
                classNum = wordToNum.get(tokens[tokens.length - 2]);
                j=0;
                for(i = tokens.length - 3; i >= 2; i--) {
                    classNum *= 10;
                    classNum += wordToNum.get(tokens[i]);
                    j++;
                }
                // pad the rest of the class number with 2s so all are
                // ten digits in length
                while(j < 10) {
                    classNum *= 10;
                    classNum += 2;
                    j++;
                }

                // store the name and class number in the parallel arrays
                names[person] = tokens[0];
                classNumbers[person] = classNum;
            } // for person in case

            // insertion sort names based on classNumbers
            for (i = 1; i < n; i++) {
                long key = classNumbers[i];
                String key1 = names[i];
                j = i - 1;
    
                while (j >= 0) {
                    if(classNumbers[j] < key) {
                        classNumbers[j + 1] = classNumbers[j];
                        names[j + 1] = names[j];
                    } else if(classNumbers[j] == key && names[j].compareTo(key1) > 0) {
                        classNumbers[j + 1] = classNumbers[j];
                        names[j + 1] = names[j];                        
                    } else {
                        break;
                    }
                    j--;
                }
                classNumbers[j + 1] = key;
                names[j + 1] = key1;
            }

            // output names and case ending line
            for(i = 0; i < n; i++) {
                System.out.println(names[i]);
            }
            System.out.println("==============================");
    
        } // for cases
    } // main
} // class ACP1