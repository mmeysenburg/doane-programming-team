import java.io.*;
import java.util.*;

/**
 * Java solution to "A Classy Problem" from Kattis.
 */
public class ACP2 {
    /**
     * Inner class representing class number/name pairs. 
     */
    private static class MyPair implements Comparable<MyPair> {
        public long cls;
        public String name;

        public MyPair(long cls, String name) {
            this.cls = cls;
            this.name = name;
        }

        @Override
        public int compareTo(MyPair o) {
            if (o.cls == cls) {
                return name.compareTo(o.name);
            } else {
                return Long.compare(o.cls, cls);
            }
        }
        
    }
    public static void main(String[] args) throws IOException {
        // read all of the input into a linked list at the start of the program
        LinkedList<String> allInput = new LinkedList<>();
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String line = br.readLine();
        while (line != null) {
            allInput.add(line);
            line = br.readLine();
        }
        Iterator<String> idx = allInput.iterator();

        // get number of test cases
        int T = Integer.parseInt(idx.next());

        // build a map to translate class names to class digits
        HashMap<String, Integer> wordToNum = new HashMap<>();
        wordToNum.put("upper", 3);
        wordToNum.put("middle", 2);
        wordToNum.put("lower", 1);

        // variables used in the loop
        int i, j, n, person;
        String[] tokens;
        long classNum = 1;
        MyPair[] pairs = new MyPair[100];

        // do all the cases
        for (int caseNum = 0; caseNum < T; caseNum++) {

            // how many names in this case?
            n = Integer.parseInt(idx.next());

            // do all the cases
            for (person = 0; person < n; person++) {
                tokens = idx.next().split("\\W");

                // translate class names to numbers, with the rightmost
                // class the most significant digit, e.g., 
                // [mom, , upper, upper, lower, middle, class] to
                // 2133222222/mom
                classNum = wordToNum.get(tokens[tokens.length - 2]);
                j=0;
                for(i = tokens.length - 3; i >= 2; i--) {
                    classNum *= 10;
                    classNum += wordToNum.get(tokens[i]);
                    j++;
                }
                // pad the rest of the class number with 2s so all are
                // ten digits in length
                while(j < 10) {
                    classNum *= 10;
                    classNum += 2;
                    j++;
                }

                // save this pair
                pairs[person] = new MyPair(classNum, tokens[0]);
            } // for person in case

            // use built-in sort to sort the portion of the pairs array
            // that have data for this case
            Arrays.sort(pairs, 0, n);

            // output names and case ending line
            for(i = 0; i < n; i++) {
                System.out.println(pairs[i].name);
            }
            System.out.println("==============================");
    
        } // for cases
    } // main
} // class ACP1