import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;

public class ACP {
    /**
     * Private inner class associating names with classes. 
     */
    private static class Pair implements Comparable<Pair> {
        public String cl;
        public String name;

        public Pair(String cl, String name) {
            this.cl = cl;
            this.name = name;
        }

        @Override
        public int compareTo(Pair o) {
            if (o.cl.equals(cl)) {
                return name.compareTo(o.name);
            } else {
                return o.cl.compareTo(cl);
            }
        }
        
    }

    public static void main(String[] args) throws IOException {
        // read all of the input into a String array
        String[] allInput = new String[60000];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int idx = 0;
        String line = br.readLine();
        while (line != null) {
            allInput[idx++] = line;
            line = br.readLine();
        }

        // process the input
        int T = Integer.parseInt(allInput[0]);
        idx = 1;
        ArrayList<Pair> pairs = new ArrayList<>(100);
        int mag = Integer.MIN_VALUE;
        for (int caseNum = 0; caseNum < T; caseNum++) {
            int n = Integer.parseInt(allInput[idx++]);
            for (int person = 0; person < n; person++) {
                String[] tokens = allInput[idx++].split("\\W");
                char[] classes = new char[tokens.length - 3];
                int k = 0;
                for (int j = tokens.length - 2; j >= 2; j--) {
                    switch (tokens[j]) {
                        case "upper":
                            classes[k++] = '3';
                            break;
                        case "middle":
                            classes[k++] = '2';
                            break;
                        case "lower":
                            classes[k++] = '1';
                            break;
                    } // switch
                } // for j

                if(classes.length > mag) {
                    mag = classes.length;
                }

                pairs.add(new Pair(new String(classes), tokens[0]));
            } // for person in case

            for (Pair p : pairs) {
                if(p.cl.length() < mag) {
                    StringBuilder sb = new StringBuilder(p.cl);
                    for (int i = p.cl.length(); i < mag; i++) {
                        sb.append('2');
                    }
                    p.cl = sb.toString();
                } // if needs padding
            } // for each person in the case

            Collections.sort(pairs);

            for(Pair p : pairs) {
                System.out.println(p.name);
            }

            System.out.println("==============================");
            pairs.clear();
            mag = Integer.MIN_VALUE;
        } // for case
    } // main
} // class ACP