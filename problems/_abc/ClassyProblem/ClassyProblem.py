'''
Python solution to "A Classy Problem" Kattis problem.
'''

# dictionary used to convert class names to digits
classNums = {'lower':'3', 'middle':'2', 'upper':'1'}

'''
Function to convert a list of class names to a sortable
numeric representation. 
'''
def classNamesToNumber(classes):
    # class names to digits
    newClasses = [classNums[x] for x in classes]
    # reverse and pad to ten digits
    newClasses.reverse()
    lim = 10 - len(newClasses)
    for i in range(lim):
        newClasses.append('2')
    # return number version of the list o digits
    return int(''.join(newClasses))


# how many cases?
T = int(input())

# process each case
for c in range(T):
    # how many names?
    n = int(input())

    pairs = [] # list to hold class number / name tuples

    # read in data for this case
    for i in range(n):
        # get name an class names for person i
        tokens = input().split()
        name = tokens[0][:-1]
        classes = tokens[1].split('-')

        # add class number, name tuple to the pairs list
        pairs.append((classNamesToNumber(classes), name))
    
    # sort and print. The sort list method sorts tuples
    # on field 0 first, then field 1, etc., so ties will
    # be sorted alphabetically by name, which is the desired
    # behavior.
    pairs.sort()
    for pair in pairs:
        print(pair[1])
    print('==============================')