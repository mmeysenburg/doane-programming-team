'''
Python solution to Autori.
'''
# add a hyphen prefix to the input so we don't need to consider
# special cases
full = '-' + input()

# use list comprehension to create a list of the first letters of
# each name in the input -- the characters after each hyphen
short = [full[i + 1] for i in range(len(full)) if full[i] == '-']

# list join w/o spaces to form the output
print(''.join(short))