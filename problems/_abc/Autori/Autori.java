
import java.util.*;

/**
 * Solution to "Autori" programming competition problem. 
 * Translate reference strings like "Knuth-Morris-Pratt" into
 * abbreviations like "KMP".
 */
public class Autori {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // read the only line of input
        String line = stdin.nextLine();
        stdin.close();

        StringBuilder output = new StringBuilder();
        // first character of the string is always in our 
        // abbreviated output
        output.append(line.charAt(0));
        // now can for any dashes in the rest of the string
        for (int i = 1; i < line.length(); i++) {
            // if current character is a dash...
            if (line.charAt(i) == '-') {
                // append next character to the output
                output.append(line.charAt(i + 1));
                i++; // skip the character we just added
            }
        }

        // output the abbreviation
        System.out.println(output.toString());
    }
}
