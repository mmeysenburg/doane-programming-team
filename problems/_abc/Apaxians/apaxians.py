'''
Python solution to the Apaxiaaaaaaaaaaaans! Kattis problem.
'''
name = input()

if len(name) == 1:
    # single character names need no further work
    print(name)
else: 
    # otherwise, remove repeated letters
    sName = ''
    i = 0
    while i < len(name):
        sName += name[i]
        j = i + 1
        while j < len(name) and name[j] == name[i]:
            j += 1
        i = j

    print(sName)