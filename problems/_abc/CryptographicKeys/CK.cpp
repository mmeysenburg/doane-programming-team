#include <cstdlib>
#include <iostream>
#include <map>
#include <cmath>

int main() {
    using namespace std;
    int origN, N;
    cin >> N;
    origN = N;
    map<int, int> factorCounts;
    int i = 2;
    int cap = (int)(ceil(sqrt(N)));
    while(i <= cap) {
        if(N % i) {
            i++;
        } else {
            N /= i;
            auto f = factorCounts.find(i);
            if(f == factorCounts.end()) {
                factorCounts[i] = 1;
            } else {
                f->second++;
            }
        }
    }
    if(N == origN) {
        cout << N << endl;
        return EXIT_SUCCESS;
    } else if(N > 1) {
        if(factorCounts.find(N) == factorCounts.end()) {
            factorCounts[N] = 1;
        } else {
            factorCounts[N] ++;
        }
    }

    int maxVal = 0, maxFactor = 0;
    for(auto i = factorCounts.begin(); i != factorCounts.end(); i++) {
        if(i->second > maxVal) {
            maxVal = i->second;
            maxFactor = i->first;
        }
    }

    cout << maxFactor << endl;

    return EXIT_SUCCESS;
}