'''
Python solution to the Cryptographic Keys problem.
'''
import math

N = int(input())
origN = int(N)

# strategy: perform a prime factorization of N, keeping 
# track of how many times each factor occurs. The factor
# that occurs most frequently is the answer!
factorCounts = {}
i = 2
cap = int(math.ceil(math.sqrt(N)))
while i <= cap:
    if N % i:
        i += 1
    else:
        N //= i
        factorCounts[i] = factorCounts.get(i, 0) + 1
# special case: if N was prime, N is the answer
if N == origN:
    print(origN)
    exit()
elif N > 1:
    # add the last factor if it was not 1
    factorCounts[i] = factorCounts.get(N, 0) + 1

# now, search the factor / counts dictionary, to find the
# factor that occurs most frequently. If there is a tie,
# the smallest factor will be reported. 
maxVal = 0
maxFactor = 0
for i in factorCounts:
    if factorCounts[i] > maxVal:
        maxVal = factorCounts[i]
        maxFactor = i

print(maxFactor)