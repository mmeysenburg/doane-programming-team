"""
Python solution to the Calories From Fat problem on Kattis.

Mark M. Meysenburg
12/21/2020
"""

def makeNumeric(tokens):
    """
    Turn input lines into numeric format. Percentages are
    made negative numbers in [0, 1].
    """
    for i in range(5):
        # choose g to Cal multiplier
        if i == 0:
            mul = 9
        elif 1 <= i <=3:
            mul = 4
        else:
            mul = 7

        if tokens[i][-1] == 'g':
            # grams to Cal float
            tokens[i] = float(tokens[i][:-1]) * mul
        elif tokens[i][-1] == 'C':
            # Cal str to Cal float
            tokens[i] = float(tokens[i][:-1])
        else:
            # % Cal to -% Cal float
            tokens[i] = float(tokens[i][:-1]) / -100.0

def calcSums(tokens):
    """
    Sum up the percents and non-percents in a line
    """
    npSum = 0
    pSum = 0
    for token in tokens:
        if token < 0:
            pSum += token
        else:
            npSum += token

    return (npSum, -pSum)

def makeCals(tokens, npSum, pSum):
    """
    Turn everything into Cal
    """
    # find total Cal for line
    tCal = npSum + (pSum * npSum) / (1.0 - pSum)
    # change percents to Cal
    for i in range(5):
        if tokens[i] < 0:
            tokens[i] = -tokens[i] * tCal

def accumulate(tokens, acc):
    """
    Accumulate line into acc
    """
    for i in range(5):
        acc[i] += tokens[i]
        
############################################################
# priming read of the first line
line = input()

# continue while we still have cases
while line[0] != '-':
    acc = [0, 0, 0, 0, 0]
    tokens = line.split()
    # read all lines in a case, accumulate total calories
    while tokens[0] != '-':
        # read input and make strings numeric
        makeNumeric(tokens)
        # get total of percent and total of Cal
        (npSum, pSum) = calcSums(tokens)
        # convert everthing to Cals
        makeCals(tokens, npSum, pSum)
        # accumulate Cals
        accumulate(tokens, acc)

        # next line in case
        tokens = input().split()

    # print the results
    cff = round(acc[0] / sum(acc) * 100)
    print('{0:d}%'.format(cff))

    # next case or terminate?
    line = input()