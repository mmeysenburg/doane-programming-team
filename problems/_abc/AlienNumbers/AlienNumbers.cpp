#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

int sourceToDecimal(string num, string lang) {
  int rightmost = num.length() - 1;
  int radix = lang.length();
  int mul = 1;
  int val = 0;
  char digit = ' ';
  for (int i = rightmost; i >= 0; i--) {
    digit = num[i];
    val += lang.find(digit) * mul;
    mul *= radix;
  }
  return val;
}

string decimalToTarget(int val, string lang) {
  int radix = lang.length();
  string tar = "";
  int remainder = 0;

  while (val != 0) {
    remainder = val % radix;
    val /= radix;
    tar = lang[remainder] + tar;
  }

  return tar;
}

int main() {
  int T;
  cin >> T;

  for (int caseNum = 1; caseNum <= T; caseNum++) {
    string alienNumber, sourceLanguage, destLanguage;

    cin >> alienNumber;
    cin >> sourceLanguage;
    cin >> destLanguage;
    printf("Case #%d: %s\n", caseNum,
           decimalToTarget(sourceToDecimal(alienNumber, sourceLanguage),
                           destLanguage)
               .c_str());
  }

  return EXIT_SUCCESS;
}