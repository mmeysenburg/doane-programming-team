
import java.util.*;

/**
 * Solution to "Alien Numbers" programming competition problem. 
 * Convert numbers from one arbitrary base / representation to 
 * another arbitrary system.
 */
public class AlienNumbers {

    /**
     * Convert from an alien system to decimal.
     * 
     * @param num String holding the alien number
     * @param lang String holding the "digits" for the system
     * @return Decimal value of the number
     */
    private static int sourceToDecimal(String num, String lang) {
        // index of rightmost "digit"
        int rightmost = num.length() - 1;
        // radix of the system
        int radix = lang.length();
        // multiplier for positional values
        int mul = 1;
        // accumulator for the decimal value
        int val = 0;
        
        // accumulate the place values from the alien number
        for (int i = rightmost; i >= 0; i--) {
            // position index * positional multiplier;
            // use indexOf() to search the alien system string
            val += lang.indexOf(num.charAt(i)) * mul;
            // increment to new power of the base
            mul *= radix;
        }
        
        return val;
    }

    /**
     * Convert a decimal value to an alien number system.
     * @param val Decimal value to convert
     * @param lang String holding the "digits" for the system
     * @return String holding the value in the alien system
     */
    private static String decimalToTarget(int val, String lang) {
        // radix of the alien number system
        int radix = lang.length();
        // accumulator for our result
        String tar = "";
        // value left over after dividing by radix
        int remainder = 0;

        // keep dividing by radix until we reach zero
        while (val != 0) {
            // get remainder
            remainder = val % radix;
            // divide by radix
            val /= radix;
            // next alien digit, from right to left, is 
            // the character corresponding to the remainder
            // in the alien system
            tar = lang.charAt(remainder) + tar;
        }

        return tar;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int T = stdin.nextInt();

        for (int i = 1; i <= T; i++) {
            String alienNumber = stdin.next();
            String source = stdin.next();
            String target = stdin.next();

            // convert source number to decimal, then that value
            // to the destination alien system
            System.out.println("Case #" + i + ": "
                    + decimalToTarget(
                            sourceToDecimal(alienNumber, source), 
                            target));
        }
        
        stdin.close();
    }
}
