import sys

'''
Solution to "Alien Numbers" programming competition problem.
Convert numbers from one arbitrary base / representation to
another arbitrary system.
'''

'''
Convert from an alien system to decimal.

Parameters
---------- 
    - num String holding the alien number
    - lang String holding the "digits" for the system

Returns
-------
    Decimal value of the number
'''
def sourceToDecimal(num, lang):
    radix = len(lang)   # radix of the language
    val = 0             # accumulator of decimal value
    mul = 1             # multiplier for positional values

    # convert each alien "digit" to decimal and accumulate
    for c in num[::-1]:
        val += lang.find(c) * mul
        mul *= radix
    return val

'''
Convert a decimal value to an alien number system.

Parameters
----------
    - num Decimal value to convert
    - lang String holding the "digits" for the system

Returns 
-------
    String holding the value in the alien system
'''
def decimalToTarget(num, lang):
    radix = len(lang)   # radix of the alien number system
    tar = ''            # accumulator string for result
    remainder = 0

    # keep dividing by radix until we reach zero
    while num != 0:
        remainder = num % radix
        num //= radix
        tar = lang[remainder] + tar
    return tar

# read all input at once
lines = sys.stdin.readlines()
caseNo = 1

# process the valid input lines
for case in lines[1:]:
    tokens = case.split()
    num = tokens[0]

    source = tokens[1]
    target = tokens[2]

    res = decimalToTarget(sourceToDecimal(num, source), target)

    print('Case #{0:d}: {1:s}'.format(caseNo, res))
    
    caseNo += 1