#include <cstdlib>
#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

void markPixels(char map[100][100], int m, int n, int row, int col) {
    if(row >= 0 && row < m && col >= 0 && col < n) {
        if(map[row][col] == '-') {
            map[row][col] = 'm';
            markPixels(map, m, n, row - 1, col);
            markPixels(map, m, n, row, col + 1);
            markPixels(map, m, n, row + 1, col);
            markPixels(map, m, n, row, col - 1);
        }
    }
}

int countStars(char map[100][100], int m, int n) {
    int num = 0;

    for(int row = 0; row < m; row++) {
        for(int col = 0; col < n; col++) {
            if(map[row][col] == '-') {
                num++;
                markPixels(map, m, n, row, col);
            } // if star
        } // for col
    } // for row
    return num;
}

int main()
{
    char map[100][100];
    int m, n, stars, caseNum = 1;
    string line;

    while(cin >> m) {
        cin >> n;
        for(int row = 0; row < m; row++) {
            cin >> line;
            for(int col = 0; col < n; col++) {
                map[row][col] = line[col];
            }
        }

        stars = countStars(map, m, n);

        printf("Case %d: %d\n", caseNum, stars);
        caseNum++;
    }
    return EXIT_SUCCESS;
}
