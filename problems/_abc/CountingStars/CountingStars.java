import java.util.*;

/**
 * Solution to the Counting Stars programming competition problem. 
 * 
 * @author Mark M. Meysenburg
 * @version 10/11/2018
 */
public class CountingStars {

    /**
     * Given a pixel that is part of a star, mark it and all of the 
     * adjacent pixels with a different symbol, so that the star is 
     * not counted twice. This method uses a recursive algorithm.
     * 
     * @param img Two dimensional array of characters holding the img
     * @param row Row of the pixel to consider
     * @param col Column of the pixel to consider
     */
    private static void markPixels(char[][] img, int row, int col) {
        // first pass base case: is the coordinate actaully on the img?
        if(row >= 0 && row < img.length && 
            col >= 0 && col < img[row].length) {

            // 2nd base case: only proceed if the pixel is part of a star
            if(img[row][col] == '-') {
                // mark the current pixel so it won't be considered again
                img[row][col] = 'm';

                // and then check all the adjacent pixels
                markPixels(img, row - 1, col);  // north
                markPixels(img, row, col + 1);  // east
                markPixels(img, row + 1, col);  // south
                markPixels(img, row, col - 1);  // west
            } // if star pixel
        } // if on the img
    } // markPixels

    /**
     * Count the number of stars in a img of pixels.
     * 
     * @param img Two dimensional array of characters holding the img
     * @return number of stars on the img.
     */
    private static int countStars(char[][] img) {
        int num = 0;

        // examine each pixel in the img, looking for part of a star
        // (represented by '-')
        for(int row = 0; row < img.length; row++) {
            for(int col = 0; col < img[row].length; col++) {
                // found a star? count it, mark it, and mark all of its 
                // adjacent star pixels with another symbol so we don't
                // count it again
                if(img[row][col] == '-') {
                    num++;
                    markPixels(img, row, col);
                } // if star
            } // for col
        } // for row

        return num;
    } // countStars

    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this program.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        // image array and case number
        char[][] img = new char[100][100];  
        int caseNum = 1;

        // read input until end of file
        while(stdin.hasNextLine()) {
            // read and parse dimensions line
            String line = stdin.nextLine();
            StringTokenizer stk = new StringTokenizer(line);
            int m = Integer.parseInt(stk.nextToken());
            int n = Integer.parseInt(stk.nextToken());

            // store the star img
            for(int row = 0; row < m; row++) {
                line = stdin.nextLine();
                for(int col = 0; col < n; col++) {
                    img[row][col] = line.charAt(col);
                }
            }

            // find number of stars
            int stars = countStars(img);

            // output results
            System.out.printf("Case %d: %d\n", caseNum, stars);
            caseNum++;
        } // while we have lines
    } // main
} // class