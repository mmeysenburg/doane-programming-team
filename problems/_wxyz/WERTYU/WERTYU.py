import sys

# left to right, top to bottom string of all possible keys
keys = "`1234567890-=QWERTYUIOP[]\\ASDFGHJKL;'ZXCVBNM,./"

# build a dictionary that translates an input character to 
# one character left on the keyboard
map = {keys[i]:keys[i-1] for i in range(1, len(keys))}

# the space character remains the same
map[' '] = ' '

# process each line 
for line in sys.stdin.readlines():
    # replace input w/ output characters and print
    print(''.join([map[ch] for ch in line[:-1]]))