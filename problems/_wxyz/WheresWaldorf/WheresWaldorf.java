
import java.util.*;

/**
 * Solution to Where's Waldorf programming competition question.
 *
 */
public class WheresWaldorf {

    /**
     * Word we're searching for.
     */
    private static char[] word;

    /**
     * Grid of letters to search in.
     */
    private static char[][] grid;

    /**
     * Number of lines in the grid.
     */
    private static int m;

    /**
     * Number of columns in the grid.
     */
    private static int n;

    /**
     * Determine if there is a match northwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean north(int r, int c) {
        int y = r - 1;
        for (int i = 1; i < word.length; i++) {
            if (y < 1 || grid[y][c] != word[i]) {
                return false;
            }
            y--;
        }
        return true;
    }

    /**
     * Determine if there is a match northeastwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean northEast(int r, int c) {
        int x = c + 1;
        int y = r - 1;
        for (int i = 1; i < word.length; i++) {
            if (y < 1 || x > n || grid[y][x] != word[i]) {
                return false;
            }
            x++;
            y--;
        }
        return true;
    }

    /**
     * Determine if there is a match eastwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean east(int r, int c) {
        int x = c + 1;
        int y = r;
        for (int i = 1; i < word.length; i++) {
            if (x > n || grid[y][x] != word[i]) {
                return false;
            }
            x++;
        }
        return true;
    }

    /**
     * Determine if there is a match southeastwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean southEast(int r, int c) {
        int x = c + 1;
        int y = r + 1;
        for (int i = 1; i < word.length; i++) {
            if (y > m || x > n || grid[y][x] != word[i]) {
                return false;
            }
            x++;
            y++;
        }
        return true;
    }

    /**
     * Determine if there is a match southwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean south(int r, int c) {
        int x = c;
        int y = r + 1;
        for (int i = 1; i < word.length; i++) {
            if (y > m || grid[y][x] != word[i]) {
                return false;
            }
            y++;
        }
        return true;
    }

    /**
     * Determine if there is a match southwestwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean southWest(int r, int c) {
        int x = c - 1;
        int y = r + 1;
        for (int i = 1; i < word.length; i++) {
            if (y > m || x < 1 || grid[y][x] != word[i]) {
                return false;
            }
            x--;
            y++;
        }
        return true;
    }

    /**
     * Determine if there is a match westwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean west(int r, int c) {
        int x = c - 1;
        int y = r;
        for (int i = 1; i < word.length; i++) {
            if (x < 1 || grid[y][x] != word[i]) {
                return false;
            }
            x--;
        }
        return true;
    }

    /**
     * Determine if there is a match northwestwards.
     *
     * @param r Row of first letter of the word.
     * @param c Column of the first letter of the word.
     */
    private static boolean northWest(int r, int c) {
        int x = c - 1;
        int y = r - 1;
        for (int i = 1; i < word.length; i++) {
            if (y < 1 || x < 1 || grid[y][x] != word[i]) {
                return false;
            }
            x--;
            y--;
        }
        return true;
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored by this application.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        grid = new char[51][51];

        int cases = Integer.parseInt(stdin.nextLine().trim());

        for (int c = 0; c < cases; c++) {
            // skip blank line separating cases
            stdin.nextLine();

            // for cases after 1, separate output by a blank line
            if (c != 0) {
                System.out.println();
            }

            // get size of grid
            m = stdin.nextInt();
            n = stdin.nextInt();
            stdin.nextLine();

            // read grid
            for (int i = 1; i <= m; i++) {
                String line = stdin.nextLine().toUpperCase();
                for (int j = 1; j <= n; j++) {
                    grid[i][j] = line.charAt(j - 1);
                }
            }

            // get number of words
            int k = Integer.parseInt(stdin.nextLine().trim());

            // find each word in the grid
            for (int i = 0; i < k; i++) {
                word = stdin.nextLine()
                        .trim().toUpperCase().toCharArray();

                boolean found = false;
                for (int row = 1; row <= m && !found; row++) {
                    for (int col = 1; col <= n && !found; col++) {
                        if (grid[row][col] == word[0]) {
                            found = north(row, col) 
                                    || northEast(row, col)
                                    || east(row, col) 
                                    || southEast(row, col)
                                    || south(row, col) 
                                    || southWest(row, col)
                                    || west(row, col) 
                                    || northWest(row, col);

                            if (found) {
                                System.out.printf("%d %d\n", row, col);
                            }
                        } // matched first character
                    } // for columns
                } // for rows
            } // for each word			
        } // for each case
    } // main
} // class
