#include <cstdio>
#include <cstdlib>
#include <ctype.h>

char word[80];

char grid[51][51];

int m, n, wLen;

bool north(int r, int c) {
  int y = r - 1;
  for (int i = 1; i < wLen; i++) {
    if (y < 1 || grid[y][c] != word[i]) {
      return false;
    }
    y--;
  }
  return true;
}

bool northEast(int r, int c) {
  int x = c + 1;
  int y = r - 1;
  for (int i = 1; i < wLen; i++) {
    if (y < 1 || x > n || grid[y][x] != word[i]) {
      return false;
    }
    x++;
    y--;
  }
  return true;
}

bool east(int r, int c) {
  int x = c + 1;
  int y = r;
  for (int i = 1; i < wLen; i++) {
    if (x > n || grid[y][x] != word[i]) {
      return false;
    }
    x++;
  }
  return true;
}

bool southEast(int r, int c) {
  int x = c + 1;
  int y = r + 1;
  for (int i = 1; i < wLen; i++) {
    if (y > m || x > n || grid[y][x] != word[i]) {
      return false;
    }
    x++;
    y++;
  }
  return true;
}

bool south(int r, int c) {
  int x = c;
  int y = r + 1;
  for (int i = 1; i < wLen; i++) {
    if (y > m || grid[y][x] != word[i]) {
      return false;
    }
    y++;
  }
  return true;
}

bool southWest(int r, int c) {
  int x = c - 1;
  int y = r + 1;
  for (int i = 1; i < wLen; i++) {
    if (y > m || x < 1 || grid[y][x] != word[i]) {
      return false;
    }
    x--;
    y++;
  }
  return true;
}

bool west(int r, int c) {
  int x = c - 1;
  int y = r;
  for (int i = 1; i < wLen; i++) {
    if (x < 1 || grid[y][x] != word[i]) {
      return false;
    }
    x--;
  }
  return true;
}

bool northWest(int r, int c) {
  int x = c - 1;
  int y = r - 1;
  for (int i = 1; i < wLen; i++) {
    if (y < 1 || x < 1 || grid[y][x] != word[i]) {
      return false;
    }
    x--;
    y--;
  }
  return true;
}

int main() {

  int cases;
  scanf("%d", &cases);
  for (int c = 0; c < cases; c++) {

    // for cases after 1, separate output by a blank line
    if (c != 0) {
      printf("\n");
    }

    // get size of the grid
    scanf("%d %d", &m, &n);

    // read grid
    for (int i = 1; i <= m; i++) {
      scanf("%s", word);
      for (int j = 1; j <= n; j++) {
        grid[i][j] = toupper(word[j - 1]);
      }
    }

    // get number of words
    int k;
    scanf("%d", &k);

    // find each word in the grid
    for (int i = 0; i < k; i++) {
      scanf("%s", word);
      char* s = word;
      wLen = 0;
      while(*s) {
        *s = toupper(*s);
        s++;
        wLen++;
      }

      bool found = false;
      for (int row = 1; row <= m && !found; row++) {
        for (int col = 1; col <= n && !found; col++) {
          if (grid[row][col] == word[0]) {
            found = north(row, col) || northEast(row, col) || east(row, col) ||
                    southEast(row, col) || south(row, col) ||
                    southWest(row, col) || west(row, col) ||
                    northWest(row, col);

            if (found) {
              printf("%d %d\n", row, col);
            }
          } // matched first character
        }   // for col
      }     // for row
    }       // for each word
  }         // for cases

  return EXIT_SUCCESS;
}