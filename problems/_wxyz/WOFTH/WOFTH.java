import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class WOFTH {
    private static long sum = 0L;

    private static void h(long n) {
        if (n == 1L) {
            return;
        } else if(n % 2L == 0L) {
            n /= 2L;
            sum += n;
            h(n);
        } else {
            n = 3L * n + 1L;
            sum += n;
            h(n);
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        sum = Long.parseLong(stdin.readLine());

        if (sum == 1L) {
            System.out.println(1);
        } else {
            h(sum);
            System.out.println(sum);
        }
    }
}