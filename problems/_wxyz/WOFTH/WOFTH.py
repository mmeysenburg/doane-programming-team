sum = 0

def h(n):
    global sum
    if n == 1:
        return 
    elif n % 2 == 0:
        n //= 2
        sum += n
        h(n)
    else:
        n = 3 * n + 1
        sum += n
        h(n)

n = int(input())

if n == 1:
    print(1)
else:
    sum = n
    h(n)
    print(sum)