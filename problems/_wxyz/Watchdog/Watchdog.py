"""
Python solution to the Watchdog problem on Kattis.

Mark M. Meysenburg
12/21/2020
"""
import math

def dis(pos1, pos2):
    """
    Return the Euclidean distance between two (x, y) points.
    """
    return math.sqrt( (pos2[0] - pos1[0]) ** 2 + 
        (pos2[1] - pos1[1]) ** 2 )

########################################################################
N = int(input())
for caseNo in range(N):
    # read S, H, and hatch locations for this case
    tokens = input().split()
    S = int(tokens[0])
    H = int(tokens[1])
    hatches = [ ]
    for hatch in range(H):
        tokens = input().split()
        hatches.append((int(tokens[0]), int(tokens[1])))

    # process this case
    anchors = [ ]
    # iterate thru all the possible anchor points that won't let the dog
    # fall off the roof
    for start in range(1, S):
        for x in range(start, S - start + 1):
            for y in range(start, S - start + 1):

                # find out which hatches are covered
                covered = 0
                for hPos in hatches:
                    if hPos != (x, y) and dis(hPos, (x, y)) <= start:
                        covered += 1

                # if we covered them all, add to possible anchor points
                if covered == len(hatches):
                    anchors.append((x, y))

    # output results for this case
    if len(anchors) == 0:
        print('poodle')
    else:
        anchors.sort()
        print(anchors[0][0], anchors[0][1])