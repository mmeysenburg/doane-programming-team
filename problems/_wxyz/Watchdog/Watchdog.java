import java.awt.Point;
import java.io.*;
import java.util.*;

public class Watchdog {

    private static class ComparePoints implements Comparator<Point> {
        @Override
        public int compare(Point p1, Point p2) {
            if(p1.x != p2.x) {
                return p1.x - p2.x;
            } else {
                return p1.y - p2.y;
            }
        }
    }

    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(
            new InputStreamReader(System.in));

        int N = Integer.parseInt(stdin.readLine());

        List<Point> hatcthes = new LinkedList<>();
        List<Point> anchors = new LinkedList<>();
        String[] tokens = null;
        int S, H, cap, covered;
        Point p = new Point();
        ComparePoints cp = new ComparePoints();

        for (int i = 0; i < N; i++) {
            tokens = stdin.readLine().split(" ");
            S = Integer.parseInt(tokens[0]);
            H = Integer.parseInt(tokens[1]);

            hatcthes.clear();
            for (int j = 0; j < H; j++) {
                tokens = stdin.readLine().split(" ");
                hatcthes.add(new Point(Integer.parseInt(tokens[0]), 
                    Integer.parseInt(tokens[1])));
            }

            anchors.clear();
            for(int start = 1; start < S; start++) {
                cap = S - start + 1;
                for(int x = start; x < cap; x++) {
                    for(int y = start; y < cap; y++) {
                        covered = 0;
                        p = new Point(x, y);
                        for(Point q : hatcthes) {
                            if(!p.equals(q) && p.distance(q.x, q.y) <= start) {
                                covered++;
                            }
                        }

                        if(covered == hatcthes.size()) {
                            anchors.add(p);
                        }
                    }
                }
            }

            if(anchors.isEmpty()) {
                System.out.println("poodle");
            } else {
                anchors.sort(cp);
                System.out.println(anchors.get(0).x + " " + anchors.get(0).y);
            }
        }
    }
}