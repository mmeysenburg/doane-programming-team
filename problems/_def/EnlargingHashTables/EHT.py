'''
Python solution to the Kattis Enlarging Hash Tables problem.
This is fast enough to get into the top 10 Python solutions
on the Kattis leaderboard.
'''
import math
import sys

# this dictionary will keep track of primality tests we have
# already done, so we don't test the same number twice.
primeSaves = {}

def isPrime(n):
    '''
    Return true if the number n is prime.
    '''
    # if we've already tested n, return the answer
    v = primeSaves.get(n, -1)
    if v != -1:
        return v
    
    # otherwise, brute force test for primality
    cap = int(math.sqrt(n)) + 1
    for i in range(2, cap):
        if n % i == 0:
            # save the primality result
            primeSaves[n] = False
            return False
    # save the primality result
    primeSaves[n] = True
    return True

# real all of the input values (except the trailing 0)
# in one operation
vals = [int(x) for x in sys.stdin.readlines()[:-1]]

# process each case
for n in vals:
    # find smallest prime > 2n
    isP = isPrime(n)
    nextN = n * 2 + 1
    while not isPrime(nextN):
        # increase nextN by 2, b/c a prime must be odd
        nextN += 2 
    
    # report results
    if isP:
        print(nextN)
    else:
        print('{0:d} ({1:d} is not prime)'.format(nextN, n))