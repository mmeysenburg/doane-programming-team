#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <map>

using namespace std;

map<int, int> primeResults;

bool isPrime(int n) {
    if(primeResults.find(n) != primeResults.end()) {
        return primeResults[n] == 1;
    }

    int cap = (int) sqrt(n);
    for(int k = 2; k <= cap; k++) {
        if(n % k == 0) {
            primeResults[n] = 0;
            return false;
        }
    }
    primeResults[n] = 1;
    return true;
}

int main() {
    int n, nextN; 
    bool wasInitPrime;

    cin >> n; 
    while(n != 0) {
        wasInitPrime = isPrime(n);

        nextN = 2 * n + 1;
        while(!isPrime(nextN)) {
            nextN += 2;
        }

        if(wasInitPrime) {
            printf("%d\n", nextN);
        } else {
            printf("%d (%d is not prime)\n", nextN, n);
        }
        cin >> n;
    }
    return EXIT_SUCCESS;
}