import java.util.Scanner;

/**
 * Solution to the Enlarging Hash Tables programming challenge problem.
 * 
 * @author Mark M. Meysenburg
 * @version 10/11/2018
 */
public class EHT2 {
    /**
     * Determine if a number n is prime, by checking all divisors
     * between 2 and sqrt(n).
     * 
     * @param n Number to check
     * @return true if n is prime, false otherwise
     */
    private static boolean isPrime(int n) {
        int cap = (int)Math.sqrt(n);
        for(int k = 2; k <= cap; k++) {
            if(n % k == 0) {
                return false;
            }
        }
        return true;
    }

    /**
     * Application entry point. 
     * 
     * @param args Command line arguments; ignored by this app. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();

        while(n != 0 ) {
            // remember if n was prime to start with
            boolean wasInitPrime = isPrime(n);

            // find next possible hash table size
            int nextN = 2 * n + 1;

            // keep incrementing until the size is prime
            while(!isPrime(nextN)) {
                nextN += 2;
            }

            // output as required
            if(wasInitPrime) {
                System.out.println(nextN);
            } else {
                System.out.println(nextN + " (" + n + " is not prime)");
            }

            // get next value of n
            n = stdin.nextInt();
        }
    }
}