import java.math.BigInteger;
import java.util.*;

/**
 * Solution to the Enlarging Hash Tables programming challenge problem.
 * Illustrates how to check if a number n is *probably* prime, to a 
 * really high degree of certainty. Faster than doing a definitive 
 * primality test. 
 * 
 * @author Mark M. Meysenburg
 * @version 09/29/2018
 */
public class EHT1 {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // priming read
        int n = stdin.nextInt();

        // flag to remember primality of n
        boolean wasInitPrime = false;

        while(n != 0) {
            // was n initially prime (probably)?  here we use a 
            // BigInteger to find out with probability 1 - 0.5^20. 
            wasInitPrime = 
                (new BigInteger(Integer.toString(n))).isProbablePrime(20);

            // compute 2n + 1 and convert into a BigInteger
            int nextN = (n << 1) + 1;
            BigInteger biN = new BigInteger(Integer.toString(nextN));

            // keep going until we find a prime
            while(!biN.isProbablePrime(20)) {
                // (2n + 1) is odd, so only check the following
                // odd numbers for primailty
                nextN += 2;
                biN = new BigInteger(Integer.toString(nextN));
            }

            // output result
            if(wasInitPrime) {
                System.out.printf("%d\n", nextN);
            } else {
                System.out.printf("%d (%d is not prime)\n", nextN, n);
            }
            
            n = stdin.nextInt();
        } // while
    } // main
} // EHT1