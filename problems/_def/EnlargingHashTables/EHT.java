import java.util.*;

public class EHT {

    private static HashSet<Integer> nonPrimes = new HashSet<>();

    private static boolean isPrime(int n) {
        if(nonPrimes.contains(n)) {
            return false;
        }

        if(n <= 1) {
            nonPrimes.add(n);
            return false;
        } else if(n <= 3) {
            return true;
        } else if (n % 2 == 0 || n % 3 == 0) {
            nonPrimes.add(n);
            return false;
        } 
        int i = 5;
        while(i * i <= n) {
            if(n % i == 0 || n % (i + 2) == 0) {
                nonPrimes.add(n);
                return false;
            }
            i += 6;
        }
        return true;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // flag for initial prime
        boolean wasInitPrime = false;

        // priming read
        int n = stdin.nextInt();
        while(n != 0) {
            // initially prime?
            wasInitPrime = isPrime(n);

            // double n the fast way
            int nextN = (n << 1) + 1;

            // find next prime
            while(!isPrime(nextN)) {
                nextN += 2;
            }

            // output result
            if(wasInitPrime) {
                System.out.printf("%d\n", nextN);
            } else {
                System.out.printf("%d (%d is not prime)\n", nextN, n);
            }

            // next number
            n = stdin.nextInt();
        }
    }
}