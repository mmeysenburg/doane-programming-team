import sys

'''
Strategy: use approximation for log of n! divided by log 2 to get 
number of bits for various values of n, match those up with the 
appropriate word sizes from the problem, and hardwire the results
into a dictionary. Then the problem is just a dictionary lookup
for each input year. The commented-out code below was used in the
process. 
'''
# import math
# def bitsForFacN(n):
#     m = n * (1 + 4 * n * (1 + 2 * n))

#     a = math.log(n) - 1
#     b = 1 / 3 * math.log(1 / 30 + m) + math.log(math.pi)
#     bits = (n * a + 0.5 * b) / math.log(2)
#     return int(math.ceil(bits))

# n = 3
# nb = bitsForFacN(n)
# while nb <= 4194304:
#     print(n, nb)
#     n += 1
#     nb = bitsForFacN(n)

# wordSizes = {}
# wordSize = 4
# for year in range(1960, 2161, 10):
#     wordSizes[year] = wordSize
#     wordSize *= 2

# for year in wordSizes:
#     print(year, wordSizes[year])

data = [int(x.replace('\n', '')) // 10 * 10 
    for x in sys.stdin.readlines()]

facstones = {1960:3, 1970:5, 1980:8, 1990:12, 2000:20, 2010:34, 2020:57,
     2030:98, 2040:170, 2050:300, 2060:536, 2070:966, 2080:1754, 
     2090:3210, 2100:5910, 2110:10944, 2120:20366, 2130:38064,
    2140:71421, 2150:134480, 2160:254016}

for year in data[:-1]:
    print(facstones[year])