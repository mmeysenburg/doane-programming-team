import sys

inp = sys.stdin.readlines()
dict = {}
for line in inp:
    outLine = ''
    for word in line.split():
        if word.upper() in dict:
            outLine += '. '
        else:
            dict[word.upper()] = -1
            outLine += word + ' '
    print(outLine)