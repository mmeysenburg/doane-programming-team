import java.util.*;

/**
 * Solution to "Filip" programming competition problem. Input two 
 * three digit numbers, and interpret them right to left vs. left to
 * right. Then, output the larger value.
 */
public class Filip {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        // split line into a two element array
        String[] sNums = stdin.nextLine().split(" ");

        // interpret A in right-to-left manner. In Unicode, the
        // digits are all consecutive, so (int)('0' - '0') is 0,
        // (int)('1' - '1') is 1, and so on. Get the int value 
        // for each digit, multiply by appropriate power of 10, and
        // add up the results.
        int A = (int) (sNums[0].charAt(0) - '0')
                + (int) (sNums[0].charAt(1) - '0') * 10
                + (int) (sNums[0].charAt(2) - '0') * 100;

        int B = (int) (sNums[1].charAt(0) - '0')
                + (int) (sNums[1].charAt(1) - '0') * 10
                + (int) (sNums[1].charAt(2) - '0') * 100;

        // output largest value in normal decimal notation
        if (A < B) {
            System.out.println(B);
        } else {
            System.out.println(A);
        }
    }
}
