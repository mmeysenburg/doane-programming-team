#include <cstdlib>
#include <iostream>

int main() {
    using namespace std;
    int seen[1000];
    int Q, k, f0, f1, f2, n, i, caseNum;

    cin >> Q;
    for(caseNum = 0; caseNum < Q; caseNum++) {
        cin >> k;
        for(i = 0; i < k; i++) {
            seen[i] = -1;
        }
        f0 = 1;
        f1 = 1;
        n = 2;
        while(true) {
            f2 = (f1 + f0) % k;
            if(seen[f2] != -1) {
                cout << seen[f2] << endl;
                break;
            }
            seen[f2] = n;
            
            f0 = f1;
            f1 = f2;
            n++;            
        }
    }
    
    return EXIT_SUCCESS;
}