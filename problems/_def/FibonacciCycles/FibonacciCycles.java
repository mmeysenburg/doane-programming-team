import java.util.*;

/**
 * Solution to the Fibonacci Cycles problem on Kattis.
 * 
 * @author Mark M. Meysenburg
 * @version 04/09/2020
 */
public class FibonacciCycles {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int Q = Integer.parseInt(stdin.nextLine());

        // maintain an array of k ints, representing values we've seen. 
        // -1 means we haven't seen the value yet. 
        int[] seen = new int[1000];

        // process the Q cases
        for (int caseNum = 0; caseNum < Q; caseNum++) {

            int k = Integer.parseInt(stdin.nextLine());

            // reset seen values that are possible for this case
            for (int i = 0; i < k; i++) {
                seen[i] = -1;
            }

            // do the Fibonacci sequence modulo k
            int f0 = 1, f1 = 1, f2, n = 2;
            while (true) {
                f2 = (f0 + f1) % k;

                // if we've seen this value % k before, we're done! Output the
                // n value that first produced that value and move on to the 
                // next case.
                if (seen[f2] != -1) {
                    System.out.println(seen[f2]);
                    break;
                }

                // if we haven't seen this value before, save the value of n
                // that produced the value and move on
                seen[f2] = n;

                f0 = f1;
                f1 = f2;
                n++;

            } // while we haven't seen a repeat

        } // for cases

    } // main

}  // class 