'''
Python solution to the Fibonacci Cycles problem on Kattis.
Modified to use a list rather than a dictionary.
'''
import sys
# read all input into a list of integers
ks = [int(x) for x in sys.stdin.readlines()]

# do the thing! (ignore Q in the list)
for k in ks[1:]:
    # create list for tracking seen values
    seen = [-1] * 1001

    # base cases for the Fibonacci sequence
    f0 = 1
    f1 = 1
    f2 = 0
    n = 2

    # go until we find a duplicate
    while True:
        # next value modulo k
        f2 = (f1 + f0) % k

        # check the dictionary - have we seen f2?
        if seen[f2] != -1:
            print(seen[f2])
            break

        # if not, record the n, get ready for next f2 
        seen[f2] = n 

        f0 = f1
        f1 = f2
        n += 1