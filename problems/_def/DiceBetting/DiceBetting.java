import java.util.*;

/**
 * Dynamic programming solution to the Kattis Dice Betting problem.
 * 
 * @author Mark M. Meysenburg
 * @version 05/11/2021
 */
public class DiceBetting {
    
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();
        int s = stdin.nextInt();
        int k = stdin.nextInt();

        double[][] dp = new double[n + 1][s + 1];
        dp[0][0] = 1;

        for(int i = 1; i <= n; i++) {
            for(int j = 1; j <= s; j++) {
                double pn = (s - (j - 1)) / (double)s;
                double po = j / (double)s;
                double wn = pn * dp[i - 1][j - 1];
                double wo = po * dp[i - 1][j];
                dp[i][j] = wn + wo;
            }
        }

        double prob = 0;
        for(int i = k; i <= s; i++) {
            prob += dp[n][i];
        }

        System.out.println(prob);

    }
}