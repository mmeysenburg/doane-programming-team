using System;

namespace EN
{
    class Name
    {
        static void Main() {
            int n = Convert.ToInt32(Console.ReadLine());
            double fact = 1.0, e = 1.0;
            for(int i = 1; i <= n; i++) {
                fact *= i;
                e += 1.0 / fact;
            }
            Console.WriteLine(e);
        }
    }
}