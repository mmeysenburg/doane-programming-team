import java.util.*;

/**
 * Solution to Problem E from the 2018 North Central North America 
 * programming contest, Euler's Number.
 *
 * @author Mark M. Meysenburg
 * @version 11/03/2018
 */
public class EulersNumber {
	/**
	 * Application entry point. 
	 * 
	 * @param args Command-line arguments; ignored by this program.
	 */
	public static void main(String[] args) {
		Scanner stdin = new Scanner(System.in);
		int n = stdin.nextInt();
		
		// Keep the value of the factorial denominator as a running
		// product, so we only do one multiplication per loop 
		// iteration. In effect, we're calculating n! only once,
		// and using the intermediate values 0!, 1!, 2!, ..., along
		// the way. 
		double fact = 1.0;
		
		// accumulator for e; we start with the first term already
		// in the sum
		double e = 1.0;
		
		// now accumulate the terms for [1, 2, 3, ..., n]. 
		for(int i = 1; i <= n; i++) {
			fact *= i;			// factorial value for current term
			e += (1.0 / fact);	// accumulate into e
		}
		
		// output the result!
		System.out.println(e);
		
	} // main
	
} // ProbE