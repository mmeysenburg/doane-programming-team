#include <cstdio>
#include <cstdlib>
#include <iostream>

int main() {
    int n;
    std::cin >> n;

    long double fact = 1.0, e = 1.0;

    for(int i = 1; i <= n; i++) {
        fact *= i;
        e += 1.0 / fact;
    }

    printf("%1.16Lf\n", e);

    return EXIT_SUCCESS;
}