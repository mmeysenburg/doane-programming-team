n = int(input())
fact = 1.0
e = 1.0
for i in range(1, n + 1):
    fact *= i
    e += 1.0 / fact

print(e)