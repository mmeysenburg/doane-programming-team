'''
Python solution to the Detailed Differences problem.
'''
n = int(input())

for c in range(n):
    line1 = input()
    line2 = input()
    line3 = ''
    # iterate through each char in the pair of lines
    for a,b in zip(line1, line2):
        # build new string based on the characters
        if a == b:
            line3 += '.'
        else:
            line3 += '*'
    # output results
    print(line1)
    print(line2)
    print(line3)
    print('')