using System;

class DD {
    public static void Main(string[] args) {
        int n = Convert.ToInt32(Console.ReadLine());
        for(int i = 0; i < n; i++) {
            string line1 = Console.ReadLine();
            string line2 = Console.ReadLine();
            string line3 = "";
            for(int j = 0; j < line1.Length; j++) {
                if(line1[j] == line2[j]) {
                    line3 += ".";
                } else {
                    line3 += "*";
                }
            }
            Console.WriteLine(line1);
            Console.WriteLine(line2);
            Console.WriteLine(line3);
            Console.WriteLine();
        }
    }
}