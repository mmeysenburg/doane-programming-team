using System;

namespace Faktor
{
    class Faktor
    {
        static void Main() {
            string[] line = Console.ReadLine().Split(' ');
            int A = Convert.ToInt32(line[0]);
            int I = Convert.ToInt32(line[1]);
            Console.WriteLine((A * I) - A + 1);
        }
    }
}