
import java.util.*;

/**
 * Solution to "Faktor" programming competition problem. 
 * How many scientists do we need to bribe?
 */
public class Faktor {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // number of articles planned
        int A = stdin.nextInt();
        // required impact factor
        int I = stdin.nextInt();

        // find number of bribes -- number of anticipated
        // citations less the number of articles
        int B = (A * I) - A + 1;

        System.out.println(B);
    }
}
