N, B, H, W = input().split()
N = int(N)
B = int(B)
H = int(H)
W = int(W)

prices = []
beds = []

for i in range(H):
    prices.append(float(input()))
    weeklyBeds = input().split()
    for j in range(W):
        weeklyBeds[j] = int(weeklyBeds[j])
    beds.append(weeklyBeds)

cost = B + 1
for h in range(H):
    for w in range(W):
        if beds[h][w] >= N:
            newCost = N * prices[h]
            if newCost <= cost:
                cost = newCost
                break

if cost <= B:
    print(int(cost))
else:
    print('stay home')