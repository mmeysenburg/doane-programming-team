# read priming n
n = int(input())

# loop until sentinel value is reached
while not n == 0:
    amounts = []    # list of amounts spent
    sum = 0         # total amount spent

    # read in each student's amount spent
    for i in range(n):
        # use total number of cents to avoid
        # issues with rounding
        f = input()
        dot = f.find('.')
        amt = 100 * int(f[0:dot]) + int(f[dot + 1:])

        sum += amt
        amounts.append(amt)

    # we need two averages, one for cases where
    # total spending is a perfect multiple of the
    # number of students; the other for when that's not
    # the case
    avg1 = sum // n
    if sum % n != 0:
        avg2 = avg1 + 1
    else:
        avg2 = avg1
    
    # count dollars greater or less than the averages
    amtAbove = 0
    amtBelow = 0

    for dollars in amounts:
        if dollars > avg2:
            amtAbove += (dollars - avg2)
        if dollars < avg1:
            amtBelow += (avg1 - dollars)

    # find the appropriate amount
    answer = max(amtAbove, amtBelow) / 100.0

    # output results
    print("$%.2f" % (answer))

    n = int(input())