import java.util.Scanner;

public class TheTrip {
    public static void main(String[] args) {
        int[] amounts = new int[1001];
        Scanner stdin = new Scanner(System.in);
        int n = Integer.parseInt(stdin.nextLine());
        while(n != 0) {
            int sum = 0;
            for (int i = 0; i < n; i++) {
                String f = stdin.nextLine();
                int dot = f.indexOf('.');
                amounts[i] = 100 * 
                    Integer.parseInt(f.substring(0, dot)) +
                    Integer.parseInt(f.substring(dot + 1));
                sum += amounts[i];
            }

            int lowAvg = sum / n;
            int hiAvg = (sum % n == 0) ? lowAvg : lowAvg + 1;

            int sumAbove = 0, sumBelow = 0;
            for (int i = 0; i < n; i++) {
                if (amounts[i] > hiAvg) {
                    sumAbove += amounts[i] - hiAvg;
                }
                if (amounts[i] < lowAvg) {
                    sumBelow += lowAvg - amounts[i];
                } 
            }     
            
            int answer = Math.max(sumAbove, sumBelow);

            System.out.printf("$%.2f\n", answer / 100.0);

            // get next value of n
            n = Integer.parseInt(stdin.nextLine());
        }
    }
}