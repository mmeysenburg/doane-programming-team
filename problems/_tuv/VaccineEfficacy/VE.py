import sys

allInp = [x.strip() for x in sys.stdin.readlines()[1:]]

realGroup = [x for x in allInp if x[0] == 'Y']
ctrlGroup = [x for x in allInp if x[0] != 'Y']

realInfA = 0
realInfB = 0
realInfC = 0
for case in realGroup:
    if case[1] == 'Y':
        realInfA += 1
    if case[2] == 'Y':
        realInfB += 1
    if case[3] == 'Y':
        realInfC += 1

ctrlInfA = 0
ctrlInfB = 0
ctrlInfC = 0
for case in ctrlGroup:
    if case[1] == 'Y':
        ctrlInfA += 1
    if case[2] == 'Y':
        ctrlInfB += 1
    if case[3] == 'Y':
        ctrlInfC += 1

realSize = len(realGroup)
realRateA = realInfA / realSize
realRateB = realInfB / realSize
realRateC = realInfC / realSize

ctrlSize = len(ctrlGroup)
ctrlRateA = ctrlInfA / ctrlSize
ctrlRateB = ctrlInfB / ctrlSize
ctrlRateC = ctrlInfC / ctrlSize

if realRateA < ctrlRateA:
    print('{0:0.6F}'.format(100* (1 - realRateA / ctrlRateA)))
else:
    print('Not effective')

if realRateB < ctrlRateB:
    print('{0:0.6F}'.format(100* (1 - realRateB / ctrlRateB)))
else:
    print('Not effective')

if realRateC < ctrlRateC:
    print('{0:0.6F}'.format(100* (1 - realRateC / ctrlRateC)))
else:
    print('Not effective')
