# Alternate Python solution to the Ultimate Binary Watch 
# Kattis problem. This saves binary representations for
# each digit in a dictionary
db = {'0':'....', '1':'...*', '2':'..*.', 
    '3':'..**', '4':'.*..', '5':'.*.*', '6':'.**.',
    '7':'.***', '8':'*...', '9':'*..*'}

# make a list of strings of binaries for each digit
f = [db[c] for c in input()]

# transpose the strings so they have most significant
# bit at the top
for r in range(4):
    print('{0:s} {1:s}   {2:s} {3:s}'.format(f[0][r], 
        f[1][r], f[2][r], f[3][r]))