# Python solution to the Ultimate Binary Watch 
# Kattis problem. 

# prepare a 2D array of characters representing
# the watch face
faceRow = [c for c in '. .   . .']
face = [faceRow[:]]
face.append(faceRow[:])
face.append(faceRow[:])
face.append(faceRow[:])

def lights(col, bin):
    '''
    turn on the lights in the specified column of the face, 
    based on the binary number bin
    '''
    r = 3
    for c in bin[::-1]:
        if c == '1':
            face[r][col] = '*'
        r -= 1

# read the time as a string
iStr = input()

# turn on the lights on the display
h1 = str(bin(int(iStr[0])))[2:]
lights(0, h1)

h2 = str(bin(int(iStr[1])))[2:]
lights(2, h2)

m1 = str(bin(int(iStr[2])))[2:]
lights(6, m1)

m2 = str(bin(int(iStr[3])))[2:]
lights(8, m2)

# print the watch face
for line in face:
    print(''.join(line))