using System;
using System.Collections.Generic;

/**
 * C# solution to Ultimate Binary Watch problem
 */
class Program {
  public static void Main (string[] args) {
    /*
     * char to string dictionary to translate between digits and
     * binary representations.
     */
    var db = new Dictionary<char, string>() {
        {'0', "...."}, {'1', "...*"}, {'2', "..*."},
        {'3', "..**"}, {'4', ".*.."}, {'5', ".*.*"},
        {'6', ".**."}, {'7', ".***"}, {'8', "*..."},
        {'9', "*..*"}
    };

    // read input line
    string inp = Console.ReadLine();

    // build the watch face
    string[] face = new string[4];
    for(int i = 0; i < 4; i++) {
        face[i] = db[inp[i]];
    }

    // output the watch face, transposing so the most
    // significant bit is at the top
    for(int i = 0; i < 4; i++) {
        Console.WriteLine(string.Format("{0:s} {1:s}   {2:s} {3:s}",
        face[0][i], face[1][i], face[2][i], face[3][i]));
    }
  } // Main
} // Program