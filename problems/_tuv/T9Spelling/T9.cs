using System;
using System.Collections.Generic;

/**
 * C# solution to T9 Spelling Kattis problem.
 */
class Program {
  public static void Main (string[] args) {
    /*
     * char to string dictionary to translate between letters and
     * keypresses.
     */
    var ltrToNums = new Dictionary<char, string>() {
      {'a', "2"}, {'b', "22"}, {'c', "222"}, 
      {'d', "3"}, {'e', "33"}, {'f', "333"}, 
      {'g', "4"}, {'h', "44"}, {'i', "444"},
      {'j', "5"}, {'k', "55"}, {'l', "555"}, 
      {'m', "6"}, {'n', "66"}, {'o', "666"}, 
      {'p', "7"}, {'q', "77"}, {'r', "777"}, {'s', "7777"},
      {'t', "8"}, {'u', "88"}, {'v', "888"}, 
      {'w', "9"}, {'x', "99"}, {'y', "999"}, {'z', "9999"}, {' ', "0"}
    };

    // read # of cases
    int n = Convert.ToInt32(Console.ReadLine());

    // process each case
    for(int i = 0; i < n; i++) {
      string inp = Console.ReadLine();

      // no special processing required for the 1st char
      char co = inp[0];
      string outp = ltrToNums[co];

      // save previous char
      char cx = co;

      // look at the rest of chars in the line
      for(int j = 1; j < inp.Length; j++) {
        // would the keypress for this char match 
        // one we just used?
        co = inp[j];
        if(ltrToNums[co][0] == ltrToNums[cx][0]) {
          // if so, place a space in output
          outp += " ";
        }

        // append the keypresses
        outp += ltrToNums[co];
        cx = co;
      }

      // write output for this case
      Console.WriteLine("Case #{0}: {1}", (i + 1), outp);

    } // for
  } // Main
} // Program