import sys

# read all input
lines = sys.stdin.readlines()

# dictionary translating characters into key presses
ltrToNum = {'a':'2', 'b':'22', 'c':'222', 
    'd':'3', 'e':'33', 'f':'333', 
    'g':'4', 'h':'44', 'i':'444',
    'j':'5', 'k':'55', 'l':'555', 
    'm':'6', 'n':'66', 'o':'666', 
    'p':'7', 'q':'77', 'r':'777', 's':'7777',
    't':'8', 'u':'88', 'v':'888', 
    'w':'9', 'x':'99', 'y':'999', 'z':'9999', ' ':'0'}

i = 1
# skip the 1st line, process the rest
for line in lines[1:]:
    # we don't have to worry about a pause for the 1st letter
    co = line[0]
    out = ltrToNum[co]

    # now take care of the rest of the line
    for c in line[1:-1]:
        # see if we have to append a pause
        no = ltrToNum[co][0]
        n = ltrToNum[c[0]][0]
        if n == no:
            out += ' '
        # append keypresses
        out += ltrToNum[c]
        # keep track of previous character
        co = c

    # output the keypress translation
    print(f'Case #{i}: {out}')
    i += 1
