T = int(input())
for i in range(T):
    params = [int(x) for x in input().split()]
    P = params[0]
    R = params[1]
    F = params[2]
    year = 0
    while P <= F:
        P *= R
        year += 1
    print(year)