import math

'''
Python solution to the Tajna programming competition problem.
'''
inp = input()
n = len(inp)

'''
Find the largest number of rows for the decrypting matrix.
'''
def findR(n):
    # start at max possible and go down
    for r in range((int)(math.sqrt(n)), 1, -1):
        if n % r == 0:
            return r
    return 1

# determine rows and columns
r = findR(n)
c = n // r

# now decrypt. mat holds the decrypted text
mat = [[' '] * c for i in range(r)]
# idx is the index into the ciphertext
idx = 0
# loop through row, column indices in column-major order
for iC in range(c):
    for iR in range(r):
        # grab next character of plaintext
        mat[iR][iC] = inp[idx]
        idx += 1

# put the output together row by row
pt = ''
for r in mat:
    pt += ''.join(r)

print(pt)