using System;

class TTS {
    static void Main() {
        int n = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(n % 2 == 0 ? "Bob" : "Alice");
    }
}