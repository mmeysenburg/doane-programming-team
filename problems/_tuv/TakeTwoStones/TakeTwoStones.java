
import java.util.*;

/**
 * Solution to "Take Two Stones" programming competition problem. 
 * Who will win this thrilling game? 
 */
public class TakeTwoStones {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int N = stdin.nextInt();

        // if N is even Bob wins, because Alice moves first, and 
        // each has to take two stones; at the end, on Bob's 
        // turn, there are 0 stones left, and since 0 is not odd,
        // Bob wins
        if (N % 2 == 0) {
            System.out.println("Bob");
        } else {
            // if N is initially odd, Alice wins, because there will
            // be one stone left when it is Bob's final turn
            System.out.println("Alice");
        }
    }
}
