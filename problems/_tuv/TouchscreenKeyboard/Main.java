import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        
        // Read the input string (the string to be typed)
        String text = scanner.nextLine();
        
        // Read the number of rows and columns in the keyboard layout
        int rows = scanner.nextInt();
        int cols = scanner.nextInt();
        scanner.nextLine();  // Consume the remaining newline
        
        // Prepare the layout of the keyboard
        char[][] layout = new char[rows][cols];
        Map<Character, int[]> charPositions = new HashMap<>();
        
        // Read the keyboard layout and store the positions of each character
        for (int i = 0; i < rows; i++) {
            String row = scanner.nextLine();
            for (int j = 0; j < cols; j++) {
                char c = row.charAt(j);
                layout[i][j] = c;
                charPositions.put(c, new int[] { i, j }); // Store row and column position of the character
            }
        }
        
        // Calculate the number of swipes
        int totalSwipes = 0;
        int[] prevPosition = charPositions.get(text.charAt(0)); // Start at the first character
        
        for (int i = 1; i < text.length(); i++) {
            char currentChar = text.charAt(i);
            int[] currentPosition = charPositions.get(currentChar);
            
            // Calculate Manhattan distance from the previous character to the current character
            int distance = Math.abs(prevPosition[0] - currentPosition[0]) + Math.abs(prevPosition[1] - currentPosition[1]);
            totalSwipes += distance;
            
            // Update previous position
            prevPosition = currentPosition;
        }
        
        // Output the total number of swipes
        System.out.println(totalSwipes);
        
        scanner.close();
    }
}
