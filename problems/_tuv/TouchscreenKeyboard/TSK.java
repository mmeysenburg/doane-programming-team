import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.io.IOException;

public class TSK {
    private static class Pair implements Comparable<Pair> {
        public String word;
        public int distance = 0;

        @Override
        public int compareTo(Pair other) {
            if (distance != other.distance) {
                return distance - other.distance;
            } else {
                return word.compareTo(other.word);
            }
        }

        @Override
        public String toString() {
            return word + " " + distance;
        }
    }

    private static int[][] dists = {{0, 5, 3, 2, 3, 3, 4, 5, 8, 6, 7, 8, 7, 6, 9, 10, 1, 4, 1, 5, 7, 4, 2, 2, 6, 1},
    {5, 0, 2, 3, 4, 2, 1, 2, 5, 3, 4, 5, 2, 1, 6, 7, 6, 3, 4, 2, 4, 1, 5, 3, 3, 4},
    {3, 2, 0, 1, 2, 2, 3, 4, 7, 5, 6, 7, 4, 3, 8, 9, 4, 3, 2, 4, 6, 1, 3, 1, 5, 2},
    {2, 3, 1, 0, 1, 1, 2, 3, 6, 4, 5, 6, 5, 4, 7, 8, 3, 2, 1, 3, 5, 2, 2, 2, 4, 3},
    {3, 4, 2, 1, 0, 2, 3, 4, 5, 5, 6, 7, 6, 5, 6, 7, 2, 1, 2, 2, 4, 3, 1, 3, 3, 4},
    {3, 2, 2, 1, 2, 0, 1, 2, 5, 3, 4, 5, 4, 3, 6, 7, 4, 1, 2, 2, 4, 1, 3, 3, 3, 4},
    {4, 1, 3, 2, 3, 1, 0, 1, 4, 2, 3, 4, 3, 2, 5, 6, 5, 2, 3, 1, 3, 2, 4, 4, 2, 5},
    {5, 2, 4, 3, 4, 2, 1, 0, 3, 1, 2, 3, 2, 1, 4, 5, 6, 3, 4, 2, 2, 3, 5, 5, 1, 6},
    {8, 5, 7, 6, 5, 5, 4, 3, 0, 2, 1, 2, 3, 4, 1, 2, 7, 4, 7, 3, 1, 6, 6, 8, 2, 9},
    {6, 3, 5, 4, 5, 3, 2, 1, 2, 0, 1, 2, 1, 2, 3, 4, 7, 4, 5, 3, 1, 4, 6, 6, 2, 7},
    {7, 4, 6, 5, 6, 4, 3, 2, 1, 1, 0, 1, 2, 3, 2, 3, 8, 5, 6, 4, 2, 5, 7, 7, 3, 8},
    {8, 5, 7, 6, 7, 5, 4, 3, 2, 2, 1, 0, 3, 4, 1, 2, 9, 6, 7, 5, 3, 6, 8, 8, 4, 9},
    {7, 2, 4, 5, 6, 4, 3, 2, 3, 1, 2, 3, 0, 1, 4, 5, 8, 5, 6, 4, 2, 3, 7, 5, 3, 6},
    {6, 1, 3, 4, 5, 3, 2, 1, 4, 2, 3, 4, 1, 0, 5, 6, 7, 4, 5, 3, 3, 2, 6, 4, 2, 5},
    {9, 6, 8, 7, 6, 6, 5, 4, 1, 3, 2, 1, 4, 5, 0, 1, 8, 5, 8, 4, 2, 7, 7, 9, 3, 10},
    {10, 7, 9, 8, 7, 7, 6, 5, 2, 4, 3, 2, 5, 6, 1, 0, 9, 6, 9, 5, 3, 8, 8, 10, 4, 11},
    {1, 6, 4, 3, 2, 4, 5, 6, 7, 7, 8, 9, 8, 7, 8, 9, 0, 3, 2, 4, 6, 5, 1, 3, 5, 2},
    {4, 3, 3, 2, 1, 1, 2, 3, 4, 4, 5, 6, 5, 4, 5, 6, 3, 0, 3, 1, 3, 2, 2, 4, 2, 5},
    {1, 4, 2, 1, 2, 2, 3, 4, 7, 5, 6, 7, 6, 5, 8, 9, 2, 3, 0, 4, 6, 3, 1, 1, 5, 2},
    {5, 2, 4, 3, 2, 2, 1, 2, 3, 3, 4, 5, 4, 3, 4, 5, 4, 1, 4, 0, 2, 3, 3, 5, 1, 6},
    {7, 4, 6, 5, 4, 4, 3, 2, 1, 1, 2, 3, 2, 3, 2, 3, 6, 3, 6, 2, 0, 5, 5, 7, 1, 8},
    {4, 1, 1, 2, 3, 1, 2, 3, 6, 4, 5, 6, 3, 2, 7, 8, 5, 2, 3, 3, 5, 0, 4, 2, 4, 3},
    {2, 5, 3, 2, 1, 3, 4, 5, 6, 6, 7, 8, 7, 6, 7, 8, 1, 2, 1, 3, 5, 4, 0, 2, 4, 3},
    {2, 3, 1, 2, 3, 3, 4, 5, 8, 6, 7, 8, 5, 4, 9, 10, 3, 4, 1, 5, 7, 2, 2, 0, 6, 1},
    {6, 3, 5, 4, 3, 3, 2, 1, 2, 2, 3, 4, 3, 2, 3, 4, 5, 2, 5, 1, 1, 4, 4, 6, 0, 7},
    {1, 4, 2, 3, 4, 4, 5, 6, 9, 7, 8, 9, 6, 5, 10, 11, 2, 5, 2, 6, 8, 3, 3, 1, 7, 0}};

    private static int charToInt(char c) {
        return (int)(c - 'a');
    }

    public static void main(String[] args) throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(br.readLine());
        for (int i = 0; i < n; i++) {
            ArrayList<Pair> pairs = new ArrayList<>();
            String[] line_1 = br.readLine().split(" ");
            int m = Integer.parseInt(line_1[1]);
            for (int j = 0; j < m; j++) {
                String word = br.readLine();
                Pair p = new Pair();
                p.word = word;
                for(int k = 0; k < word.length(); k++) {
                    p.distance += dists[charToInt(line_1[0].charAt(k))][charToInt(word.charAt(k))];
                } // for other lines in test cases
                pairs.add(p);
            } // for 1st line in test cases

            // sort in the specified order
            Collections.sort(pairs);

            // print results
            for (Pair pair : pairs) {
                System.out.println(pair);
            }

        } // for test cases
    }
}