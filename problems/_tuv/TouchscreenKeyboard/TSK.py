'''
Python solution to the Touchscreen Keyboard 
Kattis problem.
'''
def charToInt(c):
    '''
    Translate character 'a' - 'z' to int 0 - 25.
    '''
    return ord(c) - ord('a')

# arrangement of keyboard
keybd = ['qwertyuiop', 'asdfghjkl', 'zxcvbnm']
# letters for building distances matrix
alpha = 'abcdefghijklmnopqrstuvwxyz'

# distances matrix, which will hold distances between 
# any two characters 
dists = [[-1 for i in range(26)] for j in range(26)]

# populate the distances matrix 
for ch1 in alpha:
    for ch2 in alpha:
        r = charToInt(ch1)
        c = charToInt(ch2)
        if r == c:
            dists[r][c] = 0
        elif dists[r][c] == -1:
            r1 = -1
            for line in keybd:
                r1 += 1
                if ch1 in line:
                    break
            c1 = keybd[r1].find(ch1)
            r2 = -1
            for line in keybd:
                r2 += 1
                if ch2 in line:
                    break
            c2 = keybd[r2].find(ch2)

            d = int(abs(r1-r2)) + int(abs(c1-c2))
            dists[r][c] = d
            dists[c][r] = d

# now process input cases
t = int(input())
for case in range(t):
    tokens = input().split()
    word = tokens[0]
    nPoss = int(tokens[1])

    # possibles will be a list of (distance, word)
    # tuples, of all candidate words from the input
    possibles = []
    for i in range(nPoss):
        poss = input()
        dist = 0
        for j in range(len(word)):
            r = charToInt(word[j])
            c = charToInt(poss[j])
            dist += dists[r][c]
        possibles.append( (dist, poss) )
    possibles.sort()
    for t in possibles:
        print(t[1], t[0])