using System;

class Tarifa {
    static void Main() {
        int X = Convert.ToInt32(Console.ReadLine());
        int N = Convert.ToInt32(Console.ReadLine());

        int sum = X;

        for(int i = 0; i < N; i++) {
            int used = Convert.ToInt32(Console.ReadLine());
            sum = sum + X - used;
        }

        // output our result
        Console.WriteLine(sum);
    }
}