import java.util.*;

/**
 * Solution to the Tarifa programming challenge problem.
 * 
 * @author Mark M. Meysenburg
 * @version 09/29/2018
 */
public class Tarifa {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // get data cap and number of months
        int X = stdin.nextInt();
        int N = stdin.nextInt();

        // start balance with first month's allocation
        int sum = X;

        // update balance for each month...
        for(int i = 0; i < N; i++) {
            // ... based on usage for that month
            int used = stdin.nextInt();
            sum = sum + X - used;
        }

        // output our result
        System.out.println(sum);
    }
}