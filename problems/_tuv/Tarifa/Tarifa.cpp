#include <cstdlib>
#include <iostream>

int main() {
    using namespace std;

	int X, N, used, sum;

    cin >> X;
    cin >> N;

	sum = X;

    for(int i = 0; i < N; i++) {
        cin >> used;
		sum = sum + X - used;
    }

    cout << sum << endl;

    return EXIT_SUCCESS;
}