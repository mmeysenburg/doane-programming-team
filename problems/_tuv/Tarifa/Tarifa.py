import sys

X = int(sys.stdin.readline())
N = int(sys.stdin.readline())
sum = X
for i in range(N):
    used = int(sys.stdin.readline())
    sum = sum - used + X

print(sum)