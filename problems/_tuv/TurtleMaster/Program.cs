﻿using System;

class Program
{
    public static void Main(string[] args)
    {
        // build the robot map
        char[,] map = new char[8,8];
        for(int r = 0; r < 8; r++) {
            string line = Console.ReadLine();
            for(int c = 0; c < 8; c++) {
                map[r,c] = line[c];
            }
        }

        string program = Console.ReadLine();

        int[] deltaR = {-1, 0, 1, 0};
        int[] detlaC = {0, 1, 0, -1};
        int botR = 7;
        int botC = 0;
        int botD = 1;

        foreach (char c in program)
        {
            switch (c)
            {
                case 'F':
                    botR += deltaR[botD];
                    botC += detlaC[botD];

                    if(botR > 7 || botR < 0 || botC > 7 || botC < 0) {
                        Console.WriteLine("Bug!");
                        System.Environment.Exit(0);
                    } else if(map[botR, botC] == 'C' || map[botR, botC] == 'I') {
                        Console.WriteLine("Bug!");
                        System.Environment.Exit(0);
                    }
                    break;
                case 'L':
                    botD--;
                    if(botD < 0) botD = 3;
                    break;
                case 'R':
                    botD++;
                    if(botD > 3) botD = 0;
                    break;
                case 'X':
                    int zapR = botR + deltaR[botD];
                    int zapC = botC + detlaC[botD];
                    if(map[zapR, zapC] == 'I') {
                        map[zapR, zapC] = '.';
                    } else {
                        Console.WriteLine("Bug!");
                        System.Environment.Exit(0);
                    }
                    break;
            }
        }

        if(map[botR, botC] == 'D') {
            Console.WriteLine("Diamond!");
        } else {
            Console.WriteLine("Bug!");
        }
    }
}