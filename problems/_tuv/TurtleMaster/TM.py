'''
Python solution to the Turtle Master Kattis problem.
'''
# read map into 2d array of characters
board = [[c for c in input()]  for i in range(8)]
# read program into a string
program = input()

# set up deltas for turtle movement
deltaR = [-1, 0, 1, 0]
deltaC = [0, 1, 0, -1]

# set up turtle location and direction (0:N, 1:E, 2:S, 3:W)
botR = 7
botC = 0
botD = 1

# process the program and see what happens
for c in program:
    if c == 'F':
        botR += deltaR[botD]
        botC += deltaC[botD]

        if botR > 7 or botR < 0 or botC > 7 or botC < 0:
            print('Bug!')
            exit()
        elif board[botR][botC] == 'C' or board[botR][botC] == 'I':
            print('Bug!')
            exit()
    elif c == 'R':
        botD += 1
        if botD > 3:
            botD = 0
    elif c == 'L':
        botD -= 1
        if botD < 0:
            botD = 3
    elif c == 'X':
        zapR = botR + deltaR[botD]
        zapC = botC + deltaC[botD]
        if board[zapR][zapC] == 'I':
            board[zapR][zapC] = '.'
        else:
            print('Bug!')
            exit()

# if we make it this far, all we need to do is see if we're on the D
if board[botR][botC] == 'D':
    print('Diamond!')
else:
    print('Bug!')