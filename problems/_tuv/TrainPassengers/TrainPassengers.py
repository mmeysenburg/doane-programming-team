data = [int(x) for x in input().split()]
C = data[0]
n = data[1]
pgrs = 0
ok = True
for i in range(n):
    data = [int(x) for x in input().split()]
    off = data[0]
    on = data[1]
    left = data[2]

    pgrs -= off
    if pgrs < 0:
        ok = False
        break

    if on > C - pgrs:
        ok = False
        break

    pgrs += on

    if pgrs != C and left != 0:
        ok = False
        break

if left != 0 or pgrs != 0:
    ok = False    

if ok:
    print('possible')
else:
    print('impossible')