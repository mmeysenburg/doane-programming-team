
import java.util.*;

/**
 * Solution to "Vito's Family" programming competition problem. 
 * Find a new home for Vito!
 */
public class VitosFamily {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // number of test cases
        int n = stdin.nextInt();
        for (int i = 0; i < n; i++) {
            
            // number of relatives
            int r = stdin.nextInt();
            
            // read relatives into an array
            int[] s = new int[r];
            for (int rel = 0; rel < r; rel++) {
                s[rel] = stdin.nextInt();
            }
            Arrays.sort(s);

            // Vito should live at the median location, which by
            // definition will be as close as possible to everyone
            // else
            int median = s[r / 2];
            
            // now calculate sum of distances from Vito to everyone
            int distance = 0;
            for (int house : s) {
                int d = house - median;
                d = d < 0 ? -d : d;
                distance += d;
            }

            System.out.println(distance);
        }
    }
}
