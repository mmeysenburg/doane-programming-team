import java.util.*;

/**
 * Solution to the Reverse and Add programming competition problem
 * 
 * @author Mark M. Meysenburg
 * @version 02/05/2019
 */
public class ReverseAndAdd {

    /**
     * Return the reverse of a string.
     * 
     * @param x String to reverse
     * @return Reversed version of x
     */
    private static String reverseX(String x) {
        StringBuilder sb = new StringBuilder();

        for (int i = x.length() - 1; i >= 0; i--) {
            sb.append(x.charAt(i));
        }

        return sb.toString();
    }

    /**
     * Add the integers in two strings, returning the result as a 
     * string. Addition is done with signed, 64-bit longs. 
     * 
     * @param x Addend one
     * @param y Addend two
     * @return Sum of x and y, as a String
     */
    private static String add(String x, String y) {
        long s = Long.parseLong(x, 10) + Long.parseLong(y, 10);
        return Long.toString(s);
    }

    /**
     * Determine if a string is a palindrome or not.
     * 
     * @param x String to check
     * @return true if x is a palindrome, false otherwise
     */
    private static boolean isPal(String x) {
        int i = 0, j = x.length() - 1;
        while(i <= j) {
            if(x.charAt(i) != x.charAt(j)) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    /**
     * Application entry point.
     * 
     * @param args Command-line arguments; ignored by this 
     * app. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // number of trials
        int n = Integer.parseInt(stdin.nextLine());

        // process each trial
        for (int i = 0; i < n; i++) {
            // j counts number of additions until a palindrome
            int j = 1; 

            // read input, reverse it, and add
            String x = stdin.nextLine();
            String xr = reverseX(x);
            String s = add(x, xr); 

            // keep going until the sum is a palindrome
            while(!isPal(s)) {
                j++;
                x = s;
                xr = reverseX(s);
                s = add(x, xr);
            }

            // output results for this case
            System.out.println(j + " " + s);
        }
    }
}