'''
Solution to Sun and Moon, using the suggested technique
'''
sun = [int(x) for x in input().split()]
moon = [int(x) for x in input().split()]

# loop through year numbers
for y in range(1,5001):
    # the first time these conditions are true is our answer
    if (y + moon[0]) % moon[1] == 0 and (y + sun[0]) % sun[1] == 0:
        print(y)
        exit()