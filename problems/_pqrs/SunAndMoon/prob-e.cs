using System;

/*
 * C# solution to the Sun and Moon problem.
 */
class Program {
    public static void Main(string[] args) {
        // read sun data
        String[] tokens = Console.ReadLine().Split(' ');
        int ds = Convert.ToInt32(tokens[0]);
        int ys = Convert.ToInt32(tokens[1]);

        // read moon data
        tokens = Console.ReadLine().Split(' ');
        int dm = Convert.ToInt32(tokens[0]);
        int ym = Convert.ToInt32(tokens[1]);

        // find first year where sun and moon align
        for(int y = 1; y < 5001; y++) {
            if((y + dm) % ym == 0 && (y + ds) % ys == 0) {
                Console.WriteLine(y);
                System.Environment.Exit(0);
            }
        }
    }
}