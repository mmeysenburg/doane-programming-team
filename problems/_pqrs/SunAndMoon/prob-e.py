'''
"On-site" solution to sun and moon, using a list.
'''
sun = [int(x) for x in input().split()]
moon = [int(x) for x in input().split()]

# set up a list where value 's' indicates a year
# where the sun is in the right position
yrs = [0] * 5001
for y in range(sun[1] - sun[0], len(yrs), sun[1]):
    yrs[y] = 's'

# loop until we find the first moon position that 
# matches a year with the sun in the correct position
for y in range(moon[1] - moon[0], len(yrs), moon[1]):
    if yrs[y] == 's':
        print(y)
        exit()

# just in case we get this far, print 5000 
print(5000)