import java.util.*;

/**
 * Solution to the Skener problem from open.kattis.com.
 * 
 * @author Mark M. Meysenburg
 * @version 03/02/2020
 */
public class Skener {
    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this app.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        int R = Integer.parseInt(tokens[0]);
        int C = Integer.parseInt(tokens[1]);
        int ZR = Integer.parseInt(tokens[2]);
        int ZC = Integer.parseInt(tokens[3]);

        // read R rows of C columns each
        for (int i = 0; i < R; i++) {
            String line = stdin.nextLine();
            // build output line as we read
            String outline = "";
            
            for(int j = 0; j < C; j++) {
                char c = line.charAt(j);
                // multiply characters in the line based on ZC
                for(int k = 0; k < ZC; k++) {
                    outline += c;
                }
            }
            // print output line based on ZR
            for(int k = 0; k < ZR; k++) {
                System.out.println(outline);
            }
        }
    }
}