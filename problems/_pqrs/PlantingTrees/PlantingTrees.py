'''
Python solution to the "Planting Trees" Kattis problem.
'''
# skip first line
input()
# read number of days for each tree to grow, sorted
# in descending order
trees = sorted([int(x) for x in input().split()],
    reverse=True)

# figure out the day when last tree is grown
day = 1
tot = 0
# for each tree
for t in trees:
    # update tot, the number of days we have to wait
    if day + t > tot:
        tot = day + t
    day += 1

print(tot + 1)