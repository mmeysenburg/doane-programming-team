using System;

class Pot
{
    static void Main() {
        int n = Convert.ToInt32(Console.ReadLine());
        long sum = 0;
        for (int i = 0; i < n; i++)
        {
            string line = Console.ReadLine();
            long addend = (long)Math.Pow(Convert.ToInt32(line.Substring(0, line.Length - 1)),
                Convert.ToInt32(line.Substring(line.Length - 1)));
            sum += addend;
        }
        Console.WriteLine(sum);
    }
}