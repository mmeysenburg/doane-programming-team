public class OverflowDemo {
    public static void main(String[] args) {
        int prod = 1;
        for (int i = 0; i < 9; i++) {
            prod *= 999;
        }

        System.out.println(prod);
    }
}