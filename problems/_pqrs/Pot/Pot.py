"""
Solution to the "Pot" problem from Kattis.

Mark M. Meysenburg
12/19/2020
"""
N = int(input())
sum = 0
for i in range(N):
    term = input()
    # everything but the last character is the base
    number = int(term[:-1])
    # power is the last character
    powr = int(term[-1])
    # accumulate the term
    sum += number ** powr
print(sum)