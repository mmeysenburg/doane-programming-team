import java.math.BigInteger;
import java.util.Scanner;

/**
 * Java solution to the "Pot" problem on Kattis.
 * 
 * @author Mark M. Meysenburg
 * @version 12/19/2020
 */
public class Pot {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int N = Integer.parseInt(stdin.nextLine());

        BigInteger sum = new BigInteger("0");
        for (int i = 0; i < N; i++) {
            String term = stdin.nextLine();
            BigInteger number = new BigInteger(term.substring(0, term.length() - 1));
            int pow = Integer.parseInt(term.substring(term.length() - 1));
            sum = sum.add(number.pow(pow));
        }
        System.out.println(sum);
    }
}