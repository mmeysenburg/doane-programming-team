using System;

class SOTO {
    static void Main() {
        string line = Console.ReadLine();
        while(line != null) {
            string[] sVals = line.Split(' ');
            int sum = 0;
            foreach (string v in sVals)
            {
                sum += Convert.ToInt32(v);
            }   
            Console.WriteLine(sum / 2);

            line = Console.ReadLine();
        }
    }
}