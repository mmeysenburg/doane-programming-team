
import java.util.*;

/**
 * Solution to "Sum of the Others" programming competition problem. 
 * Find the sum in a line of addends
 */
public class SOTO {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        while (stdin.hasNextLine()) {
            // strategy: sum the whole line. The result is
            // twice the value of the number we're looking
            // for, so divide by 2 and output result.
            String[] vals = stdin.nextLine().split(" ");
            int sum = 0;
            for(String s : vals) {
                sum += Integer.parseInt(s);
            }
            sum /= 2;
            System.out.println(sum);
        }
    }
}
