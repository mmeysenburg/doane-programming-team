'''
 ' Python solution for the Sum of the Others programming 
 ' challenge problem.
 '
 ' Mark M. Meysenburg
 ' 05/03/2021
'''

import sys

# read all lines...
for line in sys.stdin.readlines():
    # ... get values as list of integers...
    addends = [int(x) for x in line.split()]
    # ... sum them...
    twice = sum(addends)
    # ... and divide by two to get the sum
    print(twice // 2)