import java.util.*;
/**
 * Java solution to the ReverseRot programming challenge problem.
 * 
 * @author Mark M. Meysenburg
 * @version 09/27/2018
 */

public class ReverseRot {
    public static void main(String[] args) {
        // character array representing the alphabet we are dealing with
        char[] alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.".toCharArray();

        // map character to it's index, so we don't have to do a search loop
        // later on
        Map<Character, Integer> alphToIndex = new HashMap<>();
        for(int i = 0; i < alph.length; i++) {
            alphToIndex.put(alph[i], i);
        }

        // standard input
        Scanner stdIn = new Scanner(System.in);

        // get first value of N
        int N = stdIn.nextInt();
        while(N != 0) {
            // get input, in reverse, as a character array
            char[] orig = new StringBuilder(stdIn.next()).reverse().toString().toCharArray();

            // build a new character array for the output
            char[] word = new char[orig.length];

            // do the rotation
            for(int i = 0; i < orig.length; i++) {
                // find index of the character, add N, mod 28, 
                // and use that character in the output word
                char c = alph[(alphToIndex.get(orig[i]) + N) % 28];
                word[i] = c;
            }

            // print results
            System.out.println(new String(word));

            // get next value of N
            N = stdIn.nextInt();
        }
    }
}