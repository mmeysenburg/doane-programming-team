alph = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ_.'
line = input().split()
while line[0] != '0':
    shift = int(line[0])
    rev = line[1][::-1]
    out = ''
    for c in rev:
        idx = alph.find(c)
        out += alph[(idx + shift) % 28]
    print(out)

    line = input().split()