using System;
using System.Collections.Generic;

class RR {

    static string ReverseString(string s) {
        // Convert to char array, then call Array.Reverse.
        // ... Finally use string constructor on array.
        char[] array = s.ToCharArray();
        Array.Reverse(array);
        return new string(array);
    }

    static void Main() {
        string alph = "ABCDEFGHIJKLMNOPQRSTUVWXYZ_.";
        Dictionary<char, int> map = new Dictionary<char, int>();
        for (int i = 0; i < alph.Length; i++) {
            map.Add(alph[i], i);
        }
        string[] line = Console.ReadLine().Split(' ');
        int N = Convert.ToInt32(line[0]);
        while(N != 0) {
            string orig = ReverseString(line[1]);
            char[] word = new char[orig.Length];
            for(int i = 0; i < orig.Length; i++) {
                word[i] = alph[(map[orig[i]] + N) % 28];
            }
            Console.WriteLine(new string(word));
            line = Console.ReadLine().Split(' ');
            N = Convert.ToInt32(line[0]);
        }
    }
}