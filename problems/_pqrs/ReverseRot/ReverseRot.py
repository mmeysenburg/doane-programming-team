'''
 ' Python solution to the Reverse Rot programming challenge problem.
 '
 ' Mark M. Meysenburg
 ' 5/3/2021
'''
import sys

# list of letters for constructing ciphertext
alpha = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 
    'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 
    'Y', 'Z', '_', '.']
# dictionary of letter indices 
alphaToIndex = {'A':0, 'B':1, 'C':2, 'D':3, 'E':4, 'F':5, 'G':6,
    'H':7, 'I':8, 'J':9, 'K':10, 'L':11, 'M':12, 'N':13, 'O':14, 
    'P':15, 'Q':16, 'R':17, 'S':18, 'T':19, 'U':20, 'V':21, 'W':22,
    'X':23, 'Y':24, 'Z':25, '_':26, '.':27}

# process all but the last line...
for line in sys.stdin.readlines()[:-1]:
    # grab line and shift amount
    tokens = line.split()
    N = int(tokens[0])

    # reverse input
    rev = tokens[1][::-1]

    # construct output as a list
    # ... get index of the current character
    # ... add shift value
    # ... % 28 to wrap around
    # ... conver to ciphertext character
    revRot = [alpha[(alphaToIndex[ch] + N) % 28] for ch in rev]

    # stick list elements together to form output line
    print(''.join(revRot))
