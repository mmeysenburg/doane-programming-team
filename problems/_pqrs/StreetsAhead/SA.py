# get n and q
tokens = input().split()
n = int(tokens[0])
q = int(tokens[1])

# build street, ordinal dictionary
map = {}
for i in range(n):
    map[input()] = i

# process input cases
for i in range(q):
    tokens = input().split()
    a = map[tokens[0]]
    b = map[tokens[1]]
    print(abs(a - b) - 1)