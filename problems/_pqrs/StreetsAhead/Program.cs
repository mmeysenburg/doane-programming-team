﻿using System;
using System.Collections.Generic;

class Program
{
    public static void Main(string[] args)
    {
        // read n and q
        string[] tokens = Console.ReadLine().Split(' ');
        int n = Int32.Parse(tokens[0]);
        int q = Int32.Parse(tokens[1]);

        // place strings into a name, index dictionary
        Dictionary<string, int> map = new Dictionary<string, int>();
        for(int i = 0; i < n; i++)
        {
            map[Console.ReadLine()] = i;
        }

        // determine number of streets crossed for each case
        for(int i = 0; i < q; i++)
        {
            tokens = Console.ReadLine().Split(' ');
            int a = map[tokens[0]];
            int b = map[tokens[1]];
            Console.WriteLine(Math.Abs(a - b) - 1);
        }
    }
}