'''
Python solution to the Streets Ahead kattis problem.
'''
import sys
# read all the input into a list
all_lines = sys.stdin.readlines()

# get the n, q parameters from input list
nq = [int(x) for x in all_lines[0].split()]

# build a dictionary with the street names from the input: 
# key is the street name, value is a numeric index,
# the ordinal number of the street in the input
streets = {x[:-1]:j for j,x in enumerate(all_lines[1:nq[0] + 1])}

# now determine number of streets crossed in each case
for line in all_lines[nq[0] + 1:]:
    sf = line.split()
    print(abs(streets[sf[0]] - streets[sf[1]]) - 1)