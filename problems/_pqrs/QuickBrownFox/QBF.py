import sys

letters = dict.fromkeys('a b c d e f g h i j k l m n o p q r s t u v w x y z'.split(), 0)

# read all lines
lines = [x[:-1].lower() for x in sys.stdin.readlines()]

# process for pangrams
for line in lines[1:]:
    unused = letters.copy()
    for c in line:
        if c in unused:
            unused.pop(c)

    if len(unused) == 0:
        print('pangram')
    else:
        print('missing', ''.join(unused))