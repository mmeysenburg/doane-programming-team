
import java.util.*;

/**
 * Solution to "Quick Brown Fox" programming competition problem. 
 * Determine if a string is a pangram or not.
 */
public class QuickBrownFox {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        // use a tree set here, so our output of missing 
        // letters is alredy sorted
        Set<Character> set = new TreeSet<>();

        // GOTCHA! After reading a number with Scanner, need
        // to "eat" the endline character before reading strings
        int N = stdin.nextInt();
        stdin.nextLine();

        // read all N lines
        for (int i = 0; i < N; i++) {
            // initialize set with the letters a through z.
            // Note usage of a char variable in the for loop
            set.clear();
            for (char c = 'a'; c <= 'z'; c++) {
                set.add(c);
            }

            // input line and make it all lowercase
            String line = stdin.nextLine().toLowerCase();
            // examine each character...
            for (int j = 0; j < line.length(); j++) {
                char c = line.charAt(j);
                // ... if it's a letter, remove from the set
                if (c >= 'a' && c <= 'z') {
                    set.remove(c);
                }
            }

            // Now we can do our output! An empty set means...
            if (set.isEmpty()) {
                // ... pangram!
                System.out.println("pangram");
            } else {
                // otherwise, output all the characters in the
                // set as our missing letters
                System.out.print("missing ");
                for (char c : set) {
                    System.out.print(c);
                }

                System.out.println();
            } // non-pangram

        } // for each case
    }
}
