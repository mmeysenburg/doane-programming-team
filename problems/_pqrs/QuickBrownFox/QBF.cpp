#include <cstdlib>
#include <iostream>
#include <locale>
#include <string>

using namespace std;

int main() {
  int n;            // number of cases
  locale loc;       // for tolower()
  string line;      // input line
  bool seen[26];    // array-based "set"

  // read in number of cases, *and* eat the endline
  cin >> n;
  getline(cin, line);

  // process the test cases
  for(int caseNo = 0; caseNo < n; caseNo++) {
    // reset seen values
    for(int i = 0; i < 26; i++) {
      seen[i] = false;
    }

    // read input line
    getline(cin, line);
    
    // track which characters are in the line
    for(int i = 0; i < line.size(); i++) {
      // convert each char to an index in our "set"
      int c = (int)(tolower(line[i], loc) - 'a');

      // record it if it represents a letter
      if(c >= 0 && c < 26) {
        seen[c] = true;
      }
    }

    // see how many characters were in the line
    bool flag = true;
    for(int i = 0; i < 26; i++) {
      if(!seen[i]) {
        flag = false;
        break;
      }
    }

    // output the results
    if(flag) {
      cout << "pangram" << endl;
    } else {
      cout << "missing ";
      for(int i = 0; i < 26; i++) {
        if(!seen[i]) {
          cout << (char)('a' + i);
        }
      }
      cout << endl;
    }
  }

}