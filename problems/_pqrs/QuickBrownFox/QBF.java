import java.io.IOException;
import java.io.InputStreamReader;
import java.io.BufferedReader;

public class QBF {
    public static void main(String[] args) throws IOException {
        int[] seen = new int[26];
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        int n = Integer.parseInt(stdin.readLine());
        for(int cNum = 0; cNum < n; cNum++) {
            for(int i = 0; i < 26; i++) {
                seen[i] = 0;
            }
            String line = stdin.readLine().toLowerCase();
            for(int i = 0; i < line.length(); i++) {
                int idx = (int)(line.charAt(i) - 'a');
                if(idx >= 0 && idx < 26) {
                    seen[idx] = 1;
                }
            }
            boolean flag = true;
            for(int i = 0; i < 26; i++) {
                if(seen[i] == 0) {
                    flag = false;
                    break;
                }
            }
            if(flag) {
                System.out.println("pangram");
            } else {
                System.out.print("missing ");
                for(int i = 0; i < 26; i++) {
                    if(seen[i] == 0) {
                        System.out.print((char)('a' + i));
                    }
                }
                System.out.println();
            }
        }
    }
}