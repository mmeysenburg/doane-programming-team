'''
Solution to "The Plank" Kattis programming challenge problem.
'''

def part(n):
    '''
    Recursive DFS to count number of ways a plank can be made of 
    1, 2, 3 ft. sections. 
    '''
    # negative n means we have an invalid solution, so return 0
    if n < 0:
        return 0
    # zero n means we found one way to glue up the plank
    elif n == 0:
        return 1
    else:
        # other values of n need to be partitioned, so recurse
        sum = 0
        # try lengths of 1, 2, and 3
        for i in range(1, 4):
            sum += part(n - i)
        return sum

print(part(int(input())))
