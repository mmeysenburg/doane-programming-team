p = [int(x) for x in input().split()]
clr = [int(input()) for i in range(p[1])]
lft = [i for i in range(p[0]) if i not in clr]
for i in lft:
    print(i)
print('Mario got', p[0] - len(lft), 
    'of the dangerous obstacles.')