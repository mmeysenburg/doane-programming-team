using System;

class QS
{
    static void Main() {
        int x = Convert.ToInt32(Console.ReadLine());
        int y = Convert.ToInt32(Console.ReadLine());

        if (x > 0 && y > 0) {
            Console.WriteLine("1");
        } else if (x > 0 && y < 0) {
            Console.WriteLine("4");
        } else if (x < 0 && y < 0) {
            Console.WriteLine("3");
        } else {
            Console.WriteLine("2");
        }
    }
}