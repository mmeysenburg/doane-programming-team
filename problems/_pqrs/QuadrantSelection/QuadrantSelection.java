
import java.util.*;

/**
 * Solution to "Quadrant Selection" programming competition problem. 
 * What quadrant is a specified point in? 
 */
public class QuadrantSelection {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int x = stdin.nextInt();
        int y = stdin.nextInt();
        
        stdin.close();

        if (x > 0 && y > 0) {
            System.out.println("1");
        } else if (x > 0 && y < 0) {
            System.out.println("4");
        } else if (x < 0 && y < 0) {
            System.out.println("3");
        } else {
            System.out.println("2");
        }
    }
}
