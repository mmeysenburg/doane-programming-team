'''
Python solution to the "Sort" Kattis problem.
'''
# eat the first line; we don't need it
input()
# input 2nd line as list of ints
nums = [int(x) for x in input().split()]

# use a dictionary to count how many times each value occurs
# and another to keep track of first occurrences
counts = { }
firsts = { }
for i, n in enumerate(nums):
    counts[n] = counts.get(n, 0) + 1
    if n not in firsts:
        firsts[n] = i

# sort by count (descending) then by first occurrence (ascending)
nums.sort(key=lambda x: (counts[x], -firsts[x]), reverse=True)

# convert to strings, join, and output
print(' '.join([str(x) for x in nums]))