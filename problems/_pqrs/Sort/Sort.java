import java.util.*;

/**
 * Solution to the Sorts problem on open.kattis.com. 
 * 
 * @author Mark M. Meysenburg
 * @version 03/02/2020
 */
public class Sort {

    /** Map counting frequencies of each number seen. */
    private static Map<Integer, Integer> freqs;
    /** Map containing first occurrence of each number seen. */
    private static Map<Integer, Integer> firsts;

    /**
     * Comparator to sort first by frequency (decreasing), then by
     * first occurrence (increasing).
     */
    static class SortByFreq implements Comparator<Integer> {
        /**
         * Compare two integers using this idea.
         * 
         * @param x Left integer in comparison
         * @param y Right integer in comparison
         * @return Negative if x < y, positive if x > y, 0 if x == 0
         */
        public int compare(Integer x, Integer y) {
            int v = freqs.get(y) - freqs.get(x);
            if(v == 0) {
                return firsts.get(x) - firsts.get(y);
            } else {
                return v;
            }
        }
    }

    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this app. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        int N = Integer.parseInt(tokens[0]);

        Integer[] values = new Integer[N];
        freqs = new HashMap<>();
        firsts = new HashMap<>();

        // read input values, counting frequencies and the 
        // first location of each
        for (int i = 0; i < N; i++) {
            values[i] = stdin.nextInt();
            // seen the number before? 
            if (freqs.containsKey(values[i])) {
                // if so, increment frequency
                int f = freqs.get(values[i]);
                f++;
                freqs.put(values[i], f);
            } else {
                // if not, place a 1 in frequency
                freqs.put(values[i], 1);
            }
            // if we haven't recorded the first loaction...
            if(!firsts.containsKey(values[i])) {
                // record it
                firsts.put(values[i], i);
            }
        }

        // sort the array, using the custom Comparator
        Arrays.sort(values, 0, values.length, new SortByFreq());

        // output results
        for(int i = 0; i < N; i++) {
            System.out.printf("%d ", values[i]);
        }
        System.out.println();
    }
}