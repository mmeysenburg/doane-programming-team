using System;

class R2 {
    static void Main() {
        int r1, s;
        string[] vals = Console.ReadLine().Split(' ');
        r1 = Convert.ToInt32(vals[0]);
        s = Convert.ToInt32(vals[1]);
        Console.WriteLine(2 * s - r1);
    }
}