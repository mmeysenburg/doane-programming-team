
import java.util.*;

/**
 * Solution to "R2" programming competition problem. If S = (R1 + R2) / 2, 
 * and you are given S and R1, find R2.
 */
public class R2 {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int R1 = stdin.nextInt();
        int S = stdin.nextInt();

        // simple algebra tells us that if S = (R1 + R2) / 2, then 
        // R2 = 2S - R1
        int R2 = 2 * S - R1;

        System.out.println(R2);
        
        stdin.close();
    }
}
