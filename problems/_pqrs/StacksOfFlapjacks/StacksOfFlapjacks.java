import java.util.*;

/**
 * Solution to "Stacks of Flapjacks" programming competition problem. 
 * How many flips to sort pancakes? I.e., implement a "flip sort"
 */
public class StacksOfFlapjacks {
    
    /**
     * Print the values of an array from 0 to n - 1, separated by
     * spaces. 
     * 
     * @param arr Array to print
     */
    private static void printNums(int[] arr) {
        for(int i = 0; i < arr.length - 1; i++) {
            System.out.printf("%d ", arr[i]);
        }
        System.out.println(arr[arr.length - 1]);
    }
    
    /**
     * Find the largest value in a given portion of an array.
     * 
     * @param arr Array to search
     * @param le Left index of area to search
     * @param ri Right index of area to search
     * @return Index of largest value in range arr[le..ri]
     */
    private static int findLargest(int[] arr, int le, int ri) {
        int idx = le;
        for(int i = le + 1; i <= ri; i++) {
            if(arr[i] > arr[idx]) {
                idx = i;
            }
        }
        
        return idx;
    }
    
    /**
     * Reverse the elements in a section of an array.
     * 
     * @param arr Array to manipulate
     * @param le Left index of area to reverse
     * @param ri Right index of area to reverse
     */
    private static void reverse(int[] arr, int le, int ri) {
        // until the indices meet...
        while(le < ri) {
            // swap elements at arr[le] and arr[ri]
            int t = arr[le];
            arr[le] = arr[ri];
            arr[ri] = t;
            
            // move indices closer together
            le++;
            ri--;
        }
    }
    
    /**
     * Sort an array by flipping elements. Find index of largest
     * value in unsorted portion of the array; reverse elements
     * up to and including largest element, putting largest element
     * first in the array; then, reverse from element zero to 
     * last unsorted location, putting largest element in its
     * correct position. Along the way, output where the flips
     * happen, in the notation required by the problem. 
     * 
     * @param arr Array to sort
     */
    private static void flipSort(int[] arr) {
        // i is the index of the location for next-largest 
        // value in the array
        for(int i = arr.length - 1; i > 0; i--) {
            // find largest element
            int idx = findLargest(arr, 0, i);
            
            // if it is out of order, do some reversing
            if(idx != i) {
                // if it is not already is location 0, 
                // reverse elements to make it so
                if(idx != 0) {
                    reverse(arr, 0, idx);
                    System.out.printf("%d ", arr.length - idx);
                } 
                // then reverse arr[0 .. i] to put next largest
                // in its spot
                reverse(arr, 0, i);
                System.out.printf("%d ", arr.length - i);
            }
        } // for i
        // always end with a no flip
        System.out.println("0");
    }
    
    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        
        while(stdin.hasNextLine()) {
            // read and parse input into an array 
            String[] sNums = stdin.nextLine().split(" ");
            
            // fill array with the flapjacks
            int[] nums = new int[sNums.length];
            for(int i = 0; i < sNums.length; i++) {
                nums[i] = Integer.parseInt(sNums[i]);
            }
            
            printNums(nums);
            flipSort(nums);
        }
    }
}