'''
Solution to the Roaming Romans Kattis programming competition problem.
'''
x = float(input())
p = int(round(x * 1000 * 5280 / 4854))
print(p)