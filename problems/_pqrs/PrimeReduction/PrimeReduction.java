import java.util.*;

/**
 * Solution to the "Prime Reduction" programming contest problem.
 * This *just* ekes under the time limit, by using the most naive
 * factoring algorithm.
 */
public class PrimeReduction {

    /** List to hold factors of a number. */
    private static ArrayList<Integer> factors = new ArrayList<>();

    /**
     * Do a prime factorization of n. The factors are stored in the 
     * factors list.
     *
     * @param n Number to factor
     */
    private static void factor(int n) {
        // empty list each time
        factors.clear();

        // determine max value to check
        int cap = (int)Math.sqrt(n);

        // naive algorithm: starting at 2, up to sqrt(n), see which 
        // values divide n, and add them to the list
        for (int i = 2; i <= cap; i++) {
            // use current factor value as many times as we can
            while (n % i == 0) {
                n /= i;
                factors.add(i);
            }
        }
        // if n wasn't prime, add n itself to the end of the list
        if (n > 1) {
            factors.add(n);
        }
    }

    /**
     * Application entry point.
     *
     * @param args Command-line arguments; ignored by this application.
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();
        // loop until sentinel value
        while(n != 4) {
            // perform the prime reduction algorithm on the input
            int times = 0;
            while(true) {
                times++;

                factor(n);
                if(factors.size() == 1) {
                    System.out.println(factors.get(0) + " " + times);
                    break;
                }

                n = 0;
                for (int j : factors) {
                    n += j;
                }
            }

            n = stdin.nextInt();
        }
    }
}