﻿using System; 

/// <summary>
/// C# solution to the Restaurant Opening Kattis problem. 
/// </summary>
class Program
{
    /// <summary>
    /// Cost function.
    /// </summary>
    /// <param name="map">Population map, as a 2D array of ints</param>
    /// <param name="n">Number of rows in the map</param>
    /// <param name="m">Number of columns in the map</param>
    /// <param name="r0">Row of the restaurant</param>
    /// <param name="c0">Column of the restaurant</param>
    /// <returns>Cost of a restaurant at (r0, c0)</returns>
    private static int cost(int[,] map, int n, int m, int r0, int c0)
    {
        int sum = 0;
        for(int r = 0; r < n; r++)
        {
            for(int c = 0; c < m; c++)
            {
                // if(!(r == r0 && c == c0))
                // {
                    sum += (Math.Abs(r - r0) + Math.Abs(c - c0)) * map[r, c];
                // }
            }
        }
        return sum;
    }

    public static void Main(string[] args)
    {
        // get size parameters
        string[] tokens = Console.ReadLine().Split(' ');
        int n = Int32.Parse(tokens[0]);
        int m = Int32.Parse(tokens[1]);

        // read in population map as a 2D array of ins
        int[,] map = new int[n, m];
        for (int i = 0; i < n; i++)
        {
            tokens = Console.ReadLine().Split(' ');
            for (int j = 0; j < m; j++)
            {
                map[i, j] = Int32.Parse(tokens[j]);
            }
        }

        // find location with minimal cost
        int minCost = Int32.MaxValue;
        for(int r = 0; r < n; r++)
        {
            for(int c = 0; c < m; c++)
            {
                int cst = cost(map, n, m, r, c);
                if(cst < minCost)
                {
                    minCost = cst;
                }
            }
        }

        Console.WriteLine(minCost);

    }
}