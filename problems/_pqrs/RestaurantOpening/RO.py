'''
Python solution to the Restaurant Opening Kattis problem.
'''
# read in the input parameters
dim = [int(x) for x in input().split()]
n = dim[0]
m = dim[1]

# read in the population map as a 2D array of integers
mp = []
for i in range(n):
    mp.append([int(x) for x in input().split()])

def cost(mp, n, m, r0, c0):
    '''
    Cost function, returning cost for location (r0, c0) on 
    map mp of size n rows and m colums.
    '''
    sum = 0
    for r in range(n):
        for c in range(m):
            # if not(r == r0 and c == c0):
            sum += (abs(r - r0) + abs(c - c0)) * mp[r][c]
    return sum

# main loop - find the minimum cost value
minCost = 10000000000
for r in range(n):
    for c in range(m):
        cst = cost(mp, n, m, r, c)
        if cst < minCost:
            minCost = cst

print(minCost)