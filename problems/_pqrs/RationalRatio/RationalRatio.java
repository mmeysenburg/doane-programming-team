import java.math.BigDecimal;
import java.util.*;

/**
 * RationalRatio: solution to problem C from the 2018 North Central
 * North America ACM programming contest, Ratiional Ratio. This 
 * code illustrates using a fast algorithm to find the greatest 
 * common denominator of two numbers, reducing fractions, and using
 * the Java BigDecimal class. 
 * 
 * Here's the idea, for sample case 3, 123.456 2
 * 
 * Set up two equations with one unknown, the put the repeated 
 * like this:
 * 
 * 1000x = 123456.565656...
 *   10x =   1234.565656...
 * 
 * Abstractly, we have
 * 
 * Ax = B
 * Cx = D
 * 
 * So, x = (B - D) / (A - C). Note how this eliminates the repeated
 * digits. Once we have (B - D) and (A - C), we only have to reduce
 * the fraction to its simplest form. 
 * 
 * 
 * @author Mark M. Meysenburg
 * @version 11/13/2018
 */
public class RationalRatio {

    /**
     * Iterative implementation of Stein's binary GCD algorithm, 
     * which is much faster in practice than recursive or 
     * iterative versions of Euler's algorithm. 
     * 
     * See {@link https://en.wikipedia.org/wiki/Binary_GCD_algorithm}
     * 
     * @param u First of two numbers to find GCD of
     * @param v Second of two numbers to find GCD of
     * @return GCD of u and v
     */
    private static long gcdb(long u, long v) {
        int shift;

        /* GCD(0,v) == v; GCD(u,0) == u, GCD(0,0) == 0 */
        if (u == 0L) return v;
        if (v == 0L) return u;

        /* Let shift := lg K, where K is the greatest power of 2
        dividing both u and v. */
        for(shift = 0; ((u | v) & 1) == 0; shift++) {
            u >>= 1;
            v >>= 1;
        }

        while ((u & 1) == 0) {
            u >>= 1;
        }

        /* From here on, u is always odd. */
        do {
            /* remove all factors of 2 in v -- they are not common */
            /*   note: v is not zero, so while will terminate */
            while ((v & 1) == 0) v >>= 1;

            /* Now u and v are both odd. Swap if necessary so u <= v,
            then set v = v - u (which is even). For bignums, the
            swapping is just pointer movement, and the subtraction
            can be done in-place. */
            if(u > v) {
                long t = v;
                v = u;
                u = t;
            }

            v = v - u; // Here v >= u.
        } while(v != 0L);

        /* restore common factors of 2 */
        return u << shift;
    }

    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments; ignored by this application. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // Read input line and parse into the original number (string)
        // and the number of repeated digits, as an int
        String line = stdin.nextLine();
        StringTokenizer stk = new StringTokenizer(line);
        String orig = stk.nextToken();
        int numRep = Integer.parseInt(stk.nextToken());

        // find numbers and pieces we'll need (see header comments)
        int rLoc = orig.length() - numRep;      // start of rep. digits
        String rep = orig.substring(rLoc);      // repeating digits 
        int dLoc = orig.indexOf('.');           // decimal point location
        int shift = orig.length() - dLoc - 1;   // how far to shift to 
                                                // calculate A

        // calculate terms for x = (B - D) / (A - C).
        // Use BigDecimal, to make sure we keep all of the digits
        // involved.
        BigDecimal ten = new BigDecimal("10");
        BigDecimal A = ten.pow(shift);
        BigDecimal B = new BigDecimal(orig).multiply(A).add(
            new BigDecimal("0." + rep));
        BigDecimal C = ten.pow(rLoc - dLoc - 1);
        BigDecimal D = new BigDecimal(orig).multiply(C);

        // calculate (B - D) and (A - C)
        long numerator = B.subtract(D).longValue();
        long denominator = A.subtract(C).longValue();

        // simplify the fraction 
        long g = gcdb(numerator, denominator);
        if(g != 0) {
            numerator /= g;
            denominator /= g;
        }

        // and output the results!
        System.out.println(numerator + "/" + denominator);

        stdin.close();
    }
}