'''
    Python solution to the String Matching Kattis problem.

    This solution uses the Knuth-Morris-Pratt algorithm. 
'''
import sys

# put all the input into one list, one string per line
allInput = [x.strip() for x in sys.stdin.readlines()]

def kmpTable(W):
    '''
    Build the partial match table for a word W.
    '''
    T = [-1]
    pos = 1
    cnd = 0

    while pos < len(W):
        if W[pos] == W[cnd]:
            T.append(T[cnd])
        else:
            T.append(cnd)
            while cnd >= 0 and W[pos] != W[cnd]:
                cnd = T[cnd]
        pos += 1
        cnd += 1
    T.append(cnd)

    return T

def kmp(S, W, T):
    '''
    Knuth-Morris-Pratt algorithm. Given word S, string W, and 
    partial-match table T, return list of indices where S 
    starts in W. 
    '''
    P = []
    j = 0
    k = 0

    while j < len(S):
        if W[k] == S[j]:
            j += 1
            k += 1
            if k == len(W):
                P.append(j - k)
                k = T[k]
        else:
            k = T[k]
            if k < 0:
                j += 1
                k += 1

    return P

# Now look at the inputs
for i in range(0, len(allInput), 2):
    # build partial-match table
    T = kmpTable(allInput[i])
    # Use KMP to get list of indices, as strings
    outList = [str(j) for j in kmp(allInput[i + 1], allInput[i], T)]
    # output the results, separated by strings
    print(' '.join(outList))