import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * Solution to the Primary Arithmetic programming contest problem.
 * 
 * @author Mark M. Meysenburg
 * @version 1/30/2019
 */
public class PrimaryArithmetic {
    /**
     * Application entry point. 
     * 
     * @param args Command-line arguments. Ignored. 
     */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String a, b, line;

        line = stdin.nextLine();
        StringTokenizer stk = new StringTokenizer(line);
        a = stk.nextToken(); b = stk.nextToken();
        while (!(a.equals("0") && b.equals("0"))) {
            printCarries(countCarries(a, b));
            line = stdin.nextLine();
            stk = new StringTokenizer(line);
            a = stk.nextToken(); b = stk.nextToken();
        }
    }

    /**
     * Count the number of carries that occur when adding a and b. This 
     * method converts the string parameters to array-of-digits form, 
     * then does the addition, counting the carries along the way. 
     * 
     * @param a Addend 1: a less-than-ten-digit, unsigned, decimal 
     * integer, in a String.
     * @param b Addend 2: ditto
     * @return Number of carries which occur when computing a + b. 
     */
    private static int countCarries(String a, String b) {
        int carries = 0;

        // arrays for the arithmetic; one digit per array element,
        // with least significant digit on the right
        int[] aa = new int[11], bb = new int[11], cc = new int[11];

        // convert a and b into array representation
        int i = 10;
        for(int j = a.length() - 1; j >= 0; j--) {
            aa[i--] = (int)(a.charAt(j) - '0');
        }
        i = 10;
        for(int j = b.length() - 1; j>= 0; j--) {
            bb[i--] = (int)(b.charAt(j) - '0');
        }

        // do the addition
        for(i = 10; i >= 1; i--) {
            // calculate raw sum: carry digit plus a plus b
            int sum = cc[i] + aa[i] + bb[i];
            // apply carry and count if needed
            cc[i - 1] = sum / 10;
            carries += cc[i - 1];
            // adjust current sum digit
            cc[i] %= 10;
        }

        return carries;
    }

    /**
     * Print out the number of carries in the specified format.
     * 
     * @param carries Number of carries.
     */
    private static void printCarries(int carries) {
        switch(carries) {
            case 0:
            System.out.println("No carry operation.");
            break;
            case 1:
            System.out.println("1 carry operation.");
            break;
            default:
            System.out.println(carries + " carry operations.");
            break;
        }
    }
}