'''
Python solution to the Primary Arithmetic Kattis problem.
'''
import sys

# read all lines into a list
lines = sys.stdin.readlines()

# process the lines that are test cases
for line in lines[:-1]:
    # convert a line into two lists of integers, zero padded
    tokens = line.split()
    argA = [int(x) for x in '{0:011d}'.format(int(tokens[0]))]
    argB = [int(x) for x in '{0:011d}'.format(int(tokens[1]))]

    # reverse the digits so we add left to right
    argA.reverse()
    argB.reverse()

    # now count carries, by doing digit-by-digit addition
    maxLen = len(argA)
    carries = [0] * (maxLen + 1)
    for i in range(maxLen):
        carries[i + 1] = (argA[i] + argB[i] + carries[i]) // 10

    carries = sum(carries)

    # output result
    if carries == 0:
        print('No carry operation.')
    elif carries == 1:
        print('1 carry operation.')
    else:
        print('{0:d} carry operations.'.format(carries))