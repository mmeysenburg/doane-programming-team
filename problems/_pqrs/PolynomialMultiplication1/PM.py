'''
Solution to the "Polynomial Multiplication 1" Kattis problem.
'''
T = int(input())

for case in range(T):
    deg1 = int(input())
    poly1 = [int(x) for x in input().split()]
    deg2 = int(input())
    poly2 = [int(x) for x in input().split()]

    # use an array to accumulate coefficient for each power of x
    # in the result
    result = [0] * (deg1 + deg2 + 1)

    # calculate the coefficient terms and accumulate in the array
    for i, ci in enumerate(poly1):
        for j, cj in enumerate(poly2):
            result[i + j] += ci * cj

    # find rightmost non-zero coefficient -- the degree of the 
    # result
    deg = len(result) - 1
    while result[deg] == 0:
        deg -= 1

    # output the results
    print(deg)
    for i in range(deg + 1):
        print(result[i], ' ', end='')
    print()