import java.util.*;

/**
 * Solution to Problem J from the 2018 North Central North America 
 * programming contest, Kaleidoscopic Palindromes. This program uses
 * a brute-force approach
 * 
 * @author Mark M. Meysenburg
 * @version 11/8/2018
 */
public class KaleidoscopicPalindromes {

    /**
     * Palindrdome checking method for strings.
     * 
     * @param s String to check
     * @return True if s is a palindrome, false otherwise
     */
    private static boolean isPal(String s) {
        // look at characters from the outside in
        int i = 0, j = s.length() - 1;
        while(i <= j) {
            if(s.charAt(i) != s.charAt(j)) {
                // mismatched characters? not a palindrome!
                return false;
            }
            i++;
            j--;
        }
        // if we've looked at everything without exiting,
        // it's a palindrome
        return true;
    }

    /**
     * "Palindrome" checking method for integers.
     * 
     * @param s Array of integers to check
     * @return True if the numbers in s are the same left to right
     * and right to left.
     */
    private static boolean isPal(int[] s) {
        int i = 0, j = s.length - 1;
        while(i <= j) {
            if(s[i] != s[j]) {
                return false;
            }
            i++;
            j--;
        }
        return true;
    }

    /**
    * Application entry point. 

    * @param args Command-line arguments; ignored by this application
    */
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // read input 
        int a = stdin.nextInt();
        int b = stdin.nextInt();
        int k = stdin.nextInt();

        // paranoia: if a and b are out of order, swap them
        if(a > b) {
            int t = a;
            a = b;
            b = t;
        }

        int num = 0;    // number of cases where a number is
                        // palindromic in all bases 2 <= j <= k.

        // look at all the numbers between a and b, inclusive
        for(int i = a; i <= b; i++) {
            // get i in base 2, as a string
            String bin = Integer.toString(i, 2);

            // only go further if the number is palindromic
            // in binary
            if(isPal(bin)) {

                boolean all = true;     // flag to tell if we were
                                        // palindromic in all bases

                // now look at the rest of the bases
                for(int j = 3; j <= k; j++) {

                    // first, get i in base j
                    if(j <= Character.MAX_RADIX) {

                        // if the radix is small enough, use 
                        // Integer.toString() to convert to base j
                        bin = Integer.toString(i, j);

                        // if not palindromic in base j, stop looking
                        if(!isPal(bin)) {
                            all = false;
                            j = k + 1;
                        }

                    } else {
                        // larger radices require manual conversions
                        int n = i;

                        // list for forming the number in base j
                        LinkedList<Integer> temp = new LinkedList<>();

                        while(n != 0) {
                            // compute next rightmost "digit"
                            int r = n % j;
                            temp.addFirst(r);
                            n /= j;
                        }

                        // convert linked list to an array
                        int[] dummy = new int[temp.size()];
                        int idx = 0;
                        for(int v : temp) {
                            dummy[idx++] = v;
                        }

                        // check if array is palindromic
                        if(!isPal(dummy)) {
                            all = false;
                            j = k + 1;
                        }
                    } // large base
                } // for j 

                // if number was a palindrome in all bases, 
                // increment num
                if(all) num++;

            } // if palindrome in binary

        } // for i

        System.out.println(num);
    }
}