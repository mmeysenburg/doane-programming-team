
import java.util.*;

/**
 * Solution to "Lawn Mower" programming competition problem. 
 * Help Guido tell if he mowed a soccer field correctly!
 */
public class LawnMower {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        // number of end-to-end passes
        int nx = stdin.nextInt();
        // number of side-to-side passes
        int ny = stdin.nextInt();
        // width of cut
        double w = stdin.nextDouble();

        // repeat until sentinel is reached
        while (!(nx == 0 && ny == 0 && w == 0.0)) {

            // end-to-end passes
            double[] x_i = new double[nx];
            // side-to-side passes
            double[] y_i = new double[ny];

            // read in end-to-end passes
            for (int i = 0; i < nx; i++) {
                x_i[i] = stdin.nextDouble();
            }

            // read in side-to-side passes
            for (int i = 0; i < ny; i++) {
                y_i[i] = stdin.nextDouble();
            }

            // sort the passes
            Arrays.sort(x_i);
            Arrays.sort(y_i);

            // check for end-to-end coverage
            boolean eToE = true;
            if (x_i[0] - w / 2.0 > 0.0) {
                // missed a slice up top
                eToE = false;
            } else if (x_i[nx - 1] + w / 2.0 < 75.0) {
                // missed a slice on the bottom
                eToE = false;
            } else {
                // see if we missed any in between
                for (int i = 0; i < nx - 1; i++) {
                    if (x_i[i] + w < x_i[i + 1]) {
                        eToE = false;
                        break;
                    }
                }
            }

            // check for side-to-side coverage
            boolean sToS = true;
            // only do so if the end-to-end was good
            if (eToE) {
                if (y_i[0] - w / 2.0 > 0.0) {
                    // missed on the left
                    sToS = false;
                } else if (y_i[ny - 1] + w / 2.0 < 100.0) {
                    // missed on the right
                    sToS = false;
                } else {
                    // check for misses in between
                    for (int i = 0; i < ny - 1; i++) {
                        if (y_i[i] + w < y_i[i + 1]) {
                            sToS = false;
                            break;
                        }
                    }
                }
            }

            if (eToE && sToS) {
                System.out.println("YES");
            } else {
                System.out.println("NO");
            }

            nx = stdin.nextInt();
            ny = stdin.nextInt();
            w = stdin.nextDouble();

        }
    }
}
