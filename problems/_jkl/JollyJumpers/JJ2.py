'''
Python solution to Jolly Jumpers problem, using
a dictionary to store each difference
'''
import sys

# read all input lines at the same time, convert to 
# lists of integers, and omit first value in each line
lines = [[int(x) for x in line.split()[1:]] 
         for line in sys.stdin.readlines()]

# initialize dictionary once
diffs = {}
# process each line
for line in lines:

    # reset tracking dictionary
    diffs.clear()

    # record which differences we've seen
    for i in range(len(line) - 1):
        diff = abs(line[i] - line[i + 1])
        diffs[diff] = 1

    # verify differences 1 through n - 1 are in 
    # the dictionary
    message = "Jolly"

    for i in range(1, len(line)):
        if not i in diffs:
            message = 'Not jolly'
            break

    # display results 
    print(message)