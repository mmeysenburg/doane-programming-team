
import java.util.*;

/**
 * JollyJumpers solution, using an array of booleans to model a set, 
 * tracking which absolute value differences have been seen in the 
 * sequence of numbers.
 */
public class JollyJumpers {

    public static void main(String[] args) {
        // Scanner to read input from command line
        Scanner stdin = new Scanner(System.in);
        // array of booleans to track which differences have been seen
        boolean[] found = new boolean[3000];

        // repeat until there are no more numbers
        while (stdin.hasNextInt()) {
            // how many numbers are on this line?
            int n = stdin.nextInt();

            // a single number is, by definition, a JJ
            if (n == 1) {
                // eat the single number and output
                stdin.nextInt();
                System.out.println("Jolly");

            } else {
                // reset array for tracking differences
                for (int i = 0; i < found.length; i++) {
                    found[i] = false;
                }

                // if more than one number on the line, start reading;
                // first number...
                int a = stdin.nextInt();
                for (int i = 1; i < n; i++) {
                    // second number...
                    int b = stdin.nextInt();

                    // compute absolute difference of the two values
                    int d = a - b;
                    d = d < 0 ? -d : d;

                    // remember differences we've seen
                    found[d] = d >= 1 && d < found.length && !found[d];

                    // shuffle so a is the next "first number"
                    a = b;

                } // for numbers in line

                // now iterate through the "set" to see if the sequence 
                // is a JJ
                boolean isJJ = true;
                for (int i = 1; i < n; i++) {
                    // if an element is false, it's not a JJ
                    if (!found[i]) {
                        isJJ = false;
                        break;
                    }
                }

                // do the output for this case
                if (isJJ) {
                    System.out.println("Jolly");
                } else {
                    System.out.println("Not jolly");
                }

            } // n != 1 case

        } // while more numbers

    } // main

} // class
