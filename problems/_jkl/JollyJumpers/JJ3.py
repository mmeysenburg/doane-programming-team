import sys

all_inp = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]
for line in all_inp:
    values = sorted([abs(line[i] - line[i + 1]) for i in range(1, len(line) - 1)])
    jolly = True
    for i in range(1, len(values) + 1):
        if values[i - 1] != i:
            jolly = False
            break
    print("Jolly" if jolly else "Not jolly")