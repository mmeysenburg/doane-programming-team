import sys

lines = sys.stdin.readlines()

for line in lines:
    sequence = [int(x) for x in line.split()]
    if len(sequence) == 1:
        print('Jolly')
    else:
        # calculate all the differences, store in a dictionary
        diffs = {}
        for i in range(1, len(sequence)):
            diff = int(abs(sequence[i - 1] - sequence[i]))
            diffs[diff] = 1
        # verify 1 .. N - 1 are in the dictionary
        isJolly = True
        for i in range(1, len(sequence) - 1):
            if i not in diffs:
                isJolly = False
                break
        if isJolly:
            print('Jolly')
        else:
            print('Not jolly')