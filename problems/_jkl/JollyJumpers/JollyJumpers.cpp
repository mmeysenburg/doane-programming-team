#include <cstdio>
#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    int a, b, d, i, n;
    bool isJJ, found[3000];
    
    while(cin >> n) {
        
        if(n == 1) {
            cin >> a;
            cout << "Jolly" << endl;
        } else {
        
            for(i = 0; i < 3000; i++) {
                found[i] = false;
            }
        
            cin >> a;
            for(i = 1; i < n; i++) {
                cin >> b;
                d = a - b;
                d = d < 0 ? -d : d;
                found[d] = d >= 1 && d < n && !found[d];
            
                a = b;
            } // for 
        
            isJJ = true;
            for(i = 1; i < n; i++) {
                if(!found[i]) {
                    isJJ = false;
                    break;
                }
            }
        
            if(isJJ) {
                cout << "Jolly" << endl;
            } else {
                cout << "Not jolly" << endl;
            }
            
        } // n != 1
        
    } // while
    
    return EXIT_SUCCESS;
}