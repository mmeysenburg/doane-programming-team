﻿using System;

int[] ram = new int[1000];
int[] registers = new int[10];

// fill initial ram
int pc = 0;
string line = Console.ReadLine();
while (line != null)
{
    ram[pc++] = Convert.ToInt32(line);
    line = Console.ReadLine();
}

// off we go!
pc = 0;
int numExecuted = 0;
bool isRunning = true;

while (isRunning)
{
    numExecuted++;
    string instruction = ram[pc].ToString("D3");
    int d = Convert.ToInt32(instruction.Substring(1,1));
    int n = Convert.ToInt32(instruction.Substring(2, 1));
    switch (instruction[0])
    {
        case '1':
            // halt command
            isRunning = false;
            break;

        case '2':
            // reg[d] <- n
            registers[d] = n;
            pc++;
            break;

        case '3':
            // reg[d] += n
            registers[d] = (registers[d] + n) % 1000;
            pc++;
            break;

        case '4':
            // reg[d] *= n
            registers[d] = (registers[d] * n) % 1000;
            pc++;
            break;

        case '5':
            // reg[d] <- reg[s]
            registers[d] = registers[n];
            pc++;
            break;

        case '6':
            // reg[d] += reg[s]
            registers[d] = (registers[d] + registers[n]) % 1000;
            pc++;
            break;

        case '7':
            // reg[d] *= reg[s]
            registers[d] = (registers[d] * registers[n]) % 1000;
            pc++;
            break;

        case '8':
            // reg[d] = ram[reg[a]]
            registers[d] = ram[registers[n]];
            pc++;
            break;

        case '9':
            // ram[reg[a]] = reg[s]
            ram[registers[n]] = registers[d];
            pc++;
            break;

        case '0':
            // branch non-zero
            if (registers[n] != 0)
            {
                pc = registers[d];
            }
            else
            {
                pc++;
            }
            break;

        default:
            Console.WriteLine("KERNEL PANIC! ABORT!");
            Environment.Exit(1);
            break;

    } // switch
} // while
Console.WriteLine(numExecuted);
