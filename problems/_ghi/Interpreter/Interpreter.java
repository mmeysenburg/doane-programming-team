import java.util.Arrays;
import java.util.Scanner;

/**
 * Interpreter
 */
public class Interpreter {

    public static void main(String[] args) {
        int[] ram = new int[1000];
        int[] registers = new int[10];

        // read in initial RAM contents
        Scanner stdin = new Scanner(System.in);
        int pc = 0;
        while (stdin.hasNextLine()) {
            ram[pc++] = Integer.parseInt(stdin.nextLine());
        }

        // off we go!
        pc = 0;
        int numExecuted = 0;
        boolean isRunning = true;

        while (isRunning) {
            numExecuted++;
            char[] ins = String.format("%03d", ram[pc]).toCharArray();
            int d = Integer.parseInt(Character.toString(ins[1]));
            int n = Integer.parseInt(Character.toString(ins[2]));
            // System.err.println("\t>>>> " + pc + "\t" + String.format("%03d", ram[pc]) + "\t" + numExecuted);
            // System.err.println("\t\t" + Arrays.toString(registers));
            switch (ins[0]) {
                case '1':
                    // halt command
                    isRunning = false;
                    break;

                case '2':
                    // reg[d] <- n
                    registers[d] = n;
                    pc++;
                    break;

                case '3':
                    // reg[d] += n
                    registers[d] = (registers[d] + n) % 1000;
                    pc++;
                    break;

                case '4':
                    // reg[d] *= n
                    registers[d] = (registers[d] * n) % 1000;
                    pc++;
                    break;

                case '5':
                    // reg[d] <- reg[s]
                    registers[d] = registers[n];
                    pc++;
                    break;

                case '6':
                    // reg[d] += reg[s]
                    registers[d] = (registers[d] + registers[n]) % 1000;
                    pc++;
                    break;

                case '7':
                    // reg[d] *= reg[s]
                    registers[d] = (registers[d] * registers[n]) % 1000;
                    pc++;
                    break;

                case '8':
                    // reg[d] = ram[reg[a]]
                    registers[d] = ram[registers[n]];
                    pc++;
                    break;

                case '9':
                    // ram[reg[a]] = reg[s]
                    ram[registers[n]] = registers[d];
                    pc++;
                    break;

                case '0':
                    // branch non-zero
                    if(registers[n] != 0) {
                        pc = registers[d];
                    } else {
                        pc++;
                    }
                    break;
            
                default:
                    System.err.println("KERNEL PANIC! ABORT!");
                    System.exit(1);
                    break;
            } // switch opcode
        } // instruction processing loop

        System.out.println(numExecuted);

    } // main
} // Interpreter