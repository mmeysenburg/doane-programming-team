import java.util.*;

/**
 * Java solution to the Kattis Integer Lists problem.
 */
public class IL {
    public static void main(String[] args) {
        // empty input list
        String[] emptyInp = new String[0];

        Scanner stdin = new Scanner(System.in);

        // how many test cases?
        int tc = Integer.parseInt(stdin.nextLine());
        for (int i = 0; i < tc; i++) {

            // read program
            String p = stdin.nextLine();

            stdin.nextLine(); // skip n

            // read input into an array of Strings
            String line = stdin.nextLine();
            line = line.substring(1, line.length() - 1);
            String[] inp = line.split(",");

            if (inp[0].equals("")) {
                inp = emptyInp;
            }

            // convert input array into a linked list
            LinkedList<String> lst = new LinkedList<>();
            for (String s : inp) {
                lst.add(s);
            }

            // process the program
            boolean noErr = true; // have we had an error?
            boolean isF = true; // direction of the list
            for (int j = 0; noErr && j < p.length(); j++) {
                char c = p.charAt(j);
                switch (c) {
                    case 'D':
                        // empty list D causes an error
                        if (lst.size() == 0) {
                            System.out.println("error");
                            noErr = false;
                        } else {
                            // remove from appropriate end
                            if (isF) {
                                lst.removeFirst();
                            } else {
                                lst.removeLast();
                            }
                        }
                        break;
                    case 'R':
                        // reverse direction
                        isF = !isF;
                        break;
                    default:
                        // failsafe should never be encountered
                        System.out.println("error");
                        noErr = false;
                        break;
                } // switch
            } // for char in program

            // now for the output
            if (noErr && lst.size() == 0) {
                // an empty list is OK
                System.out.println("[]");
            } else if (noErr) {
                // build the output string
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                while (lst.size() > 1) {
                    if (isF) {
                        sb.append(lst.removeFirst());
                    } else {
                        sb.append(lst.removeLast());
                    }
                    sb.append(",");
                }
                sb.append(lst.removeFirst());
                sb.append("]");
                System.out.println(sb.toString());
            } // non-error output
        } // for test cases
    } // main
} // IL