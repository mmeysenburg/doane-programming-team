'''
Python solution to the Integer Lists Kattis problem.
'''
tc = int(input()) # number of test cases

for i in range(tc):
    p = input()         # read program
    n = int(input())    # read size of list

    # read input list 
    if n == 0:
        input()
        inp = []
    else:
        inp = input()[1:-1].split(',')

    # strategy: keep an index to left and right ends
    # of the list, a boolean showing the direction
    # to print the list, and a boolean showing error
    # status
    le = 0              # index of leftmost element
    ri = len(inp) - 1   # index of rightmost element
    fwd = True          # forward or backwards flag
    err = False         # error flag

    # process the program 
    for c in p:
        if c == 'R':
            fwd = not fwd   # flag indicates which end to 
                            # pop from
        elif c == 'D':
            # once the indices cross, a 'D' is an erro
            if ri < le:
                err = True
                break
            elif fwd:
                le += 1     # move left index right
            else:
                ri -= 1     # move right index left
        else:
            # failsafe error in program
            err = True
            break

    # decide what to print
    if err:
        print('error')
    elif fwd:
        print('[' + ','.join(inp[le:ri+1]) + ']')
    else:
        out = inp[le:ri+1]
        out.reverse()
        print('[' + ','.join(out) + ']')
