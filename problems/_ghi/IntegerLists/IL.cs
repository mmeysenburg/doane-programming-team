﻿using System;
using System.Text;

/// <summary>
/// C# solution to the Integer Lists Kattis problem.
/// </summary>
class IntegerLists
{
    public static void Main(string[] args)
    {
        // Strategy is to read all input into an array of strings, and keep indices
        // to the first and last item in the array. We won't actually remove any 
        // values from the array, but rather increment / decrement the indices. 
        // A flag will keep track of whether we're going "forwards" or "backwards."
        // If forwards, a 'D' command increments the left index, simulating a removal
        // on that end. Likewise, a 'D' if we're going backwards decrements the right
        // index. Once the program has been process, we print either from the left
        // index to the right index (if we're going forwards), or from the right to
        // the left (if we're going backwards). 

        string[] emptyInp = new string[0];              // empty array placeholder
        int tc = Convert.ToInt32(Console.ReadLine());   // number of test cases

        // process each test case
        for (int i = 0; i < tc; i++)
        {
            // read program
            string p = Console.ReadLine();

            // read input size
            int n = Convert.ToInt32(Console.ReadLine());

            // read input list
            string[] inp = null;
            if (n == 0)
            {
                Console.ReadLine();                 // eat the empty list text
                inp = emptyInp;
            } else
            {
                // grab all input numbers as a string
                string inpS = Console.ReadLine();
                // peel off '[' and ']', split into array of strings
                inp = inpS[1..^1].Split(",");
            }

            // process program
            bool fwd = true, err = false;
            int le = 0, ri = inp.Length - 1;
            for(int j = 0; !err && j < p.Length; j++) 
            {
                char c = p[j];
                switch (c)
                {
                    case 'R':
                        fwd = !fwd;         // reverse direction
                        break;
                    case 'D':
                        // if the indices cross, we are trying to 
                        // delete from an empty list, which is an
                        // error
                        if(ri < le)
                        {
                            err = true;
                        } else
                        {
                            // no error? increment or decrement, 
                            // simulating a removal 
                            if(fwd)
                            {
                                le++;
                            } else
                            {
                                ri--;
                            }
                        }
                        break;
                    default:
                        // failsafe to catch invalid program character
                        err = true;
                        break;
                } // command switch
            } // for each command in program

            // process output
            if (err)
            {
                // error output is simple
                Console.WriteLine("error");
            }
            else
            {
                // use a StringBuilder to prepare one string for output,
                // and only have one Console.WriteLine() call. Individual
                // Console.WriteLine() calls may take too much time. 
                StringBuilder sb = new StringBuilder();
                sb.Append("[");
                if (fwd)
                {
                    // if going forwards, build output from left to right
                    for(int j = le; j <= ri - 1; j++)
                    {
                        sb.Append(inp[j]);
                        sb.Append(",");
                    }
                    // in non-empty list, last element does not have
                    // a comma after it
                    if(le <= ri)
                    {
                        sb.Append(inp[ri]);
                    }
                } else
                {
                    // if going backwards, build right to left
                    for(int j = ri; j >= le + 1; j--)
                    {
                        sb.Append(inp[j]);
                        sb.Append(",");
                    }
                    // no comma for last element
                    if(le <= ri)
                    {
                        sb.Append(inp[le]);
                    }
                }
                // complete output formation, and print the darn thing
                sb.Append("]");
                Console.WriteLine(sb.ToString());
            } // non-error output
        } // for test cases 
    } // method Main
} // class IntegerLists