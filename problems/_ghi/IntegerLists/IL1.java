import java.util.*;

public class IL1 {
    public static void main(String[] args) {
        String[] inp;
        String[] emptyInp = new String[0];
        Scanner stdin = new Scanner(System.in);
        int tc = Integer.parseInt(stdin.nextLine());
        int i, j, n, pLen, le, ri;
        String p, line;
        boolean fwd, err;
        char c;
        for (i = 0; i < tc; i++) {
            // read program
            p = stdin.nextLine();
            pLen = p.length();

            // read n
            n = Integer.parseInt(stdin.nextLine());

            // read input list
            le = 0;
            ri = n - 1;
            if (n != 0) {
                j = 0;
                line = stdin.nextLine();
                inp = line.substring(1, line.length() - 1).split(",");
            } else {
                line = stdin.nextLine();
                inp = emptyInp;
            }

            // process program
            fwd = true;
            err = false;
            for (j = 0; !err && j < pLen; j++) {
                c = p.charAt(j);
                switch (c) {
                    case 'R':
                        fwd = !fwd;
                        break;
                    case 'D':
                        if (ri < le) {
                            err = true;
                        } else {
                            if (fwd) {
                                le++;
                            } else {
                                ri--;
                            }
                        }
                        break;
                    default:
                        err = true;
                        break;
                } // process command
            } // process program

            // produce output
            if (err) {
                System.out.println("error");
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                if (fwd) {
                    for (j = le; j <= ri - 1; j++) {
                        sb.append(inp[j]);
                        sb.append(",");
                    }
                    if (le <= ri) {
                        sb.append(inp[j]);
                    }
                } else {
                    for (j = ri; j >= le + 1; j--) {
                        sb.append(inp[j]);
                        sb.append(",");
                    }
                    if (le <= ri) {
                        sb.append(inp[j]);
                    }
                }
                sb.append("]");
                System.out.println(sb.toString());
            } // non-error printing
        } // for test cases
    } // main
} // IL1
