#include <iostream>
#include <set>
#include <string>

using namespace std;

int main() {
    int T, n;
    string city;
    cin >> T;
    set<string> cities;
    for(int caseNum = 0; caseNum < T; caseNum++) {
        cin >> n;
        cities.clear();
        for(int i = 0; i < n; i++) {
            cin >> city;
            cities.insert(city);
        }    
        cout << cities.size() << endl;
    }
}