"""
Python solution to the "I've Been Everywhere, Man" problem on Kattis.

Mark M. Meysenburg
12/12/2020
"""
T = int(input())
for case in range(T):
    n = int(input())
    # place cities in a dictionary, then print dictionary length
    cities = { }
    for i in range(n):
        city = input()
        cities[city] = 1
    print(len(cities))