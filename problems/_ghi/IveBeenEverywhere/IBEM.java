import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

public class IBEM {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int T = Integer.parseInt(stdin.nextLine());
        for(int caseNo = 0; caseNo < T; caseNo++) {
            int n = Integer.parseInt(stdin.nextLine());
            Set<String> cities = new TreeSet<>();
            for (int i = 0; i < n; i++) {
                String city = stdin.nextLine();
                cities.add(city);
            }
            System.out.println(cities.size());
        }
        stdin.close();
    }
}