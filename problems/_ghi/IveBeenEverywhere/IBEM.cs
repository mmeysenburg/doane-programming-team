using System;
using System.Collections.Generic;

namespace IBEM {
    class IBEM {
        static void Main() {
            int cases = Convert.ToInt32(Console.ReadLine());
            for(int i = 0; i < cases; i++) {
                int cities = Convert.ToInt32(Console.ReadLine());
                Dictionary<string, int> d = new Dictionary<string, int>();
                for(int j = 0; j < cities; j++) {
                    string c = Console.ReadLine();
                    if(!d.ContainsKey(c)) {
                        d.Add(c, 1);
                    }
                }
                Console.WriteLine(d.Keys.Count);
            }
        }
    }
}