
import java.util.*;

/**
 * Java solution using simulated recursion for the Graphical Editor 
 * problem.
 */
public class GraphicalEditor {

    /**
     * Array holding the "image."
     */
    private static char[][] image;

    /**
     * Number of rows in the image.
     */
    private static int rows;

    /**
     * Number of columns in the image.
     */
    private static int cols;

    /**
     * Flood fill method: fill region containing (r, c) with a color. 
     * Recursive version works for the online judge data, but will not
     * work for filling an maximally sized blank image.
     *
     * @param r Row of initial pixel.
     * @param c Column of initial pixel.
     * @param origC Original color at (r, c).
     * @param newC Color to fill with.
     */
    private static void floodFill(int r, int c, char origC, char newC) {
        // easy base case: flood fill with same color, or
        // the pixel is already colored
        if (origC == newC || image[r][c] == newC) {
            return;
        }
        
        image[r][c] = newC;
        
        // north
        if(r - 1 >= 0 && image[r - 1][c] == origC) {
            floodFill(r - 1, c, origC, newC);
        }
        // east
        if(c + 1 < image[r].length && image[r][c + 1] == origC) {
            floodFill(r, c + 1, origC, newC);
        }
        // south
        if(r + 1 < image.length && image[r + 1][c] == origC) {
            floodFill(r + 1, c, origC, newC);
        }
        // west
        if(c - 1 >= 0 && image[r][c - 1] == origC) {
            floodFill(r, c - 1, origC, newC);
        }
    } // floodFill

    /**
     * Application entry point.
     *
     * @param args Command line arguments, ignored by the application.
     */
    public static void main(String[] args) {
        Scanner cin = new Scanner(System.in);

        // get first line, skipping any blanks
        StringTokenizer stk = new StringTokenizer(cin.nextLine());
        while (!stk.hasMoreTokens()) {
            stk = new StringTokenizer(cin.nextLine());
        }
        char command = stk.nextToken().charAt(0);
        int row, col, Y1, Y2, X1, X2;
        char newC, origC;

        // loop until the command is 'X'
        while (command != 'X') {
            switch (command) {
                case 'I':
                    // initialize: just save number of rows and cols
                    cols = Integer.parseInt(stk.nextToken());
                    rows = Integer.parseInt(stk.nextToken());
                    image = new char[rows][cols];
                // ... and flow through into initialization
                case 'C':
                    // 'O' out the current image
                    for (row = 0; row < image.length; row++) {
                        for (col = 0; col < image[row].length; col++) {
                            image[row][col] = 'O';
                        }
                    }
                    break;
                case 'L':
                    // color specific pixel
                    col = Integer.parseInt(stk.nextToken()) - 1;
                    row = Integer.parseInt(stk.nextToken()) - 1;
                    newC = stk.nextToken().charAt(0);
                    image[row][col] = newC;
                    break;
                case 'V':
                    // draw a vertical line
                    col = Integer.parseInt(stk.nextToken()) - 1;
                    Y1 = Integer.parseInt(stk.nextToken()) - 1;
                    Y2 = Integer.parseInt(stk.nextToken()) - 1;

                    // GOTCHA! are Y1 and Y2 in order?
                    if (Y1 > Y2) {
                        int t = Y1;
                        Y1 = Y2;
                        Y2 = t;
                    }

                    newC = stk.nextToken().charAt(0);
                    for (row = Y1; row <= Y2; row++) {
                        image[row][col] = newC;
                    }
                    break;
                case 'H':
                    // draw a horizontal line
                    X1 = Integer.parseInt(stk.nextToken()) - 1;
                    X2 = Integer.parseInt(stk.nextToken()) - 1;
                    row = Integer.parseInt(stk.nextToken()) - 1;

                    // GOTCHA! are X1 and X2 in order?
                    if (X1 > X2) {
                        int t = X1;
                        X1 = X2;
                        X2 = t;
                    }

                    newC = stk.nextToken().charAt(0);
                    for (col = X1; col <= X2; col++) {
                        image[row][col] = newC;
                    }
                    break;
                case 'K':
                    // draw a filled rectangle
                    X1 = Integer.parseInt(stk.nextToken()) - 1;
                    Y1 = Integer.parseInt(stk.nextToken()) - 1;
                    X2 = Integer.parseInt(stk.nextToken()) - 1;
                    Y2 = Integer.parseInt(stk.nextToken()) - 1;
                    newC = stk.nextToken().charAt(0);
                    
                    for (row = Y1; row <= Y2; row++) {
                        for (col = X1; col <= X2; col++) {
                            image[row][col] = newC;
                        }
                    }
                    break;
                case 'F':
                    // flood fill 
                    col = Integer.parseInt(stk.nextToken()) - 1;
                    row = Integer.parseInt(stk.nextToken()) - 1;
                    
                    newC = stk.nextToken().charAt(0);
                    origC = image[row][col];
                    
                    floodFill(row, col, origC, newC);
                    break;
                case 'S':
                    // save (output) 
                    System.out.println(stk.nextToken());
                    for (row = 0; row < image.length; row++) {
                        System.out.println(image[row]);
                    }
                    break;
                default:
                    // do nothing
                    break;
            }

            // get next command line
            stk = new StringTokenizer(cin.nextLine());
            while (!stk.hasMoreTokens()) {
                stk = new StringTokenizer(cin.nextLine());
            }
            command = stk.nextToken().charAt(0);
        } // while
    } // main
} // GraphicalEditor
