
import java.util.*;

/**
 * Solution to "Greedily Increasing Subsequence" programming competition
 * problem.
 */
public class GrIncSub {

    public static void main(String[] args) {

        Scanner stdin = new Scanner(System.in);

        int n = stdin.nextInt();

        // use a linked list to hold the GIS, since adding
        // to the end is efficient and we won't need random
        // access to the elements.
        List<Integer> list = new LinkedList<>();

        // x is current number being examined in the list
        int x = stdin.nextInt();

        // first number read is always the start of the GIS
        list.add(x);

        // y is last number added to the GIS
        int y = x;

        // now look at the rest of the numbers
        for (int i = 1; i < n; i++) {
            x = stdin.nextInt();

            // if the new number is greater than the last one
            // in the GIS, add it to the GIS and remember it
            // in variable y
            if (x > y) {
                list.add(x);
                y = x;
            }
        }
        stdin.close();

        // form the required output
        System.out.println(list.size());
        for (int i : list) {
            System.out.printf("%d ", i);
        }
        System.out.println();
    }
}
