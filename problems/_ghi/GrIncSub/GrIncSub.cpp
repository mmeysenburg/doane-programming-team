#include <cstdlib>
#include <iostream>
#include <list>

int main() {
    std::list<int> lst;
    int n;
    std::cin >> n;

    int x;
    std::cin >> x;
    lst.push_back(x);
    int y = x;

    for(int i = 1; i < n; i++) {
        std::cin >> x;

        if(x > y) {
            lst.push_back(x);
            y = x;
        }
    }

    std::cout << lst.size() << std::endl;
    for(int i : lst) {
        std::cout << i << " " ;
    }
    std::cout << std::endl;

    return EXIT_SUCCESS;
}