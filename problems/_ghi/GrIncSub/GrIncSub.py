import sys

# read all the input, convert into list of integers
line = [int(x) for x in sys.stdin.readlines()[1:][0].split()]

# prepare list of output integers, starting with 1st value
x = line[0]
res = [(str(x)]

# y represents last the value seen
y = x

# look at the rest of the values...
for x in line[1:]:
    # ... remembering each one that is larger
    if x > y:
        res.append(str(x))
        y = x

# print the results
print(len(res))
print(' '.join(res))