/**
 * Solution to the Incognito problem.
 * 
 * @author Mark M. Meysenburg
 * @version 10/28/2019
 */

import java.util.*;

public class Incognito {
    public static void main(String[] args) {
        // map holds (category, count) pairs, where count
        // is the number of items in that category
        Map<String, Integer> items = new HashMap<>();

        Scanner stdin = new Scanner(System.in);

        int cases = Integer.parseInt(stdin.nextLine());

        // process all input cases
        for (int i = 0; i < cases; i++) {
            int n = Integer.parseInt(stdin.nextLine());

            // remove any previous items from the map before
            // doing the current case
            items.clear();

            for(int j = 0; j < n; j++) {
                // put categories into the map...
                String[] vals = stdin.nextLine().split(" ");
                if(items.containsKey(vals[1])) {
                    // if the category is already there, increment
                    // its count by 1
                    items.put(vals[1], items.get(vals[1]) + 1);
                } else {
                    // otherwise, this is the first time we've seen
                    // this category
                    items.put(vals[1], 1);
                }
            }
            // answer is the product of all the category counts,
            // minus one, which represents using none of the 
            // items
			int sum = 1;
			for(String k : items.keySet()) {
				sum *= items.get(k) + 1;
			}
			System.out.println(sum - 1);
        }
    }
}