'''
Python solution to the Incognito Kattis problem. 
'''
numCases = int(input())

# process all cases
for i in range(numCases):
    n = int(input())
    # the dictionary tracks number of each type of apparel
    itemCount = {}
    # count how many of each type
    for j in range(n):
        tokens = input().split()
        itemCount[tokens[1]] = itemCount.get(tokens[1], 0) + 1
    # If there are n hats, we have n + 1 options for headgear
    # Accumulate product of these counts
    prod = 1
    for item in itemCount:
        prod *= itemCount[item] + 1
    # Answer is product minus one (representing no items used)
    print(prod - 1)