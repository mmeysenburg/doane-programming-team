// #include <cstdio>
#include <cstdlib>
// #include <iostream>
#include <list>
#include <string>
#include <stdio.h>

using namespace std;

list<string> syllables;

bool hku(int num, string line) {
    if(num < 0) {
        return false;
    } else if(num == 0 && line.length() - 1 == 0) {
        return true;
    } else {
        bool rVal = false;
        for(string s : syllables) {
            if(line.find(s) == 0) {
                int sLen = s.length();
                string newLine = line.substr(sLen);
                while(newLine[0] == ' ') {
                    newLine = newLine.substr(1);
                }
                rVal = rVal || hku(num - 1, newLine);
                if(rVal) {
                    break;
                }
            }
        }
        return rVal;
    }
}

int main() {
    int S;
    scanf("%d", &S);
    char inLine[101];

    for (int i = 0; i < S; i++) {
        scanf("%s", inLine);
        string syl(inLine);
        syllables.push_back(syl);
    }

    gets(inLine);
    string l1(inLine);
    // printf("%s\n", l1.c_str());
    gets(inLine);
    string l2(inLine);
    // printf("%s\n", l2.c_str());
    gets(inLine);
    string l3(inLine);
    // printf("%s\n", l3.c_str());

    if(hku(5, l1) && hku(7, l2) && hku(5, l3)) {
        printf("haiku\n");
    } else {
        printf("come back next year\n");
    }
    
    return EXIT_SUCCESS;
}