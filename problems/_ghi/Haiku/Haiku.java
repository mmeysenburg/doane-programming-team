import java.util.*;

import javax.sound.sampled.SourceDataLine;

public class Haiku {

    private static String[] syllables;

    private static boolean hku(int num, String line) {
        if(num < 0) {
            return false;
        } else if(num == 0 && line.length() == 0) {
            return true;
        } else {
            boolean rVal = false;
            for(String s : syllables) {
                if(line.startsWith(s)) {
                    String newLine = line.substring(s.length()).trim();
                    rVal = rVal || hku(num - 1, newLine);
                    if(rVal) {
                        return true;
                    }
                }
            }
            return rVal;
        }
    }
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int S = Integer.parseInt(stdin.nextLine());
        syllables = new String[S];
        for (int i = 0; i < syllables.length; i++) {
            syllables[i] = stdin.nextLine();
        }
        String l1 = stdin.nextLine();
        String l2 = stdin.nextLine();
        String l3 = stdin.nextLine();
 
        if(hku(5, l1) && hku(7, l2) && hku(5, l3)) {
            System.out.println("haiku");
        } else {
            System.out.println("come back next year");
        }
    }
}