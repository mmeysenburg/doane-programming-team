'''
Python solution to the Haiku Kattis problem.
'''
# read input. Nothing special for the syllables, just keep them
# in a list
S = int(input())
syllables = [input() for i in range(S)]
s1 = input()
s2 = input()
s3 = input()

'''
Determine if a line has a certain # of syllables.

parameters
----------
  num : number of syllables to count in the line
  line : line to check

returns
-------
  True if line contains num syllables, False otherwise
'''
def hku(num, line):
    if num < 0:
        # if num becomes negative, we have part of the line left,
        # but no valid syllables to finish it
        return False
    elif num == 0 and len(line) == 0:
        # if num == 0 and the line is empty, we found the original
        # number of syllables in the original line
        return True
    else:
        # recursive case: see if any valid syllable starts the line.
        # If so, recurse on one fewer syllable and line w/ leading
        # syllable stripped off
        for s in syllables:
            if line.startswith(s):
                if hku(num - 1, line[len(s):].strip()):
                    return True
        return False

# output our result
if hku(5, s1) and hku(7, s2) and hku(5, s3):
    print('haiku')
else:
    print('come back next year')