using System;

namespace HR {
    class HR
    {
        static void Main() {
            int n = Convert.ToInt32(Console.ReadLine());
            for (int i = 0; i < n; i++)
            {
                string[] line = Console.ReadLine().Split(' ');
                double b = Convert.ToDouble(line[0]);
                double p = Convert.ToDouble(line[1]);
                double minBPM = 60.0 / (p / (b - 1));
                double calcBPM = 60.0 * b / p;
                double maxBPM = 60.0 / (p / (b + 1));
                Console.WriteLine("{0:F4} {1:F4} {2:F4}", minBPM, calcBPM, maxBPM);
            }
        }
    }
}