
import java.util.Scanner;

/**
 * Solution to "Heart Rate" programming competition problem. 
 * Determine minimum, calculated, and maximum calculated ABPM.
 */
public class HeartRate {

    public static void main(String[] args) {
        Scanner stdIn = new Scanner(System.in);
        
        // number of cases
        int n = Integer.parseInt(stdIn.nextLine());
        for (int i = 0; i < n; i++) {
            
            // read b and p
            double b = stdIn.nextDouble();
            double p = stdIn.nextDouble();

            // Calculations, assuming beats are equally spaced.
            // We measure for a given period of time and count
            // b beats. That's the value to use for calculated 
            // version. 
            double minBPM = 60.0 / (p / (b - 1));
            double calcBPM = 60.0 * b / p;
            double maxBPM = 60.0 / (p / (b + 1));

            System.out.println(String.format("%.4f %.4f %.4f\n", 
                    minBPM, calcBPM, maxBPM));
        }
    }
}
