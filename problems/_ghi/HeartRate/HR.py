import sys

lines = [x.split() for x in sys.stdin.readlines()[1:]]
for line in lines:
    b = float(line[0])
    p = float(line[1])
    minBPM = 60.0 / (p / (b - 1))
    calcBPM = 60.0 * b / p
    maxBPM = 60.0 / (p / (b + 1))
    print('{0:.4f} {1:.4f} {2:.4f}'.format(minBPM, calcBPM, maxBPM))
