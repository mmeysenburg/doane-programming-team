'''
Solution to the 'Honour Thy (Apaxian) Parent' Kattis problem.
'''
n = input().split()

if n[0].endswith('e'):
    out = 'x'.join(n)
elif n[0].endswith('ex'):
    out = ''.join(n)
elif n[0][-1] in ['a', 'i', 'o', 'u']:
    out = n[0][:-1] + 'ex' + n[1]
else:
    out = 'ex'.join(n)

print(out)