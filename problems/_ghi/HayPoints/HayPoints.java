import java.util.*;

/**
 * Java solution to the Hay Points Kattis problem.
 * 
 * @author Mark M. Meysenburg
 * @version 03/12/2021
 */
public class HayPoints {
    /**
     * Application entry point.
     */
    public static void main(String[] args) {
        // read parameters
        Scanner stdin = new Scanner(System.in);
        String[] line = stdin.nextLine().split(" ");
        int m = Integer.parseInt(line[0]);
        int n = Integer.parseInt(line[1]);

        // build the word / points dictionary
        Map<String, Integer> dict = new HashMap<>();
        for(int i = 0; i < m; i++) {
            line = stdin.nextLine().split(" ");
            dict.put(line[0], Integer.parseInt(line[1]));
        }

        // process the cases
        for(int i = 0; i < n; i++) {
            // init accumulator, primining read
            int sum = 0;
            line = stdin.nextLine().split(" ");

            // keep going until we see the terminator
            while(!line[0].equals(".")) {
                // look at each word in the line
                for(String w : line) {
                    if (dict.containsKey(w)) {
                        sum += dict.get(w);
                    }
                }
                // read next line
                line = stdin.nextLine().split(" ");
            }
            // output results
            System.out.println(sum);
        } // for cases
    }
}