'''
Solution to the Hay Points Kattis problem.

Mark Meysenburg
3/12/2021
'''
import sys

sys.stdin
# parameterizing input
line = input().split()
m = int(line[0])
n = int(line[1])

# build word, points dictionary
dict = {}
for i in range(m):
    line = input().split()
    dict[line[0]] = int(line[1])

# process the cases
for i in range(n):
    # init accumulator, priming read
    hp = 0
    line = input()

    # look at the words in each line, accumulating
    # points as we go
    while line != '.':
        # make input into a list of point values
        line = [dict.get(w, 0) for w in line.split()]
        # accumulate sum of this line
        hp += sum(line)
        # get next line
        line = input()
        
    # output results
    print(hp)
