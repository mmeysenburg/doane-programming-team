'''
Slightly faster Python solution to Hay Points
'''
import sys

params = input().split()
m = int(params[0])
n = int(params[1])

dictLines = [input() for i in range(m)]
dict = {line.split()[0]:int(line.split()[1]) for line in dictLines}

text = [w.strip().split() for w in sys.stdin.readlines()]
dots = [i for i,x in enumerate(text) if x == ['.']]

start = 0
for stop in dots:
    print(sum([dict.get(item,0) for sublist in text[start:stop] for item in sublist]))
    start = stop
