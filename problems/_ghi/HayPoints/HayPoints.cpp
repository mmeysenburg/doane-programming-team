#include <iostream>
#include <map>
#include <string>

/*
 * C++ solution to Hay Points Kattis problem.
 */
int main() {
    using namespace std;

    int m, n;
    cin >> m;
    cin >> n;

    map<string, int> dict;
    for(int i = 0; i < m; i++) {
        string w;
        int p;
        cin >> w;
        cin >> p;
        dict[w] = p;
    }

    for(int i = 0; i < n; i++) {
        int sum = 0;
        string w;
        cin >> w;
        while(w != ".") {
            map<string, int>::iterator it = dict.find(w);
            if(it != dict.end()) {
                sum += dict[w];
            }
            cin >> w;
        }
        cout << sum << endl;
    }
}