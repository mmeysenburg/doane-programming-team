'''
 * Solution to Grass Seed Inc problem.
'''
C = float(input())
L = int(input())

area = 0.0
for i in range(L):
  dim = [float(x) for x in input().split()]
  area += dim[0] * dim[1]

cost = area * C

print('{0:0.7f}'.format(cost))