def isPal(val):
    return val == val[-1::-1]
al = {'a':0, 'b':0, 'c':0, 'd':0, 'e':0, 'f':0, 'g':0, 'h':0,
      'i':0, 'j':0, 'k':0, 'l':0, 'm':0, 'n':0, 'o':0, 'p':0,
      'q':0, 'r':0, 's':0, 't':0, 'u':0, 'v':0, 'w':0, 'x':0,
      'y':0, 'z':0}
raw = [x for x in input().lower() if x in al]
p = [1 for i in range(len(raw) - 1) for j in range(i + 1, len(raw)) if isPal(raw[i:j + 1])]
if len(p) == 0:
    print('Anti-palindrome')
else:
    print('Palindrome')
