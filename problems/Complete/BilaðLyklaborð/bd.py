inp = [c for c in input()]
oup = []
idx = 1
c = inp[0]
oup.append(c)
while idx < len(inp):
    if inp[idx] == c:
        idx+=1
    else:
        oup.append(inp[idx])
        c = inp[idx]
        idx+=1
print(''.join(oup))