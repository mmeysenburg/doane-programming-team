import sys

data = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]

# make a copy and sort it
corr = data[1][:]
corr.sort()
incorr = data[1]

# count how many are out of place
acc = 0
for i in range(len(corr)):
    if corr[i] != incorr[i]:
        acc += 1
print(acc)