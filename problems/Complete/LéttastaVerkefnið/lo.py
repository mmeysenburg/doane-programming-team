N = int(input())
M = int(input())
pNames = [x for x in input().split()]
pScores = [0 for i in range(N)]
for i in range(M):
    scores = [int(x) for x in input().split()]
    for j in range(N):
        pScores[j] += scores[j]

maxIdx = -1
max = -1
for i in range(N):
    if pScores[i] > max:
        max = pScores[i]
        maxIdx = i

print(pNames[maxIdx])