import java.util.Arrays;
import java.util.Collections;
import java.util.Scanner;

public class FM {
    static public class Order {
        public  Order(int num, int weight) {
            this.num = num;
            this.weight = weight;
        }

        @Override
        public String toString() {
            return "[" + num + ", " + weight + "]";
        }

        public int num;
        public int weight;
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        int n = Integer.parseInt(tokens[0]);
        int m = Integer.parseInt(tokens[1]);

        tokens = stdin.nextLine().split(" ");
        Integer[] weights = new Integer[tokens.length];
        for(int i = 0; i < weights.length; i++) {
            weights[i] = Integer.parseInt(tokens[i]);
        }
        Arrays.sort(weights, Collections.reverseOrder());

        Order[] orders = new Order[m];
        for(int i = 0; i < m; i++) {
            tokens = stdin.nextLine().split(" ");
            orders[i] = new Order(Integer.parseInt(tokens[0]),
                Integer.parseInt(tokens[1]));
        }
        Arrays.sort(orders, (Order x, Order y) -> y.weight - x.weight);

        int profit = 0;
        int oix = 0;
        for (Integer w : weights) {
            if (oix >= m) {
                break;
            } else {
                orders[oix].num--;
                profit += w * orders[oix].weight;

                if (orders[oix].num <= 0) {
                    oix++;
                }
            }
        }

        System.out.println(profit);
    }
}