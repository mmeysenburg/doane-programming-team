'''
Python solution to Fishmongers
'''
from sys import stdin

# read all the input, split on whitespace, and 
# convert to integers
inp = [ [int(x) for x in line.split()] 
       for line in stdin.readlines() ]

# sort fish by weight, in descending order
weights = sorted(inp[1], reverse=True)

# sort orders by amount willing to pay, descending order
orders = sorted(inp[2:], key=lambda ord: ord[1], 
                reverse=True)

profit = 0   # how much we make
oix = 0      # index into the order list

# for each of our fish...
for w in weights:
    # if there are no more buyers, exit
    if oix >= inp[0][1]:
        break

    # Sell current fish to max profit buyer.
    # Deduct one from # of fish they want to buy...
    orders[oix][0] -= 1

    # ... and add to our profit
    profit += w * orders[oix][1]

    # if we've sold all we can to max profit dude,
    # move to next highest
    if orders[oix][0] == 0:
        oix += 1

# report profit 
print(profit)