﻿using System;
class FishMongers
{
    public struct Order
    {
        public long num;
        public long weight;
    }

    public static void Main(string[] args)
    {
        int m, n;
        string[] tokens = Console.ReadLine().Split(' ');
        n = Convert.ToInt32(tokens[0]);
        m = Convert.ToInt32(tokens[1]);

        long[] weights = Array.ConvertAll(Console.ReadLine().Trim().Split(' '), Convert.ToInt64);
        Array.Sort(weights, delegate(long x, long y) { return x == y ? 0 : y > x ? 1 : -1; });
       
        Order[] orders = new Order[m];
        for (int i = 0; i < m; i++)
        {
            tokens = Console.ReadLine().Split(' ');
            orders[i] = new Order();
            orders[i].num = Convert.ToInt64(tokens[0]);
            orders[i].weight = Convert.ToInt64(tokens[1]);
        }
        Array.Sort(orders, delegate(Order x, Order y) { return Math.Sign(y.weight - x.weight); });

        long profit = 0;
        int oix = 0;
        for (int i = 0; oix < m && i < weights.Length; i++)
        {
            orders[oix].num -= 1;
            profit += weights[i] * orders[oix].weight;

            if (orders[oix].num <= 0) 
            { 
                oix += 1;
            }
        }

        Console.WriteLine(profit);
    }
}