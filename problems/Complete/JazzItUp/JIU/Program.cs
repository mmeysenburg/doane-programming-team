﻿using System;

class JIU
{
    public static void Main(string[] args)
    {
        int n = Convert.ToInt32(Console.ReadLine());
        for(int m = 2; m < n; m++)
        {
            int prod = m * n;
            bool found = false;
            for(int k = 2; ! found && k <= (int)Math.Sqrt(prod); k++)
            {
                found = prod % (k * k) == 0;
            }
            if(!found)
            {
                Console.WriteLine(m);
                break;
            }
        }
    }
}