'''
Brute-force Python solution to the Jazz it Up! 
Kattis problem
'''
from math import sqrt

n = int(input())

# look at all possible m's
for m in range(2, n):
    prod = m * n
    found = False

    # search for a k^2 that is a factor of mn
    for k in range(2, int(sqrt(prod)) + 1):
        if prod % (k * k) == 0:
            found = True
            break
    # no k^2 factor? print and bail
    if not found:
        print(m)
        exit()