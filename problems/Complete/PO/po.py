from sys import stdin
inp = stdin.readlines()
sum = 0
for i in range(2, len(inp), 2):
    sum += int(inp[i])
if sum > 0:
    print('Usch, vinst')
elif sum < 0:
    print('Nekad')
else:
    print('Lagom')