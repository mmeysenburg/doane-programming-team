b = [input().split() for i in range(3)]

def row_win(b, r, ch):
    return b[r][0] == ch and b[r][1] == ch and b[r][2] == ch

def col_win(b, c, ch):
    return b[0][c] == ch and b[1][c] == ch and b[2][c] == ch

def diag_win(b, ch):
    return (b[0][0] == ch and b[1][1] == ch and b[2][2] == ch) or \
        (b[0][2] == ch and b[1][1] == ch and b[2][0] == ch)

def all_rows(b, ch):
    return row_win(b, 0, ch) or row_win(b, 1, ch) or row_win(b, 2, ch)

def all_cols(b, ch):
    return col_win(b, 0, ch) or col_win(b, 1, ch) or col_win(b, 2, ch)

if all_rows(b, 'O') or all_cols(b, 'O') or diag_win(b, 'O'):
    print('Abdullah har vunnit')
elif all_rows(b, 'X') or all_cols(b, 'X') or diag_win(b, 'X'):
    print('Johan har vunnit')
else:
    print('ingen har vunnit')