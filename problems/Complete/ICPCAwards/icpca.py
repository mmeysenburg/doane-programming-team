'''
Python solution for ICPC Awards Kattis problem.
'''
import sys

# read all the input, except for the first line
allinp = [x.split() for x in sys.stdin.readlines()[1:]]

# build a dictionary of university : team pairs, with
# the first team from the university only
f = {}
for x in allinp:
    if x[0] not in f:
        f[x[0]] = x[1]

# print the 1st 12 university : team combos
for k in list(f.items())[:12]:
    print('{0:s} {1:s}'.format(k[0], k[1]))