'''
Python solution to the Ads Kattis problem.
'''
import string
import sys

def markImage(lines, r, c, images):
    '''
    Change the border of a discovered image from '+' to '∓'.

    parameters
    ----------
    lines : list of lists, representing 2D array of characters
      in the Web page
    r, c : coordinates of the upper-left corner of the image
    images : list of (area, r1, c1, r2, c2) tuples describing
      all of the images in the page
    '''
    # mark top border of image
    endC = c
    for c1 in range(c, len(lines[r])):
        if lines[r][c1] == '+':
            lines[r][c1] = '∓'
        else:
            break
        endC = c1

    # mark left & right image borders
    endR = r + 1
    for r1 in range(r + 1, len(lines)):
        if lines[r1][c] == '+':
            lines[r1][c] = '∓'
            lines[r1][endC] = '∓'
        else:
            break
        endR = r1

    # mark bottom border
    for c1 in range(c + 1, endC):
        lines[endR][c1] = '∓'

    # append a tuple describing the image; this will be 
    # sorted by area later to look for / remove ads
    wid = endC - c + 1
    hei = endR - r + 1
    images.append((wid * hei, r, c, endR, endC))

def findImages(lines, images):
    '''
    Find and mark all of the images in the page

    parameters
    ----------
    lines : list of lists, representing 2D array of characters
      in the Web page
    images : list of (area, r1, c1, r2, c2) tuples describing
      all of the images in the page    
    '''
    for r in range(len(lines)):
        for c in range(len(lines[r])):
            if lines[r][c] == '+':
                markImage(lines, r, c, images)

def removeAd(lines, img):
    '''
    Remove an image from the page, because it is an ad

    parameters
    ----------
    lines : list of lists, representing 2D array of characters
      in the Web page
    img : (area, r1, c1, r2, c2) tuple describing the image to remove
    '''
    for r in range(img[1], img[3] + 1):
        for c in range(img[2], img[4] + 1):
            lines[r][c] = ' '

def removeAds(lines, images, allowed_dic):
    '''
    Remove images that contain ads from the page.

    parameters
    ----------
    lines : list of lists, representing 2D array of characters
      in the Web page
    images : list of (area, r1, c1, r2, c2) tuples describing
      all of the images in the page
    allowed_dic : dictionary of characters allowed in the image
    '''
    # look at each image, from smallest to largest, and remove
    # each one that contains an ad
    for img in images:
        for r in range(img[1] + 1, img[3]):
            for c in range(img[2] + 1, img[4]):
                if lines[r][c] not in allowed_dic:
                    removeAd(lines, img)

###################### main program ###########################################

# read in Web page as 2D array of characters, without the end \n
lines = [[c for c in line[:-1]] for line in sys.stdin.readlines()[1:]]

# prep data structure for characters allowed in image
allowed_list = [c for c in string.ascii_letters] + \
    [c for c in string.digits] + \
    ['?', '!', ',', '.', ' ', '∓']
allowed_dic = {x:0 for x in allowed_list}

# find and mark images
images = []
findImages(lines, images)
# sort image tuples by area, smallest to largest
images.sort()

# remove ads
removeAds(lines, images, allowed_dic)

# print out processed Web page
for line in lines:
    print(''.join(line).replace('∓', '+'))