import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Scanner;

/**
 * Java solution to the Keystrokes Kattis problem. 
 * 
 * Strategy: use a doubly-linked list to efficiently
 * build the password. 
 */
public class KS {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        char[] inp = stdin.nextLine().toCharArray();

        // we'll build the password in a linked list,
        // with a bidirectional iterator serving as the
        // cursor
        LinkedList<Character> pwd = new LinkedList<>();
        ListIterator<Character> cursor = pwd.listIterator(0);
        for (char c : inp) {
            switch (c) {
                case 'L':
                    cursor.previous();
                    break;
                case 'R':
                    cursor.next();
                    break;
                case 'B':
                    cursor.previous();
                    cursor.remove();
                    break;
                default:
                    cursor.add(c);
                    break;
            }
        }
        // now format the password for printing
        StringBuilder sb = new StringBuilder();
        for(char c : pwd) {
            sb.append(c);
        }
        System.out.println(sb.toString());
    }
}