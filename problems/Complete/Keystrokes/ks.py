p = [c for c in input().strip()]
i = 0
while i < len(p):
    if p[i] == 'L':
        p.pop(i)
        i -= 1
    elif p[i] == 'R':
        p.pop(i)
        i += 1
    elif p[i] == 'B':
        p.pop(i - 1)
    else:
        i += 1

print(''.join(p)) 