import sys
cities = [x.strip() for x in sys.stdin.readlines()][2::2]
counts = {}
for city in cities:
    counts[city] = counts.get(city, 0) + 1

for city in counts:
    print(city, counts[city])