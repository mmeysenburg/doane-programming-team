'''
Python solution to the Kattis Ding Dong Ditch problem. 
'''
# skip N, Q line
input()

# read and sort anger values in ascending order
angers = sorted([int(x) for x in input().split()])
# convert angers into a cumulative sum list
for i in range(1, len(angers)):
    angers[i] += angers[i - 1]

# read number of houses for each friend
num_houses = [int(x) - 1 for x in input().split()]

# print smallest anger value for each friend
for nh in num_houses:
    print(angers[nh])