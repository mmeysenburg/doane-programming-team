import sys

inp = [x for x in sys.stdin.readlines()]
vals = {}
orders = {} 
for line in inp[1:]:
    tok = line.split()
    idx = 0
    for c in tok[1]:
        if c in vals:
            vals[c].append(int(tok[0]))
        else:
            vals[c] = [int(tok[0])]
        if c not in orders:
            orders[c] = -1
        else:
            if orders[c] < 0:
                orders[c] = idx
        idx += 1

out = []
for key in vals:
    period = vals[key][-1] - vals[key][-2]
    first = vals[key][0]
    firstOcc = orders[key]
    out.append((-period, first, firstOcc, key))

out.sort()
print(out[0][3])