'''
Python solution to Pea Soup and Pancakes Kattis problem
'''
n = int(input())
for i in range(n):
    k = int(input())
    name = input()
    menu = [input() for j in range(k)]
    if ('pea soup' in menu) and ('pancakes' in menu):
        print(name)
        exit()
print('Anywhere is fine I guess')