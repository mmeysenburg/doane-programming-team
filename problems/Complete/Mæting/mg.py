# skip 1st line
input()
# list of monday attendees
mon = input().strip().split()
# dictionary of tuesday attendees
tue = {x:'0' for x in input().strip().split()}
# print results
print(' '.join([x for x in mon if x in tue]))
