n = int(input())
x = int(input())
y = int(input())
L = sorted([int(l) for l in input().split()])

i = 0
sum = 0
cont = True
while cont and i < n:
    sum += L[i] * x
    i += 1
    avg = sum / i
    if avg > y:
        i -= 1
        cont = False
print(i)