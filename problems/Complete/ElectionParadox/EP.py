'''
Python solution to the Election Paradox problem.
'''
from sys import stdin

# get the region populations (2nd line of the input), sorted big to small
regions = sorted([int(x) for x in stdin.readlines()[1].split()], reverse=True)

# Find one less than half the number of regions.
# A candidate can win that many regions and still lose.
div = len(regions) // 2

# Sum the number of votes before the div cutoff, plus the 
# number of votes you could win without winning each of the
# other regions.
print(sum(regions[:div]) + sum([x // 2 for x in regions[div:]]))
