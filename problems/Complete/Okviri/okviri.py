line = input()

grid = []
for i in range(5):
    grid.append(['.' for j in range(5 * len(line) - len(line) + 1)])

for i in range(len(line)):
    if (i + 1) % 3 != 0:
        ltr_idx = 4 * i + 2
        grid[2][ltr_idx] = line[i]
        grid[0][ltr_idx] = '#'
        grid[1][ltr_idx - 1] = '#'
        grid[1][ltr_idx + 1] = '#'
        grid[2][ltr_idx - 2] = '#'
        grid[2][ltr_idx + 2] = '#'
        grid[3][ltr_idx - 1] = '#'
        grid[3][ltr_idx + 1] = '#'
        grid[4][ltr_idx] = '#'

for i in range(len(line)):
    if (i + 1) % 3 == 0:
        ltr_idx = 4 * i + 2
        grid[2][ltr_idx] = line[i]
        grid[0][ltr_idx] = '*'
        grid[1][ltr_idx - 1] = '*'
        grid[1][ltr_idx + 1] = '*'
        grid[2][ltr_idx - 2] = '*'
        grid[2][ltr_idx + 2] = '*'
        grid[3][ltr_idx - 1] = '*'
        grid[3][ltr_idx + 1] = '*'
        grid[4][ltr_idx] = '*'

for out in grid:
    print(''.join(out))