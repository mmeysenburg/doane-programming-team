import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Scanner;

public class SA {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tokens = stdin.nextLine().split(" ");
        BigDecimal a = new BigDecimal(tokens[0]);
        BigDecimal b = new BigDecimal(tokens[1]);
        BigDecimal c = new BigDecimal(tokens[2]);

        BigDecimal ans = a.multiply(b);
        ans = ans.divide(c, 10, RoundingMode.CEILING);
        System.out.println(ans);
    }
}