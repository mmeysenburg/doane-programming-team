abc = [int(x) for x in input().split()]
ans = abc[0] * abc[1] / abc[2]
print('{:.7f}'.format(ans))