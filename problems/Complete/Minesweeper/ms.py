'''
Python solution to the Kattis Minesweeper problem.
'''
# read the metadata line
n, m, k = [int(x) for x in input().strip().split()]

# construct the board, initially all empty
board = [['.']*m for i in range(n)]

# read all mine locations
for i in range(k):
    r, c = [int(x) - 1 for x in input().strip().split()]
    # place the mine on the board
    board[r][c] = '*'

# print all rows of the board
for row in board:
    print(''.join(row))