'''
Python solution to the Hot Hike Kattis problem.
'''
import sys

# get the 2nd line of input as a list of integers
t_s = [int(x) for x in sys.stdin.readlines()[1].split()]

# now look at all of the hiking days, finding the minimum
# high temp 
t_i = 0
t_max = 41
for i in range(len(t_s) - 2):
    t = max(t_s[i], t_s[i + 2])
    t_max = min(t, t_max)
    if t < t_max:
        t_max = t
        t_i = i

print('{0:d} {1:d}'.format(t_i + 1, t_max))