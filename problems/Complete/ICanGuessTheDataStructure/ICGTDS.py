'''
Attempted Python solution to the I Can Guess The Data Structure
Kattis problem. Uses the same strategy as the C++ version, but 
it exceeds the time limit on the second test case. 
'''
import sys
from collections import deque
from queue import PriorityQueue

s = deque()
q = deque()
pq = PriorityQueue()

i_lines = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]
lines = []
while len(i_lines) > 0:
    n = i_lines[0][0]
    lines.append(i_lines[1:n + 1])
    i_lines = i_lines[n + 1:]

for line in lines:
    s.clear()
    q.clear()
    pq.queue.clear()

    cbs = True
    cbq = True
    cbpq = True

    for cmd in line:
        if cmd[0] == 1:
            if cbs:
                s.append(cmd[1])
            if cbq:
                q.append(cmd[1])
            if cbpq:
                pq.put(-cmd[1])
        elif cmd[0] == 2:
            if cbs:
                if len(s) == 0 or s[-1] != cmd[1]:
                    cbs = False
                else:
                    s.pop()
            if cbq:
                if len(q) == 0 or q[0] != cmd[1]:
                    cbq = False
                else:
                    q.popleft()
            if cbpq:
                if pq.qsize() == 0 or pq.queue[0] != -cmd[1]:
                    cbpq = False
                else:
                    pq.get()

    if cbs and not cbq and not cbpq:
        print('stack')
    elif cbq and not cbs and not cbpq:
        print('queue')
    elif cbpq and not cbs and not cbq:
        print('priority queue')
    elif not cbs and not cbq and not cbpq:
        print('impossible')
    else:
        print('not sure')