/**
 * C++ solution to the I Can Guess The Data Structure
 * Kattis problem.
 * 
 * Strategy: put the inputs into the data structures
 * and compare outputs as we go, disqualifying structures
 * that can't be correct.
 */
#include <cstdlib>
#include <iostream>
#include <stack>
#include <queue>

using namespace std;

int main() {
    short n, c, v;

    while(cin >> n) {
        queue<short> q;
        stack<short> s;
        priority_queue<short> pq;

        bool cbs = true, cbq = true, cbpq = true;

        for(short i = (short)0; i < n; i++) {
            cin >> c >> v;
            switch(c) {
                case (short)1:
                    if(cbs) s.push(v);
                    if(cbq) q.push(v);
                    if(cbpq) pq.push(v);
                    break;
                case (short)2:
                    if(cbs) {
                        if(s.empty() || s.top() != v)
                            cbs = false;
                        else
                            s.pop();
                    }
                    if(cbq) {
                        if(q.empty() || q.front() != v) 
                            cbq = false;
                        else
                            q.pop();
                    }
                    if(cbpq) {
                        if(pq.empty() || pq.top() != v) 
                            cbpq = false;
                        else    
                            pq.pop();
                    }
            } // switch command
        } // for commands

        if(cbs && !cbq && !cbpq) {
            cout << "stack" << endl;
        } else if(cbq && !cbs && !cbpq) {
            cout << "queue" << endl;
        } else if(cbpq && !cbs && !cbq) {
            cout << "priority queue" << endl;
        } else if(!cbs && !cbq && !cbpq) {
            cout << "impossible" << endl;
        } else {
            cout << "not sure" << endl;
        }
    } // while cases 
}