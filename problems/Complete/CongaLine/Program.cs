﻿using System;
class CongaLine
{
    private static void append(ref int last, int mic, int[] next, int[] prev)
    {
        next[last] = mic;
        prev[mic] = last;
        next[mic] = -1;
        last = mic;
    }

    private static void insert(int mic, int pard, int[] next, int[] prev)
    {
        next[mic] = next[pard];
        prev[mic] = pard;
        prev[next[pard]] = mic;
        next[pard] = mic;
    }

    private static void remove(int mic, int[] next, int[] prev)
    {
        next[prev[mic]] = next[mic];
        prev[next[mic]] = prev[mic];
    }

    private static void removeFirst(ref int first, int mic, int[] next, int[] prev)
    {
        first = next[mic];
        prev[first] = -1;
    }

    private static void removeLast(ref int last, int mic, int[] next, int[] prev)
    {
        next[prev[mic]] = -1;
        last = prev[mic];
    }

    public static void Main(string[] args)
    {
        int N, Q;

        string[] tokens = Console.ReadLine().Split(' ');
        N = Convert.ToInt32(tokens[0]);
        Q = Convert.ToInt32(tokens[1]);

        string[] line = new string[N * 2];
        int[] next = new int[N * 2];
        int[] prev = new int[N * 2];
        int[] partner = new int[N * 2];
        int mic = 0;
        int first = 0;
        int last = 2 * N - 1;

        for (int i = 0; i < N; i++)
        {
            tokens = Console.ReadLine().Split(' ');
            line[2 * i] = tokens[0];
            line[2 * i + 1] = tokens[1];

            partner[2 * i] = 2 * i + 1;
            partner[2 * i + 1] = 2 * i;
        }

        for(int i = 0; i < last; i++)
        {
            next[i] = i + 1;
        }
        next[last] = -1;

        for(int i = 1; i <= last; i++)
        {
            prev[i] = i - 1; 
        }
        prev[0] = -1;

        string commands = " ";
        commands = Console.ReadLine();
        foreach(char c in commands)
        {
            switch(c)
            {
                case 'F':
                    mic = prev[mic];
                    break;
                case 'B':
                    mic = next[mic];
                    break;
                case 'R':
                    if(mic == last)
                    {
                        mic = first;
                    } else
                    {
                        if(mic == first)
                        {
                            removeFirst(ref first, mic, next, prev);

                            append(ref last, mic, next, prev);

                            mic = first;
                        } else
                        {
                            int t = next[mic];

                            remove(mic, next, prev);

                            append(ref last, mic, next, prev);

                            mic = t;
                        }
                    }
                    break;
                case 'C':
                    // pass and move behind partner

                    // am i last in line?
                    if (mic == last)
                    {
                        // yes!

                        // look up my partner
                        int pard = partner[mic];

                        // am I already behind my partner? Only move if not
                        if (prev[mic] != pard)
                        {
                            // remove me from the line
                            removeLast(ref last, mic, next, prev);

                            // put me behind my partner
                            insert(mic, pard, next, prev);
                        }

                        // pass mic to first in line
                        mic = first;
                    }
                    else
                    {
                        // no!
                        // look up my partner
                        int pard = partner[mic];

                        int t = next[mic];  // person to pass to

                        // am I already behind my partner? Only move if not
                        if (prev[mic] != pard)
                        {
                            // am I at the front of the line?
                            if (mic == first)
                            {
                                // yes!
                                // remove me from the line
                                removeFirst(ref first, mic, next, prev);

                                // is my partner last?
                                if (pard == last)
                                {
                                    // yes!
                                    // add me to the back
                                    append(ref last, mic, next, prev);
                                }
                                else
                                {
                                    // no!
                                    // put me behind my partner
                                    insert(mic, pard, next, prev);
                                }
                            }
                            else
                            {
                                // no! i'm not first
                                // remove me from the line
                                remove(mic, next, prev);

                                // is my partner last?
                                if (pard == last)
                                {
                                    // yes!
                                    // add me to the back
                                    append(ref last, mic, next, prev);
                                }
                                else
                                {
                                    // no! partner is not last
                                    // put me behind my partner
                                    insert(mic, pard, next, prev);
                                } // if partner last
                            } // if first in line
                        } // if i need to move

                        // pass mic to next person
                        mic = t;
                    }
                    break;
                case 'P':
                    Console.WriteLine(line[partner[mic]]);
                    break;
            } // switch
        } // for command 

        Console.WriteLine("");
        mic = first;
        while(mic != -1)
        {
            Console.WriteLine(line[mic]);
            mic = next[mic];
        }
    } // Main
} // CongaLine