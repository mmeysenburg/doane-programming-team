#include <cstdlib>
#include <iostream>
#include <string>

using namespace std;

// add me to the end of the line
void append(int &last, int mic, int *next, int *prev) {
    next[last] = mic;
    prev[mic] = last;
    next[mic] = -1;
    last = mic;
}

// add me after my partner
void insert(int mic, int pard, int *next, int *prev) {
    next[mic] = next[pard];
    prev[mic] = pard;
    prev[next[pard]] = mic;
    next[pard] = mic;
}

// remove me from the line
void remove(int mic, int *next, int *prev) {
    next[prev[mic]] = next[mic];
    prev[next[mic]] = prev[mic];
}

// remove me from the front of the line
void removeFirst(int &first, int mic, int *next, int *prev) {
    first = next[mic];
    prev[first] = -1;
}

// remove me from the end of the line
void removeLast(int &last, int mic, int *next, int *prev) {
    next[prev[mic]] = -1;
    last = prev[mic];
}

/**
 * @brief Main program.
 * 
 * @return int 
 */
int main() {
    int N, Q;

    // read parameters
    cin >> N >> Q;

    // allocate data structures
    string *line = new string[N * 2];   // the conga line names
    int *next = new int[N * 2];         // next[i] = person after person i
    int *prev = new int[N * 2];         // prev[i] = person before person i
    int *partner = new int[N * 2];      // partner[i] = person i's partner
    int mic = 0;                        // current mic index
    int first = 0;                      // index of 1st person
    int last = 2 * N - 1;               // index of last person

    // read names
    for(int i = 0; i < N; i++) {
        // names from input
        cin >> line[2 * i];
        cin >> line[2 * i + 1];

        // initial partner indices
        partner[2 * i] = 2 * i + 1;
        partner[2 * i + 1] = 2 * i;
    }

    // populate next and prev arrays
    for(int i = 0; i < last; i++) {
        next[i] = i + 1;
    }
    next[last] = -1;
    for(int i = 1; i <= last; i++) {
        prev[i] = i - 1;
    }
    prev[0] = -1;

    // read and process the commands
    string commands;
    cin >> commands;
    for(char c : commands) {
        switch(c) {
        case 'F':
            // pass mic to previous person
            mic = prev[mic];
            break;
        case 'B':
            // pass mic to next person
            mic = next[mic];
            break;
        case 'R':
            // pass and move to back

            // am I already last in line?
            if(mic == last) {
                // yes! just pass mic to front
                mic = first;
            } else {
                // no! I need to move

                // am I at the front of the line?
                if(mic == first) {
                    // yes!

                    // remove myself from the line
                    removeFirst(first, mic, next, prev);

                    // add myself to the end
                    append(last, mic, next, prev);

                    // pass the mic
                    mic = first;
                } else {
                    // no!
                    int t = next[mic]; // person to pass to

                    // remove myself from the line
                    remove(mic, next, prev);

                    // add myself to the end
                    append(last, mic, next, prev);

                    // pass the mic
                    mic = t;
                }
            }
            break;
        case 'C':
            // pass and move behind partner
            
            // am i last in line?
            if(mic == last) {
                // yes!

                // look up my partner
                int pard = partner[mic];

                // am I already behind my partner? Only move if not
                if(prev[mic] != pard) {
                    // remove me from the line
                    removeLast(last, mic, next, prev);

                    // put me behind my partner
                    insert(mic, pard, next, prev);
                }

                // pass mic to first in line
                mic = first;
            } else {
                // no!
                // look up my partner
                int pard = partner[mic];

                int t = next[mic];  // person to pass to

                // am I already behind my partner? Only move if not
                if(prev[mic] != pard) {
                    // am I at the front of the line?
                    if(mic == first) {
                        // yes!
                        // remove me from the line
                        removeFirst(first, mic, next, prev);

                        // is my partner last?
                        if(pard == last) {
                            // yes!
                            // add me to the back
                            append(last, mic, next, prev);
                        } else {
                            // no!
                            // put me behind my partner
                            insert(mic, pard, next, prev);
                        }
                    } else {
                        // no! i'm not first
                        // remove me from the line
                        remove(mic, next, prev);

                        // is my partner last?
                        if(pard == last) {
                            // yes!
                            // add me to the back
                            append(last, mic, next, prev);
                        } else {
                            // no! partner is not last
                            // put me behind my partner
                            insert(mic, pard, next, prev);
                        } // if partner last
                    } // if first in line
                } // if i need to move

                // pass mic to next person
                mic = t;
            }
            break;
        case 'P':
            // look up my partner and print their name
            cout << line[partner[mic]] << endl;
            break;
        } // switch command
        // dump(line, next, prev, partner, first, last, mic, last + 1);
    } // for command

    // output conga line
    cout << endl;
    mic = first;
    while(mic != -1) {
        cout << line[mic] << endl;
        mic = next[mic];
    }

    // free memeory
    delete [] line;
    delete [] next;
    delete [] prev;
    delete [] partner;

    return EXIT_SUCCESS;
}