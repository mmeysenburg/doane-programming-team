bs = [input(), input(), input()]
b = [[c for c in line] for line in bs]

def rowWin(b, r, ch):
    return b[r][0] == ch and b[r][1] == ch and b[r][2] == ch

def colWin(b, c, ch):
    return b[0][c] == ch and b[1][c] == ch and b[2][c] == ch

def diagWin(b, ch):
    return (b[0][0] == ch and b[1][1] == ch and b[2][2] == ch) or \
        (b[0][2] == ch and b[1][1] == ch and b[2][0] == ch)

def allRows(b, ch):
    return rowWin(b, 0, ch) or rowWin(b, 1, ch) or rowWin(b, 2, ch)

def allCols(b, ch):
    return colWin(b, 0, ch) or colWin(b, 1, ch) or colWin(b, 2, ch)

if allRows(b, 'O') or allCols(b, 'O') or diagWin(b, 'O'):
    print('Jebb')
else:
    print('Neibb')