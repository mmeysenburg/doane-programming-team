import sys

def findFactors(val):
    factors = []
    for n in range(2, 10):
        if val % n == 0:
            factors.append(n)
    factors.reverse()
    return factors

vals = [int(x[:-1]) for x in sys.stdin.readlines()[:-1]]

for val in vals:
    if val < 10:
        print('1{:d}'.format(val))
    else:
        factors = findFactors(val)
        remainder = val
        i = 0
        prod = 0
        power = 1
        while i < len(factors) and remainder > 1:
            if factors[i] <= remainder and remainder % factors[i] == 0:
                remainder //= factors[i]
                prod += factors[i] * power
                power *= 10
            else:
                i += 1
        if remainder == 1:
            print(prod)
        else:
            print('There is no such number.')