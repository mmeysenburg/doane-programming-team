import sys

memory = {}
lines = [int(x) for x in sys.stdin.readlines()]

numCases = lines[0]
idx = 1
for case in range(numCases):
    n = lines[idx]
    idx += 1
    memory.clear()
    longest = 0
    curr = 0
    answer = 0
    for i in range(1, n + 1):
        if lines[idx] in memory:
            longest = max(longest, memory[lines[idx]])
            curr = i - longest - 1
        curr += 1
        memory[lines[idx]] = i
        answer = max(answer, curr)
        idx += 1
    print(answer)