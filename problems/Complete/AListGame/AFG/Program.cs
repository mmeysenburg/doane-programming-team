﻿using System;

class AListGame
{
    private static int factor(int n)
    {
        int num = 0, i = 2;
        while (i * i <= n)
        {
            if(n % i != 0)
            {
                i++;
            } else
            {
                n /= i;
                num++;
            }
        }
        if (n > 1) num++;

        return num;
    }
    public static void Main(string[] args)
    {
        int x = Convert.ToInt32(Console.ReadLine());
        Console.WriteLine(factor(x));
    }
}