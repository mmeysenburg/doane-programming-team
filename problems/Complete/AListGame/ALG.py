'''
Python solution to the "A List Game" Kattis problem.
'''
def prime_factors(n):
    '''
    Perform a prime factorization of the number n, returning
    the number of factors.
    '''
    num = 0
    i = 2
    while i * i <= n:
        if n % i:
            i += 1
        else:
            n //= i
            num += 1
    if n > 1:
        num += 1
    return num

# the answer to the problem will be the
# number of prime factors of x
print(prime_factors(int(input())))