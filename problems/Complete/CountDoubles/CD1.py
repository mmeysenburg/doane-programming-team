'''
Python solution to the Count Doubles Kattis problem.

Brute force strategy: look at all of the subarrays and 
see if there are at least two evens; print the number of 
the qualifying subarrays.
'''
import sys

# read all input data into a list of two lists of integers
d = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]
# map input integers to 1s if even, 0 otherwise
d[1] = list(map(lambda x: 1 if x % 2 == 0 else 0, d[1]))

# count number of subarrays with at least two evens
print(len([1 for i in range(0, d[0][0] - d[0][1] + 1) 
           if sum(d[1][i:i + d[0][1]]) >= 2]))