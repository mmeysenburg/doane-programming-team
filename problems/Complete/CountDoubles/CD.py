'''
Python solution to the Count Doubles Kattis problem.

Brute force strategy: look at all of the subarrays and 
see if there are at least two evens; print the number of 
the qualifying subarrays.
'''
import sys

# read all input data into a list of two lists of integers
d = [[int(x) for x in line.split()] for line in sys.stdin.readlines()]

def has_two_evens(sa, i, j):
    '''
    See if a subarray has at least two evens.
    '''
    sum = 0
    for ii in range(i, j):
        if sa[ii] % 2 == 0:
            sum += 1
            if sum >= 2:
                return True
    return False

# count number of subarrays with at least two evens
sum = 0
for i in range(0, d[0][0] - d[0][1] + 1):
    if has_two_evens(d[1], i, i + d[0][1]):
        sum += 1
print(sum)