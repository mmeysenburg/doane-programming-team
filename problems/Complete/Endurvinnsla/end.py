import sys

# read all the input lines into one list
allinp = [x.strip() for x in sys.stdin.readlines()]

# count the number of non-plastic items in the bag
non_plas = [1 for i in allinp[3:] if i == 'ekki plast']

# determine if we meet the threshold or not
if sum(non_plas) / float(allinp[2]) <= float(allinp[1]):
    print('Jebb')   # recycle
else:
    print('Neibb')  # burn