#include <cstdlib>
#include <iostream>

using namespace std;

int main() {
    size_t n;
    long d;
    cin >> n;
    cin >> d;

    long* pMoney = new long[n];
    for (size_t i = 0; i < n; i++)
    {
        cin >> pMoney[i];
    }

    long sum = 0;
    size_t idx = 0;
    while (pMoney[idx] > 0)
    {
        sum += pMoney[idx];
        pMoney[idx] = 0;
        idx = (int)((idx + d) % n);
    }

    cout << sum << endl;
    
    

    delete [] pMoney;

    return EXIT_SUCCESS;
}