tkns = [int(x) for x in input().split()]
n = tkns[0]
d = tkns[1]
money = [int(x) for x in input().split()]
sum = 0
idx = 0
while money[idx] > 0:
    sum += money[idx]
    money[idx] = 0
    idx = (idx + d) % len(money)

print(sum)