import java.util.Scanner;

public class P {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        String[] tkns = stdin.nextLine().split(" ");
        int n = Integer.parseInt(tkns[0]);
        long d = Long.parseLong(tkns[1]);
        tkns = stdin.nextLine().split(" ");
        long[] money = new long[n];
        for (int i = 0; i < money.length; i++) {
            money[i] = Long.parseLong(tkns[i]);
        }

        long sum = 0;
        int idx = 0;
        while (money[idx] > 0) {
            sum += money[idx];
            money[idx] = 0;
            idx = (int)((idx + d) % n);
        }

        System.out.println(sum);
    }
}