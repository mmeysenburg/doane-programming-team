﻿using System;

class Peningar
{
    public static void Main(string[] args)
    {
        string[] tkns = Console.ReadLine().Split(' ');
        int n = Convert.ToInt32(tkns[0]);
        long d = Convert.ToInt64(tkns[1]);

        long[] money = Array.ConvertAll(Console.ReadLine().Trim().Split(' '), Convert.ToInt64);
        long sum = 0;
        int idx = 0;
        while (money[idx] > 0)
        {
            sum += money[idx];
            money[idx] = 0;
            idx = Convert.ToInt32((idx + d) % money.Length);
        }

        Console.WriteLine(sum);
    }
}