n = [int(x) for x in input().split()]

s = [[int(x) for x in input().split()] for i in range(n[0])]

# initial time driven is length of road minus the distance
# to 1st stoplight, plus time spent waiting at 1st light
time = n[1] - s[0][0] + s[0][1]

# see if we wait any during the rest of the drive
for i in range(1, n[0]):
    arr = (time + s[i][0] - s[i - 1][0]) % (s[i][1] + s[i][2])
    print('\t',time,arr)
    if arr < s[i][1]:
        time += s[i][1]

# report result
print(time)