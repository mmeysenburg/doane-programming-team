parms = [int(x) for x in input().split()]
lights = [[0,0,0]] + [[int(x) for x in input().split()] for i in range(parms[0])]

time = 0

# now add in delays for the lights
for i in range(1, len(lights)):
    # time from light to light
    time += lights[i][0] - lights[i - 1][0]
    # add delay at red light if needed
    lTime = 0
    isRed = True
    while lTime <= time:
        if isRed:
            lTime += lights[i][1]
            isRed = False
        else:
            lTime += lights[i][2]
            isRed = True
    if not isRed:
        time += (lTime - time)

# add distance from last light to end
time += parms[1] - lights[-1][0]

print(time)
