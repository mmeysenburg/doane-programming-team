The Pygments Python package can format code as HTML for inclusion on Canvas. Install with 

pip install Pygments

Then use it like this: 

C:\Users\mark.meysenburg\AppData\Local\Packages\PythonSoftwareFoundation.Python.3.12_qbz5n2kfra8p0\LocalCache\local-packages\Python312\Scripts\pygmentize -f html -O style=colorful,linenos=1 -l python HRF.py > HRF.html

Path is included b/c I can't edit the Path variable on my new laptop :(