import java.util.*;

public class SetUnique {
    public static void main(String[] args) {
        // scanner to read from standard input
        Scanner stdin = new Scanner(System.in);
        // set for detecting duplicates
        Set<Integer> set = new HashSet<>();
        // counter for number of elements in S
        int n = 0;

        // read all the input...
        while (stdin.hasNextLine()) {
            // ... adding each to the set ...
            set.add(Integer.parseInt(stdin.nextLine()));
            // ... and counting how many we see
            n++;
        }
        stdin.close();

        // now, see if the size of the set equals the number of ints read
        if (n == set.size()) {
            System.out.println("No duplicates detected");
        } else {
            System.out.println("Duplicates detected");
        }
    }
}
