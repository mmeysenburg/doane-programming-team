\documentclass[11pt,twoside,letterpaper]{article}

\usepackage[letterpaper,top=0.75in,bottom=0.75in,inner=0.75in,outer=1in,twoside,bindingoffset=0.35in,includefoot]{geometry}				% Lulu.com book style
\usepackage{listings}		% for source listings
\usepackage{color}		% for colored source listings


% configure listings
\definecolor{mygreen}{rgb}{0, 0.6, 0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{ltgray}{rgb}{0.75, 0.75, 0.75}
\definecolor{mymauve}{rgb}{0.58,0,0.82}
\lstset{
	basicstyle=\ttfamily\footnotesize,
	commentstyle=\color{mygreen},
	keywordstyle=\color{blue},
	numbers=left,
	numbersep=5pt,
	numberstyle=\tiny\color{mygray},
	frame=single,
	keepspaces=true,
	stringstyle=\color{mymauve},
	tabsize=2,
	language=Java
}

\begin{document}

\title{What are data structures good for?}
\author{Mark M. Meysenburg}
\date{\today}
\maketitle

\begin{abstract}
Modern programming languages, like C++, Java, and Python, provide a basic but very versatile collection of \textit{data structures}, which are ways of storing and organizing data so that it can be accessed and modified in an efficient manner. This document introduces the basic data structures available in Java, and discusses the types of problems that can be solved with each. 
\end{abstract}

Most of the data structures covered below implement the \lstinline!Collection<E>! interface. According to the Java 8 API documentation, ``A collection represents a group of objects, known as its \textit{elements}. Some collections allow duplicate elements and others do not. Some are ordered and others unordered.'' Think of a \lstinline!Collection! as an abstract, generic data structure.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Sets
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sets}

According to the Java 8 API documentation, a \lstinline!Set! is ``A collection that contains no duplicate elements.'' Sets are most often used for fast membership testing. For example, consider a spell checker. The application would look at each word in a document, and check to see if each is in a dictionary of correctly spelled words. A \lstinline!Set! is a perfect data structure for holding the dictionary. 

\subsection{Sets in Java}

The abstract \lstinline!Set<E>! interface is the root set data structure in Java. There are two concrete types of sets in Java: \lstinline!TreeSet! and \lstinline!HashSet!.

%----------------------------------------------------------------------------------------------
% TreeSet<E>
%----------------------------------------------------------------------------------------------
\subsubsection{\lstinline!TreeSet<E>!}

\fcolorbox{black}{ltgray}{\parbox{0.9\textwidth}{%
	\vskip10pt
	\leftskip20pt \rightskip20pt {
	
\begin{description}
	\item[Class] \lstinline!TreeSet<E>!
	\item[Required import] \lstinline!import java.util.TreeSet;! or \lstinline!import java.util.*!
	\item[Operation time complexity] $\Theta \left( \lg n \right)$
	\item[Element requirements] Elements must implement \lstinline!Comparable!, or the \lstinline!TreeSet! must be constructed with a \lstinline!Comparator! that orders the elements
\end{description}
	}
	\vskip10pt
	
	}
}

\vskip10pt

The \lstinline!TreeSet<E>! uses a red-black binary search tree to store the elements in the set. This implies that the basic operations (add, remove, and contains) all have time complexity of $\Theta \left( \lg n \right)$. A sometimes useful side-effect of using the BST is that iterating over the elements of a \lstinline!TreeSet<E>! will visit the elements in their natural ordering, which is usually non-descending order. So, if you iterated over a spell check dictionary stored in a \lstinline!TreeSet<String>!, the loop would visit the words in the dictionary in alphabetical order.

%----------------------------------------------------------------------------------------------
% HashSet<E>
%----------------------------------------------------------------------------------------------
\subsubsection{\lstinline!HashSet<E>!}

\fcolorbox{black}{ltgray}{\parbox{0.9\textwidth}{%
	\vskip10pt
	\leftskip20pt \rightskip20pt {
	
\begin{description}
	\item[Class] \lstinline!HashSet<E>!
	\item[Required import] \lstinline!import java.util.HashSet;! or \lstinline!import java.util.*!
	\item[Operation time complexity] $\Theta \left(  1 \right)$, or rarely in the worst case, $\Theta \left( n \right)$
	\item[Element requirements] Elements must have an effective implementation of \lstinline!hashCode()!. Every Java object already has a \lstinline!hashCode()! implementation, that typically works by translating the object's address in memory into an \lstinline!int! value. 
\end{description}
	}
	\vskip10pt
	
	}
}

\vskip10pt

The \lstinline!HashSet! uses a hash table to store the elements in the set. This implies that the basic operations (add, remove, and contains) all have time complexity of $\Theta \left( 1 \right)$. However, the elements do not appear in any particular order when one iterates over the structure, and the time complexity can become $\Theta \left( n \right)$, for instance, if the hash function used for the structure's elements puts them all in the same bucket of the hash table. 

%----------------------------------------------------------------------------------------------
% Simulating sets
%----------------------------------------------------------------------------------------------
\subsection{Simulating Sets}

If we need to store non-negative integers (or some other objects that are easily mapped to a range of non-negative integers), and if the possible range of the integers is reasonably small (say, in the range $\left[ 0, 1000000\right)$), then we can use an array of \lstinline!boolean! as our set, without instantiating one of the \lstinline!Set<E>! classes. When the array is instantiated, all of the values in it are initialized to \lstinline!false!. Then, to add element \lstinline!i! to the ``set'' held in an array named \lstinline!set!, simply set \lstinline!set[i] = true!. To check to see if \lstinline!i! is in the set, check the truth value of element \lstinline!set[i]!. 

%----------------------------------------------------------------------------------------------
% Sample set uses
%----------------------------------------------------------------------------------------------
\subsection{Sample uses}

Sets could be used to solve problems like the following.

\begin{description}
	%************************************************************************
	% Uniqueness Testing
	%************************************************************************
	\item[Uniqueness Testing] Given a collection of items, $S$, determine if all the elements of $S$ are distinct. Here is a code snippet that solves this problem, for a stream of integers coming from the standard input, assuming that there is one integer per line in the input. This code uses \lstinline!HashSet!, but there's no preference, other than performance. 
	
	\lstinputlisting{set-unique/SetUnique.java}
	
	%***********************************************************************
	% Deleting Duplicates
	%***********************************************************************
	\item[Deleting Duplicates] Given a collection of items, $S$, remove all but one copy of any repeated elements in $S$. The following code snippet inputs a list of strings, removes the duplicates, and outputs the results. The results are printed in the same order and case as they were read, but only the first occurrence of each repeated word is output. A \lstinline!HashSet! is used to improve performance, since there is no need to iterate through the words alphabetically. A \lstinline!List! is also used, to store the original words, in their original order and case. 
	
	\lstinputlisting{set-duplicates/DeleteDuplicates.java}

\end{description}

\end{document}