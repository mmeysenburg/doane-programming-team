import java.util.*;

/**
 * MakeWords
 */
public class MakeWords {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        Random prng = new Random();
        List<String> list = new ArrayList<>();

        while(stdin.hasNextLine()) {
            String orig = stdin.nextLine().trim();
            int reps = prng.nextInt(5);
            for(int i = 0; i < reps; i++) {
                if(prng.nextDouble() < 0.5) {
                    list.add(orig);
                } else {
                    list.add(orig.toUpperCase());
                }
            }
        }
        stdin.close();

        String[] words = new String[list.size()];
        words = list.toArray(words);
        for(int i = 0; i < words.length; i++) {
            int idx = prng.nextInt(words.length);
            String t = words[i];
            words[i] = words[idx];
            words[idx] = t;
        }

        for(String s : words) {
            System.out.println(s);
        }
    }
    
}