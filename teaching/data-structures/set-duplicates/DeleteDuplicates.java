import java.util.*;

/**
 * DeleteDuplicates
 */ 
public class DeleteDuplicates {
    public static void main(String[] args) {
        // scanner to read from standard input
        Scanner stdin = new Scanner(System.in);
        // set for deleting duplicates
        Set<String> set = new HashSet<>();
        // list to hold original input
        List<String> originals = new LinkedList<>();

        // read all the input...
        while (stdin.hasNextLine()) {
            String word = stdin.nextLine().trim();
            // ... add original to list...
            originals.add(word);
            // ... add uppercase version to set; this
            // automatically removes duplicate words
            set.add(word.toUpperCase());
        }
        stdin.close();

        // now, iterate through originals...
        for (String s : originals) {
            // ... and if the word is still in the set,
            // remove it and print it out; if it is not
            // in the set, do not print. 
            if (set.remove(s.toUpperCase())) {
                System.out.println(s);
            }
        }
    }
    
}