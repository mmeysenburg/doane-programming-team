import java.util.*;

public class Read {
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        while (stdin.hasNextDouble()) {
            double d = stdin.nextDouble() * 100.0;
            System.out.println(d);
        }
    }
}