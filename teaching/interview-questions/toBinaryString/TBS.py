def toBinaryString(x):
    '''
    Return the unsigned binary representation of x,
    as a String.
    '''
    binary = ''
    digit = x % 2
    binary = str(digit) + binary
    x //= 2
    while x != 0:
        digit = x % 2
        binary = str(digit) + binary
        x //= 2

    return binary


for i in range(16):
    print(toBinaryString(i))