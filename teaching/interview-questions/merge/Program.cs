﻿using System;
using System.Collections.Generic;

class Program
{
    private static int[] merge(int[] A, int[] B)
    {
        // make new array consisting of copied inputs
        int[] C = new int[A.Length + B.Length];
        Array.Copy(A, 0, C, 0, A.Length);
        Array.Copy(B, 0, C, A.Length, B.Length);

        // sort new list
        Array.Sort(C);

        return C;
    }

    public static void Main(string[] args)
    {
        int[] A = { 1, 1, 6, 10, 4 };
        int[] B = { 6, 4, 9, 10, 2 };
        int[] C = merge(A, B);

        for (int i = 0; i < A.Length; i++)
        {
            Console.Write(A[i] + " ");
        }
        Console.WriteLine();

        for (int i = 0; i < B.Length; i++)
        {
            Console.Write(B[i] + " ");
        }
        Console.WriteLine();

        for (int i = 0; i < C.Length; i++)
        {
            Console.Write(C[i] + " ");
        }
        Console.WriteLine();
    }
}