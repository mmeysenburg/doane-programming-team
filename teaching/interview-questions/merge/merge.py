def merge(A, B):
    '''
    Merge two lists of integers
    '''
    C = A + B
    C.sort()
    return C

import random
A = [random.randrange(1,11) for i in range(5)]
B = [random.randrange(1, 11) for i in range(5)]

print(A)
print(B)
out = merge(A, B)
print(A)
print(B)
print(out)