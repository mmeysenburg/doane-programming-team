'''
Python program to analyze a file of words and find the
word with the largest number of vowels.
'''
# list of vowels for membership testing
vowels = ['a', 'e', 'i', 'o', 'u']

word_with_most_vowels = ''
max_num_vowels = 0

with open('dictionary.txt', 'r') as inFile:
    # process every word in the file
    for word in inFile:
        num_vowels = 0
        # look at each character in the word
        for c in word:
            if c in vowels:
                num_vowels += 1
        # is this the largest number of vowels we've seen? 
        if num_vowels > max_num_vowels:
            max_num_vowels = num_vowels
            word_with_most_vowels = word

    # report results
    print('Word with the most vowels:', word_with_most_vowels)