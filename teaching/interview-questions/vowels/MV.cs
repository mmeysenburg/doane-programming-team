﻿class MaxVowels
{
    public static void Main(string[] args)
    {
        // read all words into an array of strings
        string[] words = File.ReadAllLines("dictionary.txt");

        // list of vowels for membership testing
        var vowels = new List<char>() {'a', 'e', 'i', 'o','u'};

        int maxNumV = 0;
        string maxVWord = "";

        // process every word in the file
        foreach(string word in words)
        {
            int numV = 0;
            // look at each character in the word
            foreach(char c in word)
            {
                if(vowels.Contains(c))
                {
                    numV++;
                }
            }

            // is this the largest number of vowels we've seen? 
            if(numV > maxNumV)
            {
                maxNumV = numV;
                maxVWord = word;
            }
        }

        // report results
        Console.WriteLine("Word with the most vowels: " + maxVWord);
    }
}