﻿using System;
using System.Collections.Generic;

/// <summary>
/// Class illustrating palindrome checking in C#.
/// </summary>
class Program
{
    /// <summary>
    ///     Iterative palindrome detection. Bring two indices 
    ///     in from the ends.If we find two characters that 
    ///     aren't the same, we do not have a palindrom. If we 
    ///     get all the way through without a mismatch, we do 
    ///     have a palindrome.
    /// </summary>
    /// <param name="w">word to check</param>
    /// <returns>true if w is a palindrome, false otherwise</returns>
    private static bool isPal(string w)
    {
        int i = 0, j = w.Length - 1;
        while (i <= j)
        {
            if (w[i] != w[j])
            {
                return false;
            }
            i++; j--;
        }
        return true;
    }

    /// <summary>
    /// Recursive palindrome detection. Base case 1: a string of 0 
    /// or 1 characters is a palindrome.Base case 2: a string with 
    /// different first and last characters is not a palindrome. 
    /// General case: recurse on string without first and last character. 
    /// </summary>
    /// <param name="w">word to check</param>
    /// <returns>true if w is a palindrome, false otherwise</returns>
    private static bool recIsPal(string w)
    {
        if(w.Length <= 1)
        {
            return true;
        } else if(w[0] != w[w.Length-1])
        {
            return false;
        } else
        {
            return recIsPal(w.Substring(1, w.Length - 2));
        }
    }

    public static void Main(string[] args)
    {
        // read all words into a list
        List<string> list = new List<String>();
        string line;
        while( (line = Console.ReadLine()) != null && line != "")
        {
            list.Add(line);
        }
        // add the special case empty string
        list.Add("");

        // print results
        foreach(string w in list)
        {
            string ans = recIsPal(w) ? "is" : "is not";
            Console.WriteLine(String.Format("\"{0:s}\" {1:s} a palindrome",
                w, ans));
        }
    }
}