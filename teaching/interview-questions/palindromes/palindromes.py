import sys

# read list of words from standard input
words = [x[:-1] for x in sys.stdin.readlines()]

# add a special case, empty string
words.append('')

def isPal(w):
    '''
    Iterative palindrome detection. Bring two indices
    in from the ends. If we find two characters that
    aren't the same, we do not have a palindrom. If we
    get all the way through without a mismatch, we do 
    have a palindrome.
    '''
    i = 0
    j = len(w) - 1
    while i <= j:
        if w[i] != w[j]:
            return False
        i += 1
        j -= 1
    return True

def recIsPal(w):
    '''
    Recursive palindrome detection. Base case 1: a string of 0
    or 1 characters is a palindrome. Base case 2: a string with
    different first and last characters is not a palindrome. 
    General case: recurse on string without first and last
    character. 
    '''
    if len(w) <= 1:
        return True
    elif w[0] != w[-1]:
        return False
    else:
        return recIsPal(w[1:-2])

def tinyIsPal(s):
    '''
    Palindrome detection using slicing. Create a copy of the
    string s, but in reverse, then compare them. 
    '''
    return s == s[-1::-1]

# exercise the functions
for w in words:
    ans = 'is' if tinyIsPal(w) else 'is not'
    print('"{0:s}" {1:s} a palindrome'.format(w, ans))