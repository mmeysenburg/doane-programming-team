def mostFrequentElement(arr):
    dict = {}
    for i in arr:
        dict[i] = dict.get(i, 0) + 1

    maxVal = max(list(dict.values()))

    maxes = [x for x in dict if dict[x] == maxVal]

    return min(maxes)

import random
arr = []
for i in range(50):
    arr.append(random.randint(0, 20))

print(arr)

print(mostFrequentElement(arr))