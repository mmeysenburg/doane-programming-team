def removeDuplicates(orig):
    noDupes = []
    seenIt = {}
    for x in orig:
        if x not in seenIt:
            noDupes.append(x)
            seenIt[x] = 0
    return noDupes

import random

orig = [random.randint(1, 20) for i in range(20)]
print(orig)
noDupes = removeDuplicates(orig)
print(noDupes)