def areAnagrams(w1, w2):
    '''
    Return true if w1 and w2 are anagrams,
    false otherwise
    '''
    # O(1)
    if len(w1) != len(w2):
        return False

    # O(n)
    w1 = list(w1)
    w2 = list(w2)

    # O(n lg n)
    w1.sort()
    w2.sort()

    # O(n)
    return w1 == w2


words = [('Tar', 'Rat'), ('Arc', 'Car'), ('Elbow', 'Below'), ('State', 'Taste'), ('Cider', 'Cried'), 
    ('Dusty', 'Study'), ('Night', 'Thing'), ('Inch', 'Chin'), ('Brag', 'Grab'), ('Cat', 'Act'), 
    ('Bored', 'Robed'), ('Save', 'Vase'), ('Angel', 'Glean'), ('Stressed', 'Desserts'), ('pig', 'git'),
    ('two', 'three'), ('sts', 'stss')]

for pair in words:
    print(areAnagrams(pair[0].lower(), pair[1].lower()))
    