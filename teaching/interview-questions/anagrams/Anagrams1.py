def areAnagrams(w1, w2):
    '''
    Return true if w1 and w2 are anagrams,
    false otherwise
    '''
    # O(1)
    if len(w1) != len(w2):
        return False

    # O(n^2)
    for c in w1:
        if c not in w2:
            return False

    # O(n^2)
    for c in w2:
        if c not in w1:
            return False

    # O(1)
    return True    

words = [('Tar', 'Rat'), ('Arc', 'Car'), ('Elbow', 'Below'), ('State', 'Taste'), ('Cider', 'Cried'), 
    ('Dusty', 'Study'), ('Night', 'Thing'), ('Inch', 'Chin'), ('Brag', 'Grab'), ('Cat', 'Act'), 
    ('Bored', 'Robed'), ('Save', 'Vase'), ('Angel', 'Glean'), ('Stressed', 'Desserts'), ('pig', 'git'),
    ('two', 'three'), ('sts', 'stss')]

for pair in words:
    print(areAnagrams(pair[0].lower(), pair[1].lower()))
    