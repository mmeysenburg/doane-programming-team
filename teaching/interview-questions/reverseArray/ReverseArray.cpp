#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>

void print(int *pArr, int n) {
    std::cout << "[";
    for(int i = 0; i < n - 1; i++) {
        std::cout << pArr[i] << ", ";
    }
    std::cout << pArr[n - 1] << "]" << std::endl;
}

void reverseArray(int *pArr, int n) {
    for(int i = 0; i < n / 2; i++) {
        int t = pArr[i];
        pArr[i] = pArr[n - 1 - i];
        pArr[n - 1 - i] = t;
    }
}

int main() {
    int n = 21;
    int *pArr = new int[n];

    for(int i = 0; i < n; i++) {
        pArr[i] = i;
    }

    print(pArr, n);
    reverseArray(pArr, n);
    print(pArr, n);

    delete [] pArr;

    return EXIT_SUCCESS;
}