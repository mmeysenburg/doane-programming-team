def permutations(s):
    '''
    Given string s, return a list containing all of the permutations of s.
    '''
    # make string into a list
    ls = [c for c in s]
    out = []
    generate(out, len(ls), ls)
    return out

def generate(out, k, A):
    '''
    Recursive generation of permutations.
    '''
    if k == 1:
        out.append(''.join(A))
    else:
        generate(out, k - 1, A)

        for i in range(k - 1):
            if k % 2 == 0:
                t = A[i]
                A[i] = A[k - 1]
                A[k - 1] = t
            else:
                t = A[0]
                A[0] = A[k - 1]
                A[k - 1] = t
            generate(out, k - 1, A)

out = permutations('YOU')
for s in out:
    print(s)