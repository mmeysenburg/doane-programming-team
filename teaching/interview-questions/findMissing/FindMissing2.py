import random

def findMissing(myList):
    n = len(myList) + 1
    predictedSum = n * (n + 1) // 2
    actualSum = sum(myList)
    return predictedSum - actualSum

myList = list(range(1, 51))
myList.remove(random.randint(1, 50))
print(myList)
random.shuffle(myList)
print(myList)
missing = findMissing(myList)
print(missing)