import random

def findMissing(myList):
    nums = {}
    for i in range(1, 51):
        nums[i] = ''
    for i in myList:
        del nums[i]
    return list(nums.keys())[0]

myList = list(range(1, 51))
myList.remove(random.randint(1, 50))
print(myList)
random.shuffle(myList)
print(myList)
missing = findMissing(myList)
print(missing)