import random

def findMissing(myList):
    newList = myList[:]
    newList.sort()
    for i in range(len(myList)):
        if (i + 1) != newList[i]:
            return i + 1
    return len(myList) + 1

myList = list(range(1, 51))
myList.remove(random.randint(1, 50))
print(myList)
random.shuffle(myList)
print(myList)
missing = findMissing(myList)
print(missing)