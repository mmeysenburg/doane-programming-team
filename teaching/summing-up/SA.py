tokens = [int(x) for x in input().strip().split()]
casenum = 1
while not(tokens[0] == 0 and tokens[1] == 0):
    tokens.sort()
    print('Case {:d}: {:d}'.format(casenum, sum([i for i in range(tokens[0] + 1, tokens[1])])))
    casenum += 1
    tokens = [int(x) for x in input().strip().split()]