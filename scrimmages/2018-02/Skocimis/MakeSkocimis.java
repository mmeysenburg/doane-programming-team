import java.util.*;

/**
 * MakeSkocimis
 */
public class MakeSkocimis {

    public static void main(String[] args) {
        Random prng = new Random();
        int[] ks = {0, 0, 0};

        for (int i = 0; i < 100; i++) {
            ks[0] = prng.nextInt(100) + 1;
            ks[1] = prng.nextInt(100) + 1;
            ks[2] = prng.nextInt(100) + 1;

            Arrays.sort(ks);

            if (ks[1] == ks[0]) {
                ks[1]++;
            }
            while (ks[2] <= ks[1]) {
                ks[2]++;
            }

            System.out.println(ks[0] + " " + ks[1] + " " + ks[2]);
        }
        System.out.println();
    }
}