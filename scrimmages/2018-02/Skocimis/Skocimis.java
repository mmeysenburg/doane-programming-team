import java.io.*;

/**
 * Skocimis
 */
public class Skocimis {

    public static void main(String[] args) throws IOException {
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        String[] vals = stdin.readLine().split(" ");
        int A = Integer.parseInt(vals[0]);
        int B = Integer.parseInt(vals[1]);
        int C = Integer.parseInt(vals[2]);

        while (!(A == 0 && B == 0 && C == 0)) {
            System.out.println(Math.max(C - B - 1, Math.max(B - A - 1, 0)));   
            
            vals = stdin.readLine().split(" ");
            A = Integer.parseInt(vals[0]);
            B = Integer.parseInt(vals[1]);
            C = Integer.parseInt(vals[2]); }
    }
}