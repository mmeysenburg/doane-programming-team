import java.io.*;

/**
 * Solution to the Prozor programming competition problem
 * 
 * @author Mark M. Meysenburg
 * @version 3/6/20190
 */
public class Prozor {
    public static void main(String[] args) throws IOException{
        BufferedReader stdin = new BufferedReader(
            new InputStreamReader(System.in));

        String[] strings = stdin.readLine().split(" ");
        int r = Integer.parseInt(strings[0]);
        int s = Integer.parseInt(strings[1]);
        int k = Integer.parseInt(strings[2]);
        int i, j, ki, kj, flies, x, y, maxFlies, kimax, kjmax;
        String line;

        while (!(r == 0 && s == 0 && k == 0)) {
            char[][] window = new char[r][s];
            for (i = 0; i < r; i++) {
                line = stdin.readLine();
                for (j = 0; j < s; j++) {
                    window[i][j] = line.charAt(j);
                }
            }
    
            ki = 0; kj = 0;
            maxFlies = Integer.MIN_VALUE;
            for (i = 0; i <= r - k; i++) {
                for (j = 0; j <= s - k; j++) {
                    flies = 0;
                    for (y = i + 1; y < i + k - 1; y++) {
                        for (x = j + 1; x < j + k - 1; x++) {
                            if (window[y][x] == '*') {
                                flies++;
                            } // if squish
                        } // for x
                    } // for y
                    if (flies > maxFlies) {
                        ki = i; kj = j;
                        maxFlies = flies;
                    }
                }
            }
    
            System.out.println(maxFlies);
            kimax = (ki + k - 1);
            kjmax = (kj + k - 1);
            for (i = 0; i < r; i++) {
                for (j = 0; j < s; j++) {
                    if ((i == ki && j == kj) ||
                        (i == ki && j == kjmax) ||
                        (i ==  kimax && j == kj) ||
                        (i == kimax && j == kjmax)) {
                        
                        System.out.print('+');
                    }
                    else if ((i == ki || i == kimax) && (j >= kj && j <= kjmax)) {
                        System.out.print('-');
                    }
                    else if ((j == kj || j == kjmax) && (i >= ki && i <= kimax)) {
                        System.out.print('|');
                    } 
                    else {
                        System.out.print(window[i][j]);
                    }
                }
                System.out.println();
            }

            strings = stdin.readLine().split(" ");
            r = Integer.parseInt(strings[0]);
            s = Integer.parseInt(strings[1]);
            k = Integer.parseInt(strings[2]);
                    
        }

    }
}