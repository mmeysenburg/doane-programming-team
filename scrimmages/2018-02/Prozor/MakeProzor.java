import java.util.Random;

public class MakeProzor {
    public static void main(String[] args) {
        Random prng = new Random();

        int[] r = {3, 7, 9, 3, 100, 3, 100, 100, 100 };
        int[] s = {5, 6, 9, 100, 3, 3, 100, 100, 100 };
        int[] k = {3, 4, 6, 3, 3, 3, 3, 25, 100};

        for (int n = 0; n < r.length; n++) {
            System.out.printf("%d %d %d\n", r[n], s[n], k[n]);
            for (int i = 0; i < r[n]; i++) {
                for (int j = 0; j < s[n]; j++) {
                    if (prng.nextDouble() < 0.15) {
                        System.out.print('*');
                    } else {
                        System.out.print('.');
                    }
                }
                System.out.println();
            }                
        }
    }
}