import java.util.*;

public class DiceCup {
    
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int N = stdin.nextInt();
        int M = stdin.nextInt();
        while (!(N == 0 && M == 0)) {
            int[] sums = new int[41];

            for (int i = 1; i <= N; i++) {
                for (int j = 1; j <= M; j++) {
                    sums[i + j]++;
                }
            }
    
            int max = Integer.MIN_VALUE;
            for (int i = 1; i < sums.length; i++) {
                if (sums[i] > max) {
                    max = sums[i];
                }
            }
            System.out.print(N + ", " + M + ": ");
            for (int i = 1; i < sums.length; i++) {
                if (sums[i] == max) {
                    System.out.print(i + " ");
                }
            }
            System.out.println();
            
            N = stdin.nextInt();
            M = stdin.nextInt();
        }
    }
}