\documentclass[10pt,letterpaper,twoside]{article}
\usepackage[left=1in, right=1in, top=1in, bottom=1in]{geometry}
\usepackage{algorithm}
\usepackage[noend]{algpseudocode}

\begin{document}

\section{The Binary Search Efficiency Doubter}

The binary search is a classic algorithm in computer science. For this problem we will use the following pseudocode to define our binary search:

\begin{algorithm}

\emph{Perform a binary search on the array with values in $a[0]$ through $a[n-1]$, to find, if it exists, the value $x$. The values in the array may be assumed to be in strictly increasing order.}

\begin{algorithmic}[1]
\Procedure{BinarySearch}{$a, n, x$}
\State $low \gets 0$
\State $high \gets n - 1$
\While{$low \leq high$}
	\State $mid = \frac{low + high}{2}$
	\If{$a[mid] = x$} 
		\State Return $mid$
	\EndIf
	\If{$a[mid] < x$}
		\State $low = mid + 1$
	\EndIf
	\If{$a[mid] > x$}
		\State $high = mid - 1$
	\EndIf
\EndWhile
\State Return -1
\EndProcedure
\end{algorithmic}
\end{algorithm}

Professors teach that this is an efficient algorithm with worst case number of times through the loop of roughly $\log_2n$ and an average case that is slightly better than that. A student who is not convinced decides to build lists of various sizes and search for every number in the list and keep track of how many times the loop is executed. In the following example, the number of times the loop is executed to find each value is indicated below the corresponding value.

\begin{table}[h]
\begin{center}
\begin{tabular}{|c|c|c|c|c|c|c|c|}
\hline
the list & 12 & 16 & 23 & 34 & 42 & 57 & 65 \\ \hline
loop count & 3 & 2 & 3 & 1 & 3 & 2 & 3 \\ \hline
\end{tabular}
\end{center}
\end{table}

So, for this loop, the total loop count is 17.

It should be clear that any list of length 7 will have a total loop count of 17 under the assumptions that the list is sorted and all the values are different. That is, the length of the list determines the total loop count.

The problem here is to determine the total loop count given the length of the list. You may assume that the answer for any test case in the input fits in a signed 64-bit integer.

\subsection*{Input}

There may be multiple cases. Input for each case will consist of a single positive integer $n$, which gives the length of the list. You may assume that $2 \leq n \leq 10,000,000$ and that there are no more than 100 cases. Cases are delimited by arbitrary white space. Process until an end-of-file is detected.

\subsection*{Output}

For each input case, print the total loop count to find all the values in a list of size $n$. Follow this format exactly: ``Case'', one space, the case number, a colon and one space, and the answer for that case with no trailing spaces.

Sample input and output are on the reverse of this sheet.

\clearpage

\subsection*{Sample Input}

{\tt
\begin{verbatim}
3      7

    321
124
\end{verbatim}
}

\subsection*{Sample Output}

{\tt
\begin{verbatim}
Case 1: 5
Case 2: 17
Case 3: 2387
Case 4: 748
\end{verbatim}
}

\cleardoublepage

\section{Breaking a Dollar}

Using only the U.S. coins worth 1, 5, 10, 25, 50, and 100 cents, there are exactly 293 ways in which the U.S. dollar can be represented. Canada has no coin with a value of 50 cents, so there are only 243 ways in which one Canadian dollar can be represented. Suppose you are give a new set of denominations for the coins (each of which we will assume represents some integral number of cents less than equal to 100, but greater than 0). In how many ways could 100 cents be represented?

\subsection*{Input}

The input will contain multiple cases. The input for each case will begin with an integer $N$ (at least 1, but no more than 10) that indicates the number of unique coin denominations. By \emph{unique} it is meant that there will not be two (or more) different coins with the same value. The value of $N$ will be followed by $N$ integers giving the denominations of the coins.

Input for the last case will be followed by a single integer -1.

\subsection*{Output}

For each case, display the case number(they start with 1 and increase sequentially) and the number of different combinations of those coins that total 100 cents. Separate the output for consecutive cases with a blank line.

\subsection*{Sample Input}

{\tt
\begin{verbatim}
6 1 5 10 25 50 100
5 1 5 10 25 100
-1
\end{verbatim}
}

\subsection*{Sample Output}

{\tt
\begin{verbatim}
Case 1: 293 combinations of coins

Case 2: 243 combinations of coins

\end{verbatim}
}

\cleardoublepage

\section{Making Money}
A trick sometimes used by parents to teach their children the value of money is to give them a penny --
 just a penny! -- and then promise that for each day they don't spend it, the parents will double it.
 All students of computing know that long before a month has elapsed without spending a cent, the
 parents will not likely be able to make good on their promise.

100-percent compounded daily interest on an investment is, of course, unattainable in normal financial
 dealings, but we are all continually reminded of the power of compound interest, even with the
 relatively low interest rates available today.

But exactly how much money can be made with compound interest? Assume for example, an initial investment
 of \$100.00, and annual interest rate of 6.00 percent, and that interest is compounded monthly. That
 is, the interest earned during the preceding month is added to the principle as the end of the month.
 (For our purposes, we'll assume a month is exactly 1/12th of a year.)

At the end of the first month, the money will have earned 0.5 percent interest (1/12th of 6.00 percent),
 or \$0.50. This is added to the \$100.00 invested, so that during the next month, interest is paid on 
\$100.50. During the next month, another 0.05 percent interest is earned, which is exactly \$0.5025. We
 will assume that the bank, being conservative, will not pay any interest less than \$0.01, so our
 investment is credited with an additional \$0.50 at the end of the second month, for a whopping total
 of \$101.00. Continuing in the same manner, at the end of 12 months our investment will total 
\$106.12, \$0.12 more than simple 6.00 percent interest with no compounding.

Given an amount $P$ to be invested for a year with $I$ percent interest, compounded $C$ times during the year at equal intervals, what is the total return on the investment?

\subsection*{Input}

There will be multiple cases to consider. The input for each case is a single line containing the initial investment about $P$, given in dollars and cents (but no fractional cents, and no larger than \$100,000.00), the annual interest rate $I$ given as a real number with two fractional digits representing a percentage, greater than zero but less than 100, and the number of compounding intervals per year ($C$), an integer between 1 and 365. The last case will be followed by a line containing ``0.00 0.00 0''.

\subsection*{Output}

For each input case, display the case number $(1, 2, \ldots)$, the initial investment $(P)$, the annual interest rate $(I)$, the number of compounding intervals per year $(C)$, and the value of the investment at the end of the year. Your output should follow the format shown in the examples below.

\subsection*{Sample Input}

{\tt
\begin{verbatim}
100.00 6.00 1
100.00 6.00 12
1000.00 6.00 12
0.00 0.00 0
\end{verbatim}
}

\subsection*{Sample Output}

{\tt
\begin{verbatim}
Case 1. $100.00 at 6.00% APR compounded 1 times yields $106.00
Case 2. $100.00 at 6.00% APR compounded 12 times yields $106.12
Case 3. $1,000.00 at 6.00% APR compounded 12 times yields $1,061.63
\end{verbatim}
}

\cleardoublepage

\section{Strings with Same Letters}

A professor assigned a program to her class for which the output is a string of lower-case letters. Unfortunately, she did not specify an ordering of the characters in the string, so she is having difficulty grading student submissions. Because of that, she has requested that the Doane Programming Teams help by writing a program that inputs pairs of strings of lower-case letters and determines whether or not the strings have the same letters, though possibly in different orders. Note that repeated letters are important; the string ``abc'' and ``aabbbcccc'' are not viewed as having the same letters since the second one has more copies of each letter.

\subsection*{Input}

Input to be processed will be pairs of lines containing nonempty strings of lower-case letters. All input will be valid until the end of file indicator. End of fill will be signaled by two lines that contain just the word ``END'' in upper case. No input line will be longer than 1,000 characters.

\subsection*{Output}

Report whether pairs of strings have the same letters or not. Follow this format exactly: ``Case'', a space, the case number, a colon and one space, and the result given as ``same'' or ``different'' (lower-case, no punctuation). Do not print any trailing spaces.

\subsection*{Sample Input}

{\tt
\begin{verbatim}
testing
intestg
abc
aabbbcccc
abcabcbcc
aabbbcccc
abc
xyz
END
END
\end{verbatim}
}

\subsection*{Sample Output}

{\tt
\begin{verbatim}
Case 1: same
Case 2: different
Case 3: same
Case 4: different
\end{verbatim}
}

\end{document}
