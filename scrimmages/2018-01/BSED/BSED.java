
import java.util.Scanner;

public class BSED {
    
    private static long loopCount = 0;
    
    private static int bsearch(int[] list, int target) {
        int lo = 0, hi = list.length - 1;
        while(lo <= hi) {
            loopCount++;
            int mid = (lo + hi) / 2;
            if(list[mid] == target) return mid;
            if(list[mid] < target) lo = mid + 1;
            if(list[mid] > target) hi = mid - 1;
        }
        
        return -1;
    } // bsearch

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int n, caseNo = 1;
        
        while(stdin.hasNextInt()) {
            n = stdin.nextInt();
            int[] list = new int[n];
            
            for(int i = 0; i < list.length; i++) {
                list[i] = i;
            }
            
            long totCount = 0;
            for(int i = 0; i < list.length; i++) {
                loopCount = 0;
                bsearch(list, i);
                totCount += loopCount;
            }
            
            System.out.println("Case " + caseNo + ": " + totCount);
            caseNo++;
        } // while
    } // main
    
} // ProbA
