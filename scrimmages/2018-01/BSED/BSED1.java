import java.util.*;

/**
 * BSED1
 */
public class BSED1 {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);
        int caseNum = 1;

        while (stdin.hasNextDouble()) {
            double d = stdin.nextDouble();

            int logN = (int)Math.ceil(Math.log10(d) / Math.log10(2.0));
            int mul = 1, sum = 0;
            double dSum = 0.0;
            for(int i = 1; i <= logN; i++) {
                if((dSum + mul) <= d) {
                    sum += mul * i;
                    dSum += mul;
                    mul *= 2;
                } else {
                    while(dSum < d) {
                        sum += i;
                        dSum++;
                    }
                }
            }
            if(logN == (Math.log10(d) / Math.log10(2.0))) {
                sum += logN + 1;
            }

            System.out.printf("Case %d: %d\n", caseNum, sum);
            caseNum++;
        }
    }
}