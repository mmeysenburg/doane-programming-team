import java.util.*;

/**
 * C
 */
public class C {
    private static long curr = 0L;

    private static void move(long numDisks, String fromPost, String sparePost, String toPost, int i, long k) {
        if (numDisks == 0L) {
            return;
        } else {
            move(numDisks - 1, fromPost, toPost, sparePost, i, k);
            curr++;
            if (curr == k) {
                System.out.printf("Case %d: %d %s %s\n", i, numDisks, fromPost, toPost);
                return;
            }
            move(numDisks - 1, sparePost, fromPost, toPost, i, k);
        }
        
    }
    
    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        long k = stdin.nextLong();
        long n = stdin.nextLong();
        int i = 1;

        while(!(k == 0L && n == 0L)) {
            curr = 0L;
            move(n, "A", "B", "C", i, k);
            i++;

            k = stdin.nextLong();
            n = stdin.nextLong();
        }

        stdin.close();
    }
}