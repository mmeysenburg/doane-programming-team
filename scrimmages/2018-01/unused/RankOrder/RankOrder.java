
import java.util.Collections;
import java.util.LinkedList;
import java.util.Scanner;

public class RankOrder  {

    private class Rating implements Comparable {
        int cNum, score;
        
        public Rating(int cNum, int score) {
            this.cNum = cNum;
            this.score = score;
        }

        @Override
        public int compareTo(Object o) {
            Rating other = (Rating)o;
            return score - other.score;
        }
        
        @Override
        public String toString() {
            return "(" + cNum + ", " + score + ")";
        }
    }
    
    public void compute() {
        Scanner stdin = new Scanner(System.in);
        int caseNo = 1;
        
        while(stdin.hasNextInt()) {
            int N = stdin.nextInt();
            
            LinkedList<Rating> judge1 = new LinkedList<>();
            LinkedList<Rating> judge2 = new LinkedList<>();
            
            // read first judge's scores
            for(int i = 1; i <= N; i++) {
                int s = stdin.nextInt();
                judge1.add(new Rating(i, s));
            }
            
            // read second judge's scores
            for(int i = 1; i <= N; i++) {
                int s = stdin.nextInt();
                judge2.add(new Rating(i, s));
            }
            
            Collections.sort(judge1);
            Collections.sort(judge2);

            int idx = -1;
            for(int i = 0; i < N; i++) {
                Rating j1 = judge1.get(i);
                Rating j2 = judge2.get(i);
                
                if(j1.cNum != j2.cNum) {
                    idx = i + 2;
                    break;
                }
            }
            
            String msg = "";
            if(idx == -1) {
                msg += "agree";
            } else {
                msg += idx;
            }
            
            System.out.println("Case " + caseNo + ": " + msg);
            caseNo++;
        } // while input
    }

    public static void main(String[] args) {
        RankOrder pc = new RankOrder();
        pc.compute();
    }    
}
