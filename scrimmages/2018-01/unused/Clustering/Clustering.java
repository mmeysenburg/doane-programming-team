import java.awt.geom.Point2D;
import java.util.*;

/**
 * Clustering
 */
public class Clustering {

    private static void printGroup(TreeSet<Integer> group) {
        int n = 0;
        for(int i : group) {
            System.out.printf("%d", i);
            n++;
            if(n < group.size()) {
                System.out.printf(", ");
            } else {
                System.out.println();
            }
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        int N = stdin.nextInt();
        while (N != 0) {
            Point2D.Double[] individuals = new Point2D.Double[N + 1];
            TreeSet<Integer> remaining = new TreeSet<>();
            TreeSet<Integer> group1 = new TreeSet<>();
            TreeSet<Integer> group2 = new TreeSet<>();
            TreeSet<Integer> group3 = new TreeSet<>();

            for(int i = 1; i <= N; i++) {
                individuals[i] = new Point2D.Double(stdin.nextDouble(), stdin.nextDouble());
                remaining.add(i);
            }

            double[][] distances = new double[N + 1][N + 1];
            double minDistance1 = Double.MAX_VALUE;
            int minA1 = -1, minB1 = -1;
            for(int i = 1; i <= N; i++) {
                distances[i][i] = 0.0;
                for(int j = i + 1; j <= N; j++) {
                    double d = individuals[i].distance(individuals[j]);
                    // int id = (int)d * 1000;
                    // d = id / 1000.0;
                    distances[i][j] = d;
                    distances[j][i] = d;
                    if(d < minDistance1) {
                        minDistance1 = d;
                        minA1 = i;
                        minB1 = j;
                    }
                }
            }

            // form initial first group
            group1.add(minA1);
            group1.add(minB1);
            remaining.remove(minA1);
            remaining.remove(minB1);

            // form initial second group
            minDistance1 = Double.MAX_VALUE;
            minA1 = minB1 = -1;
            for(int i = 1; i <= N; i++) {
                for(int j = i + 1; j <= N; j++) {
                    if(remaining.contains(i) && remaining.contains(j)) {
                        if(distances[i][j] < minDistance1) {
                            minDistance1 = distances[i][j];
                            minA1 = i;
                            minB1 = j;
                        }
                    }
                }
            }
            group2.add(minA1);
            group2.add(minB1);
            remaining.remove(minA1);
            remaining.remove(minB1);

            // place the rest of the individuals
            while (!remaining.isEmpty()) {
                minDistance1 = Double.MAX_VALUE;
                minA1 = -1; minB1 = -1;  
                double minDistance2 = Double.MAX_VALUE;
                int minA2 = -1, minB2 = -1;
                double minDistance3 = Double.MAX_VALUE;
                int minA3 = -1, minB3 = -1;

                for(int E : remaining) {
                    for(int x : group1) {
                        if (distances[E][x] < minDistance1) {
                            minDistance1 = distances[E][x];
                            minA1 = E;
                            minB1 = x;
                        }
                    }
                    for(int x : group2) {
                        if (distances[E][x] < minDistance2) {
                            minDistance2 = distances[E][x];
                            minA2 = E;
                            minB2 = x;
                        }
                    }
                    for(int x : remaining) {
                        if (E != x && distances[E][x] < minDistance3) {
                            minDistance3 = distances[E][x];
                            minA3 = E;
                            minB3 = x;
                        }
                    }
                }

                // which group to add to?
                if(Math.abs(minDistance1 - minDistance3) < 0.001 || Math.abs(minDistance2 - minDistance3) < 0.001) {
                    double[] ds = {minDistance1, minDistance2, minDistance3};
                    Arrays.sort(ds);
                    if(minDistance1 == ds[0]) {
                        group1.add(minA1);
                        remaining.remove(minA1);
                    } else if(minDistance2 == ds[0]) {
                        group2.add(minA2);
                        remaining.remove(minA2);
                    } else {
                        group3.add(minA3);
                        remaining.remove(minA3);
                    }
                } else if(minDistance1 < minDistance2 && minDistance1 < minDistance3) {
                    group1.add(minA1);
                    remaining.remove(minA1);
                } else if(minDistance2 < minDistance1 && minDistance2 < minDistance3) {
                    group2.add(minA2);
                    remaining.remove(minA2);
                } else if(minDistance3 < minDistance1 && minDistance3 < minDistance2) {
                    group3.add(minA3);
                    group3.add(minB3);
                    remaining.remove(minA3);
                    remaining.remove(minB3);
                } else {
                    double[] ds = {minDistance1, minDistance2, minDistance3};
                    Arrays.sort(ds);
                    if(minDistance1 == ds[0]) {
                        group1.add(minA1);
                        remaining.remove(minA1);
                    } else if(minDistance2 == ds[0]) {
                        group2.add(minA2);
                        remaining.remove(minA2);
                    } else {
                        group3.add(minA3);
                        remaining.remove(minA3);
                    }
                }
            }
            System.out.printf("Case %d:\n", caseNum);
            System.out.printf("  Group 1: ");
            printGroup(group1);
            System.out.printf("  Group 2: ");
            printGroup(group2);
            if(!group3.isEmpty()) {
                System.out.printf("  Group 3: ");
                printGroup(group3);
            }
            caseNum++;
            N = stdin.nextInt();
        }

        stdin.close();
    }
}