import java.awt.geom.Point2D;
import java.util.*;

/**
 * Clustering1
 */
public class Clustering1 {

    private static void printGroup(TreeSet<Integer> group) {
        int n = 0;
        for(int i : group) {
            System.out.printf("%d", i);
            n++;
            if(n < group.size()) {
                System.out.printf(", ");
            } else {
                System.out.println();
            }
        }
    }

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        int caseNum = 1;
        int N = stdin.nextInt();
        while (N != 0) {

            // read in points
            Point2D.Double[] individuals = new Point2D.Double[N + 1];
            TreeSet<Integer> ungrouped = new TreeSet<>();
            for(int i = 1; i <= N; i++) {
                individuals[i] = new Point2D.Double(stdin.nextDouble(), stdin.nextDouble());
                ungrouped.add(i);
            }

            // calculate distances between everyone
            double[][] distances = new double[N + 1][N+ 1];
            for(int i = 1; i <= N; i++) {
                for(int j = 1; j <= N; j++) {
                    distances[i][j] = individuals[i].distance(individuals[j]);
                }
            }

            // initialize groups
            TreeSet<Integer> group1 = new TreeSet<>();
            TreeSet<Integer> group2 = new TreeSet<>();
            TreeSet<Integer> group3 = new TreeSet<>();

            // initial members of group 1
            double md1 = Double.MAX_VALUE;
            int A = -1, B = -1;
            for (int i = 1; i <= N; i++) {
                for(int j = i + 1; j <= N; j++) {
                    if(distances[i][j] < md1) {
                        md1 = distances[i][j];
                        A = i;
                        B = j;
                    }
                }
            }
            group1.add(A);
            group1.add(B);
            ungrouped.remove(A);
            ungrouped.remove(B);

            // initial values of group 2
            md1 = Double.MAX_VALUE;
            int C = -1, D = -1;
            for(int i = 1; i <= N; i++) {
                for(int j = i + 1; j <= N; j++) {
                    if(!group1.contains(i) && !group1.contains(j)) {
                        if(distances[i][j] < md1) {
                            md1 = distances[i][j];
                            C = i;
                            D = j;
                        }
                    }
                }
            }
            group2.add(C);
            group2.add(D);
            ungrouped.remove(C);
            ungrouped.remove(D);

            // group everyone else
            while (!ungrouped.isEmpty()) {
                double md2, md3, md4;
                md1 = md2 = md3 = md4 = Double.MAX_VALUE;
                int E1, E2, E3, E4, F;
                E1 = E2 = E3 = E4 = F = -1;

                // find minimum distance to group1, group2, and ungrouped
                for(int E : ungrouped) {
                    for(int i : group1) {
                        if(distances[E][i] < md1) {
                            md1 = distances[E][i];
                            E1 = E;
                        }
                    } // for group 1
                    for(int i : group2) {
                        if(distances[E][i] < md2) {
                            md2 = distances[E][i];
                            E2 = E;
                        }
                    } // for group 2
                    for(int i : group3) {
                        if(distances[E][i] < md3) {
                            md3 = distances[E][i];
                            E3 = E;
                        }
                    } // for group 3
                    for(int i : ungrouped) {
                        if(distances[E][i] < md4) {
                            md4 = distances[E][i];
                            E4 = E;
                            F = i;
                        }
                    } // for ungrouped
                } // for all ungrouped

                // where does this person go?
                if(md1 < Math.min(md2, Math.min(md3, md4))) {
                    group1.add(E1);
                    ungrouped.remove(E1);
                } else if(md2 < Math.min(md1, Math.min(md3, md4))) {
                    group2.add(E2);
                    ungrouped.remove(E2);
                // } else if(md3 < Math.min(md1, Math.min(md2, md4))) {
                //     group3.add(E3);
                //     ungrouped.remove(E3);
                } else {
                    group3.add(E4);
                    group3.add(F);
                    ungrouped.remove(E4);
                    ungrouped.remove(F);
                }
            }
            // make output
            System.out.printf("Case %d:\n", caseNum);
            System.out.printf("  Group 1: ");
            printGroup(group1);
            System.out.printf("  Group 2: ");
            printGroup(group2);
            if(!group3.isEmpty()){
                System.out.printf("  Group 3: ");
                printGroup(group3);
            }

            // prepare for next case
            caseNum++;
            N = stdin.nextInt();
        }

        stdin.close();
    }
}