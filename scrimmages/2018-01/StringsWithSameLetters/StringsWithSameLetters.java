import java.util.*;

/**
 * StringsWithSameLetters
 */
public class StringsWithSameLetters {

    public static void main(String[] args) {
        Scanner stdin = new Scanner(System.in);

        String a = stdin.nextLine();
        String b = stdin.nextLine();

        int caseNum = 1;

        while(!(a.equals("END") && b.equals("END"))) {
            char[] aa = a.toCharArray();
            char[] bb = b.toCharArray();

            Arrays.sort(aa);
            Arrays.sort(bb);

            a = new String(aa);
            b = new String(bb);

            if(a.equals(b)) {
                System.out.printf("Case %d: same\n", caseNum);
            } else {
                System.out.printf("Case %d: different\n", caseNum);
            }

            caseNum++;

            a = stdin.nextLine();
            b = stdin.nextLine();
        }
    }
}